<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

require_once("include/dbcommon.php");
require_once('include/xtempl.php');
require_once('classes/chartpage.php');
require_once('classes/searchclause.php');
add_nocache_headers();

require_once("include/cm1_votos_Chart_variables.php");






$layout = new TLayout("chart_bootstrap1", "OfficeOffice", "MobileOffice");
$layout->version = 3;
	$layout->bootstrapTheme = "cerulean";
		$layout->customCssPageName = "cm1_votos_Chart_chart";
$layout->blocks["center"] = array();
$layout->containers["tabs"] = array();
$layout->container_properties["tabs"] = array(  );
$layout->containers["tabs"][] = array("name"=>"bsgrid_tabs",
	"block"=>"grid_tabs", "substyle"=>1  );

$layout->skins["tabs"] = "";

$layout->blocks["center"][] = "tabs";
$layout->containers["message"] = array();
$layout->container_properties["message"] = array(  );
$layout->containers["message"][] = array("name"=>"message",
	"block"=>"message_block", "substyle"=>1  );

$layout->skins["message"] = "";

$layout->blocks["center"][] = "message";
$layout->containers["form"] = array();
$layout->container_properties["form"] = array(  );
$layout->containers["form"][] = array("name"=>"chart",
	"block"=>"", "substyle"=>1  );

$layout->skins["form"] = "";

$layout->blocks["center"][] = "form";
$layout->blocks["left"] = array();
$layout->containers["left"] = array();
$layout->container_properties["left"] = array(  );
$layout->containers["left"][] = array("name"=>"searchpanel",
	"block"=>"searchPanel", "substyle"=>1  );

$layout->skins["left"] = "";

$layout->blocks["left"][] = "left";
$layout->blocks["top"] = array();
$layout->containers["menu"] = array();
$layout->container_properties["menu"] = array(  );
$layout->containers["menu"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"hdr" );
$layout->containers["hdr"] = array();
$layout->container_properties["hdr"] = array(  );
$layout->containers["hdr"][] = array("name"=>"logo",
	"block"=>"logo_block", "substyle"=>1  );

$layout->containers["hdr"][] = array("name"=>"bsnavbarcollapse",
	"block"=>"collapse_block", "substyle"=>1  );

$layout->skins["hdr"] = "";


$layout->containers["menu"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"menu_1" );
$layout->containers["menu_1"] = array();
$layout->container_properties["menu_1"] = array(  );
$layout->containers["menu_1"][] = array("name"=>"hmenu",
	"block"=>"menu_block", "substyle"=>1  );

$layout->containers["menu_1"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"more" );
$layout->containers["more"] = array();
$layout->container_properties["more"] = array(  );
$layout->containers["more"][] = array("name"=>"morebutton",
	"block"=>"more_list", "substyle"=>1  );

$layout->skins["more"] = "";


$layout->containers["menu_1"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"more_1" );
$layout->containers["more_1"] = array();
$layout->container_properties["more_1"] = array(  );
$layout->containers["more_1"][] = array("name"=>"search",
	"block"=>"searchform_block", "substyle"=>1  );

$layout->skins["more_1"] = "";


$layout->skins["menu_1"] = "";


$layout->skins["menu"] = "";

$layout->blocks["top"][] = "menu";
$layout->blocks["topmiddle"] = array();
$layout->containers["breadcrumbs"] = array();
$layout->container_properties["breadcrumbs"] = array(  );
$layout->containers["breadcrumbs"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"bc" );
$layout->containers["bc"] = array();
$layout->container_properties["bc"] = array(  );
$layout->containers["bc"][] = array("name"=>"breadcrumbs",
	"block"=>"breadcrumbs", "substyle"=>1  );

$layout->skins["bc"] = "";


$layout->skins["breadcrumbs"] = "";

$layout->blocks["topmiddle"][] = "breadcrumbs";
$layout->containers["masterinfo"] = array();
$layout->container_properties["masterinfo"] = array(  );
$layout->containers["masterinfo"][] = array("name"=>"masterinfo",
	"block"=>"mastertable_block", "substyle"=>1  );

$layout->skins["masterinfo"] = "";

$layout->blocks["topmiddle"][] = "masterinfo";
$page_layouts["cm1_votos_Chart_chart"] = $layout;



// add master layouts 




$layout = new TLayout("masterlist_bootstrap", "OfficeOffice", "MobileOffice");
$layout->version = 3;
	$layout->bootstrapTheme = "cerulean";
		$layout->customCssPageName = "convocatoria_masterlist";
$layout->blocks["bare"] = array();
$layout->containers["column"] = array();
$layout->container_properties["column"] = array(  );
$layout->containers["column"][] = array("name"=>"masterlistheader",
	"block"=>"masterlist_title", "substyle"=>1  );

$layout->skins["column"] = "";

$layout->blocks["bare"][] = "column";
$layout->containers["mastergrid"] = array();
$layout->container_properties["mastergrid"] = array(  );
$layout->containers["mastergrid"][] = array("name"=>"masterlistgrid",
	"block"=>"", "substyle"=>1  );

$layout->skins["mastergrid"] = "";

$layout->blocks["bare"][] = "mastergrid";
$page_layouts["convocatoria_masterlist"] = $layout;





$mode = postvalue("mode");
if( $mode == "listdetails" )
	$pageMode = CHART_DETAILS;
elseif( $mode == "listdetailspopup" )
	$pageMode = CHART_POPUPDETAILS;
elseif( $mode == "dashchart" )
	$pageMode = CHART_DASHBOARD;
elseif( $mode == "dashdetails" )
	$pageMode = CHART_DASHDETAILS;
else
	$pageMode = CHART_SIMPLE;

$detailsMode = $pageMode != CHART_SIMPLE;

$xt = new Xtempl( $detailsMode );

$id = postvalue("id");
$id = $id ? $id : 1;

// set params for a RunnerPage constructor
$params = array();
$params["id"] = $id;
$params["xt"] = &$xt;
$params["mode"] = $pageMode;
$params["tName"] = $strTableName;
$params["pageType"] = PAGE_CHART;
$params["masterTable"] = postvalue("mastertable");
$params['masterPageType'] = postvalue("masterpagetype");

$params["isGroupSecurity"] = $isGroupSecurity; // a global variable
$params["masterKeysReq"] = array();

if( $pageMode = CHART_DASHBOARD )
{
	$params["dashElementName"] = postvalue("dashelement");
	$params["dashTName"] = postvalue("table");
}

if( isset($_REQUEST["masterKeys"]) )
	$masterKeys = my_json_decode( $_REQUEST["masterKeys"] );
	
$i = 0;
while (true) 
{
	$i++;
	if (isset($_REQUEST["masterkey".$i]))
		$_masterKey = $_REQUEST["masterkey".$i];
	elseif (isset($masterKeys["masterkey".$i]))
		$_masterKey = $masterKeys["masterkey".$i];
	else
		break;
	$params["masterKeysReq"][$i] = $_masterKey;
}

$pageObject = new ChartPage($params);
$pageObject->init();

if( $pageObject->processSaveSearch() )
	exit();

$pageObject->process();
?>