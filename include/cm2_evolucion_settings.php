<?php
require_once(getabspath("classes/cipherer.php"));



$tdatacm2_evolucion = array();
$tdatacm2_evolucion[".ShortName"] = "cm2_evolucion";

//	field labels
$fieldLabelscm2_evolucion = array();
$pageTitlescm2_evolucion = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm2_evolucion["Spanish"] = array();
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm2_evolucion[""] = array();
}

//	search fields
$tdatacm2_evolucion[".searchFields"] = array();

// all search fields
$tdatacm2_evolucion[".allSearchFields"] = array();

// good like search fields
$tdatacm2_evolucion[".googleLikeFields"] = array();

$tdatacm2_evolucion[".dashElements"] = array();

	$dbelement = array( "elementName" => "cm2_candidatura_list", "table" => "cm2_candidatura", "type" => 0);
	$dbelement["cellName"] = "cell_0_0";

					$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;


	$tdatacm2_evolucion[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm2_municipio_list", "table" => "cm2_municipio", "type" => 0);
	$dbelement["cellName"] = "cell_1_0";

					$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "cm2_candidatura";

	$tdatacm2_evolucion[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm2_distrito_list", "table" => "cm2_distrito", "type" => 0);
	$dbelement["cellName"] = "cell_2_0";

					$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "cm2_candidatura";

	$tdatacm2_evolucion[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm2_escanos_Report_report", "table" => "cm2_escanos Report", "type" => 2);
	$dbelement["cellName"] = "cell_3_0";

			

	$tdatacm2_evolucion[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm2_municipio_details", "table" => "cm2_municipio", "type" => 5);
	$dbelement["cellName"] = "cell_1_1";

				$dbelement["notUsedDetailTables"] = array();
	$dbelement["initialTabDetailTable"] = "";
	$dbelement["details"] = array();
	$dbelement["details"]["cm2_elecmunicandi Chart"] = array(
		"add" => 1,
		"edit" => 1,
		"view" => 1,
		"delete" => 1
	);
	$dbelement["details"]["cm2_elecmunicandi2 Chart"] = array(
		"add" => 1,
		"edit" => 1,
		"view" => 1,
		"delete" => 1
	);

$dbelement["masterTable"] = "cm2_candidatura";

	$tdatacm2_evolucion[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm2_candidatura_details", "table" => "cm2_candidatura", "type" => 5);
	$dbelement["cellName"] = "cell_0_1";

				$dbelement["notUsedDetailTables"] = array();
	$dbelement["initialTabDetailTable"] = "cm2_escanos_chart";
	$dbelement["details"] = array();
	$dbelement["details"]["cm2_escanos_chart"] = array(
		"add" => 1,
		"edit" => 1,
		"view" => 1,
		"delete" => 1
	);
	$dbelement["details"]["cm2_escanos2 Chart"] = array(
		"add" => 1,
		"edit" => 1,
		"view" => 1,
		"delete" => 1
	);
	$dbelement["details"]["cm2_municipio"] = array(
		"add" => 0,
		"edit" => 0,
		"view" => 0,
		"delete" => 0
	);
	$dbelement["details"]["cm2_distrito"] = array(
		"add" => 0,
		"edit" => 0,
		"view" => 0,
		"delete" => 0
	);
	$dbelement["details"]["electos"] = array(
		"add" => 0,
		"edit" => 0,
		"view" => 1,
		"delete" => 0
	);
	$dbelement["notUsedDetailTables"][] = "cm2_distrito";
	$dbelement["notUsedDetailTables"][] = "cm2_municipio";
	$dbelement["notUsedDetailTables"][] = "electos";


	$tdatacm2_evolucion[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm2_distrito_details", "table" => "cm2_distrito", "type" => 5);
	$dbelement["cellName"] = "cell_2_1";

				$dbelement["notUsedDetailTables"] = array();
	$dbelement["initialTabDetailTable"] = "";
	$dbelement["details"] = array();
	$dbelement["details"]["cm2_elecdistricandi Chart"] = array(
		"add" => 1,
		"edit" => 1,
		"view" => 1,
		"delete" => 1
	);
	$dbelement["details"]["cm2_elecdistricandi2 Chart"] = array(
		"add" => 1,
		"edit" => 1,
		"view" => 1,
		"delete" => 1
	);

$dbelement["masterTable"] = "cm2_candidatura";

	$tdatacm2_evolucion[".dashElements"][] = $dbelement;

$tdatacm2_evolucion[".shortTableName"] = "cm2_evolucion";
$tdatacm2_evolucion[".entityType"] = 4;



include_once(getabspath("include/cm2_evolucion_events.php"));
$tableEvents["cm2_evolucion"] = new eventclass_cm2_evolucion;
$tdatacm2_evolucion[".hasEvents"] = true;


$tdatacm2_evolucion[".tableType"] = "dashboard";



$tdatacm2_evolucion[".addPageEvents"] = false;

$tables_data["cm2_evolucion"]=&$tdatacm2_evolucion;
$field_labels["cm2_evolucion"] = &$fieldLabelscm2_evolucion;
$page_titles["cm2_evolucion"] = &$pageTitlescm2_evolucion;

?>