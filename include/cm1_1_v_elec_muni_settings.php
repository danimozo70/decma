<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm1_1_v_elec_muni = array();
	$tdatacm1_1_v_elec_muni[".truncateText"] = true;
	$tdatacm1_1_v_elec_muni[".NumberOfChars"] = 80;
	$tdatacm1_1_v_elec_muni[".ShortName"] = "cm1_1_v_elec_muni";
	$tdatacm1_1_v_elec_muni[".OwnerID"] = "";
	$tdatacm1_1_v_elec_muni[".OriginalTable"] = "v_elec_muni";

//	field labels
$fieldLabelscm1_1_v_elec_muni = array();
$fieldToolTipscm1_1_v_elec_muni = array();
$pageTitlescm1_1_v_elec_muni = array();
$placeHolderscm1_1_v_elec_muni = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm1_1_v_elec_muni["Spanish"] = array();
	$fieldToolTipscm1_1_v_elec_muni["Spanish"] = array();
	$placeHolderscm1_1_v_elec_muni["Spanish"] = array();
	$pageTitlescm1_1_v_elec_muni["Spanish"] = array();
	$fieldLabelscm1_1_v_elec_muni["Spanish"]["NumeroMunicipio"] = "Número Municipio";
	$fieldToolTipscm1_1_v_elec_muni["Spanish"]["NumeroMunicipio"] = "";
	$placeHolderscm1_1_v_elec_muni["Spanish"]["NumeroMunicipio"] = "";
	$fieldLabelscm1_1_v_elec_muni["Spanish"]["NombreMunicipio"] = "Municipio";
	$fieldToolTipscm1_1_v_elec_muni["Spanish"]["NombreMunicipio"] = "";
	$placeHolderscm1_1_v_elec_muni["Spanish"]["NombreMunicipio"] = "";
	$fieldLabelscm1_1_v_elec_muni["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldToolTipscm1_1_v_elec_muni["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderscm1_1_v_elec_muni["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelscm1_1_v_elec_muni["Spanish"]["Censo"] = "Censo";
	$fieldToolTipscm1_1_v_elec_muni["Spanish"]["Censo"] = "";
	$placeHolderscm1_1_v_elec_muni["Spanish"]["Censo"] = "";
	$fieldLabelscm1_1_v_elec_muni["Spanish"]["Certificacion"] = "Votos Certificación";
	$fieldToolTipscm1_1_v_elec_muni["Spanish"]["Certificacion"] = "";
	$placeHolderscm1_1_v_elec_muni["Spanish"]["Certificacion"] = "";
	$fieldLabelscm1_1_v_elec_muni["Spanish"]["Interventores"] = "Votos Interventores";
	$fieldToolTipscm1_1_v_elec_muni["Spanish"]["Interventores"] = "";
	$placeHolderscm1_1_v_elec_muni["Spanish"]["Interventores"] = "";
	$fieldLabelscm1_1_v_elec_muni["Spanish"]["Nulo"] = "Votos Nulos";
	$fieldToolTipscm1_1_v_elec_muni["Spanish"]["Nulo"] = "";
	$placeHolderscm1_1_v_elec_muni["Spanish"]["Nulo"] = "";
	$fieldLabelscm1_1_v_elec_muni["Spanish"]["Blanco"] = "Votos Blancos";
	$fieldToolTipscm1_1_v_elec_muni["Spanish"]["Blanco"] = "";
	$placeHolderscm1_1_v_elec_muni["Spanish"]["Blanco"] = "";
	$fieldLabelscm1_1_v_elec_muni["Spanish"]["Validos"] = "Votos Válidos";
	$fieldToolTipscm1_1_v_elec_muni["Spanish"]["Validos"] = "";
	$placeHolderscm1_1_v_elec_muni["Spanish"]["Validos"] = "";
	$pageTitlescm1_1_v_elec_muni["Spanish"]["view"] = "Votos en el Municipio de {%NombreMunicipio}";
	$pageTitlescm1_1_v_elec_muni["Spanish"]["list"] = "<strong>{%master.Titulo}</strong>, Votos por Municipio";
	if (count($fieldToolTipscm1_1_v_elec_muni["Spanish"]))
		$tdatacm1_1_v_elec_muni[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm1_1_v_elec_muni[""] = array();
	$fieldToolTipscm1_1_v_elec_muni[""] = array();
	$placeHolderscm1_1_v_elec_muni[""] = array();
	$pageTitlescm1_1_v_elec_muni[""] = array();
	if (count($fieldToolTipscm1_1_v_elec_muni[""]))
		$tdatacm1_1_v_elec_muni[".isUseToolTips"] = true;
}


	$tdatacm1_1_v_elec_muni[".NCSearch"] = true;



$tdatacm1_1_v_elec_muni[".shortTableName"] = "cm1_1_v_elec_muni";
$tdatacm1_1_v_elec_muni[".nSecOptions"] = 0;
$tdatacm1_1_v_elec_muni[".recsPerRowList"] = 1;
$tdatacm1_1_v_elec_muni[".recsPerRowPrint"] = 1;
$tdatacm1_1_v_elec_muni[".mainTableOwnerID"] = "";
$tdatacm1_1_v_elec_muni[".moveNext"] = 1;
$tdatacm1_1_v_elec_muni[".entityType"] = 1;

$tdatacm1_1_v_elec_muni[".strOriginalTableName"] = "v_elec_muni";

	



$tdatacm1_1_v_elec_muni[".showAddInPopup"] = false;

$tdatacm1_1_v_elec_muni[".showEditInPopup"] = false;

$tdatacm1_1_v_elec_muni[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
						
	;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdatacm1_1_v_elec_muni[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm1_1_v_elec_muni[".fieldsForRegister"] = array();

$tdatacm1_1_v_elec_muni[".listAjax"] = false;

	$tdatacm1_1_v_elec_muni[".audit"] = false;

	$tdatacm1_1_v_elec_muni[".locking"] = false;



$tdatacm1_1_v_elec_muni[".list"] = true;



$tdatacm1_1_v_elec_muni[".reorderRecordsByHeader"] = true;

$tdatacm1_1_v_elec_muni[".strClickActionJSON"] = "{\"fields\":{\"Blanco\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"Censo\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"Certificacion\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"Convocatoria_idConvocatoria\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"Interventores\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"NombreMunicipio\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"Nulo\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"NumeroMunicipio\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}},\"Validos\":{\"action\":\"noaction\",\"codeData\":{\"snippet\":\"\"},\"gridData\":{},\"openData\":{}}},\"row\":{\"action\":\"open\",\"codeData\":{},\"gridData\":{\"action\":\"checkbox\",\"table\":\"cm1_municipio_voto Chart\"},\"openData\":{\"how\":\"popup\",\"page\":\"view\",\"table\":\"cm1_municipio_voto Chart\",\"url\":\"\"}}}";


$tdatacm1_1_v_elec_muni[".view"] = true;





$tdatacm1_1_v_elec_muni[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm1_1_v_elec_muni[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm1_1_v_elec_muni[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm1_1_v_elec_muni[".searchSaving"] = false;
//

$tdatacm1_1_v_elec_muni[".showSearchPanel"] = true;
		$tdatacm1_1_v_elec_muni[".flexibleSearch"] = true;

$tdatacm1_1_v_elec_muni[".isUseAjaxSuggest"] = true;

$tdatacm1_1_v_elec_muni[".rowHighlite"] = true;





$tdatacm1_1_v_elec_muni[".ajaxCodeSnippetAdded"] = false;

$tdatacm1_1_v_elec_muni[".buttonsAdded"] = false;

$tdatacm1_1_v_elec_muni[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm1_1_v_elec_muni[".isUseTimeForSearch"] = false;



$tdatacm1_1_v_elec_muni[".badgeColor"] = "e8926f";

$tdatacm1_1_v_elec_muni[".detailsLinksOnList"] = "1";

$tdatacm1_1_v_elec_muni[".allSearchFields"] = array();
$tdatacm1_1_v_elec_muni[".filterFields"] = array();
$tdatacm1_1_v_elec_muni[".requiredSearchFields"] = array();

$tdatacm1_1_v_elec_muni[".allSearchFields"][] = "NombreMunicipio";
	$tdatacm1_1_v_elec_muni[".allSearchFields"][] = "Convocatoria_idConvocatoria";
	$tdatacm1_1_v_elec_muni[".allSearchFields"][] = "Censo";
	$tdatacm1_1_v_elec_muni[".allSearchFields"][] = "Certificacion";
	$tdatacm1_1_v_elec_muni[".allSearchFields"][] = "Interventores";
	$tdatacm1_1_v_elec_muni[".allSearchFields"][] = "Nulo";
	$tdatacm1_1_v_elec_muni[".allSearchFields"][] = "Blanco";
	$tdatacm1_1_v_elec_muni[".allSearchFields"][] = "Validos";
	

$tdatacm1_1_v_elec_muni[".googleLikeFields"] = array();
$tdatacm1_1_v_elec_muni[".googleLikeFields"][] = "NombreMunicipio";


$tdatacm1_1_v_elec_muni[".advSearchFields"] = array();
$tdatacm1_1_v_elec_muni[".advSearchFields"][] = "NombreMunicipio";

$tdatacm1_1_v_elec_muni[".tableType"] = "list";

$tdatacm1_1_v_elec_muni[".printerPageOrientation"] = 0;
$tdatacm1_1_v_elec_muni[".nPrinterPageScale"] = 100;

$tdatacm1_1_v_elec_muni[".nPrinterSplitRecords"] = 40;

$tdatacm1_1_v_elec_muni[".nPrinterPDFSplitRecords"] = 40;



$tdatacm1_1_v_elec_muni[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatacm1_1_v_elec_muni[".pageSize"] = 14;

$tdatacm1_1_v_elec_muni[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm1_1_v_elec_muni[".strOrderBy"] = $tstrOrderBy;

$tdatacm1_1_v_elec_muni[".orderindexes"] = array();

$tdatacm1_1_v_elec_muni[".sqlHead"] = "SELECT NumeroMunicipio,  	NombreMunicipio,  	Convocatoria_idConvocatoria,  	Censo,  	Certificacion,  	Interventores,  	Nulo,  	Blanco,  	Validos";
$tdatacm1_1_v_elec_muni[".sqlFrom"] = "FROM v_elec_muni";
$tdatacm1_1_v_elec_muni[".sqlWhereExpr"] = "";
$tdatacm1_1_v_elec_muni[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 11;
$arrRPP[] = 22;
$arrRPP[] = 33;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm1_1_v_elec_muni[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm1_1_v_elec_muni[".arrGroupsPerPage"] = $arrGPP;

$tdatacm1_1_v_elec_muni[".highlightSearchResults"] = true;

$tableKeyscm1_1_v_elec_muni = array();
$tableKeyscm1_1_v_elec_muni[] = "NumeroMunicipio";
$tableKeyscm1_1_v_elec_muni[] = "Convocatoria_idConvocatoria";
$tdatacm1_1_v_elec_muni[".Keys"] = $tableKeyscm1_1_v_elec_muni;

$tdatacm1_1_v_elec_muni[".listFields"] = array();
$tdatacm1_1_v_elec_muni[".listFields"][] = "NombreMunicipio";
$tdatacm1_1_v_elec_muni[".listFields"][] = "Censo";
$tdatacm1_1_v_elec_muni[".listFields"][] = "Certificacion";
$tdatacm1_1_v_elec_muni[".listFields"][] = "Interventores";
$tdatacm1_1_v_elec_muni[".listFields"][] = "Nulo";
$tdatacm1_1_v_elec_muni[".listFields"][] = "Blanco";
$tdatacm1_1_v_elec_muni[".listFields"][] = "Validos";

$tdatacm1_1_v_elec_muni[".hideMobileList"] = array();


$tdatacm1_1_v_elec_muni[".viewFields"] = array();
$tdatacm1_1_v_elec_muni[".viewFields"][] = "Censo";
$tdatacm1_1_v_elec_muni[".viewFields"][] = "Certificacion";
$tdatacm1_1_v_elec_muni[".viewFields"][] = "Interventores";
$tdatacm1_1_v_elec_muni[".viewFields"][] = "Nulo";
$tdatacm1_1_v_elec_muni[".viewFields"][] = "Blanco";
$tdatacm1_1_v_elec_muni[".viewFields"][] = "Validos";

$tdatacm1_1_v_elec_muni[".addFields"] = array();

$tdatacm1_1_v_elec_muni[".masterListFields"] = array();
$tdatacm1_1_v_elec_muni[".masterListFields"][] = "Censo";
$tdatacm1_1_v_elec_muni[".masterListFields"][] = "Certificacion";
$tdatacm1_1_v_elec_muni[".masterListFields"][] = "Interventores";
$tdatacm1_1_v_elec_muni[".masterListFields"][] = "Nulo";
$tdatacm1_1_v_elec_muni[".masterListFields"][] = "Blanco";
$tdatacm1_1_v_elec_muni[".masterListFields"][] = "Validos";

$tdatacm1_1_v_elec_muni[".inlineAddFields"] = array();

$tdatacm1_1_v_elec_muni[".editFields"] = array();

$tdatacm1_1_v_elec_muni[".inlineEditFields"] = array();

$tdatacm1_1_v_elec_muni[".updateSelectedFields"] = array();


$tdatacm1_1_v_elec_muni[".exportFields"] = array();

$tdatacm1_1_v_elec_muni[".importFields"] = array();

$tdatacm1_1_v_elec_muni[".printFields"] = array();


//	NumeroMunicipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "NumeroMunicipio";
	$fdata["GoodName"] = "NumeroMunicipio";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("cm1_1_v_elec_muni","NumeroMunicipio");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "NumeroMunicipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NumeroMunicipio";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm1_1_v_elec_muni["NumeroMunicipio"] = $fdata;
//	NombreMunicipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "NombreMunicipio";
	$fdata["GoodName"] = "NombreMunicipio";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("cm1_1_v_elec_muni","NombreMunicipio");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "NombreMunicipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NombreMunicipio";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_v_elec_muni["NombreMunicipio"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("cm1_1_v_elec_muni","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_v_elec_muni["Convocatoria_idConvocatoria"] = $fdata;
//	Censo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Censo";
	$fdata["GoodName"] = "Censo";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("cm1_1_v_elec_muni","Censo");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Censo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Censo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_v_elec_muni["Censo"] = $fdata;
//	Certificacion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Certificacion";
	$fdata["GoodName"] = "Certificacion";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("cm1_1_v_elec_muni","Certificacion");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Certificacion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Certificacion";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_v_elec_muni["Certificacion"] = $fdata;
//	Interventores
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Interventores";
	$fdata["GoodName"] = "Interventores";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("cm1_1_v_elec_muni","Interventores");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Interventores";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Interventores";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_v_elec_muni["Interventores"] = $fdata;
//	Nulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Nulo";
	$fdata["GoodName"] = "Nulo";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("cm1_1_v_elec_muni","Nulo");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Nulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Nulo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_v_elec_muni["Nulo"] = $fdata;
//	Blanco
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "Blanco";
	$fdata["GoodName"] = "Blanco";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("cm1_1_v_elec_muni","Blanco");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Blanco";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Blanco";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_v_elec_muni["Blanco"] = $fdata;
//	Validos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "Validos";
	$fdata["GoodName"] = "Validos";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("cm1_1_v_elec_muni","Validos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Validos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Validos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_v_elec_muni["Validos"] = $fdata;


$tables_data["cm1_1_v_elec_muni"]=&$tdatacm1_1_v_elec_muni;
$field_labels["cm1_1_v_elec_muni"] = &$fieldLabelscm1_1_v_elec_muni;
$fieldToolTips["cm1_1_v_elec_muni"] = &$fieldToolTipscm1_1_v_elec_muni;
$placeHolders["cm1_1_v_elec_muni"] = &$placeHolderscm1_1_v_elec_muni;
$page_titles["cm1_1_v_elec_muni"] = &$pageTitlescm1_1_v_elec_muni;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm1_1_v_elec_muni"] = array();
//	cm1_municipio_voto Chart
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm1_municipio_voto Chart";
		$detailsParam["dOriginalTable"] = "v_municipio_voto";
		$detailsParam["proceedLink"] = true;
			$detailsParam["dType"]=PAGE_CHART;
		$detailsParam["dShortTable"] = "cm1_municipio_voto_Chart";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm1_municipio_voto_Chart");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "2";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 1;
		
	$detailsTablesData["cm1_1_v_elec_muni"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm1_1_v_elec_muni"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm1_1_v_elec_muni"][$dIndex]["masterKeys"][]="Convocatoria_idConvocatoria";

	$detailsTablesData["cm1_1_v_elec_muni"][$dIndex]["masterKeys"][]="NumeroMunicipio";

				$detailsTablesData["cm1_1_v_elec_muni"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm1_1_v_elec_muni"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";

		
	$detailsTablesData["cm1_1_v_elec_muni"][$dIndex]["detailKeys"][]="Municipio";
//	elecmunicandi
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="elecmunicandi";
		$detailsParam["dOriginalTable"] = "elecmunicandi";
		$detailsParam["proceedLink"] = false;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "elecmunicandi";
	$detailsParam["dCaptionTable"] = GetTableCaption("elecmunicandi");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 1;
		
	$detailsTablesData["cm1_1_v_elec_muni"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm1_1_v_elec_muni"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm1_1_v_elec_muni"][$dIndex]["masterKeys"][]="Convocatoria_idConvocatoria";

	$detailsTablesData["cm1_1_v_elec_muni"][$dIndex]["masterKeys"][]="NumeroMunicipio";

				$detailsTablesData["cm1_1_v_elec_muni"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm1_1_v_elec_muni"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";

		
	$detailsTablesData["cm1_1_v_elec_muni"][$dIndex]["detailKeys"][]="Municipio";

// tables which are master tables for current table (detail)
$masterTablesData["cm1_1_v_elec_muni"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm1_1_Convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm1_1_Convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm1_1_v_elec_muni"][0] = $masterParams;
				$masterTablesData["cm1_1_v_elec_muni"][0]["masterKeys"] = array();
	$masterTablesData["cm1_1_v_elec_muni"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["cm1_1_v_elec_muni"][0]["detailKeys"] = array();
	$masterTablesData["cm1_1_v_elec_muni"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm1_1_v_elec_muni()
{
$proto1=array();
$proto1["m_strHead"] = "SELECT";
$proto1["m_strFieldList"] = "NumeroMunicipio,  	NombreMunicipio,  	Convocatoria_idConvocatoria,  	Censo,  	Certificacion,  	Interventores,  	Nulo,  	Blanco,  	Validos";
$proto1["m_strFrom"] = "FROM v_elec_muni";
$proto1["m_strWhere"] = "";
$proto1["m_strOrderBy"] = "";
	
		;
			$proto1["cipherer"] = null;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto1["m_where"] = $obj;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto1["m_having"] = $obj;
$proto1["m_fieldlist"] = array();
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "NumeroMunicipio",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "cm1_1_v_elec_muni"
));

$proto7["m_sql"] = "NumeroMunicipio";
$proto7["m_srcTableName"] = "cm1_1_v_elec_muni";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto1["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "NombreMunicipio",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "cm1_1_v_elec_muni"
));

$proto9["m_sql"] = "NombreMunicipio";
$proto9["m_srcTableName"] = "cm1_1_v_elec_muni";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto1["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "cm1_1_v_elec_muni"
));

$proto11["m_sql"] = "Convocatoria_idConvocatoria";
$proto11["m_srcTableName"] = "cm1_1_v_elec_muni";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto1["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "Censo",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "cm1_1_v_elec_muni"
));

$proto13["m_sql"] = "Censo";
$proto13["m_srcTableName"] = "cm1_1_v_elec_muni";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto1["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Certificacion",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "cm1_1_v_elec_muni"
));

$proto15["m_sql"] = "Certificacion";
$proto15["m_srcTableName"] = "cm1_1_v_elec_muni";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto1["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "Interventores",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "cm1_1_v_elec_muni"
));

$proto17["m_sql"] = "Interventores";
$proto17["m_srcTableName"] = "cm1_1_v_elec_muni";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto1["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "Nulo",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "cm1_1_v_elec_muni"
));

$proto19["m_sql"] = "Nulo";
$proto19["m_srcTableName"] = "cm1_1_v_elec_muni";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto1["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "Blanco",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "cm1_1_v_elec_muni"
));

$proto21["m_sql"] = "Blanco";
$proto21["m_srcTableName"] = "cm1_1_v_elec_muni";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto1["m_fieldlist"][]=$obj;
						$proto23=array();
			$obj = new SQLField(array(
	"m_strName" => "Validos",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "cm1_1_v_elec_muni"
));

$proto23["m_sql"] = "Validos";
$proto23["m_srcTableName"] = "cm1_1_v_elec_muni";
$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "";
$obj = new SQLFieldListItem($proto23);

$proto1["m_fieldlist"][]=$obj;
$proto1["m_fromlist"] = array();
												$proto25=array();
$proto25["m_link"] = "SQLL_MAIN";
			$proto26=array();
$proto26["m_strName"] = "v_elec_muni";
$proto26["m_srcTableName"] = "cm1_1_v_elec_muni";
$proto26["m_columns"] = array();
$proto26["m_columns"][] = "NumeroMunicipio";
$proto26["m_columns"][] = "NombreMunicipio";
$proto26["m_columns"][] = "Convocatoria_idConvocatoria";
$proto26["m_columns"][] = "Censo";
$proto26["m_columns"][] = "Certificacion";
$proto26["m_columns"][] = "Interventores";
$proto26["m_columns"][] = "Nulo";
$proto26["m_columns"][] = "Blanco";
$proto26["m_columns"][] = "Validos";
$obj = new SQLTable($proto26);

$proto25["m_table"] = $obj;
$proto25["m_sql"] = "v_elec_muni";
$proto25["m_alias"] = "";
$proto25["m_srcTableName"] = "cm1_1_v_elec_muni";
$proto27=array();
$proto27["m_sql"] = "";
$proto27["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto27["m_column"]=$obj;
$proto27["m_contained"] = array();
$proto27["m_strCase"] = "";
$proto27["m_havingmode"] = false;
$proto27["m_inBrackets"] = false;
$proto27["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto27);

$proto25["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto25);

$proto1["m_fromlist"][]=$obj;
$proto1["m_groupby"] = array();
$proto1["m_orderby"] = array();
$proto1["m_srcTableName"]="cm1_1_v_elec_muni";		
$obj = new SQLQuery($proto1);

	return $obj;
}
$queryData_cm1_1_v_elec_muni = createSqlQuery_cm1_1_v_elec_muni();


	
		;

									

$tdatacm1_1_v_elec_muni[".sqlquery"] = $queryData_cm1_1_v_elec_muni;

$tableEvents["cm1_1_v_elec_muni"] = new eventsBase;
$tdatacm1_1_v_elec_muni[".hasEvents"] = false;

?>