<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm1_2_Electos = array();
	$tdatacm1_2_Electos[".truncateText"] = true;
	$tdatacm1_2_Electos[".NumberOfChars"] = 80;
	$tdatacm1_2_Electos[".ShortName"] = "cm1_2_Electos";
	$tdatacm1_2_Electos[".OwnerID"] = "";
	$tdatacm1_2_Electos[".OriginalTable"] = "electos";

//	field labels
$fieldLabelscm1_2_Electos = array();
$fieldToolTipscm1_2_Electos = array();
$pageTitlescm1_2_Electos = array();
$placeHolderscm1_2_Electos = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm1_2_Electos["Spanish"] = array();
	$fieldToolTipscm1_2_Electos["Spanish"] = array();
	$placeHolderscm1_2_Electos["Spanish"] = array();
	$pageTitlescm1_2_Electos["Spanish"] = array();
	$fieldLabelscm1_2_Electos["Spanish"]["idElectos"] = "Id Interno";
	$fieldToolTipscm1_2_Electos["Spanish"]["idElectos"] = "";
	$placeHolderscm1_2_Electos["Spanish"]["idElectos"] = "";
	$fieldLabelscm1_2_Electos["Spanish"]["Orden"] = "Orden";
	$fieldToolTipscm1_2_Electos["Spanish"]["Orden"] = "";
	$placeHolderscm1_2_Electos["Spanish"]["Orden"] = "";
	$fieldLabelscm1_2_Electos["Spanish"]["NombreyApellidos"] = "Nombre y Apellidos";
	$fieldToolTipscm1_2_Electos["Spanish"]["NombreyApellidos"] = "";
	$placeHolderscm1_2_Electos["Spanish"]["NombreyApellidos"] = "";
	$fieldLabelscm1_2_Electos["Spanish"]["Coeficiente"] = "Coeficiente aplicado";
	$fieldToolTipscm1_2_Electos["Spanish"]["Coeficiente"] = "";
	$placeHolderscm1_2_Electos["Spanish"]["Coeficiente"] = "";
	$fieldLabelscm1_2_Electos["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldToolTipscm1_2_Electos["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderscm1_2_Electos["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelscm1_2_Electos["Spanish"]["Candidatura_idCandidatura"] = "Candidatura";
	$fieldToolTipscm1_2_Electos["Spanish"]["Candidatura_idCandidatura"] = "";
	$placeHolderscm1_2_Electos["Spanish"]["Candidatura_idCandidatura"] = "";
	$pageTitlescm1_2_Electos["Spanish"]["list"] = "<strong>{%master.Convocatoria_idConvocatoria}</strong>, Diputados";
	if (count($fieldToolTipscm1_2_Electos["Spanish"]))
		$tdatacm1_2_Electos[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm1_2_Electos[""] = array();
	$fieldToolTipscm1_2_Electos[""] = array();
	$placeHolderscm1_2_Electos[""] = array();
	$pageTitlescm1_2_Electos[""] = array();
	if (count($fieldToolTipscm1_2_Electos[""]))
		$tdatacm1_2_Electos[".isUseToolTips"] = true;
}


	$tdatacm1_2_Electos[".NCSearch"] = true;



$tdatacm1_2_Electos[".shortTableName"] = "cm1_2_Electos";
$tdatacm1_2_Electos[".nSecOptions"] = 0;
$tdatacm1_2_Electos[".recsPerRowList"] = 1;
$tdatacm1_2_Electos[".recsPerRowPrint"] = 1;
$tdatacm1_2_Electos[".mainTableOwnerID"] = "";
$tdatacm1_2_Electos[".moveNext"] = 1;
$tdatacm1_2_Electos[".entityType"] = 1;

$tdatacm1_2_Electos[".strOriginalTableName"] = "electos";

	



$tdatacm1_2_Electos[".showAddInPopup"] = true;

$tdatacm1_2_Electos[".showEditInPopup"] = true;

$tdatacm1_2_Electos[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["add"] = "add";
			;
$popupPagesLayoutNames["edit"] = "add";
						
	;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdatacm1_2_Electos[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm1_2_Electos[".fieldsForRegister"] = array();

$tdatacm1_2_Electos[".listAjax"] = false;

	$tdatacm1_2_Electos[".audit"] = false;

	$tdatacm1_2_Electos[".locking"] = false;



$tdatacm1_2_Electos[".list"] = true;





$tdatacm1_2_Electos[".exportFormatting"] = 2;
$tdatacm1_2_Electos[".exportDelimiter"] = ",";
		
$tdatacm1_2_Electos[".view"] = true;


$tdatacm1_2_Electos[".exportTo"] = true;

$tdatacm1_2_Electos[".printFriendly"] = true;


$tdatacm1_2_Electos[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm1_2_Electos[".allowShowHideFields"] = true;
//

// Allow Fields Reordering in GRID
$tdatacm1_2_Electos[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm1_2_Electos[".searchSaving"] = false;
//

$tdatacm1_2_Electos[".showSearchPanel"] = true;
		$tdatacm1_2_Electos[".flexibleSearch"] = true;

$tdatacm1_2_Electos[".isUseAjaxSuggest"] = true;

$tdatacm1_2_Electos[".rowHighlite"] = true;





$tdatacm1_2_Electos[".ajaxCodeSnippetAdded"] = false;

$tdatacm1_2_Electos[".buttonsAdded"] = false;

$tdatacm1_2_Electos[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm1_2_Electos[".isUseTimeForSearch"] = false;



$tdatacm1_2_Electos[".badgeColor"] = "4169e1";


$tdatacm1_2_Electos[".allSearchFields"] = array();
$tdatacm1_2_Electos[".filterFields"] = array();
$tdatacm1_2_Electos[".requiredSearchFields"] = array();

$tdatacm1_2_Electos[".allSearchFields"][] = "idElectos";
	$tdatacm1_2_Electos[".allSearchFields"][] = "Convocatoria_idConvocatoria";
	$tdatacm1_2_Electos[".allSearchFields"][] = "Candidatura_idCandidatura";
	$tdatacm1_2_Electos[".allSearchFields"][] = "Orden";
	$tdatacm1_2_Electos[".allSearchFields"][] = "NombreyApellidos";
	$tdatacm1_2_Electos[".allSearchFields"][] = "Coeficiente";
	
$tdatacm1_2_Electos[".filterFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_2_Electos[".filterFields"][] = "Candidatura_idCandidatura";

$tdatacm1_2_Electos[".googleLikeFields"] = array();
$tdatacm1_2_Electos[".googleLikeFields"][] = "idElectos";
$tdatacm1_2_Electos[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_2_Electos[".googleLikeFields"][] = "Candidatura_idCandidatura";
$tdatacm1_2_Electos[".googleLikeFields"][] = "Orden";
$tdatacm1_2_Electos[".googleLikeFields"][] = "NombreyApellidos";
$tdatacm1_2_Electos[".googleLikeFields"][] = "Coeficiente";


$tdatacm1_2_Electos[".advSearchFields"] = array();
$tdatacm1_2_Electos[".advSearchFields"][] = "idElectos";
$tdatacm1_2_Electos[".advSearchFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_2_Electos[".advSearchFields"][] = "Candidatura_idCandidatura";
$tdatacm1_2_Electos[".advSearchFields"][] = "Orden";
$tdatacm1_2_Electos[".advSearchFields"][] = "NombreyApellidos";
$tdatacm1_2_Electos[".advSearchFields"][] = "Coeficiente";

$tdatacm1_2_Electos[".tableType"] = "list";

$tdatacm1_2_Electos[".printerPageOrientation"] = 0;
$tdatacm1_2_Electos[".nPrinterPageScale"] = 100;

$tdatacm1_2_Electos[".nPrinterSplitRecords"] = 40;

$tdatacm1_2_Electos[".nPrinterPDFSplitRecords"] = 40;



$tdatacm1_2_Electos[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatacm1_2_Electos[".pageSize"] = 12;

$tdatacm1_2_Electos[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY Candidatura_idCandidatura, Orden";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm1_2_Electos[".strOrderBy"] = $tstrOrderBy;

$tdatacm1_2_Electos[".orderindexes"] = array();
	$tdatacm1_2_Electos[".orderindexes"][] = array(3, (1 ? "ASC" : "DESC"), "Candidatura_idCandidatura");

	$tdatacm1_2_Electos[".orderindexes"][] = array(4, (1 ? "ASC" : "DESC"), "Orden");


$tdatacm1_2_Electos[".sqlHead"] = "SELECT idElectos,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Orden,  NombreyApellidos,  Coeficiente";
$tdatacm1_2_Electos[".sqlFrom"] = "FROM electos";
$tdatacm1_2_Electos[".sqlWhereExpr"] = "";
$tdatacm1_2_Electos[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 12;
$arrRPP[] = 24;
$arrRPP[] = 36;
$arrRPP[] = 48;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm1_2_Electos[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm1_2_Electos[".arrGroupsPerPage"] = $arrGPP;

$tdatacm1_2_Electos[".highlightSearchResults"] = true;

$tableKeyscm1_2_Electos = array();
$tableKeyscm1_2_Electos[] = "idElectos";
$tdatacm1_2_Electos[".Keys"] = $tableKeyscm1_2_Electos;

$tdatacm1_2_Electos[".listFields"] = array();
$tdatacm1_2_Electos[".listFields"][] = "Candidatura_idCandidatura";
$tdatacm1_2_Electos[".listFields"][] = "Orden";
$tdatacm1_2_Electos[".listFields"][] = "NombreyApellidos";
$tdatacm1_2_Electos[".listFields"][] = "Coeficiente";

$tdatacm1_2_Electos[".hideMobileList"] = array();


$tdatacm1_2_Electos[".viewFields"] = array();
$tdatacm1_2_Electos[".viewFields"][] = "idElectos";
$tdatacm1_2_Electos[".viewFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_2_Electos[".viewFields"][] = "Candidatura_idCandidatura";
$tdatacm1_2_Electos[".viewFields"][] = "Orden";
$tdatacm1_2_Electos[".viewFields"][] = "NombreyApellidos";
$tdatacm1_2_Electos[".viewFields"][] = "Coeficiente";

$tdatacm1_2_Electos[".addFields"] = array();

$tdatacm1_2_Electos[".masterListFields"] = array();
$tdatacm1_2_Electos[".masterListFields"][] = "idElectos";
$tdatacm1_2_Electos[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_2_Electos[".masterListFields"][] = "Candidatura_idCandidatura";
$tdatacm1_2_Electos[".masterListFields"][] = "Orden";
$tdatacm1_2_Electos[".masterListFields"][] = "NombreyApellidos";
$tdatacm1_2_Electos[".masterListFields"][] = "Coeficiente";

$tdatacm1_2_Electos[".inlineAddFields"] = array();

$tdatacm1_2_Electos[".editFields"] = array();

$tdatacm1_2_Electos[".inlineEditFields"] = array();

$tdatacm1_2_Electos[".updateSelectedFields"] = array();


$tdatacm1_2_Electos[".exportFields"] = array();
$tdatacm1_2_Electos[".exportFields"][] = "idElectos";
$tdatacm1_2_Electos[".exportFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_2_Electos[".exportFields"][] = "Candidatura_idCandidatura";
$tdatacm1_2_Electos[".exportFields"][] = "Orden";
$tdatacm1_2_Electos[".exportFields"][] = "NombreyApellidos";
$tdatacm1_2_Electos[".exportFields"][] = "Coeficiente";

$tdatacm1_2_Electos[".importFields"] = array();

$tdatacm1_2_Electos[".printFields"] = array();
$tdatacm1_2_Electos[".printFields"][] = "idElectos";
$tdatacm1_2_Electos[".printFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_2_Electos[".printFields"][] = "Candidatura_idCandidatura";
$tdatacm1_2_Electos[".printFields"][] = "Orden";
$tdatacm1_2_Electos[".printFields"][] = "NombreyApellidos";
$tdatacm1_2_Electos[".printFields"][] = "Coeficiente";


//	idElectos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idElectos";
	$fdata["GoodName"] = "idElectos";
	$fdata["ownerTable"] = "electos";
	$fdata["Label"] = GetFieldLabel("cm1_2_Electos","idElectos");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idElectos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idElectos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_2_Electos["idElectos"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "electos";
	$fdata["Label"] = GetFieldLabel("cm1_2_Electos","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 2;
		$fdata["filterTotalFields"] = "idElectos";
		$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = true;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 0;

			
	
	
//end of Filters settings


	$tdatacm1_2_Electos["Convocatoria_idConvocatoria"] = $fdata;
//	Candidatura_idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Candidatura_idCandidatura";
	$fdata["GoodName"] = "Candidatura_idCandidatura";
	$fdata["ownerTable"] = "electos";
	$fdata["Label"] = GetFieldLabel("cm1_2_Electos","Candidatura_idCandidatura");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Candidatura_idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Candidatura_idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "candidatura";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idCandidatura";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Codigo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 2;
		$fdata["filterTotalFields"] = "idElectos";
		$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = true;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 0;

			
	
	
//end of Filters settings


	$tdatacm1_2_Electos["Candidatura_idCandidatura"] = $fdata;
//	Orden
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Orden";
	$fdata["GoodName"] = "Orden";
	$fdata["ownerTable"] = "electos";
	$fdata["Label"] = GetFieldLabel("cm1_2_Electos","Orden");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Orden";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Orden";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_2_Electos["Orden"] = $fdata;
//	NombreyApellidos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "NombreyApellidos";
	$fdata["GoodName"] = "NombreyApellidos";
	$fdata["ownerTable"] = "electos";
	$fdata["Label"] = GetFieldLabel("cm1_2_Electos","NombreyApellidos");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "NombreyApellidos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NombreyApellidos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=100";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_2_Electos["NombreyApellidos"] = $fdata;
//	Coeficiente
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Coeficiente";
	$fdata["GoodName"] = "Coeficiente";
	$fdata["ownerTable"] = "electos";
	$fdata["Label"] = GetFieldLabel("cm1_2_Electos","Coeficiente");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Coeficiente";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Coeficiente";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_2_Electos["Coeficiente"] = $fdata;


$tables_data["cm1_2_Electos"]=&$tdatacm1_2_Electos;
$field_labels["cm1_2_Electos"] = &$fieldLabelscm1_2_Electos;
$fieldToolTips["cm1_2_Electos"] = &$fieldToolTipscm1_2_Electos;
$placeHolders["cm1_2_Electos"] = &$placeHolderscm1_2_Electos;
$page_titles["cm1_2_Electos"] = &$pageTitlescm1_2_Electos;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm1_2_Electos"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["cm1_2_Electos"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm1_2_Electos"][0] = $masterParams;
				$masterTablesData["cm1_2_Electos"][0]["masterKeys"] = array();
	$masterTablesData["cm1_2_Electos"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["cm1_2_Electos"][0]["detailKeys"] = array();
	$masterTablesData["cm1_2_Electos"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
		
	
				$strOriginalDetailsTable="escanos";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm1_2_Candidaturas";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm1_2_Candidaturas";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm1_2_Electos"][1] = $masterParams;
				$masterTablesData["cm1_2_Electos"][1]["masterKeys"] = array();
	$masterTablesData["cm1_2_Electos"][1]["masterKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["cm1_2_Electos"][1]["masterKeys"][]="Candidatura_idCandidatura";
				$masterTablesData["cm1_2_Electos"][1]["detailKeys"] = array();
	$masterTablesData["cm1_2_Electos"][1]["detailKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["cm1_2_Electos"][1]["detailKeys"][]="Candidatura_idCandidatura";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm1_2_Electos()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "idElectos,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Orden,  NombreyApellidos,  Coeficiente";
$proto3["m_strFrom"] = "FROM electos";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "ORDER BY Candidatura_idCandidatura, Orden";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idElectos",
	"m_strTable" => "electos",
	"m_srcTableName" => "cm1_2_Electos"
));

$proto9["m_sql"] = "idElectos";
$proto9["m_srcTableName"] = "cm1_2_Electos";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "electos",
	"m_srcTableName" => "cm1_2_Electos"
));

$proto11["m_sql"] = "Convocatoria_idConvocatoria";
$proto11["m_srcTableName"] = "cm1_2_Electos";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "electos",
	"m_srcTableName" => "cm1_2_Electos"
));

$proto13["m_sql"] = "Candidatura_idCandidatura";
$proto13["m_srcTableName"] = "cm1_2_Electos";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "electos",
	"m_srcTableName" => "cm1_2_Electos"
));

$proto15["m_sql"] = "Orden";
$proto15["m_srcTableName"] = "cm1_2_Electos";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "NombreyApellidos",
	"m_strTable" => "electos",
	"m_srcTableName" => "cm1_2_Electos"
));

$proto17["m_sql"] = "NombreyApellidos";
$proto17["m_srcTableName"] = "cm1_2_Electos";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "Coeficiente",
	"m_strTable" => "electos",
	"m_srcTableName" => "cm1_2_Electos"
));

$proto19["m_sql"] = "Coeficiente";
$proto19["m_srcTableName"] = "cm1_2_Electos";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto21=array();
$proto21["m_link"] = "SQLL_MAIN";
			$proto22=array();
$proto22["m_strName"] = "electos";
$proto22["m_srcTableName"] = "cm1_2_Electos";
$proto22["m_columns"] = array();
$proto22["m_columns"][] = "idElectos";
$proto22["m_columns"][] = "Convocatoria_idConvocatoria";
$proto22["m_columns"][] = "Candidatura_idCandidatura";
$proto22["m_columns"][] = "Orden";
$proto22["m_columns"][] = "NombreyApellidos";
$proto22["m_columns"][] = "Coeficiente";
$obj = new SQLTable($proto22);

$proto21["m_table"] = $obj;
$proto21["m_sql"] = "electos";
$proto21["m_alias"] = "";
$proto21["m_srcTableName"] = "cm1_2_Electos";
$proto23=array();
$proto23["m_sql"] = "";
$proto23["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto23["m_column"]=$obj;
$proto23["m_contained"] = array();
$proto23["m_strCase"] = "";
$proto23["m_havingmode"] = false;
$proto23["m_inBrackets"] = false;
$proto23["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto23);

$proto21["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto21);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto25=array();
						$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "electos",
	"m_srcTableName" => "cm1_2_Electos"
));

$proto25["m_column"]=$obj;
$proto25["m_bAsc"] = 1;
$proto25["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto25);

$proto3["m_orderby"][]=$obj;					
												$proto27=array();
						$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "electos",
	"m_srcTableName" => "cm1_2_Electos"
));

$proto27["m_column"]=$obj;
$proto27["m_bAsc"] = 1;
$proto27["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto27);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="cm1_2_Electos";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_cm1_2_Electos = createSqlQuery_cm1_2_Electos();


	
		;

						

$tdatacm1_2_Electos[".sqlquery"] = $queryData_cm1_2_Electos;

$tableEvents["cm1_2_Electos"] = new eventsBase;
$tdatacm1_2_Electos[".hasEvents"] = false;

?>