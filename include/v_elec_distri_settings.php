<?php
require_once(getabspath("classes/cipherer.php"));




$tdatav_elec_distri = array();
	$tdatav_elec_distri[".truncateText"] = true;
	$tdatav_elec_distri[".NumberOfChars"] = 80;
	$tdatav_elec_distri[".ShortName"] = "v_elec_distri";
	$tdatav_elec_distri[".OwnerID"] = "";
	$tdatav_elec_distri[".OriginalTable"] = "v_elec_distri";

//	field labels
$fieldLabelsv_elec_distri = array();
$fieldToolTipsv_elec_distri = array();
$pageTitlesv_elec_distri = array();
$placeHoldersv_elec_distri = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelsv_elec_distri["Spanish"] = array();
	$fieldToolTipsv_elec_distri["Spanish"] = array();
	$placeHoldersv_elec_distri["Spanish"] = array();
	$pageTitlesv_elec_distri["Spanish"] = array();
	$fieldLabelsv_elec_distri["Spanish"]["NumeroMunicipio"] = "Numero Municipio";
	$fieldToolTipsv_elec_distri["Spanish"]["NumeroMunicipio"] = "";
	$placeHoldersv_elec_distri["Spanish"]["NumeroMunicipio"] = "";
	$fieldLabelsv_elec_distri["Spanish"]["NumeroDistrito"] = "Numero Distrito";
	$fieldToolTipsv_elec_distri["Spanish"]["NumeroDistrito"] = "";
	$placeHoldersv_elec_distri["Spanish"]["NumeroDistrito"] = "";
	$fieldLabelsv_elec_distri["Spanish"]["NombreDistrito"] = "Distrito";
	$fieldToolTipsv_elec_distri["Spanish"]["NombreDistrito"] = "";
	$placeHoldersv_elec_distri["Spanish"]["NombreDistrito"] = "";
	$fieldLabelsv_elec_distri["Spanish"]["idElecDistriTotal"] = "Id Elec Distri Total";
	$fieldToolTipsv_elec_distri["Spanish"]["idElecDistriTotal"] = "";
	$placeHoldersv_elec_distri["Spanish"]["idElecDistriTotal"] = "";
	$fieldLabelsv_elec_distri["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria IdConvocatoria";
	$fieldToolTipsv_elec_distri["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHoldersv_elec_distri["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelsv_elec_distri["Spanish"]["Censo"] = "Censo";
	$fieldToolTipsv_elec_distri["Spanish"]["Censo"] = "";
	$placeHoldersv_elec_distri["Spanish"]["Censo"] = "";
	$fieldLabelsv_elec_distri["Spanish"]["Certificacion"] = "Votos Certificación";
	$fieldToolTipsv_elec_distri["Spanish"]["Certificacion"] = "";
	$placeHoldersv_elec_distri["Spanish"]["Certificacion"] = "";
	$fieldLabelsv_elec_distri["Spanish"]["Interventores"] = "Votos Interventores";
	$fieldToolTipsv_elec_distri["Spanish"]["Interventores"] = "";
	$placeHoldersv_elec_distri["Spanish"]["Interventores"] = "";
	$fieldLabelsv_elec_distri["Spanish"]["Nulo"] = "Votos Nulos";
	$fieldToolTipsv_elec_distri["Spanish"]["Nulo"] = "";
	$placeHoldersv_elec_distri["Spanish"]["Nulo"] = "";
	$fieldLabelsv_elec_distri["Spanish"]["Blanco"] = "Votos Blancos";
	$fieldToolTipsv_elec_distri["Spanish"]["Blanco"] = "";
	$placeHoldersv_elec_distri["Spanish"]["Blanco"] = "";
	$fieldLabelsv_elec_distri["Spanish"]["Validos"] = "Votos Válidos";
	$fieldToolTipsv_elec_distri["Spanish"]["Validos"] = "";
	$placeHoldersv_elec_distri["Spanish"]["Validos"] = "";
	$pageTitlesv_elec_distri["Spanish"]["view"] = "Votos en Madrid, Distrito de [{%NombreDistrito}]";
	$pageTitlesv_elec_distri["Spanish"]["list"] = "<strong>{%master.Titulo}</strong>, Votos en los Distritos de Madrid";
	if (count($fieldToolTipsv_elec_distri["Spanish"]))
		$tdatav_elec_distri[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsv_elec_distri[""] = array();
	$fieldToolTipsv_elec_distri[""] = array();
	$placeHoldersv_elec_distri[""] = array();
	$pageTitlesv_elec_distri[""] = array();
	if (count($fieldToolTipsv_elec_distri[""]))
		$tdatav_elec_distri[".isUseToolTips"] = true;
}


	$tdatav_elec_distri[".NCSearch"] = true;



$tdatav_elec_distri[".shortTableName"] = "v_elec_distri";
$tdatav_elec_distri[".nSecOptions"] = 0;
$tdatav_elec_distri[".recsPerRowList"] = 1;
$tdatav_elec_distri[".recsPerRowPrint"] = 1;
$tdatav_elec_distri[".mainTableOwnerID"] = "";
$tdatav_elec_distri[".moveNext"] = 1;
$tdatav_elec_distri[".entityType"] = 0;

$tdatav_elec_distri[".strOriginalTableName"] = "v_elec_distri";

	



$tdatav_elec_distri[".showAddInPopup"] = false;

$tdatav_elec_distri[".showEditInPopup"] = false;

$tdatav_elec_distri[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatav_elec_distri[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatav_elec_distri[".fieldsForRegister"] = array();

$tdatav_elec_distri[".listAjax"] = false;

	$tdatav_elec_distri[".audit"] = false;

	$tdatav_elec_distri[".locking"] = false;



$tdatav_elec_distri[".list"] = true;



$tdatav_elec_distri[".reorderRecordsByHeader"] = true;



$tdatav_elec_distri[".view"] = true;





$tdatav_elec_distri[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatav_elec_distri[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatav_elec_distri[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatav_elec_distri[".searchSaving"] = false;
//

$tdatav_elec_distri[".showSearchPanel"] = true;
		$tdatav_elec_distri[".flexibleSearch"] = true;

$tdatav_elec_distri[".isUseAjaxSuggest"] = true;

$tdatav_elec_distri[".rowHighlite"] = true;





$tdatav_elec_distri[".ajaxCodeSnippetAdded"] = false;

$tdatav_elec_distri[".buttonsAdded"] = false;

$tdatav_elec_distri[".addPageEvents"] = false;

// use timepicker for search panel
$tdatav_elec_distri[".isUseTimeForSearch"] = false;



$tdatav_elec_distri[".badgeColor"] = "00c2c5";

$tdatav_elec_distri[".detailsLinksOnList"] = "2";

$tdatav_elec_distri[".allSearchFields"] = array();
$tdatav_elec_distri[".filterFields"] = array();
$tdatav_elec_distri[".requiredSearchFields"] = array();

$tdatav_elec_distri[".allSearchFields"][] = "NombreDistrito";
	$tdatav_elec_distri[".allSearchFields"][] = "Censo";
	$tdatav_elec_distri[".allSearchFields"][] = "Certificacion";
	$tdatav_elec_distri[".allSearchFields"][] = "Interventores";
	$tdatav_elec_distri[".allSearchFields"][] = "Nulo";
	$tdatav_elec_distri[".allSearchFields"][] = "Blanco";
	$tdatav_elec_distri[".allSearchFields"][] = "Validos";
	

$tdatav_elec_distri[".googleLikeFields"] = array();
$tdatav_elec_distri[".googleLikeFields"][] = "NumeroMunicipio";
$tdatav_elec_distri[".googleLikeFields"][] = "NumeroDistrito";
$tdatav_elec_distri[".googleLikeFields"][] = "NombreDistrito";
$tdatav_elec_distri[".googleLikeFields"][] = "idElecDistriTotal";
$tdatav_elec_distri[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdatav_elec_distri[".googleLikeFields"][] = "Censo";
$tdatav_elec_distri[".googleLikeFields"][] = "Certificacion";
$tdatav_elec_distri[".googleLikeFields"][] = "Interventores";
$tdatav_elec_distri[".googleLikeFields"][] = "Nulo";
$tdatav_elec_distri[".googleLikeFields"][] = "Blanco";
$tdatav_elec_distri[".googleLikeFields"][] = "Validos";


$tdatav_elec_distri[".advSearchFields"] = array();
$tdatav_elec_distri[".advSearchFields"][] = "NombreDistrito";
$tdatav_elec_distri[".advSearchFields"][] = "Censo";
$tdatav_elec_distri[".advSearchFields"][] = "Certificacion";
$tdatav_elec_distri[".advSearchFields"][] = "Interventores";
$tdatav_elec_distri[".advSearchFields"][] = "Nulo";
$tdatav_elec_distri[".advSearchFields"][] = "Blanco";
$tdatav_elec_distri[".advSearchFields"][] = "Validos";

$tdatav_elec_distri[".tableType"] = "list";

$tdatav_elec_distri[".printerPageOrientation"] = 0;
$tdatav_elec_distri[".nPrinterPageScale"] = 100;

$tdatav_elec_distri[".nPrinterSplitRecords"] = 40;

$tdatav_elec_distri[".nPrinterPDFSplitRecords"] = 40;



$tdatav_elec_distri[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatav_elec_distri[".pageSize"] = 3;

$tdatav_elec_distri[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatav_elec_distri[".strOrderBy"] = $tstrOrderBy;

$tdatav_elec_distri[".orderindexes"] = array();

$tdatav_elec_distri[".sqlHead"] = "SELECT NumeroMunicipio,  	NumeroDistrito,  	NombreDistrito,  	idElecDistriTotal,  	Convocatoria_idConvocatoria,  	Censo,  	Certificacion,  	Interventores,  	Nulo,  	Blanco,  	Validos";
$tdatav_elec_distri[".sqlFrom"] = "FROM v_elec_distri";
$tdatav_elec_distri[".sqlWhereExpr"] = "";
$tdatav_elec_distri[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatav_elec_distri[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatav_elec_distri[".arrGroupsPerPage"] = $arrGPP;

$tdatav_elec_distri[".highlightSearchResults"] = true;

$tableKeysv_elec_distri = array();
$tableKeysv_elec_distri[] = "idElecDistriTotal";
$tdatav_elec_distri[".Keys"] = $tableKeysv_elec_distri;

$tdatav_elec_distri[".listFields"] = array();
$tdatav_elec_distri[".listFields"][] = "NombreDistrito";
$tdatav_elec_distri[".listFields"][] = "Censo";
$tdatav_elec_distri[".listFields"][] = "Certificacion";
$tdatav_elec_distri[".listFields"][] = "Interventores";
$tdatav_elec_distri[".listFields"][] = "Nulo";
$tdatav_elec_distri[".listFields"][] = "Blanco";
$tdatav_elec_distri[".listFields"][] = "Validos";

$tdatav_elec_distri[".hideMobileList"] = array();


$tdatav_elec_distri[".viewFields"] = array();
$tdatav_elec_distri[".viewFields"][] = "Censo";
$tdatav_elec_distri[".viewFields"][] = "Certificacion";
$tdatav_elec_distri[".viewFields"][] = "Interventores";
$tdatav_elec_distri[".viewFields"][] = "Nulo";
$tdatav_elec_distri[".viewFields"][] = "Blanco";
$tdatav_elec_distri[".viewFields"][] = "Validos";

$tdatav_elec_distri[".addFields"] = array();

$tdatav_elec_distri[".masterListFields"] = array();
$tdatav_elec_distri[".masterListFields"][] = "NumeroMunicipio";
$tdatav_elec_distri[".masterListFields"][] = "NumeroDistrito";
$tdatav_elec_distri[".masterListFields"][] = "NombreDistrito";
$tdatav_elec_distri[".masterListFields"][] = "idElecDistriTotal";
$tdatav_elec_distri[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdatav_elec_distri[".masterListFields"][] = "Censo";
$tdatav_elec_distri[".masterListFields"][] = "Certificacion";
$tdatav_elec_distri[".masterListFields"][] = "Interventores";
$tdatav_elec_distri[".masterListFields"][] = "Nulo";
$tdatav_elec_distri[".masterListFields"][] = "Blanco";
$tdatav_elec_distri[".masterListFields"][] = "Validos";

$tdatav_elec_distri[".inlineAddFields"] = array();

$tdatav_elec_distri[".editFields"] = array();

$tdatav_elec_distri[".inlineEditFields"] = array();

$tdatav_elec_distri[".updateSelectedFields"] = array();


$tdatav_elec_distri[".exportFields"] = array();

$tdatav_elec_distri[".importFields"] = array();

$tdatav_elec_distri[".printFields"] = array();


//	NumeroMunicipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "NumeroMunicipio";
	$fdata["GoodName"] = "NumeroMunicipio";
	$fdata["ownerTable"] = "v_elec_distri";
	$fdata["Label"] = GetFieldLabel("v_elec_distri","NumeroMunicipio");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "NumeroMunicipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NumeroMunicipio";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_elec_distri["NumeroMunicipio"] = $fdata;
//	NumeroDistrito
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "NumeroDistrito";
	$fdata["GoodName"] = "NumeroDistrito";
	$fdata["ownerTable"] = "v_elec_distri";
	$fdata["Label"] = GetFieldLabel("v_elec_distri","NumeroDistrito");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "NumeroDistrito";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NumeroDistrito";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_elec_distri["NumeroDistrito"] = $fdata;
//	NombreDistrito
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "NombreDistrito";
	$fdata["GoodName"] = "NombreDistrito";
	$fdata["ownerTable"] = "v_elec_distri";
	$fdata["Label"] = GetFieldLabel("v_elec_distri","NombreDistrito");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "NombreDistrito";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NombreDistrito";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_distri["NombreDistrito"] = $fdata;
//	idElecDistriTotal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "idElecDistriTotal";
	$fdata["GoodName"] = "idElecDistriTotal";
	$fdata["ownerTable"] = "v_elec_distri";
	$fdata["Label"] = GetFieldLabel("v_elec_distri","idElecDistriTotal");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idElecDistriTotal";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idElecDistriTotal";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_elec_distri["idElecDistriTotal"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "v_elec_distri";
	$fdata["Label"] = GetFieldLabel("v_elec_distri","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_elec_distri["Convocatoria_idConvocatoria"] = $fdata;
//	Censo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Censo";
	$fdata["GoodName"] = "Censo";
	$fdata["ownerTable"] = "v_elec_distri";
	$fdata["Label"] = GetFieldLabel("v_elec_distri","Censo");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Censo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Censo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_distri["Censo"] = $fdata;
//	Certificacion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Certificacion";
	$fdata["GoodName"] = "Certificacion";
	$fdata["ownerTable"] = "v_elec_distri";
	$fdata["Label"] = GetFieldLabel("v_elec_distri","Certificacion");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Certificacion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Certificacion";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_distri["Certificacion"] = $fdata;
//	Interventores
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "Interventores";
	$fdata["GoodName"] = "Interventores";
	$fdata["ownerTable"] = "v_elec_distri";
	$fdata["Label"] = GetFieldLabel("v_elec_distri","Interventores");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Interventores";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Interventores";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_distri["Interventores"] = $fdata;
//	Nulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "Nulo";
	$fdata["GoodName"] = "Nulo";
	$fdata["ownerTable"] = "v_elec_distri";
	$fdata["Label"] = GetFieldLabel("v_elec_distri","Nulo");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Nulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Nulo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_distri["Nulo"] = $fdata;
//	Blanco
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "Blanco";
	$fdata["GoodName"] = "Blanco";
	$fdata["ownerTable"] = "v_elec_distri";
	$fdata["Label"] = GetFieldLabel("v_elec_distri","Blanco");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Blanco";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Blanco";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_distri["Blanco"] = $fdata;
//	Validos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "Validos";
	$fdata["GoodName"] = "Validos";
	$fdata["ownerTable"] = "v_elec_distri";
	$fdata["Label"] = GetFieldLabel("v_elec_distri","Validos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Validos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Validos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_distri["Validos"] = $fdata;


$tables_data["v_elec_distri"]=&$tdatav_elec_distri;
$field_labels["v_elec_distri"] = &$fieldLabelsv_elec_distri;
$fieldToolTips["v_elec_distri"] = &$fieldToolTipsv_elec_distri;
$placeHolders["v_elec_distri"] = &$placeHoldersv_elec_distri;
$page_titles["v_elec_distri"] = &$pageTitlesv_elec_distri;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["v_elec_distri"] = array();
//	cm1_distrito_voto Chart
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm1_distrito_voto Chart";
		$detailsParam["dOriginalTable"] = "v_distrito_voto";
		$detailsParam["proceedLink"] = true;
			$detailsParam["dType"]=PAGE_CHART;
		$detailsParam["dShortTable"] = "cm1_distrito_voto_Chart";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm1_distrito_voto_Chart");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = 0;
	$detailsParam["hideChild"] = false;
					$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 1;
		
	$detailsTablesData["v_elec_distri"][$dIndex] = $detailsParam;

	
		$detailsTablesData["v_elec_distri"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["v_elec_distri"][$dIndex]["masterKeys"][]="Convocatoria_idConvocatoria";

	$detailsTablesData["v_elec_distri"][$dIndex]["masterKeys"][]="NumeroMunicipio";

	$detailsTablesData["v_elec_distri"][$dIndex]["masterKeys"][]="NumeroDistrito";

				$detailsTablesData["v_elec_distri"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["v_elec_distri"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";

		
	$detailsTablesData["v_elec_distri"][$dIndex]["detailKeys"][]="Municipio";

		
	$detailsTablesData["v_elec_distri"][$dIndex]["detailKeys"][]="Distrito";

// tables which are master tables for current table (detail)
$masterTablesData["v_elec_distri"] = array();


	
				$strOriginalDetailsTable="v_elec_muni";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="v_elec_muni";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "v_elec_muni";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "1";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 0;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["v_elec_distri"][0] = $masterParams;
				$masterTablesData["v_elec_distri"][0]["masterKeys"] = array();
	$masterTablesData["v_elec_distri"][0]["masterKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["v_elec_distri"][0]["masterKeys"][]="NumeroMunicipio";
				$masterTablesData["v_elec_distri"][0]["detailKeys"] = array();
	$masterTablesData["v_elec_distri"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["v_elec_distri"][0]["detailKeys"][]="NumeroMunicipio";
		
	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["v_elec_distri"][1] = $masterParams;
				$masterTablesData["v_elec_distri"][1]["masterKeys"] = array();
	$masterTablesData["v_elec_distri"][1]["masterKeys"][]="idConvocatoria";
				$masterTablesData["v_elec_distri"][1]["detailKeys"] = array();
	$masterTablesData["v_elec_distri"][1]["detailKeys"][]="Convocatoria_idConvocatoria";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_v_elec_distri()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "NumeroMunicipio,  	NumeroDistrito,  	NombreDistrito,  	idElecDistriTotal,  	Convocatoria_idConvocatoria,  	Censo,  	Certificacion,  	Interventores,  	Nulo,  	Blanco,  	Validos";
$proto0["m_strFrom"] = "FROM v_elec_distri";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "NumeroMunicipio",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "v_elec_distri"
));

$proto6["m_sql"] = "NumeroMunicipio";
$proto6["m_srcTableName"] = "v_elec_distri";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "NumeroDistrito",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "v_elec_distri"
));

$proto8["m_sql"] = "NumeroDistrito";
$proto8["m_srcTableName"] = "v_elec_distri";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "NombreDistrito",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "v_elec_distri"
));

$proto10["m_sql"] = "NombreDistrito";
$proto10["m_srcTableName"] = "v_elec_distri";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "idElecDistriTotal",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "v_elec_distri"
));

$proto12["m_sql"] = "idElecDistriTotal";
$proto12["m_srcTableName"] = "v_elec_distri";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "v_elec_distri"
));

$proto14["m_sql"] = "Convocatoria_idConvocatoria";
$proto14["m_srcTableName"] = "v_elec_distri";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Censo",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "v_elec_distri"
));

$proto16["m_sql"] = "Censo";
$proto16["m_srcTableName"] = "v_elec_distri";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "Certificacion",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "v_elec_distri"
));

$proto18["m_sql"] = "Certificacion";
$proto18["m_srcTableName"] = "v_elec_distri";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "Interventores",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "v_elec_distri"
));

$proto20["m_sql"] = "Interventores";
$proto20["m_srcTableName"] = "v_elec_distri";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "Nulo",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "v_elec_distri"
));

$proto22["m_sql"] = "Nulo";
$proto22["m_srcTableName"] = "v_elec_distri";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "Blanco",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "v_elec_distri"
));

$proto24["m_sql"] = "Blanco";
$proto24["m_srcTableName"] = "v_elec_distri";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "Validos",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "v_elec_distri"
));

$proto26["m_sql"] = "Validos";
$proto26["m_srcTableName"] = "v_elec_distri";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto28=array();
$proto28["m_link"] = "SQLL_MAIN";
			$proto29=array();
$proto29["m_strName"] = "v_elec_distri";
$proto29["m_srcTableName"] = "v_elec_distri";
$proto29["m_columns"] = array();
$proto29["m_columns"][] = "NumeroMunicipio";
$proto29["m_columns"][] = "NumeroDistrito";
$proto29["m_columns"][] = "NombreDistrito";
$proto29["m_columns"][] = "idElecDistriTotal";
$proto29["m_columns"][] = "Convocatoria_idConvocatoria";
$proto29["m_columns"][] = "Censo";
$proto29["m_columns"][] = "Certificacion";
$proto29["m_columns"][] = "Interventores";
$proto29["m_columns"][] = "Nulo";
$proto29["m_columns"][] = "Blanco";
$proto29["m_columns"][] = "Validos";
$obj = new SQLTable($proto29);

$proto28["m_table"] = $obj;
$proto28["m_sql"] = "v_elec_distri";
$proto28["m_alias"] = "";
$proto28["m_srcTableName"] = "v_elec_distri";
$proto30=array();
$proto30["m_sql"] = "";
$proto30["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto30["m_column"]=$obj;
$proto30["m_contained"] = array();
$proto30["m_strCase"] = "";
$proto30["m_havingmode"] = false;
$proto30["m_inBrackets"] = false;
$proto30["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto30);

$proto28["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto28);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="v_elec_distri";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_v_elec_distri = createSqlQuery_v_elec_distri();


	
		;

											

$tdatav_elec_distri[".sqlquery"] = $queryData_v_elec_distri;

$tableEvents["v_elec_distri"] = new eventsBase;
$tdatav_elec_distri[".hasEvents"] = false;

?>