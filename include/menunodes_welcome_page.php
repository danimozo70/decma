<?php

function getMenuNodes_welcome_page($menuNodesObject)
{
	// create menu nodes arr
	$menuNodesObject->menuNodes["welcome_page"] = array();

	$menuNode = array();
	$menuNode["id"] = "1";
	$menuNode["name"] = "DECMA";
	$menuNode["href"] = "";
	$menuNode["type"] = "Group";
	$menuNode["table"] = "";
	$menuNode["style"] = "";
	$menuNode["params"] = "";
	$menuNode["parent"] = "0";
	$menuNode["nameType"] = "Text";
	$menuNode["linkType"] = "None";
	$menuNode["pageType"] = "List";//
	$menuNode["openType"] = "None";
	
	$menuNode["icon"] = "glyphicon-th";
	$menuNode["iconType"] = "2";

			$menuNode["color"] = "";
	
	$menuNode["title"] = "DECMA";

	$menuNode["comments"] = "<span style=\"color:rgb(255, 0, 0);\">D</span>ifusión de resultados <span style=\"color:rgb(255, 0, 0);\">E</span>lectorales de la <span style=\"color:rgb(255, 0, 0);\">C</span>omunidad de <span style=\"color:rgb(255, 0, 0);\">MA</span>drid.<BR><BR>- Voto Válido es la suma de votos de Candidaturas + el voto en Blanco. <br> - Los % están calculados con respecto al voto Válido";


	$menuNodesObject->menuNodes["welcome_page"][] = $menuNode;
	$menuNode = array();
	$menuNode["id"] = "2";
	$menuNode["name"] = "";
	$menuNode["href"] = "mypage.htm";
	$menuNode["type"] = "Leaf";
	$menuNode["table"] = "cm1_escanos";
	$menuNode["style"] = "";
	$menuNode["params"] = "";
	$menuNode["parent"] = "1";
	$menuNode["nameType"] = "Text";
	$menuNode["linkType"] = "Internal";
	$menuNode["pageType"] = "Dashboard";//
	$menuNode["openType"] = "None";
	
	$menuNode["icon"] = "glyphicon-signal";
	$menuNode["iconType"] = "2";

			$menuNode["color"] = "#"."4682B4";
	
	$menuNode["title"] = "Escaños";

	$menuNode["comments"] = "Consulta del resultado de las Elecciones a la Asamblea de Madrid por cada una de las convocatorias que ha habido.";


	$menuNodesObject->menuNodes["welcome_page"][] = $menuNode;
	$menuNode = array();
	$menuNode["id"] = "3";
	$menuNode["name"] = "Votos por municipio";
	$menuNode["href"] = "mypage.htm";
	$menuNode["type"] = "Leaf";
	$menuNode["table"] = "cm1_escanos1";
	$menuNode["style"] = "";
	$menuNode["params"] = "";
	$menuNode["parent"] = "1";
	$menuNode["nameType"] = "Text";
	$menuNode["linkType"] = "Internal";
	$menuNode["pageType"] = "Dashboard";//
	$menuNode["openType"] = "None";
	
	$menuNode["icon"] = "glyphicon-signal";
	$menuNode["iconType"] = "2";

			$menuNode["color"] = "";
	
	$menuNode["title"] = "Votos por municipio";

	$menuNode["comments"] = "Consulta de los resultados de cada una de las Convocatorias agrupados por Municipios y distritos de Madrid";


	$menuNodesObject->menuNodes["welcome_page"][] = $menuNode;
	$menuNode = array();
	$menuNode["id"] = "4";
	$menuNode["name"] = "Diputados electos";
	$menuNode["href"] = "mypage.htm";
	$menuNode["type"] = "Leaf";
	$menuNode["table"] = "cm1_escanos2";
	$menuNode["style"] = "";
	$menuNode["params"] = "";
	$menuNode["parent"] = "1";
	$menuNode["nameType"] = "Text";
	$menuNode["linkType"] = "Internal";
	$menuNode["pageType"] = "Dashboard";//
	$menuNode["openType"] = "None";
	
	$menuNode["icon"] = "glyphicon-signal";
	$menuNode["iconType"] = "2";

			$menuNode["color"] = "";
	
	$menuNode["title"] = "Diputados electos";

	$menuNode["comments"] = "Consulta de los diputados elegidos en cada una de las convocatorias y por cada una de las candidaturas que han obtenido escaños";


	$menuNodesObject->menuNodes["welcome_page"][] = $menuNode;
	$menuNode = array();
	$menuNode["id"] = "5";
	$menuNode["name"] = "Evolución del voto";
	$menuNode["href"] = "mypage.htm";
	$menuNode["type"] = "Leaf";
	$menuNode["table"] = "cm2_evolucion";
	$menuNode["style"] = "";
	$menuNode["params"] = "";
	$menuNode["parent"] = "1";
	$menuNode["nameType"] = "Text";
	$menuNode["linkType"] = "Internal";
	$menuNode["pageType"] = "Dashboard";//
	$menuNode["openType"] = "None";
	
	$menuNode["icon"] = "glyphicon-tasks";
	$menuNode["iconType"] = "2";

			$menuNode["color"] = "#"."FF6820";
	
	$menuNode["title"] = "Evolución del voto";

	$menuNode["comments"] = "Evolución de los escaños y voto por candidatura en el ámbito general, por municipio y por distrito de Madrid";


	$menuNodesObject->menuNodes["welcome_page"][] = $menuNode;
	$menuNode = array();
	$menuNode["id"] = "6";
	$menuNode["name"] = "Mapa- Votos";
	$menuNode["href"] = "mypage.htm";
	$menuNode["type"] = "Leaf";
	$menuNode["table"] = "cm3_mapas1";
	$menuNode["style"] = "";
	$menuNode["params"] = "";
	$menuNode["parent"] = "1";
	$menuNode["nameType"] = "Text";
	$menuNode["linkType"] = "Internal";
	$menuNode["pageType"] = "Dashboard";//
	$menuNode["openType"] = "None";
	
	$menuNode["icon"] = "glyphicon-screenshot";
	$menuNode["iconType"] = "2";

			$menuNode["color"] = "#"."009300";
	
	$menuNode["title"] = "Mapa- Votos";

	$menuNode["comments"] = "Consulta de la distribución de los votos por cada una de las convocatorias y por cada candidatura.";


	$menuNodesObject->menuNodes["welcome_page"][] = $menuNode;
	$menuNode = array();
	$menuNode["id"] = "7";
	$menuNode["name"] = "Mapa-mas votado";
	$menuNode["href"] = "mypage.htm";
	$menuNode["type"] = "Leaf";
	$menuNode["table"] = "cm4_tematico";
	$menuNode["style"] = "";
	$menuNode["params"] = "";
	$menuNode["parent"] = "1";
	$menuNode["nameType"] = "Text";
	$menuNode["linkType"] = "Internal";
	$menuNode["pageType"] = "Dashboard";//
	$menuNode["openType"] = "None";
	
	$menuNode["icon"] = "glyphicon-screenshot";
	$menuNode["iconType"] = "2";

			$menuNode["color"] = "#"."009300";
	
	$menuNode["title"] = "Mapa-mas votado";

	$menuNode["comments"] = "Consulta de la candidatura más votada por cada una de las convocatorias.";


	$menuNodesObject->menuNodes["welcome_page"][] = $menuNode;
}
?>
