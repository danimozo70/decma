<?php
require_once(getabspath("classes/cipherer.php"));




$tdataelecmunicandi = array();
	$tdataelecmunicandi[".truncateText"] = true;
	$tdataelecmunicandi[".NumberOfChars"] = 80;
	$tdataelecmunicandi[".ShortName"] = "elecmunicandi";
	$tdataelecmunicandi[".OwnerID"] = "";
	$tdataelecmunicandi[".OriginalTable"] = "elecmunicandi";

//	field labels
$fieldLabelselecmunicandi = array();
$fieldToolTipselecmunicandi = array();
$pageTitleselecmunicandi = array();
$placeHolderselecmunicandi = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelselecmunicandi["Spanish"] = array();
	$fieldToolTipselecmunicandi["Spanish"] = array();
	$placeHolderselecmunicandi["Spanish"] = array();
	$pageTitleselecmunicandi["Spanish"] = array();
	$fieldLabelselecmunicandi["Spanish"]["ElecCandidatura_idElecCandidatura"] = "Candidatura";
	$fieldToolTipselecmunicandi["Spanish"]["ElecCandidatura_idElecCandidatura"] = "";
	$placeHolderselecmunicandi["Spanish"]["ElecCandidatura_idElecCandidatura"] = "";
	$fieldLabelselecmunicandi["Spanish"]["idElecMuniCandi"] = "Id Interno";
	$fieldToolTipselecmunicandi["Spanish"]["idElecMuniCandi"] = "";
	$placeHolderselecmunicandi["Spanish"]["idElecMuniCandi"] = "";
	$fieldLabelselecmunicandi["Spanish"]["Municipio"] = "Municipio";
	$fieldToolTipselecmunicandi["Spanish"]["Municipio"] = "";
	$placeHolderselecmunicandi["Spanish"]["Municipio"] = "";
	$fieldLabelselecmunicandi["Spanish"]["Votos"] = "Votos";
	$fieldToolTipselecmunicandi["Spanish"]["Votos"] = "";
	$placeHolderselecmunicandi["Spanish"]["Votos"] = "";
	$fieldLabelselecmunicandi["Spanish"]["PorcVotos"] = "%Votos";
	$fieldToolTipselecmunicandi["Spanish"]["PorcVotos"] = "";
	$placeHolderselecmunicandi["Spanish"]["PorcVotos"] = "";
	$fieldLabelselecmunicandi["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldToolTipselecmunicandi["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderselecmunicandi["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelselecmunicandi["Spanish"]["Candidatura_idCandidatura"] = "Candidatura";
	$fieldToolTipselecmunicandi["Spanish"]["Candidatura_idCandidatura"] = "";
	$placeHolderselecmunicandi["Spanish"]["Candidatura_idCandidatura"] = "";
	if (count($fieldToolTipselecmunicandi["Spanish"]))
		$tdataelecmunicandi[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelselecmunicandi[""] = array();
	$fieldToolTipselecmunicandi[""] = array();
	$placeHolderselecmunicandi[""] = array();
	$pageTitleselecmunicandi[""] = array();
	if (count($fieldToolTipselecmunicandi[""]))
		$tdataelecmunicandi[".isUseToolTips"] = true;
}


	$tdataelecmunicandi[".NCSearch"] = true;



$tdataelecmunicandi[".shortTableName"] = "elecmunicandi";
$tdataelecmunicandi[".nSecOptions"] = 0;
$tdataelecmunicandi[".recsPerRowList"] = 1;
$tdataelecmunicandi[".recsPerRowPrint"] = 1;
$tdataelecmunicandi[".mainTableOwnerID"] = "";
$tdataelecmunicandi[".moveNext"] = 1;
$tdataelecmunicandi[".entityType"] = 0;

$tdataelecmunicandi[".strOriginalTableName"] = "elecmunicandi";

	



$tdataelecmunicandi[".showAddInPopup"] = true;

$tdataelecmunicandi[".showEditInPopup"] = true;

$tdataelecmunicandi[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["add"] = "view_bootstrap";
			;
$popupPagesLayoutNames["edit"] = "view_bootstrap";
			;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdataelecmunicandi[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataelecmunicandi[".fieldsForRegister"] = array();

$tdataelecmunicandi[".listAjax"] = false;

	$tdataelecmunicandi[".audit"] = false;

	$tdataelecmunicandi[".locking"] = false;



$tdataelecmunicandi[".list"] = true;






$tdataelecmunicandi[".inlineAdd"] = true;





$tdataelecmunicandi[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdataelecmunicandi[".allowShowHideFields"] = true;
//

// Allow Fields Reordering in GRID
$tdataelecmunicandi[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdataelecmunicandi[".searchSaving"] = false;
//

$tdataelecmunicandi[".showSearchPanel"] = true;
		$tdataelecmunicandi[".flexibleSearch"] = true;

$tdataelecmunicandi[".isUseAjaxSuggest"] = true;

$tdataelecmunicandi[".rowHighlite"] = true;





$tdataelecmunicandi[".ajaxCodeSnippetAdded"] = false;

$tdataelecmunicandi[".buttonsAdded"] = false;

$tdataelecmunicandi[".addPageEvents"] = false;

// use timepicker for search panel
$tdataelecmunicandi[".isUseTimeForSearch"] = false;



$tdataelecmunicandi[".badgeColor"] = "6493ea";


$tdataelecmunicandi[".allSearchFields"] = array();
$tdataelecmunicandi[".filterFields"] = array();
$tdataelecmunicandi[".requiredSearchFields"] = array();



$tdataelecmunicandi[".googleLikeFields"] = array();
$tdataelecmunicandi[".googleLikeFields"][] = "idElecMuniCandi";
$tdataelecmunicandi[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdataelecmunicandi[".googleLikeFields"][] = "Candidatura_idCandidatura";
$tdataelecmunicandi[".googleLikeFields"][] = "Municipio";
$tdataelecmunicandi[".googleLikeFields"][] = "Votos";
$tdataelecmunicandi[".googleLikeFields"][] = "PorcVotos";



$tdataelecmunicandi[".tableType"] = "list";

$tdataelecmunicandi[".printerPageOrientation"] = 0;
$tdataelecmunicandi[".nPrinterPageScale"] = 100;

$tdataelecmunicandi[".nPrinterSplitRecords"] = 40;

$tdataelecmunicandi[".nPrinterPDFSplitRecords"] = 40;



$tdataelecmunicandi[".geocodingEnabled"] = false;










// view page pdf

// print page pdf

$tdataelecmunicandi[".totalsFields"] = array();
$tdataelecmunicandi[".totalsFields"][] = array(
	"fName" => "Votos",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');
$tdataelecmunicandi[".totalsFields"][] = array(
	"fName" => "PorcVotos",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');

$tdataelecmunicandi[".pageSize"] = 30;

$tdataelecmunicandi[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY Votos DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataelecmunicandi[".strOrderBy"] = $tstrOrderBy;

$tdataelecmunicandi[".orderindexes"] = array();
	$tdataelecmunicandi[".orderindexes"][] = array(5, (0 ? "ASC" : "DESC"), "Votos");


$tdataelecmunicandi[".sqlHead"] = "SELECT idElecMuniCandi,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Municipio,  Votos,  PorcVotos";
$tdataelecmunicandi[".sqlFrom"] = "FROM elecmunicandi";
$tdataelecmunicandi[".sqlWhereExpr"] = "";
$tdataelecmunicandi[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataelecmunicandi[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataelecmunicandi[".arrGroupsPerPage"] = $arrGPP;

$tdataelecmunicandi[".highlightSearchResults"] = true;

$tableKeyselecmunicandi = array();
$tableKeyselecmunicandi[] = "idElecMuniCandi";
$tdataelecmunicandi[".Keys"] = $tableKeyselecmunicandi;

$tdataelecmunicandi[".listFields"] = array();
$tdataelecmunicandi[".listFields"][] = "Candidatura_idCandidatura";
$tdataelecmunicandi[".listFields"][] = "Votos";
$tdataelecmunicandi[".listFields"][] = "PorcVotos";

$tdataelecmunicandi[".hideMobileList"] = array();


$tdataelecmunicandi[".viewFields"] = array();

$tdataelecmunicandi[".addFields"] = array();

$tdataelecmunicandi[".masterListFields"] = array();
$tdataelecmunicandi[".masterListFields"][] = "idElecMuniCandi";
$tdataelecmunicandi[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdataelecmunicandi[".masterListFields"][] = "Candidatura_idCandidatura";
$tdataelecmunicandi[".masterListFields"][] = "Municipio";
$tdataelecmunicandi[".masterListFields"][] = "Votos";
$tdataelecmunicandi[".masterListFields"][] = "PorcVotos";

$tdataelecmunicandi[".inlineAddFields"] = array();
$tdataelecmunicandi[".inlineAddFields"][] = "Convocatoria_idConvocatoria";
$tdataelecmunicandi[".inlineAddFields"][] = "Municipio";
$tdataelecmunicandi[".inlineAddFields"][] = "Candidatura_idCandidatura";
$tdataelecmunicandi[".inlineAddFields"][] = "Votos";
$tdataelecmunicandi[".inlineAddFields"][] = "PorcVotos";

$tdataelecmunicandi[".editFields"] = array();

$tdataelecmunicandi[".inlineEditFields"] = array();

$tdataelecmunicandi[".updateSelectedFields"] = array();


$tdataelecmunicandi[".exportFields"] = array();

$tdataelecmunicandi[".importFields"] = array();

$tdataelecmunicandi[".printFields"] = array();


//	idElecMuniCandi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idElecMuniCandi";
	$fdata["GoodName"] = "idElecMuniCandi";
	$fdata["ownerTable"] = "elecmunicandi";
	$fdata["Label"] = GetFieldLabel("elecmunicandi","idElecMuniCandi");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idElecMuniCandi";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idElecMuniCandi";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecmunicandi["idElecMuniCandi"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "elecmunicandi";
	$fdata["Label"] = GetFieldLabel("elecmunicandi","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
		$fdata["bInlineAdd"] = true;

	
	
	

	
	
	
	
		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecmunicandi["Convocatoria_idConvocatoria"] = $fdata;
//	Candidatura_idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Candidatura_idCandidatura";
	$fdata["GoodName"] = "Candidatura_idCandidatura";
	$fdata["ownerTable"] = "elecmunicandi";
	$fdata["Label"] = GetFieldLabel("elecmunicandi","Candidatura_idCandidatura");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
		$fdata["bInlineAdd"] = true;

	
	
	

	
	
	
	
		$fdata["strField"] = "Candidatura_idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Candidatura_idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "candidatura";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idCandidatura";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "concat(Codigo,' - ',Titulo)";
	
	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecmunicandi["Candidatura_idCandidatura"] = $fdata;
//	Municipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Municipio";
	$fdata["GoodName"] = "Municipio";
	$fdata["ownerTable"] = "elecmunicandi";
	$fdata["Label"] = GetFieldLabel("elecmunicandi","Municipio");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
		$fdata["bInlineAdd"] = true;

	
	
	

	
	
	
	
		$fdata["strField"] = "Municipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Municipio";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "municipio";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "NumeroMunicipio";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "NombreMunicipio";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecmunicandi["Municipio"] = $fdata;
//	Votos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Votos";
	$fdata["GoodName"] = "Votos";
	$fdata["ownerTable"] = "elecmunicandi";
	$fdata["Label"] = GetFieldLabel("elecmunicandi","Votos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
		$fdata["bInlineAdd"] = true;

	
	
	

	
	
	
	
		$fdata["strField"] = "Votos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Votos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecmunicandi["Votos"] = $fdata;
//	PorcVotos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "PorcVotos";
	$fdata["GoodName"] = "PorcVotos";
	$fdata["ownerTable"] = "elecmunicandi";
	$fdata["Label"] = GetFieldLabel("elecmunicandi","PorcVotos");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

	
		$fdata["bInlineAdd"] = true;

	
	
	

	
	
	
	
		$fdata["strField"] = "PorcVotos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "PorcVotos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecmunicandi["PorcVotos"] = $fdata;


$tables_data["elecmunicandi"]=&$tdataelecmunicandi;
$field_labels["elecmunicandi"] = &$fieldLabelselecmunicandi;
$fieldToolTips["elecmunicandi"] = &$fieldToolTipselecmunicandi;
$placeHolders["elecmunicandi"] = &$placeHolderselecmunicandi;
$page_titles["elecmunicandi"] = &$pageTitleselecmunicandi;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["elecmunicandi"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["elecmunicandi"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 0;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["elecmunicandi"][0] = $masterParams;
				$masterTablesData["elecmunicandi"][0]["masterKeys"] = array();
	$masterTablesData["elecmunicandi"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["elecmunicandi"][0]["detailKeys"] = array();
	$masterTablesData["elecmunicandi"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
		
	
				$strOriginalDetailsTable="v_elec_muni";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm1_1_v_elec_muni";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm1_1_v_elec_muni";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 1;
	$masterParams["proceedLink"]= 0;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["elecmunicandi"][1] = $masterParams;
				$masterTablesData["elecmunicandi"][1]["masterKeys"] = array();
	$masterTablesData["elecmunicandi"][1]["masterKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["elecmunicandi"][1]["masterKeys"][]="NumeroMunicipio";
				$masterTablesData["elecmunicandi"][1]["detailKeys"] = array();
	$masterTablesData["elecmunicandi"][1]["detailKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["elecmunicandi"][1]["detailKeys"][]="Municipio";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_elecmunicandi()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "idElecMuniCandi,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Municipio,  Votos,  PorcVotos";
$proto3["m_strFrom"] = "FROM elecmunicandi";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "ORDER BY Votos DESC";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idElecMuniCandi",
	"m_strTable" => "elecmunicandi",
	"m_srcTableName" => "elecmunicandi"
));

$proto9["m_sql"] = "idElecMuniCandi";
$proto9["m_srcTableName"] = "elecmunicandi";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "elecmunicandi",
	"m_srcTableName" => "elecmunicandi"
));

$proto11["m_sql"] = "Convocatoria_idConvocatoria";
$proto11["m_srcTableName"] = "elecmunicandi";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "elecmunicandi",
	"m_srcTableName" => "elecmunicandi"
));

$proto13["m_sql"] = "Candidatura_idCandidatura";
$proto13["m_srcTableName"] = "elecmunicandi";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Municipio",
	"m_strTable" => "elecmunicandi",
	"m_srcTableName" => "elecmunicandi"
));

$proto15["m_sql"] = "Municipio";
$proto15["m_srcTableName"] = "elecmunicandi";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "elecmunicandi",
	"m_srcTableName" => "elecmunicandi"
));

$proto17["m_sql"] = "Votos";
$proto17["m_srcTableName"] = "elecmunicandi";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "PorcVotos",
	"m_strTable" => "elecmunicandi",
	"m_srcTableName" => "elecmunicandi"
));

$proto19["m_sql"] = "PorcVotos";
$proto19["m_srcTableName"] = "elecmunicandi";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto21=array();
$proto21["m_link"] = "SQLL_MAIN";
			$proto22=array();
$proto22["m_strName"] = "elecmunicandi";
$proto22["m_srcTableName"] = "elecmunicandi";
$proto22["m_columns"] = array();
$proto22["m_columns"][] = "idElecMuniCandi";
$proto22["m_columns"][] = "Convocatoria_idConvocatoria";
$proto22["m_columns"][] = "Candidatura_idCandidatura";
$proto22["m_columns"][] = "Municipio";
$proto22["m_columns"][] = "Votos";
$proto22["m_columns"][] = "PorcVotos";
$obj = new SQLTable($proto22);

$proto21["m_table"] = $obj;
$proto21["m_sql"] = "elecmunicandi";
$proto21["m_alias"] = "";
$proto21["m_srcTableName"] = "elecmunicandi";
$proto23=array();
$proto23["m_sql"] = "";
$proto23["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto23["m_column"]=$obj;
$proto23["m_contained"] = array();
$proto23["m_strCase"] = "";
$proto23["m_havingmode"] = false;
$proto23["m_inBrackets"] = false;
$proto23["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto23);

$proto21["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto21);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto25=array();
						$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "elecmunicandi",
	"m_srcTableName" => "elecmunicandi"
));

$proto25["m_column"]=$obj;
$proto25["m_bAsc"] = 0;
$proto25["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto25);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="elecmunicandi";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_elecmunicandi = createSqlQuery_elecmunicandi();


	
		;

						

$tdataelecmunicandi[".sqlquery"] = $queryData_elecmunicandi;

$tableEvents["elecmunicandi"] = new eventsBase;
$tdataelecmunicandi[".hasEvents"] = false;

?>