<?php
require_once(getabspath("classes/cipherer.php"));



$tdatacm3_mapas = array();
$tdatacm3_mapas[".ShortName"] = "cm3_mapas";

//	field labels
$fieldLabelscm3_mapas = array();
$pageTitlescm3_mapas = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm3_mapas["Spanish"] = array();
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm3_mapas[""] = array();
}

//	search fields
$tdatacm3_mapas[".searchFields"] = array();

// all search fields
$tdatacm3_mapas[".allSearchFields"] = array();

// good like search fields
$tdatacm3_mapas[".googleLikeFields"] = array();

$tdatacm3_mapas[".dashElements"] = array();

	$dbelement = array( "elementName" => "cm3_convocatoria_list", "table" => "cm3_convocatoria", "type" => 0);
	$dbelement["cellName"] = "cell_0_0";

			$dbelement["width"] = 400;
			$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;


	$tdatacm3_mapas[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "v_candidatura_mapa_list", "table" => "v_candidatura_mapa", "type" => 0);
	$dbelement["cellName"] = "cell_1_0";

			$dbelement["width"] = 400;
			$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "cm3_convocatoria";

	$tdatacm3_mapas[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm3_mapa_municipio_list", "table" => "cm3_mapa_municipio", "type" => 0);
	$dbelement["cellName"] = "cell_0_1";

			$dbelement["width"] = 800;
			$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "v_candidatura_mapa";

	$tdatacm3_mapas[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm3_mapa_distrito_madrid_list", "table" => "cm3_mapa_distrito_madrid", "type" => 0);
	$dbelement["cellName"] = "cell_2_1";

			$dbelement["width"] = 800;
			$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "v_candidatura_mapa";

	$tdatacm3_mapas[".dashElements"][] = $dbelement;

$tdatacm3_mapas[".shortTableName"] = "cm3_mapas";
$tdatacm3_mapas[".entityType"] = 4;



include_once(getabspath("include/cm3_mapas_events.php"));
$tableEvents["cm3_mapas"] = new eventclass_cm3_mapas;
$tdatacm3_mapas[".hasEvents"] = true;


$tdatacm3_mapas[".tableType"] = "dashboard";



$tdatacm3_mapas[".addPageEvents"] = false;

$tables_data["cm3_mapas"]=&$tdatacm3_mapas;
$field_labels["cm3_mapas"] = &$fieldLabelscm3_mapas;
$page_titles["cm3_mapas"] = &$pageTitlescm3_mapas;

?>