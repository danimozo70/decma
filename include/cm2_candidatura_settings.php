<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm2_candidatura = array();
	$tdatacm2_candidatura[".truncateText"] = true;
	$tdatacm2_candidatura[".NumberOfChars"] = 80;
	$tdatacm2_candidatura[".ShortName"] = "cm2_candidatura";
	$tdatacm2_candidatura[".OwnerID"] = "";
	$tdatacm2_candidatura[".OriginalTable"] = "candidatura";

//	field labels
$fieldLabelscm2_candidatura = array();
$fieldToolTipscm2_candidatura = array();
$pageTitlescm2_candidatura = array();
$placeHolderscm2_candidatura = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm2_candidatura["Spanish"] = array();
	$fieldToolTipscm2_candidatura["Spanish"] = array();
	$placeHolderscm2_candidatura["Spanish"] = array();
	$pageTitlescm2_candidatura["Spanish"] = array();
	$fieldLabelscm2_candidatura["Spanish"]["idCandidatura"] = "Id interno";
	$fieldToolTipscm2_candidatura["Spanish"]["idCandidatura"] = "";
	$placeHolderscm2_candidatura["Spanish"]["idCandidatura"] = "";
	$fieldLabelscm2_candidatura["Spanish"]["Codigo"] = "Siglas";
	$fieldToolTipscm2_candidatura["Spanish"]["Codigo"] = "";
	$placeHolderscm2_candidatura["Spanish"]["Codigo"] = "";
	$fieldLabelscm2_candidatura["Spanish"]["Color"] = "Color";
	$fieldToolTipscm2_candidatura["Spanish"]["Color"] = "";
	$placeHolderscm2_candidatura["Spanish"]["Color"] = "";
	$fieldLabelscm2_candidatura["Spanish"]["Votos"] = "Votos";
	$fieldToolTipscm2_candidatura["Spanish"]["Votos"] = "";
	$placeHolderscm2_candidatura["Spanish"]["Votos"] = "";
	$fieldLabelscm2_candidatura["Spanish"]["PorVotos"] = "Por Votos";
	$fieldToolTipscm2_candidatura["Spanish"]["PorVotos"] = "";
	$placeHolderscm2_candidatura["Spanish"]["PorVotos"] = "";
	$fieldLabelscm2_candidatura["Spanish"]["escanos"] = "Escanos";
	$fieldToolTipscm2_candidatura["Spanish"]["escanos"] = "";
	$placeHolderscm2_candidatura["Spanish"]["escanos"] = "";
	if (count($fieldToolTipscm2_candidatura["Spanish"]))
		$tdatacm2_candidatura[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm2_candidatura[""] = array();
	$fieldToolTipscm2_candidatura[""] = array();
	$placeHolderscm2_candidatura[""] = array();
	$pageTitlescm2_candidatura[""] = array();
	if (count($fieldToolTipscm2_candidatura[""]))
		$tdatacm2_candidatura[".isUseToolTips"] = true;
}


	$tdatacm2_candidatura[".NCSearch"] = true;



$tdatacm2_candidatura[".shortTableName"] = "cm2_candidatura";
$tdatacm2_candidatura[".nSecOptions"] = 0;
$tdatacm2_candidatura[".recsPerRowList"] = 1;
$tdatacm2_candidatura[".recsPerRowPrint"] = 1;
$tdatacm2_candidatura[".mainTableOwnerID"] = "";
$tdatacm2_candidatura[".moveNext"] = 1;
$tdatacm2_candidatura[".entityType"] = 1;

$tdatacm2_candidatura[".strOriginalTableName"] = "candidatura";

	



$tdatacm2_candidatura[".showAddInPopup"] = true;

$tdatacm2_candidatura[".showEditInPopup"] = true;

$tdatacm2_candidatura[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
					
;
$popupPagesLayoutNames["add"] = "add";
			;
$popupPagesLayoutNames["edit"] = "add";
			;
$popupPagesLayoutNames["view"] = "add";
$tdatacm2_candidatura[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm2_candidatura[".fieldsForRegister"] = array();

$tdatacm2_candidatura[".listAjax"] = false;

	$tdatacm2_candidatura[".audit"] = false;

	$tdatacm2_candidatura[".locking"] = false;



$tdatacm2_candidatura[".list"] = true;











$tdatacm2_candidatura[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm2_candidatura[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm2_candidatura[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm2_candidatura[".searchSaving"] = false;
//

$tdatacm2_candidatura[".showSearchPanel"] = true;
		$tdatacm2_candidatura[".flexibleSearch"] = true;

$tdatacm2_candidatura[".isUseAjaxSuggest"] = true;

$tdatacm2_candidatura[".rowHighlite"] = true;





$tdatacm2_candidatura[".ajaxCodeSnippetAdded"] = false;

$tdatacm2_candidatura[".buttonsAdded"] = false;

$tdatacm2_candidatura[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm2_candidatura[".isUseTimeForSearch"] = false;



$tdatacm2_candidatura[".badgeColor"] = "00C2C5";


$tdatacm2_candidatura[".allSearchFields"] = array();
$tdatacm2_candidatura[".filterFields"] = array();
$tdatacm2_candidatura[".requiredSearchFields"] = array();



$tdatacm2_candidatura[".googleLikeFields"] = array();
$tdatacm2_candidatura[".googleLikeFields"][] = "idCandidatura";
$tdatacm2_candidatura[".googleLikeFields"][] = "Codigo";
$tdatacm2_candidatura[".googleLikeFields"][] = "Color";
$tdatacm2_candidatura[".googleLikeFields"][] = "Votos";
$tdatacm2_candidatura[".googleLikeFields"][] = "PorVotos";
$tdatacm2_candidatura[".googleLikeFields"][] = "escanos";



$tdatacm2_candidatura[".tableType"] = "list";

$tdatacm2_candidatura[".printerPageOrientation"] = 0;
$tdatacm2_candidatura[".nPrinterPageScale"] = 100;

$tdatacm2_candidatura[".nPrinterSplitRecords"] = 40;

$tdatacm2_candidatura[".nPrinterPDFSplitRecords"] = 40;



$tdatacm2_candidatura[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatacm2_candidatura[".pageSize"] = 6;

$tdatacm2_candidatura[".warnLeavingPages"] = true;



$tstrOrderBy = "order by Escanos desc, Votos desc";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm2_candidatura[".strOrderBy"] = $tstrOrderBy;

$tdatacm2_candidatura[".orderindexes"] = array();
	$tdatacm2_candidatura[".orderindexes"][] = array(6, (0 ? "ASC" : "DESC"), "e.Escanos");

	$tdatacm2_candidatura[".orderindexes"][] = array(4, (0 ? "ASC" : "DESC"), "e.Votos");


$tdatacm2_candidatura[".sqlHead"] = "SELECT ca.`idCandidatura`,  concat(ca.`Codigo`,' - ',ca.`Titulo`) Codigo,  ca.`Color`,  Votos,  PorVotos,  escanos";
$tdatacm2_candidatura[".sqlFrom"] = "FROM  candidatura ca  join  ( SELECT  es.Candidatura_idCandidatura,  sum(es.Votos) Votos,  sum(es.PorcVotos) PorVotos,  sum(es.Escanos) Escanos  FROM escanos es  group by 1      ) e on (e.Candidatura_idCandidatura = ca.idCandidatura)";
$tdatacm2_candidatura[".sqlWhereExpr"] = "";
$tdatacm2_candidatura[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm2_candidatura[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm2_candidatura[".arrGroupsPerPage"] = $arrGPP;

$tdatacm2_candidatura[".highlightSearchResults"] = true;

$tableKeyscm2_candidatura = array();
$tableKeyscm2_candidatura[] = "idCandidatura";
$tdatacm2_candidatura[".Keys"] = $tableKeyscm2_candidatura;

$tdatacm2_candidatura[".listFields"] = array();
$tdatacm2_candidatura[".listFields"][] = "Codigo";
$tdatacm2_candidatura[".listFields"][] = "Color";

$tdatacm2_candidatura[".hideMobileList"] = array();


$tdatacm2_candidatura[".viewFields"] = array();

$tdatacm2_candidatura[".addFields"] = array();

$tdatacm2_candidatura[".masterListFields"] = array();

$tdatacm2_candidatura[".inlineAddFields"] = array();

$tdatacm2_candidatura[".editFields"] = array();

$tdatacm2_candidatura[".inlineEditFields"] = array();

$tdatacm2_candidatura[".updateSelectedFields"] = array();


$tdatacm2_candidatura[".exportFields"] = array();

$tdatacm2_candidatura[".importFields"] = array();

$tdatacm2_candidatura[".printFields"] = array();


//	idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idCandidatura";
	$fdata["GoodName"] = "idCandidatura";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm2_candidatura","idCandidatura");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ca.`idCandidatura`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_candidatura["idCandidatura"] = $fdata;
//	Codigo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Codigo";
	$fdata["GoodName"] = "Codigo";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("cm2_candidatura","Codigo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Codigo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "concat(ca.`Codigo`,' - ',ca.`Titulo`)";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_candidatura["Codigo"] = $fdata;
//	Color
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Color";
	$fdata["GoodName"] = "Color";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm2_candidatura","Color");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Color";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ca.`Color`";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "ColorPicker");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_candidatura["Color"] = $fdata;
//	Votos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Votos";
	$fdata["GoodName"] = "Votos";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("cm2_candidatura","Votos");
	$fdata["FieldType"] = 14;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Votos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Votos";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_candidatura["Votos"] = $fdata;
//	PorVotos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "PorVotos";
	$fdata["GoodName"] = "PorVotos";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("cm2_candidatura","PorVotos");
	$fdata["FieldType"] = 14;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "PorVotos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "PorVotos";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_candidatura["PorVotos"] = $fdata;
//	escanos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "escanos";
	$fdata["GoodName"] = "escanos";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("cm2_candidatura","escanos");
	$fdata["FieldType"] = 14;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "escanos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "escanos";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_candidatura["escanos"] = $fdata;


$tables_data["cm2_candidatura"]=&$tdatacm2_candidatura;
$field_labels["cm2_candidatura"] = &$fieldLabelscm2_candidatura;
$fieldToolTips["cm2_candidatura"] = &$fieldToolTipscm2_candidatura;
$placeHolders["cm2_candidatura"] = &$placeHolderscm2_candidatura;
$page_titles["cm2_candidatura"] = &$pageTitlescm2_candidatura;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm2_candidatura"] = array();
//	cm2_escanos_chart
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm2_escanos_chart";
		$detailsParam["dOriginalTable"] = "escanos";
		$detailsParam["proceedLink"] = true;
			$detailsParam["dType"]=PAGE_CHART;
		$detailsParam["dShortTable"] = "cm2_escanos_chart";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm2_escanos_chart");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "1";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm2_candidatura"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm2_candidatura"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm2_candidatura"][$dIndex]["masterKeys"][]="idCandidatura";

				$detailsTablesData["cm2_candidatura"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm2_candidatura"][$dIndex]["detailKeys"][]="Candidatura_idCandidatura";
//	cm2_escanos2 Chart
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm2_escanos2 Chart";
		$detailsParam["dOriginalTable"] = "escanos";
		$detailsParam["proceedLink"] = true;
			$detailsParam["dType"]=PAGE_CHART;
		$detailsParam["dShortTable"] = "cm2_escanos2_Chart";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm2_escanos2_Chart");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm2_candidatura"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm2_candidatura"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm2_candidatura"][$dIndex]["masterKeys"][]="idCandidatura";

				$detailsTablesData["cm2_candidatura"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm2_candidatura"][$dIndex]["detailKeys"][]="Candidatura_idCandidatura";
//	cm2_municipio
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm2_municipio";
		$detailsParam["dOriginalTable"] = "municipio";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm2_municipio";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm2_municipio");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm2_candidatura"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm2_candidatura"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm2_candidatura"][$dIndex]["masterKeys"][]="idCandidatura";

				$detailsTablesData["cm2_candidatura"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm2_candidatura"][$dIndex]["detailKeys"][]="idCandidatura";
//	cm2_distrito
	
	

		$dIndex = 3;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm2_distrito";
		$detailsParam["dOriginalTable"] = "distrito";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm2_distrito";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm2_distrito");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm2_candidatura"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm2_candidatura"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm2_candidatura"][$dIndex]["masterKeys"][]="idCandidatura";

				$detailsTablesData["cm2_candidatura"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm2_candidatura"][$dIndex]["detailKeys"][]="idCandidatura";
//	electos
	
	

		$dIndex = 4;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="electos";
		$detailsParam["dOriginalTable"] = "electos";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "electos";
	$detailsParam["dCaptionTable"] = GetTableCaption("electos");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm2_candidatura"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm2_candidatura"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm2_candidatura"][$dIndex]["masterKeys"][]="idCandidatura";

				$detailsTablesData["cm2_candidatura"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm2_candidatura"][$dIndex]["detailKeys"][]="Candidatura_idCandidatura";

// tables which are master tables for current table (detail)
$masterTablesData["cm2_candidatura"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm2_candidatura()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "ca.`idCandidatura`,  concat(ca.`Codigo`,' - ',ca.`Titulo`) Codigo,  ca.`Color`,  Votos,  PorVotos,  escanos";
$proto3["m_strFrom"] = "FROM  candidatura ca  join  ( SELECT  es.Candidatura_idCandidatura,  sum(es.Votos) Votos,  sum(es.PorcVotos) PorVotos,  sum(es.Escanos) Escanos  FROM escanos es  group by 1      ) e on (e.Candidatura_idCandidatura = ca.idCandidatura)";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "order by Escanos desc, Votos desc";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idCandidatura",
	"m_strTable" => "ca",
	"m_srcTableName" => "cm2_candidatura"
));

$proto9["m_sql"] = "ca.`idCandidatura`";
$proto9["m_srcTableName"] = "cm2_candidatura";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$proto12=array();
$proto12["m_functiontype"] = "SQLF_CUSTOM";
$proto12["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "ca.`Codigo`"
));

$proto12["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "' - '"
));

$proto12["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "ca.`Titulo`"
));

$proto12["m_arguments"][]=$obj;
$proto12["m_strFunctionName"] = "concat";
$obj = new SQLFunctionCall($proto12);

$proto11["m_sql"] = "concat(ca.`Codigo`,' - ',ca.`Titulo`)";
$proto11["m_srcTableName"] = "cm2_candidatura";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "Codigo";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Color",
	"m_strTable" => "ca",
	"m_srcTableName" => "cm2_candidatura"
));

$proto16["m_sql"] = "ca.`Color`";
$proto16["m_srcTableName"] = "cm2_candidatura";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto3["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "e",
	"m_srcTableName" => "cm2_candidatura"
));

$proto18["m_sql"] = "Votos";
$proto18["m_srcTableName"] = "cm2_candidatura";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto3["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "PorVotos",
	"m_strTable" => "e",
	"m_srcTableName" => "cm2_candidatura"
));

$proto20["m_sql"] = "PorVotos";
$proto20["m_srcTableName"] = "cm2_candidatura";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto3["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "e",
	"m_srcTableName" => "cm2_candidatura"
));

$proto22["m_sql"] = "escanos";
$proto22["m_srcTableName"] = "cm2_candidatura";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto24=array();
$proto24["m_link"] = "SQLL_MAIN";
			$proto25=array();
$proto25["m_strName"] = "candidatura";
$proto25["m_srcTableName"] = "cm2_candidatura";
$proto25["m_columns"] = array();
$proto25["m_columns"][] = "idCandidatura";
$proto25["m_columns"][] = "Codigo";
$proto25["m_columns"][] = "Titulo";
$proto25["m_columns"][] = "Descripcion";
$proto25["m_columns"][] = "Color";
$proto25["m_columns"][] = "Logo";
$obj = new SQLTable($proto25);

$proto24["m_table"] = $obj;
$proto24["m_sql"] = "candidatura ca";
$proto24["m_alias"] = "ca";
$proto24["m_srcTableName"] = "cm2_candidatura";
$proto26=array();
$proto26["m_sql"] = "";
$proto26["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto26["m_column"]=$obj;
$proto26["m_contained"] = array();
$proto26["m_strCase"] = "";
$proto26["m_havingmode"] = false;
$proto26["m_inBrackets"] = false;
$proto26["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto26);

$proto24["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto24);

$proto3["m_fromlist"][]=$obj;
												$proto28=array();
$proto28["m_link"] = "SQLL_INNERJOIN";
			$proto29=array();
$proto29["m_strHead"] = " SELECT";
$proto29["m_strFieldList"] = "es.Candidatura_idCandidatura,  sum(es.Votos) Votos,  sum(es.PorcVotos) PorVotos,  sum(es.Escanos) Escanos";
$proto29["m_strFrom"] = "FROM escanos es";
$proto29["m_strWhere"] = "";
$proto29["m_strOrderBy"] = "";
	
		;
			$proto29["cipherer"] = null;
$proto31=array();
$proto31["m_sql"] = "";
$proto31["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto31["m_column"]=$obj;
$proto31["m_contained"] = array();
$proto31["m_strCase"] = "";
$proto31["m_havingmode"] = false;
$proto31["m_inBrackets"] = false;
$proto31["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto31);

$proto29["m_where"] = $obj;
$proto33=array();
$proto33["m_sql"] = "";
$proto33["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto33["m_column"]=$obj;
$proto33["m_contained"] = array();
$proto33["m_strCase"] = "";
$proto33["m_havingmode"] = false;
$proto33["m_inBrackets"] = false;
$proto33["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto33);

$proto29["m_having"] = $obj;
$proto29["m_fieldlist"] = array();
						$proto35=array();
			$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "es",
	"m_srcTableName" => "cm2_candidatura"
));

$proto35["m_sql"] = "es.Candidatura_idCandidatura";
$proto35["m_srcTableName"] = "cm2_candidatura";
$proto35["m_expr"]=$obj;
$proto35["m_alias"] = "";
$obj = new SQLFieldListItem($proto35);

$proto29["m_fieldlist"][]=$obj;
						$proto37=array();
			$proto38=array();
$proto38["m_functiontype"] = "SQLF_SUM";
$proto38["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "es",
	"m_srcTableName" => "cm2_candidatura"
));

$proto38["m_arguments"][]=$obj;
$proto38["m_strFunctionName"] = "sum";
$obj = new SQLFunctionCall($proto38);

$proto37["m_sql"] = "sum(es.Votos)";
$proto37["m_srcTableName"] = "cm2_candidatura";
$proto37["m_expr"]=$obj;
$proto37["m_alias"] = "Votos";
$obj = new SQLFieldListItem($proto37);

$proto29["m_fieldlist"][]=$obj;
						$proto40=array();
			$proto41=array();
$proto41["m_functiontype"] = "SQLF_SUM";
$proto41["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "PorcVotos",
	"m_strTable" => "es",
	"m_srcTableName" => "cm2_candidatura"
));

$proto41["m_arguments"][]=$obj;
$proto41["m_strFunctionName"] = "sum";
$obj = new SQLFunctionCall($proto41);

$proto40["m_sql"] = "sum(es.PorcVotos)";
$proto40["m_srcTableName"] = "cm2_candidatura";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "PorVotos";
$obj = new SQLFieldListItem($proto40);

$proto29["m_fieldlist"][]=$obj;
						$proto43=array();
			$proto44=array();
$proto44["m_functiontype"] = "SQLF_SUM";
$proto44["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "es",
	"m_srcTableName" => "cm2_candidatura"
));

$proto44["m_arguments"][]=$obj;
$proto44["m_strFunctionName"] = "sum";
$obj = new SQLFunctionCall($proto44);

$proto43["m_sql"] = "sum(es.Escanos)";
$proto43["m_srcTableName"] = "cm2_candidatura";
$proto43["m_expr"]=$obj;
$proto43["m_alias"] = "Escanos";
$obj = new SQLFieldListItem($proto43);

$proto29["m_fieldlist"][]=$obj;
$proto29["m_fromlist"] = array();
												$proto46=array();
$proto46["m_link"] = "SQLL_MAIN";
			$proto47=array();
$proto47["m_strName"] = "escanos";
$proto47["m_srcTableName"] = "cm2_candidatura";
$proto47["m_columns"] = array();
$proto47["m_columns"][] = "idEscanos";
$proto47["m_columns"][] = "Convocatoria_idConvocatoria";
$proto47["m_columns"][] = "Candidatura_idCandidatura";
$proto47["m_columns"][] = "Votos";
$proto47["m_columns"][] = "PorcVotos";
$proto47["m_columns"][] = "Escanos";
$obj = new SQLTable($proto47);

$proto46["m_table"] = $obj;
$proto46["m_sql"] = "escanos es";
$proto46["m_alias"] = "es";
$proto46["m_srcTableName"] = "cm2_candidatura";
$proto48=array();
$proto48["m_sql"] = "";
$proto48["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto48["m_column"]=$obj;
$proto48["m_contained"] = array();
$proto48["m_strCase"] = "";
$proto48["m_havingmode"] = false;
$proto48["m_inBrackets"] = false;
$proto48["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto48);

$proto46["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto46);

$proto29["m_fromlist"][]=$obj;
$proto29["m_groupby"] = array();
												$proto50=array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "1"
));

$proto50["m_column"]=$obj;
$obj = new SQLGroupByItem($proto50);

$proto29["m_groupby"][]=$obj;
$proto29["m_orderby"] = array();
$proto29["m_srcTableName"]="cm2_candidatura";		
$obj = new SQLQuery($proto29);

$proto28["m_table"] = $obj;
$proto28["m_sql"] = "join  ( SELECT  es.Candidatura_idCandidatura,  sum(es.Votos) Votos,  sum(es.PorcVotos) PorVotos,  sum(es.Escanos) Escanos  FROM escanos es  group by 1      ) e on (e.Candidatura_idCandidatura = ca.idCandidatura)";
$proto28["m_alias"] = "e";
$proto28["m_srcTableName"] = "cm2_candidatura";
$proto52=array();
$proto52["m_sql"] = "e.Candidatura_idCandidatura = ca.idCandidatura";
$proto52["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "e",
	"m_srcTableName" => "cm2_candidatura"
));

$proto52["m_column"]=$obj;
$proto52["m_contained"] = array();
$proto52["m_strCase"] = "= ca.idCandidatura";
$proto52["m_havingmode"] = false;
$proto52["m_inBrackets"] = true;
$proto52["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto52);

$proto28["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto28);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto54=array();
						$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "e",
	"m_srcTableName" => "cm2_candidatura"
));

$proto54["m_column"]=$obj;
$proto54["m_bAsc"] = 0;
$proto54["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto54);

$proto3["m_orderby"][]=$obj;					
												$proto56=array();
						$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "e",
	"m_srcTableName" => "cm2_candidatura"
));

$proto56["m_column"]=$obj;
$proto56["m_bAsc"] = 0;
$proto56["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto56);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="cm2_candidatura";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_cm2_candidatura = createSqlQuery_cm2_candidatura();


	
		;

						

$tdatacm2_candidatura[".sqlquery"] = $queryData_cm2_candidatura;

$tableEvents["cm2_candidatura"] = new eventsBase;
$tdatacm2_candidatura[".hasEvents"] = false;

?>