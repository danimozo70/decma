<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm3_convocatoria = array();
	$tdatacm3_convocatoria[".truncateText"] = true;
	$tdatacm3_convocatoria[".NumberOfChars"] = 80;
	$tdatacm3_convocatoria[".ShortName"] = "cm3_convocatoria";
	$tdatacm3_convocatoria[".OwnerID"] = "";
	$tdatacm3_convocatoria[".OriginalTable"] = "convocatoria";

//	field labels
$fieldLabelscm3_convocatoria = array();
$fieldToolTipscm3_convocatoria = array();
$pageTitlescm3_convocatoria = array();
$placeHolderscm3_convocatoria = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm3_convocatoria["Spanish"] = array();
	$fieldToolTipscm3_convocatoria["Spanish"] = array();
	$placeHolderscm3_convocatoria["Spanish"] = array();
	$pageTitlescm3_convocatoria["Spanish"] = array();
	$fieldLabelscm3_convocatoria["Spanish"]["idConvocatoria"] = "Id Interno";
	$fieldToolTipscm3_convocatoria["Spanish"]["idConvocatoria"] = "";
	$placeHolderscm3_convocatoria["Spanish"]["idConvocatoria"] = "";
	$fieldLabelscm3_convocatoria["Spanish"]["Orden"] = "Orden";
	$fieldToolTipscm3_convocatoria["Spanish"]["Orden"] = "";
	$placeHolderscm3_convocatoria["Spanish"]["Orden"] = "";
	$fieldLabelscm3_convocatoria["Spanish"]["EsAsamblea"] = "Es Asamblea?";
	$fieldToolTipscm3_convocatoria["Spanish"]["EsAsamblea"] = "";
	$placeHolderscm3_convocatoria["Spanish"]["EsAsamblea"] = "";
	$fieldLabelscm3_convocatoria["Spanish"]["Titulo"] = "Título";
	$fieldToolTipscm3_convocatoria["Spanish"]["Titulo"] = "";
	$placeHolderscm3_convocatoria["Spanish"]["Titulo"] = "";
	$fieldLabelscm3_convocatoria["Spanish"]["Descripcion"] = "Descripción";
	$fieldToolTipscm3_convocatoria["Spanish"]["Descripcion"] = "";
	$placeHolderscm3_convocatoria["Spanish"]["Descripcion"] = "";
	if (count($fieldToolTipscm3_convocatoria["Spanish"]))
		$tdatacm3_convocatoria[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm3_convocatoria[""] = array();
	$fieldToolTipscm3_convocatoria[""] = array();
	$placeHolderscm3_convocatoria[""] = array();
	$pageTitlescm3_convocatoria[""] = array();
	if (count($fieldToolTipscm3_convocatoria[""]))
		$tdatacm3_convocatoria[".isUseToolTips"] = true;
}


	$tdatacm3_convocatoria[".NCSearch"] = true;



$tdatacm3_convocatoria[".shortTableName"] = "cm3_convocatoria";
$tdatacm3_convocatoria[".nSecOptions"] = 0;
$tdatacm3_convocatoria[".recsPerRowList"] = 1;
$tdatacm3_convocatoria[".recsPerRowPrint"] = 1;
$tdatacm3_convocatoria[".mainTableOwnerID"] = "";
$tdatacm3_convocatoria[".moveNext"] = 0;
$tdatacm3_convocatoria[".entityType"] = 1;

$tdatacm3_convocatoria[".strOriginalTableName"] = "convocatoria";

	



$tdatacm3_convocatoria[".showAddInPopup"] = true;

$tdatacm3_convocatoria[".showEditInPopup"] = true;

$tdatacm3_convocatoria[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["add"] = "view_bootstrap";
			;
$popupPagesLayoutNames["edit"] = "view_bootstrap";
			;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdatacm3_convocatoria[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm3_convocatoria[".fieldsForRegister"] = array();

$tdatacm3_convocatoria[".listAjax"] = false;

	$tdatacm3_convocatoria[".audit"] = false;

	$tdatacm3_convocatoria[".locking"] = false;



$tdatacm3_convocatoria[".list"] = true;











$tdatacm3_convocatoria[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm3_convocatoria[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm3_convocatoria[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm3_convocatoria[".searchSaving"] = false;
//

	$tdatacm3_convocatoria[".showSearchPanel"] = false;

$tdatacm3_convocatoria[".isUseAjaxSuggest"] = true;

$tdatacm3_convocatoria[".rowHighlite"] = true;





$tdatacm3_convocatoria[".ajaxCodeSnippetAdded"] = false;

$tdatacm3_convocatoria[".buttonsAdded"] = false;

$tdatacm3_convocatoria[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm3_convocatoria[".isUseTimeForSearch"] = false;



$tdatacm3_convocatoria[".badgeColor"] = "6493EA";

$tdatacm3_convocatoria[".detailsLinksOnList"] = "1";

$tdatacm3_convocatoria[".allSearchFields"] = array();
$tdatacm3_convocatoria[".filterFields"] = array();
$tdatacm3_convocatoria[".requiredSearchFields"] = array();






$tdatacm3_convocatoria[".tableType"] = "list";

$tdatacm3_convocatoria[".printerPageOrientation"] = 0;
$tdatacm3_convocatoria[".nPrinterPageScale"] = 100;

$tdatacm3_convocatoria[".nPrinterSplitRecords"] = 40;

$tdatacm3_convocatoria[".nPrinterPDFSplitRecords"] = 40;



$tdatacm3_convocatoria[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatacm3_convocatoria[".pageSize"] = 2;

$tdatacm3_convocatoria[".warnLeavingPages"] = true;



$tstrOrderBy = "order by Orden";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm3_convocatoria[".strOrderBy"] = $tstrOrderBy;

$tdatacm3_convocatoria[".orderindexes"] = array();
	$tdatacm3_convocatoria[".orderindexes"][] = array(2, (1 ? "ASC" : "DESC"), "convocatoria.Orden");


$tdatacm3_convocatoria[".sqlHead"] = "SELECT idConvocatoria,  	Orden,  	EsAsamblea,  	Titulo,  	Descripcion";
$tdatacm3_convocatoria[".sqlFrom"] = "FROM convocatoria  join (  SELECT  distinct elem.`Convocatoria_idConvocatoria` FROM elecmunicandi elem) ele   on (idConvocatoria = ele.`Convocatoria_idConvocatoria` )";
$tdatacm3_convocatoria[".sqlWhereExpr"] = "";
$tdatacm3_convocatoria[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm3_convocatoria[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm3_convocatoria[".arrGroupsPerPage"] = $arrGPP;

$tdatacm3_convocatoria[".highlightSearchResults"] = true;

$tableKeyscm3_convocatoria = array();
$tableKeyscm3_convocatoria[] = "idConvocatoria";
$tdatacm3_convocatoria[".Keys"] = $tableKeyscm3_convocatoria;

$tdatacm3_convocatoria[".listFields"] = array();
$tdatacm3_convocatoria[".listFields"][] = "Titulo";
$tdatacm3_convocatoria[".listFields"][] = "Descripcion";

$tdatacm3_convocatoria[".hideMobileList"] = array();


$tdatacm3_convocatoria[".viewFields"] = array();

$tdatacm3_convocatoria[".addFields"] = array();

$tdatacm3_convocatoria[".masterListFields"] = array();
$tdatacm3_convocatoria[".masterListFields"][] = "idConvocatoria";
$tdatacm3_convocatoria[".masterListFields"][] = "Orden";
$tdatacm3_convocatoria[".masterListFields"][] = "EsAsamblea";
$tdatacm3_convocatoria[".masterListFields"][] = "Titulo";
$tdatacm3_convocatoria[".masterListFields"][] = "Descripcion";

$tdatacm3_convocatoria[".inlineAddFields"] = array();

$tdatacm3_convocatoria[".editFields"] = array();

$tdatacm3_convocatoria[".inlineEditFields"] = array();

$tdatacm3_convocatoria[".updateSelectedFields"] = array();


$tdatacm3_convocatoria[".exportFields"] = array();

$tdatacm3_convocatoria[".importFields"] = array();

$tdatacm3_convocatoria[".printFields"] = array();


//	idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idConvocatoria";
	$fdata["GoodName"] = "idConvocatoria";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm3_convocatoria","idConvocatoria");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idConvocatoria";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_convocatoria["idConvocatoria"] = $fdata;
//	Orden
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Orden";
	$fdata["GoodName"] = "Orden";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm3_convocatoria","Orden");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Orden";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Orden";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_convocatoria["Orden"] = $fdata;
//	EsAsamblea
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "EsAsamblea";
	$fdata["GoodName"] = "EsAsamblea";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm3_convocatoria","EsAsamblea");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "EsAsamblea";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "EsAsamblea";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_convocatoria["EsAsamblea"] = $fdata;
//	Titulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Titulo";
	$fdata["GoodName"] = "Titulo";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm3_convocatoria","Titulo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Titulo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=30";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_convocatoria["Titulo"] = $fdata;
//	Descripcion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Descripcion";
	$fdata["GoodName"] = "Descripcion";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm3_convocatoria","Descripcion");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Descripcion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Descripcion";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=500";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_convocatoria["Descripcion"] = $fdata;


$tables_data["cm3_convocatoria"]=&$tdatacm3_convocatoria;
$field_labels["cm3_convocatoria"] = &$fieldLabelscm3_convocatoria;
$fieldToolTips["cm3_convocatoria"] = &$fieldToolTipscm3_convocatoria;
$placeHolders["cm3_convocatoria"] = &$placeHolderscm3_convocatoria;
$page_titles["cm3_convocatoria"] = &$pageTitlescm3_convocatoria;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm3_convocatoria"] = array();
//	v_candidatura_mapa
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="v_candidatura_mapa";
		$detailsParam["dOriginalTable"] = "v_candidatura_mapa";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "v_candidatura_mapa";
	$detailsParam["dCaptionTable"] = GetTableCaption("v_candidatura_mapa");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "2";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm3_convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm3_convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm3_convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["cm3_convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm3_convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";

// tables which are master tables for current table (detail)
$masterTablesData["cm3_convocatoria"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm3_convocatoria()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "idConvocatoria,  	Orden,  	EsAsamblea,  	Titulo,  	Descripcion";
$proto3["m_strFrom"] = "FROM convocatoria  join (  SELECT  distinct elem.`Convocatoria_idConvocatoria` FROM elecmunicandi elem) ele   on (idConvocatoria = ele.`Convocatoria_idConvocatoria` )";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "order by Orden";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idConvocatoria",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm3_convocatoria"
));

$proto9["m_sql"] = "idConvocatoria";
$proto9["m_srcTableName"] = "cm3_convocatoria";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm3_convocatoria"
));

$proto11["m_sql"] = "Orden";
$proto11["m_srcTableName"] = "cm3_convocatoria";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "EsAsamblea",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm3_convocatoria"
));

$proto13["m_sql"] = "EsAsamblea";
$proto13["m_srcTableName"] = "cm3_convocatoria";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm3_convocatoria"
));

$proto15["m_sql"] = "Titulo";
$proto15["m_srcTableName"] = "cm3_convocatoria";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "Descripcion",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm3_convocatoria"
));

$proto17["m_sql"] = "Descripcion";
$proto17["m_srcTableName"] = "cm3_convocatoria";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto19=array();
$proto19["m_link"] = "SQLL_MAIN";
			$proto20=array();
$proto20["m_strName"] = "convocatoria";
$proto20["m_srcTableName"] = "cm3_convocatoria";
$proto20["m_columns"] = array();
$proto20["m_columns"][] = "idConvocatoria";
$proto20["m_columns"][] = "Orden";
$proto20["m_columns"][] = "EsAsamblea";
$proto20["m_columns"][] = "Titulo";
$proto20["m_columns"][] = "Descripcion";
$obj = new SQLTable($proto20);

$proto19["m_table"] = $obj;
$proto19["m_sql"] = "convocatoria";
$proto19["m_alias"] = "";
$proto19["m_srcTableName"] = "cm3_convocatoria";
$proto21=array();
$proto21["m_sql"] = "";
$proto21["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto21["m_column"]=$obj;
$proto21["m_contained"] = array();
$proto21["m_strCase"] = "";
$proto21["m_havingmode"] = false;
$proto21["m_inBrackets"] = false;
$proto21["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto21);

$proto19["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto19);

$proto3["m_fromlist"][]=$obj;
												$proto23=array();
$proto23["m_link"] = "SQLL_INNERJOIN";
			$proto24=array();
$proto24["m_strHead"] = "  SELECT  distinct";
$proto24["m_strFieldList"] = "elem.`Convocatoria_idConvocatoria`";
$proto24["m_strFrom"] = "FROM elecmunicandi elem";
$proto24["m_strWhere"] = "";
$proto24["m_strOrderBy"] = "";
	
		;
			$proto24["cipherer"] = null;
$proto26=array();
$proto26["m_sql"] = "";
$proto26["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto26["m_column"]=$obj;
$proto26["m_contained"] = array();
$proto26["m_strCase"] = "";
$proto26["m_havingmode"] = false;
$proto26["m_inBrackets"] = false;
$proto26["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto26);

$proto24["m_where"] = $obj;
$proto28=array();
$proto28["m_sql"] = "";
$proto28["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto28["m_column"]=$obj;
$proto28["m_contained"] = array();
$proto28["m_strCase"] = "";
$proto28["m_havingmode"] = false;
$proto28["m_inBrackets"] = false;
$proto28["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto28);

$proto24["m_having"] = $obj;
$proto24["m_fieldlist"] = array();
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "elem",
	"m_srcTableName" => "cm3_convocatoria"
));

$proto30["m_sql"] = "elem.`Convocatoria_idConvocatoria`";
$proto30["m_srcTableName"] = "cm3_convocatoria";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto24["m_fieldlist"][]=$obj;
$proto24["m_fromlist"] = array();
												$proto32=array();
$proto32["m_link"] = "SQLL_MAIN";
			$proto33=array();
$proto33["m_strName"] = "elecmunicandi";
$proto33["m_srcTableName"] = "cm3_convocatoria";
$proto33["m_columns"] = array();
$proto33["m_columns"][] = "idElecMuniCandi";
$proto33["m_columns"][] = "Convocatoria_idConvocatoria";
$proto33["m_columns"][] = "Candidatura_idCandidatura";
$proto33["m_columns"][] = "Municipio";
$proto33["m_columns"][] = "Votos";
$proto33["m_columns"][] = "PorcVotos";
$obj = new SQLTable($proto33);

$proto32["m_table"] = $obj;
$proto32["m_sql"] = "elecmunicandi elem";
$proto32["m_alias"] = "elem";
$proto32["m_srcTableName"] = "cm3_convocatoria";
$proto34=array();
$proto34["m_sql"] = "";
$proto34["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto34["m_column"]=$obj;
$proto34["m_contained"] = array();
$proto34["m_strCase"] = "";
$proto34["m_havingmode"] = false;
$proto34["m_inBrackets"] = false;
$proto34["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto34);

$proto32["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto32);

$proto24["m_fromlist"][]=$obj;
$proto24["m_groupby"] = array();
$proto24["m_orderby"] = array();
$proto24["m_srcTableName"]="cm3_convocatoria";		
$obj = new SQLQuery($proto24);

$proto23["m_table"] = $obj;
$proto23["m_sql"] = "join (  SELECT  distinct elem.`Convocatoria_idConvocatoria` FROM elecmunicandi elem) ele   on (idConvocatoria = ele.`Convocatoria_idConvocatoria` )";
$proto23["m_alias"] = "ele";
$proto23["m_srcTableName"] = "cm3_convocatoria";
$proto36=array();
$proto36["m_sql"] = "idConvocatoria = ele.`Convocatoria_idConvocatoria` ";
$proto36["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "idConvocatoria",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm3_convocatoria"
));

$proto36["m_column"]=$obj;
$proto36["m_contained"] = array();
$proto36["m_strCase"] = "= ele.`Convocatoria_idConvocatoria`";
$proto36["m_havingmode"] = false;
$proto36["m_inBrackets"] = true;
$proto36["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto36);

$proto23["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto23);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto38=array();
						$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm3_convocatoria"
));

$proto38["m_column"]=$obj;
$proto38["m_bAsc"] = 1;
$proto38["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto38);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="cm3_convocatoria";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_cm3_convocatoria = createSqlQuery_cm3_convocatoria();


	
		;

					

$tdatacm3_convocatoria[".sqlquery"] = $queryData_cm3_convocatoria;

$tableEvents["cm3_convocatoria"] = new eventsBase;
$tdatacm3_convocatoria[".hasEvents"] = false;

?>