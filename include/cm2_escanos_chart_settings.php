<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm2_escanos_chart = array();
	$tdatacm2_escanos_chart[".ShortName"] = "cm2_escanos_chart";
	$tdatacm2_escanos_chart[".OwnerID"] = "";
	$tdatacm2_escanos_chart[".OriginalTable"] = "escanos";

//	field labels
$fieldLabelscm2_escanos_chart = array();
$fieldToolTipscm2_escanos_chart = array();
$pageTitlescm2_escanos_chart = array();
$placeHolderscm2_escanos_chart = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm2_escanos_chart["Spanish"] = array();
	$fieldToolTipscm2_escanos_chart["Spanish"] = array();
	$placeHolderscm2_escanos_chart["Spanish"] = array();
	$pageTitlescm2_escanos_chart["Spanish"] = array();
	$fieldLabelscm2_escanos_chart["Spanish"]["idEscanos"] = "Id Escanos";
	$fieldToolTipscm2_escanos_chart["Spanish"]["idEscanos"] = "";
	$placeHolderscm2_escanos_chart["Spanish"]["idEscanos"] = "";
	$fieldLabelscm2_escanos_chart["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria IdConvocatoria";
	$fieldToolTipscm2_escanos_chart["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderscm2_escanos_chart["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelscm2_escanos_chart["Spanish"]["Candidatura_idCandidatura"] = "Candidatura IdCandidatura";
	$fieldToolTipscm2_escanos_chart["Spanish"]["Candidatura_idCandidatura"] = "";
	$placeHolderscm2_escanos_chart["Spanish"]["Candidatura_idCandidatura"] = "";
	$fieldLabelscm2_escanos_chart["Spanish"]["Votos"] = "Votos";
	$fieldToolTipscm2_escanos_chart["Spanish"]["Votos"] = "";
	$placeHolderscm2_escanos_chart["Spanish"]["Votos"] = "";
	$fieldLabelscm2_escanos_chart["Spanish"]["PorcVotos"] = "Porc Votos";
	$fieldToolTipscm2_escanos_chart["Spanish"]["PorcVotos"] = "";
	$placeHolderscm2_escanos_chart["Spanish"]["PorcVotos"] = "";
	$fieldLabelscm2_escanos_chart["Spanish"]["Escanos"] = "";
	$fieldToolTipscm2_escanos_chart["Spanish"]["Escanos"] = "";
	$placeHolderscm2_escanos_chart["Spanish"]["Escanos"] = "";
	$pageTitlescm2_escanos_chart["Spanish"]["chart"] = "Escaños Totales";
	if (count($fieldToolTipscm2_escanos_chart["Spanish"]))
		$tdatacm2_escanos_chart[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm2_escanos_chart[""] = array();
	$fieldToolTipscm2_escanos_chart[""] = array();
	$placeHolderscm2_escanos_chart[""] = array();
	$pageTitlescm2_escanos_chart[""] = array();
	if (count($fieldToolTipscm2_escanos_chart[""]))
		$tdatacm2_escanos_chart[".isUseToolTips"] = true;
}


	$tdatacm2_escanos_chart[".NCSearch"] = true;

	$tdatacm2_escanos_chart[".ChartRefreshTime"] = 0;


$tdatacm2_escanos_chart[".shortTableName"] = "cm2_escanos_chart";
$tdatacm2_escanos_chart[".nSecOptions"] = 0;
$tdatacm2_escanos_chart[".recsPerRowPrint"] = 1;
$tdatacm2_escanos_chart[".mainTableOwnerID"] = "";
$tdatacm2_escanos_chart[".moveNext"] = 1;
$tdatacm2_escanos_chart[".entityType"] = 3;

$tdatacm2_escanos_chart[".strOriginalTableName"] = "escanos";

	



$tdatacm2_escanos_chart[".showAddInPopup"] = false;

$tdatacm2_escanos_chart[".showEditInPopup"] = false;

$tdatacm2_escanos_chart[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacm2_escanos_chart[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm2_escanos_chart[".fieldsForRegister"] = array();

$tdatacm2_escanos_chart[".listAjax"] = false;

	$tdatacm2_escanos_chart[".audit"] = false;

	$tdatacm2_escanos_chart[".locking"] = false;

$tdatacm2_escanos_chart[".edit"] = true;
$tdatacm2_escanos_chart[".afterEditAction"] = 1;
$tdatacm2_escanos_chart[".closePopupAfterEdit"] = 1;
$tdatacm2_escanos_chart[".afterEditActionDetTable"] = "";

$tdatacm2_escanos_chart[".add"] = true;
$tdatacm2_escanos_chart[".afterAddAction"] = 1;
$tdatacm2_escanos_chart[".closePopupAfterAdd"] = 1;
$tdatacm2_escanos_chart[".afterAddActionDetTable"] = "";

$tdatacm2_escanos_chart[".list"] = true;



$tdatacm2_escanos_chart[".reorderRecordsByHeader"] = true;



$tdatacm2_escanos_chart[".view"] = true;




$tdatacm2_escanos_chart[".delete"] = true;

$tdatacm2_escanos_chart[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm2_escanos_chart[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm2_escanos_chart[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm2_escanos_chart[".searchSaving"] = false;
//

$tdatacm2_escanos_chart[".showSearchPanel"] = true;
		$tdatacm2_escanos_chart[".flexibleSearch"] = true;

$tdatacm2_escanos_chart[".isUseAjaxSuggest"] = true;






$tdatacm2_escanos_chart[".ajaxCodeSnippetAdded"] = false;

$tdatacm2_escanos_chart[".buttonsAdded"] = false;

$tdatacm2_escanos_chart[".addPageEvents"] = true;

// use timepicker for search panel
$tdatacm2_escanos_chart[".isUseTimeForSearch"] = false;



$tdatacm2_escanos_chart[".badgeColor"] = "ff6820";


$tdatacm2_escanos_chart[".allSearchFields"] = array();
$tdatacm2_escanos_chart[".filterFields"] = array();
$tdatacm2_escanos_chart[".requiredSearchFields"] = array();

$tdatacm2_escanos_chart[".allSearchFields"][] = "idEscanos";
	$tdatacm2_escanos_chart[".allSearchFields"][] = "Convocatoria_idConvocatoria";
	$tdatacm2_escanos_chart[".allSearchFields"][] = "Candidatura_idCandidatura";
	$tdatacm2_escanos_chart[".allSearchFields"][] = "Votos";
	$tdatacm2_escanos_chart[".allSearchFields"][] = "PorcVotos";
	$tdatacm2_escanos_chart[".allSearchFields"][] = "Escanos";
	

$tdatacm2_escanos_chart[".googleLikeFields"] = array();
$tdatacm2_escanos_chart[".googleLikeFields"][] = "idEscanos";
$tdatacm2_escanos_chart[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".googleLikeFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".googleLikeFields"][] = "Votos";
$tdatacm2_escanos_chart[".googleLikeFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".googleLikeFields"][] = "Escanos";


$tdatacm2_escanos_chart[".advSearchFields"] = array();
$tdatacm2_escanos_chart[".advSearchFields"][] = "idEscanos";
$tdatacm2_escanos_chart[".advSearchFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".advSearchFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".advSearchFields"][] = "Votos";
$tdatacm2_escanos_chart[".advSearchFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".advSearchFields"][] = "Escanos";

$tdatacm2_escanos_chart[".tableType"] = "chart";

$tdatacm2_escanos_chart[".printerPageOrientation"] = 0;
$tdatacm2_escanos_chart[".nPrinterPageScale"] = 100;

$tdatacm2_escanos_chart[".nPrinterSplitRecords"] = 40;

$tdatacm2_escanos_chart[".nPrinterPDFSplitRecords"] = 40;



$tdatacm2_escanos_chart[".geocodingEnabled"] = false;



// chart settings
$tdatacm2_escanos_chart[".chartType"] = "Line";
// end of chart settings


$tdatacm2_escanos_chart[".listGridLayout"] = 3;





// view page pdf

// print page pdf



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm2_escanos_chart[".strOrderBy"] = $tstrOrderBy;

$tdatacm2_escanos_chart[".orderindexes"] = array();

$tdatacm2_escanos_chart[".sqlHead"] = "SELECT idEscanos,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Votos,  PorcVotos,  Escanos";
$tdatacm2_escanos_chart[".sqlFrom"] = "FROM escanos";
$tdatacm2_escanos_chart[".sqlWhereExpr"] = "(Escanos <> 0)";
$tdatacm2_escanos_chart[".sqlTail"] = "";

//fill array of tabs for list page
$arrGridTabs = array();
$arrGridTabs[] = array(
	'tabId' => "",
	'name' => "All data",
	'nameType' => 'Text',
	'where' => "",	
	'showRowCount' => 0,
	'hideEmpty' => 0,	
);				  
$tdatacm2_escanos_chart[".arrGridTabs"] = $arrGridTabs;











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm2_escanos_chart[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm2_escanos_chart[".arrGroupsPerPage"] = $arrGPP;

$tdatacm2_escanos_chart[".highlightSearchResults"] = true;

$tableKeyscm2_escanos_chart = array();
$tableKeyscm2_escanos_chart[] = "idEscanos";
$tdatacm2_escanos_chart[".Keys"] = $tableKeyscm2_escanos_chart;

$tdatacm2_escanos_chart[".listFields"] = array();
$tdatacm2_escanos_chart[".listFields"][] = "idEscanos";
$tdatacm2_escanos_chart[".listFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".listFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".listFields"][] = "Votos";
$tdatacm2_escanos_chart[".listFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".listFields"][] = "Escanos";

$tdatacm2_escanos_chart[".hideMobileList"] = array();


$tdatacm2_escanos_chart[".viewFields"] = array();
$tdatacm2_escanos_chart[".viewFields"][] = "idEscanos";
$tdatacm2_escanos_chart[".viewFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".viewFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".viewFields"][] = "Votos";
$tdatacm2_escanos_chart[".viewFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".viewFields"][] = "Escanos";

$tdatacm2_escanos_chart[".addFields"] = array();
$tdatacm2_escanos_chart[".addFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".addFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".addFields"][] = "Votos";
$tdatacm2_escanos_chart[".addFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".addFields"][] = "Escanos";

$tdatacm2_escanos_chart[".masterListFields"] = array();
$tdatacm2_escanos_chart[".masterListFields"][] = "idEscanos";
$tdatacm2_escanos_chart[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".masterListFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".masterListFields"][] = "Votos";
$tdatacm2_escanos_chart[".masterListFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".masterListFields"][] = "Escanos";

$tdatacm2_escanos_chart[".inlineAddFields"] = array();
$tdatacm2_escanos_chart[".inlineAddFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".inlineAddFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".inlineAddFields"][] = "Votos";
$tdatacm2_escanos_chart[".inlineAddFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".inlineAddFields"][] = "Escanos";

$tdatacm2_escanos_chart[".editFields"] = array();
$tdatacm2_escanos_chart[".editFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".editFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".editFields"][] = "Votos";
$tdatacm2_escanos_chart[".editFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".editFields"][] = "Escanos";

$tdatacm2_escanos_chart[".inlineEditFields"] = array();
$tdatacm2_escanos_chart[".inlineEditFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".inlineEditFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".inlineEditFields"][] = "Votos";
$tdatacm2_escanos_chart[".inlineEditFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".inlineEditFields"][] = "Escanos";

$tdatacm2_escanos_chart[".updateSelectedFields"] = array();
$tdatacm2_escanos_chart[".updateSelectedFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".updateSelectedFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".updateSelectedFields"][] = "Votos";
$tdatacm2_escanos_chart[".updateSelectedFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".updateSelectedFields"][] = "Escanos";


$tdatacm2_escanos_chart[".exportFields"] = array();
$tdatacm2_escanos_chart[".exportFields"][] = "idEscanos";
$tdatacm2_escanos_chart[".exportFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".exportFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".exportFields"][] = "Votos";
$tdatacm2_escanos_chart[".exportFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".exportFields"][] = "Escanos";

$tdatacm2_escanos_chart[".importFields"] = array();
$tdatacm2_escanos_chart[".importFields"][] = "idEscanos";
$tdatacm2_escanos_chart[".importFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".importFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".importFields"][] = "Votos";
$tdatacm2_escanos_chart[".importFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".importFields"][] = "Escanos";

$tdatacm2_escanos_chart[".printFields"] = array();
$tdatacm2_escanos_chart[".printFields"][] = "idEscanos";
$tdatacm2_escanos_chart[".printFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_chart[".printFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_chart[".printFields"][] = "Votos";
$tdatacm2_escanos_chart[".printFields"][] = "PorcVotos";
$tdatacm2_escanos_chart[".printFields"][] = "Escanos";


//	idEscanos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idEscanos";
	$fdata["GoodName"] = "idEscanos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm2_escanos_chart","idEscanos");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idEscanos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idEscanos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_escanos_chart["idEscanos"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm2_escanos_chart","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_escanos_chart["Convocatoria_idConvocatoria"] = $fdata;
//	Candidatura_idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Candidatura_idCandidatura";
	$fdata["GoodName"] = "Candidatura_idCandidatura";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm2_escanos_chart","Candidatura_idCandidatura");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Candidatura_idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Candidatura_idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "candidatura";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idCandidatura";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "concat(Codigo,' -  ',Titulo)";
	
	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_escanos_chart["Candidatura_idCandidatura"] = $fdata;
//	Votos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Votos";
	$fdata["GoodName"] = "Votos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm2_escanos_chart","Votos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Votos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Votos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_escanos_chart["Votos"] = $fdata;
//	PorcVotos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "PorcVotos";
	$fdata["GoodName"] = "PorcVotos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm2_escanos_chart","PorcVotos");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "PorcVotos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "PorcVotos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_escanos_chart["PorcVotos"] = $fdata;
//	Escanos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Escanos";
	$fdata["GoodName"] = "Escanos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm2_escanos_chart","Escanos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Escanos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Escanos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_escanos_chart["Escanos"] = $fdata;

	$tdatacm2_escanos_chart[".chartXml"] = '<chart>
		<attr value="tables">
			<attr value="0">cm2_escanos_chart</attr>
		</attr>
		<attr value="chart_type">
			<attr value="type">line</attr>
		</attr>

		<attr value="parameters">';
	$tdatacm2_escanos_chart[".chartXml"] .= '<attr value="0">
			<attr value="name">Escanos</attr>';
	$tdatacm2_escanos_chart[".chartXml"] .= '</attr>';
	$tdatacm2_escanos_chart[".chartXml"] .= '<attr value="1">
		<attr value="name">Convocatoria_idConvocatoria</attr>
	</attr>';
	$tdatacm2_escanos_chart[".chartXml"] .= '</attr>
			<attr value="appearance">';


	$tdatacm2_escanos_chart[".chartXml"] .= '<attr value="head">'.xmlencode("Escaños").'</attr>
<attr value="foot">'.xmlencode("Convocatorias").'</attr>
<attr value="y_axis_label">'.xmlencode("Convocatoria_idConvocatoria").'</attr>


<attr value="slegend">false</attr>
<attr value="sgrid">true</attr>
<attr value="sname">false</attr>
<attr value="sval">true</attr>
<attr value="sanim">true</attr>
<attr value="sstacked">false</attr>
<attr value="slog">true</attr>
<attr value="aqua">0</attr>
<attr value="cview">0</attr>
<attr value="is3d">0</attr>
<attr value="isstacked">0</attr>
<attr value="linestyle">1</attr>
<attr value="autoupdate">0</attr>
<attr value="autoupmin">60</attr>';
$tdatacm2_escanos_chart[".chartXml"] .= '</attr>

<attr value="fields">';
	$tdatacm2_escanos_chart[".chartXml"] .= '<attr value="0">
		<attr value="name">idEscanos</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm2_escanos_chart","idEscanos")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm2_escanos_chart[".chartXml"] .= '<attr value="1">
		<attr value="name">Convocatoria_idConvocatoria</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm2_escanos_chart","Convocatoria_idConvocatoria")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm2_escanos_chart[".chartXml"] .= '<attr value="2">
		<attr value="name">Candidatura_idCandidatura</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm2_escanos_chart","Candidatura_idCandidatura")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm2_escanos_chart[".chartXml"] .= '<attr value="3">
		<attr value="name">Votos</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm2_escanos_chart","Votos")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm2_escanos_chart[".chartXml"] .= '<attr value="4">
		<attr value="name">PorcVotos</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm2_escanos_chart","PorcVotos")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm2_escanos_chart[".chartXml"] .= '<attr value="5">
		<attr value="name">Escanos</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm2_escanos_chart","Escanos")).'</attr>
		<attr value="search"></attr>
	</attr>';
$tdatacm2_escanos_chart[".chartXml"] .= '</attr>


<attr value="settings">
<attr value="name">cm2_escanos_chart</attr>
<attr value="short_table_name">cm2_escanos_chart</attr>
</attr>

</chart>';

$tables_data["cm2_escanos_chart"]=&$tdatacm2_escanos_chart;
$field_labels["cm2_escanos_chart"] = &$fieldLabelscm2_escanos_chart;
$fieldToolTips["cm2_escanos_chart"] = &$fieldToolTipscm2_escanos_chart;
$placeHolders["cm2_escanos_chart"] = &$placeHolderscm2_escanos_chart;
$page_titles["cm2_escanos_chart"] = &$pageTitlescm2_escanos_chart;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm2_escanos_chart"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["cm2_escanos_chart"] = array();


	
				$strOriginalDetailsTable="candidatura";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm2_candidatura";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm2_candidatura";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm2_escanos_chart"][0] = $masterParams;
				$masterTablesData["cm2_escanos_chart"][0]["masterKeys"] = array();
	$masterTablesData["cm2_escanos_chart"][0]["masterKeys"][]="idCandidatura";
				$masterTablesData["cm2_escanos_chart"][0]["detailKeys"] = array();
	$masterTablesData["cm2_escanos_chart"][0]["detailKeys"][]="Candidatura_idCandidatura";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm2_escanos_chart()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idEscanos,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Votos,  PorcVotos,  Escanos";
$proto0["m_strFrom"] = "FROM escanos";
$proto0["m_strWhere"] = "(Escanos <> 0)";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "Escanos <> 0";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos_chart"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "<> 0";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idEscanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos_chart"
));

$proto6["m_sql"] = "idEscanos";
$proto6["m_srcTableName"] = "cm2_escanos_chart";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos_chart"
));

$proto8["m_sql"] = "Convocatoria_idConvocatoria";
$proto8["m_srcTableName"] = "cm2_escanos_chart";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos_chart"
));

$proto10["m_sql"] = "Candidatura_idCandidatura";
$proto10["m_srcTableName"] = "cm2_escanos_chart";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos_chart"
));

$proto12["m_sql"] = "Votos";
$proto12["m_srcTableName"] = "cm2_escanos_chart";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "PorcVotos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos_chart"
));

$proto14["m_sql"] = "PorcVotos";
$proto14["m_srcTableName"] = "cm2_escanos_chart";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos_chart"
));

$proto16["m_sql"] = "Escanos";
$proto16["m_srcTableName"] = "cm2_escanos_chart";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "escanos";
$proto19["m_srcTableName"] = "cm2_escanos_chart";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "idEscanos";
$proto19["m_columns"][] = "Convocatoria_idConvocatoria";
$proto19["m_columns"][] = "Candidatura_idCandidatura";
$proto19["m_columns"][] = "Votos";
$proto19["m_columns"][] = "PorcVotos";
$proto19["m_columns"][] = "Escanos";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "escanos";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "cm2_escanos_chart";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="cm2_escanos_chart";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_cm2_escanos_chart = createSqlQuery_cm2_escanos_chart();


	
		;

						

$tdatacm2_escanos_chart[".sqlquery"] = $queryData_cm2_escanos_chart;

include_once(getabspath("include/cm2_escanos_chart_events.php"));
$tableEvents["cm2_escanos_chart"] = new eventclass_cm2_escanos_chart;
$tdatacm2_escanos_chart[".hasEvents"] = true;

?>