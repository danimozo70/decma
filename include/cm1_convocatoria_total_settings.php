<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm1_convocatoria_total = array();
	$tdatacm1_convocatoria_total[".truncateText"] = true;
	$tdatacm1_convocatoria_total[".NumberOfChars"] = 80;
	$tdatacm1_convocatoria_total[".ShortName"] = "cm1_convocatoria_total";
	$tdatacm1_convocatoria_total[".OwnerID"] = "";
	$tdatacm1_convocatoria_total[".OriginalTable"] = "v_convocatoria_total";

//	field labels
$fieldLabelscm1_convocatoria_total = array();
$fieldToolTipscm1_convocatoria_total = array();
$pageTitlescm1_convocatoria_total = array();
$placeHolderscm1_convocatoria_total = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm1_convocatoria_total["Spanish"] = array();
	$fieldToolTipscm1_convocatoria_total["Spanish"] = array();
	$placeHolderscm1_convocatoria_total["Spanish"] = array();
	$pageTitlescm1_convocatoria_total["Spanish"] = array();
	$fieldLabelscm1_convocatoria_total["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldToolTipscm1_convocatoria_total["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderscm1_convocatoria_total["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelscm1_convocatoria_total["Spanish"]["Censo"] = "Censo";
	$fieldToolTipscm1_convocatoria_total["Spanish"]["Censo"] = "";
	$placeHolderscm1_convocatoria_total["Spanish"]["Censo"] = "";
	$fieldLabelscm1_convocatoria_total["Spanish"]["CensoOficial"] = "Censo Oficial";
	$fieldToolTipscm1_convocatoria_total["Spanish"]["CensoOficial"] = "";
	$placeHolderscm1_convocatoria_total["Spanish"]["CensoOficial"] = "";
	$fieldLabelscm1_convocatoria_total["Spanish"]["Certificacion"] = "Votos Certificación";
	$fieldToolTipscm1_convocatoria_total["Spanish"]["Certificacion"] = "";
	$placeHolderscm1_convocatoria_total["Spanish"]["Certificacion"] = "";
	$fieldLabelscm1_convocatoria_total["Spanish"]["CertificacionAlta"] = "Certificacion Alta";
	$fieldToolTipscm1_convocatoria_total["Spanish"]["CertificacionAlta"] = "";
	$placeHolderscm1_convocatoria_total["Spanish"]["CertificacionAlta"] = "";
	$fieldLabelscm1_convocatoria_total["Spanish"]["CertificacionError"] = "Certificacion Error";
	$fieldToolTipscm1_convocatoria_total["Spanish"]["CertificacionError"] = "";
	$placeHolderscm1_convocatoria_total["Spanish"]["CertificacionError"] = "";
	$fieldLabelscm1_convocatoria_total["Spanish"]["Voto"] = "Voto";
	$fieldToolTipscm1_convocatoria_total["Spanish"]["Voto"] = "";
	$placeHolderscm1_convocatoria_total["Spanish"]["Voto"] = "";
	$fieldLabelscm1_convocatoria_total["Spanish"]["Interventores"] = "Votos Interventores";
	$fieldToolTipscm1_convocatoria_total["Spanish"]["Interventores"] = "";
	$placeHolderscm1_convocatoria_total["Spanish"]["Interventores"] = "";
	$fieldLabelscm1_convocatoria_total["Spanish"]["Nulo"] = "Votos Nulos";
	$fieldToolTipscm1_convocatoria_total["Spanish"]["Nulo"] = "";
	$placeHolderscm1_convocatoria_total["Spanish"]["Nulo"] = "";
	$fieldLabelscm1_convocatoria_total["Spanish"]["Blanco"] = "Votos Blancos";
	$fieldToolTipscm1_convocatoria_total["Spanish"]["Blanco"] = "";
	$placeHolderscm1_convocatoria_total["Spanish"]["Blanco"] = "";
	$fieldLabelscm1_convocatoria_total["Spanish"]["Validos"] = "Votos Válidos";
	$fieldToolTipscm1_convocatoria_total["Spanish"]["Validos"] = "";
	$placeHolderscm1_convocatoria_total["Spanish"]["Validos"] = "";
	$pageTitlescm1_convocatoria_total["Spanish"]["list"] = "<strong>{%master.Titulo}</strong>, Resumen";
	if (count($fieldToolTipscm1_convocatoria_total["Spanish"]))
		$tdatacm1_convocatoria_total[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm1_convocatoria_total[""] = array();
	$fieldToolTipscm1_convocatoria_total[""] = array();
	$placeHolderscm1_convocatoria_total[""] = array();
	$pageTitlescm1_convocatoria_total[""] = array();
	if (count($fieldToolTipscm1_convocatoria_total[""]))
		$tdatacm1_convocatoria_total[".isUseToolTips"] = true;
}


	$tdatacm1_convocatoria_total[".NCSearch"] = true;



$tdatacm1_convocatoria_total[".shortTableName"] = "cm1_convocatoria_total";
$tdatacm1_convocatoria_total[".nSecOptions"] = 0;
$tdatacm1_convocatoria_total[".recsPerRowList"] = 1;
$tdatacm1_convocatoria_total[".recsPerRowPrint"] = 1;
$tdatacm1_convocatoria_total[".mainTableOwnerID"] = "";
$tdatacm1_convocatoria_total[".moveNext"] = 1;
$tdatacm1_convocatoria_total[".entityType"] = 0;

$tdatacm1_convocatoria_total[".strOriginalTableName"] = "v_convocatoria_total";

	



$tdatacm1_convocatoria_total[".showAddInPopup"] = false;

$tdatacm1_convocatoria_total[".showEditInPopup"] = false;

$tdatacm1_convocatoria_total[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacm1_convocatoria_total[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm1_convocatoria_total[".fieldsForRegister"] = array();

$tdatacm1_convocatoria_total[".listAjax"] = false;

	$tdatacm1_convocatoria_total[".audit"] = false;

	$tdatacm1_convocatoria_total[".locking"] = false;



$tdatacm1_convocatoria_total[".list"] = true;











$tdatacm1_convocatoria_total[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm1_convocatoria_total[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm1_convocatoria_total[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm1_convocatoria_total[".searchSaving"] = false;
//

$tdatacm1_convocatoria_total[".showSearchPanel"] = true;
		$tdatacm1_convocatoria_total[".flexibleSearch"] = true;

$tdatacm1_convocatoria_total[".isUseAjaxSuggest"] = true;

$tdatacm1_convocatoria_total[".rowHighlite"] = true;





$tdatacm1_convocatoria_total[".ajaxCodeSnippetAdded"] = false;

$tdatacm1_convocatoria_total[".buttonsAdded"] = false;

$tdatacm1_convocatoria_total[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm1_convocatoria_total[".isUseTimeForSearch"] = false;



$tdatacm1_convocatoria_total[".badgeColor"] = "b22222";


$tdatacm1_convocatoria_total[".allSearchFields"] = array();
$tdatacm1_convocatoria_total[".filterFields"] = array();
$tdatacm1_convocatoria_total[".requiredSearchFields"] = array();

$tdatacm1_convocatoria_total[".allSearchFields"][] = "Censo";
	$tdatacm1_convocatoria_total[".allSearchFields"][] = "Certificacion";
	$tdatacm1_convocatoria_total[".allSearchFields"][] = "Interventores";
	$tdatacm1_convocatoria_total[".allSearchFields"][] = "Nulo";
	$tdatacm1_convocatoria_total[".allSearchFields"][] = "Blanco";
	$tdatacm1_convocatoria_total[".allSearchFields"][] = "Validos";
	

$tdatacm1_convocatoria_total[".googleLikeFields"] = array();
$tdatacm1_convocatoria_total[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_convocatoria_total[".googleLikeFields"][] = "Censo";
$tdatacm1_convocatoria_total[".googleLikeFields"][] = "CensoOficial";
$tdatacm1_convocatoria_total[".googleLikeFields"][] = "Certificacion";
$tdatacm1_convocatoria_total[".googleLikeFields"][] = "CertificacionAlta";
$tdatacm1_convocatoria_total[".googleLikeFields"][] = "CertificacionError";
$tdatacm1_convocatoria_total[".googleLikeFields"][] = "Voto";
$tdatacm1_convocatoria_total[".googleLikeFields"][] = "Interventores";
$tdatacm1_convocatoria_total[".googleLikeFields"][] = "Nulo";
$tdatacm1_convocatoria_total[".googleLikeFields"][] = "Blanco";
$tdatacm1_convocatoria_total[".googleLikeFields"][] = "Validos";


$tdatacm1_convocatoria_total[".advSearchFields"] = array();
$tdatacm1_convocatoria_total[".advSearchFields"][] = "Censo";
$tdatacm1_convocatoria_total[".advSearchFields"][] = "Certificacion";
$tdatacm1_convocatoria_total[".advSearchFields"][] = "Interventores";
$tdatacm1_convocatoria_total[".advSearchFields"][] = "Nulo";
$tdatacm1_convocatoria_total[".advSearchFields"][] = "Blanco";
$tdatacm1_convocatoria_total[".advSearchFields"][] = "Validos";

$tdatacm1_convocatoria_total[".tableType"] = "list";

$tdatacm1_convocatoria_total[".printerPageOrientation"] = 0;
$tdatacm1_convocatoria_total[".nPrinterPageScale"] = 100;

$tdatacm1_convocatoria_total[".nPrinterSplitRecords"] = 40;

$tdatacm1_convocatoria_total[".nPrinterPDFSplitRecords"] = 40;



$tdatacm1_convocatoria_total[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatacm1_convocatoria_total[".pageSize"] = 20;

$tdatacm1_convocatoria_total[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm1_convocatoria_total[".strOrderBy"] = $tstrOrderBy;

$tdatacm1_convocatoria_total[".orderindexes"] = array();

$tdatacm1_convocatoria_total[".sqlHead"] = "SELECT Convocatoria_idConvocatoria,  	Censo,  	CensoOficial,  	Certificacion,  	CertificacionAlta,  	CertificacionError,  	Voto,  	Interventores,  	Nulo,  	Blanco,  	Validos";
$tdatacm1_convocatoria_total[".sqlFrom"] = "FROM v_convocatoria_total";
$tdatacm1_convocatoria_total[".sqlWhereExpr"] = "";
$tdatacm1_convocatoria_total[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm1_convocatoria_total[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm1_convocatoria_total[".arrGroupsPerPage"] = $arrGPP;

$tdatacm1_convocatoria_total[".highlightSearchResults"] = true;

$tableKeyscm1_convocatoria_total = array();
$tableKeyscm1_convocatoria_total[] = "Convocatoria_idConvocatoria";
$tdatacm1_convocatoria_total[".Keys"] = $tableKeyscm1_convocatoria_total;

$tdatacm1_convocatoria_total[".listFields"] = array();
$tdatacm1_convocatoria_total[".listFields"][] = "Censo";
$tdatacm1_convocatoria_total[".listFields"][] = "Certificacion";
$tdatacm1_convocatoria_total[".listFields"][] = "Interventores";
$tdatacm1_convocatoria_total[".listFields"][] = "Nulo";
$tdatacm1_convocatoria_total[".listFields"][] = "Blanco";
$tdatacm1_convocatoria_total[".listFields"][] = "Validos";

$tdatacm1_convocatoria_total[".hideMobileList"] = array();


$tdatacm1_convocatoria_total[".viewFields"] = array();

$tdatacm1_convocatoria_total[".addFields"] = array();

$tdatacm1_convocatoria_total[".masterListFields"] = array();
$tdatacm1_convocatoria_total[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_convocatoria_total[".masterListFields"][] = "Censo";
$tdatacm1_convocatoria_total[".masterListFields"][] = "CensoOficial";
$tdatacm1_convocatoria_total[".masterListFields"][] = "Certificacion";
$tdatacm1_convocatoria_total[".masterListFields"][] = "CertificacionAlta";
$tdatacm1_convocatoria_total[".masterListFields"][] = "CertificacionError";
$tdatacm1_convocatoria_total[".masterListFields"][] = "Voto";
$tdatacm1_convocatoria_total[".masterListFields"][] = "Interventores";
$tdatacm1_convocatoria_total[".masterListFields"][] = "Nulo";
$tdatacm1_convocatoria_total[".masterListFields"][] = "Blanco";
$tdatacm1_convocatoria_total[".masterListFields"][] = "Validos";

$tdatacm1_convocatoria_total[".inlineAddFields"] = array();

$tdatacm1_convocatoria_total[".editFields"] = array();

$tdatacm1_convocatoria_total[".inlineEditFields"] = array();

$tdatacm1_convocatoria_total[".updateSelectedFields"] = array();


$tdatacm1_convocatoria_total[".exportFields"] = array();

$tdatacm1_convocatoria_total[".importFields"] = array();

$tdatacm1_convocatoria_total[".printFields"] = array();


//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "v_convocatoria_total";
	$fdata["Label"] = GetFieldLabel("v_convocatoria_total","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm1_convocatoria_total["Convocatoria_idConvocatoria"] = $fdata;
//	Censo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Censo";
	$fdata["GoodName"] = "Censo";
	$fdata["ownerTable"] = "v_convocatoria_total";
	$fdata["Label"] = GetFieldLabel("v_convocatoria_total","Censo");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Censo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Censo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_convocatoria_total["Censo"] = $fdata;
//	CensoOficial
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "CensoOficial";
	$fdata["GoodName"] = "CensoOficial";
	$fdata["ownerTable"] = "v_convocatoria_total";
	$fdata["Label"] = GetFieldLabel("v_convocatoria_total","CensoOficial");
	$fdata["FieldType"] = 14;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "CensoOficial";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "CensoOficial";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm1_convocatoria_total["CensoOficial"] = $fdata;
//	Certificacion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Certificacion";
	$fdata["GoodName"] = "Certificacion";
	$fdata["ownerTable"] = "v_convocatoria_total";
	$fdata["Label"] = GetFieldLabel("v_convocatoria_total","Certificacion");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Certificacion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Certificacion";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_convocatoria_total["Certificacion"] = $fdata;
//	CertificacionAlta
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "CertificacionAlta";
	$fdata["GoodName"] = "CertificacionAlta";
	$fdata["ownerTable"] = "v_convocatoria_total";
	$fdata["Label"] = GetFieldLabel("v_convocatoria_total","CertificacionAlta");
	$fdata["FieldType"] = 14;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "CertificacionAlta";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "CertificacionAlta";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm1_convocatoria_total["CertificacionAlta"] = $fdata;
//	CertificacionError
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "CertificacionError";
	$fdata["GoodName"] = "CertificacionError";
	$fdata["ownerTable"] = "v_convocatoria_total";
	$fdata["Label"] = GetFieldLabel("v_convocatoria_total","CertificacionError");
	$fdata["FieldType"] = 14;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "CertificacionError";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "CertificacionError";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm1_convocatoria_total["CertificacionError"] = $fdata;
//	Voto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Voto";
	$fdata["GoodName"] = "Voto";
	$fdata["ownerTable"] = "v_convocatoria_total";
	$fdata["Label"] = GetFieldLabel("v_convocatoria_total","Voto");
	$fdata["FieldType"] = 14;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Voto";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Voto";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm1_convocatoria_total["Voto"] = $fdata;
//	Interventores
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "Interventores";
	$fdata["GoodName"] = "Interventores";
	$fdata["ownerTable"] = "v_convocatoria_total";
	$fdata["Label"] = GetFieldLabel("v_convocatoria_total","Interventores");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Interventores";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Interventores";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_convocatoria_total["Interventores"] = $fdata;
//	Nulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "Nulo";
	$fdata["GoodName"] = "Nulo";
	$fdata["ownerTable"] = "v_convocatoria_total";
	$fdata["Label"] = GetFieldLabel("v_convocatoria_total","Nulo");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Nulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Nulo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_convocatoria_total["Nulo"] = $fdata;
//	Blanco
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "Blanco";
	$fdata["GoodName"] = "Blanco";
	$fdata["ownerTable"] = "v_convocatoria_total";
	$fdata["Label"] = GetFieldLabel("v_convocatoria_total","Blanco");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Blanco";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Blanco";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_convocatoria_total["Blanco"] = $fdata;
//	Validos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "Validos";
	$fdata["GoodName"] = "Validos";
	$fdata["ownerTable"] = "v_convocatoria_total";
	$fdata["Label"] = GetFieldLabel("v_convocatoria_total","Validos");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Validos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Validos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_convocatoria_total["Validos"] = $fdata;


$tables_data["v_convocatoria_total"]=&$tdatacm1_convocatoria_total;
$field_labels["v_convocatoria_total"] = &$fieldLabelscm1_convocatoria_total;
$fieldToolTips["v_convocatoria_total"] = &$fieldToolTipscm1_convocatoria_total;
$placeHolders["v_convocatoria_total"] = &$placeHolderscm1_convocatoria_total;
$page_titles["v_convocatoria_total"] = &$pageTitlescm1_convocatoria_total;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["v_convocatoria_total"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["v_convocatoria_total"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["v_convocatoria_total"][0] = $masterParams;
				$masterTablesData["v_convocatoria_total"][0]["masterKeys"] = array();
	$masterTablesData["v_convocatoria_total"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["v_convocatoria_total"][0]["detailKeys"] = array();
	$masterTablesData["v_convocatoria_total"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm1_convocatoria_total()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "Convocatoria_idConvocatoria,  	Censo,  	CensoOficial,  	Certificacion,  	CertificacionAlta,  	CertificacionError,  	Voto,  	Interventores,  	Nulo,  	Blanco,  	Validos";
$proto0["m_strFrom"] = "FROM v_convocatoria_total";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "v_convocatoria_total",
	"m_srcTableName" => "v_convocatoria_total"
));

$proto6["m_sql"] = "Convocatoria_idConvocatoria";
$proto6["m_srcTableName"] = "v_convocatoria_total";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Censo",
	"m_strTable" => "v_convocatoria_total",
	"m_srcTableName" => "v_convocatoria_total"
));

$proto8["m_sql"] = "Censo";
$proto8["m_srcTableName"] = "v_convocatoria_total";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "CensoOficial",
	"m_strTable" => "v_convocatoria_total",
	"m_srcTableName" => "v_convocatoria_total"
));

$proto10["m_sql"] = "CensoOficial";
$proto10["m_srcTableName"] = "v_convocatoria_total";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "Certificacion",
	"m_strTable" => "v_convocatoria_total",
	"m_srcTableName" => "v_convocatoria_total"
));

$proto12["m_sql"] = "Certificacion";
$proto12["m_srcTableName"] = "v_convocatoria_total";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "CertificacionAlta",
	"m_strTable" => "v_convocatoria_total",
	"m_srcTableName" => "v_convocatoria_total"
));

$proto14["m_sql"] = "CertificacionAlta";
$proto14["m_srcTableName"] = "v_convocatoria_total";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "CertificacionError",
	"m_strTable" => "v_convocatoria_total",
	"m_srcTableName" => "v_convocatoria_total"
));

$proto16["m_sql"] = "CertificacionError";
$proto16["m_srcTableName"] = "v_convocatoria_total";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "Voto",
	"m_strTable" => "v_convocatoria_total",
	"m_srcTableName" => "v_convocatoria_total"
));

$proto18["m_sql"] = "Voto";
$proto18["m_srcTableName"] = "v_convocatoria_total";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "Interventores",
	"m_strTable" => "v_convocatoria_total",
	"m_srcTableName" => "v_convocatoria_total"
));

$proto20["m_sql"] = "Interventores";
$proto20["m_srcTableName"] = "v_convocatoria_total";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "Nulo",
	"m_strTable" => "v_convocatoria_total",
	"m_srcTableName" => "v_convocatoria_total"
));

$proto22["m_sql"] = "Nulo";
$proto22["m_srcTableName"] = "v_convocatoria_total";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "Blanco",
	"m_strTable" => "v_convocatoria_total",
	"m_srcTableName" => "v_convocatoria_total"
));

$proto24["m_sql"] = "Blanco";
$proto24["m_srcTableName"] = "v_convocatoria_total";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "Validos",
	"m_strTable" => "v_convocatoria_total",
	"m_srcTableName" => "v_convocatoria_total"
));

$proto26["m_sql"] = "Validos";
$proto26["m_srcTableName"] = "v_convocatoria_total";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto28=array();
$proto28["m_link"] = "SQLL_MAIN";
			$proto29=array();
$proto29["m_strName"] = "v_convocatoria_total";
$proto29["m_srcTableName"] = "v_convocatoria_total";
$proto29["m_columns"] = array();
$proto29["m_columns"][] = "Convocatoria_idConvocatoria";
$proto29["m_columns"][] = "Censo";
$proto29["m_columns"][] = "CensoOficial";
$proto29["m_columns"][] = "Certificacion";
$proto29["m_columns"][] = "CertificacionAlta";
$proto29["m_columns"][] = "CertificacionError";
$proto29["m_columns"][] = "Voto";
$proto29["m_columns"][] = "Interventores";
$proto29["m_columns"][] = "Nulo";
$proto29["m_columns"][] = "Blanco";
$proto29["m_columns"][] = "Validos";
$obj = new SQLTable($proto29);

$proto28["m_table"] = $obj;
$proto28["m_sql"] = "v_convocatoria_total";
$proto28["m_alias"] = "";
$proto28["m_srcTableName"] = "v_convocatoria_total";
$proto30=array();
$proto30["m_sql"] = "";
$proto30["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto30["m_column"]=$obj;
$proto30["m_contained"] = array();
$proto30["m_strCase"] = "";
$proto30["m_havingmode"] = false;
$proto30["m_inBrackets"] = false;
$proto30["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto30);

$proto28["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto28);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="v_convocatoria_total";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_cm1_convocatoria_total = createSqlQuery_cm1_convocatoria_total();


	
		;

											

$tdatacm1_convocatoria_total[".sqlquery"] = $queryData_cm1_convocatoria_total;

$tableEvents["v_convocatoria_total"] = new eventsBase;
$tdatacm1_convocatoria_total[".hasEvents"] = false;

?>