<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm1_1_Convocatoria = array();
	$tdatacm1_1_Convocatoria[".truncateText"] = true;
	$tdatacm1_1_Convocatoria[".NumberOfChars"] = 80;
	$tdatacm1_1_Convocatoria[".ShortName"] = "cm1_1_Convocatoria";
	$tdatacm1_1_Convocatoria[".OwnerID"] = "";
	$tdatacm1_1_Convocatoria[".OriginalTable"] = "convocatoria";

//	field labels
$fieldLabelscm1_1_Convocatoria = array();
$fieldToolTipscm1_1_Convocatoria = array();
$pageTitlescm1_1_Convocatoria = array();
$placeHolderscm1_1_Convocatoria = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm1_1_Convocatoria["Spanish"] = array();
	$fieldToolTipscm1_1_Convocatoria["Spanish"] = array();
	$placeHolderscm1_1_Convocatoria["Spanish"] = array();
	$pageTitlescm1_1_Convocatoria["Spanish"] = array();
	$fieldLabelscm1_1_Convocatoria["Spanish"]["idConvocatoria"] = "Id Interno";
	$fieldToolTipscm1_1_Convocatoria["Spanish"]["idConvocatoria"] = "";
	$placeHolderscm1_1_Convocatoria["Spanish"]["idConvocatoria"] = "";
	$fieldLabelscm1_1_Convocatoria["Spanish"]["Orden"] = "Orden";
	$fieldToolTipscm1_1_Convocatoria["Spanish"]["Orden"] = "";
	$placeHolderscm1_1_Convocatoria["Spanish"]["Orden"] = "";
	$fieldLabelscm1_1_Convocatoria["Spanish"]["EsAsamblea"] = "Es Asamblea?";
	$fieldToolTipscm1_1_Convocatoria["Spanish"]["EsAsamblea"] = "";
	$placeHolderscm1_1_Convocatoria["Spanish"]["EsAsamblea"] = "";
	$fieldLabelscm1_1_Convocatoria["Spanish"]["Titulo"] = "Título";
	$fieldToolTipscm1_1_Convocatoria["Spanish"]["Titulo"] = "";
	$placeHolderscm1_1_Convocatoria["Spanish"]["Titulo"] = "";
	$fieldLabelscm1_1_Convocatoria["Spanish"]["Descripcion"] = "Descripción";
	$fieldToolTipscm1_1_Convocatoria["Spanish"]["Descripcion"] = "";
	$placeHolderscm1_1_Convocatoria["Spanish"]["Descripcion"] = "";
	if (count($fieldToolTipscm1_1_Convocatoria["Spanish"]))
		$tdatacm1_1_Convocatoria[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm1_1_Convocatoria[""] = array();
	$fieldToolTipscm1_1_Convocatoria[""] = array();
	$placeHolderscm1_1_Convocatoria[""] = array();
	$pageTitlescm1_1_Convocatoria[""] = array();
	if (count($fieldToolTipscm1_1_Convocatoria[""]))
		$tdatacm1_1_Convocatoria[".isUseToolTips"] = true;
}


	$tdatacm1_1_Convocatoria[".NCSearch"] = true;



$tdatacm1_1_Convocatoria[".shortTableName"] = "cm1_1_Convocatoria";
$tdatacm1_1_Convocatoria[".nSecOptions"] = 0;
$tdatacm1_1_Convocatoria[".recsPerRowList"] = 1;
$tdatacm1_1_Convocatoria[".recsPerRowPrint"] = 1;
$tdatacm1_1_Convocatoria[".mainTableOwnerID"] = "";
$tdatacm1_1_Convocatoria[".moveNext"] = 1;
$tdatacm1_1_Convocatoria[".entityType"] = 1;

$tdatacm1_1_Convocatoria[".strOriginalTableName"] = "convocatoria";

	



$tdatacm1_1_Convocatoria[".showAddInPopup"] = true;

$tdatacm1_1_Convocatoria[".showEditInPopup"] = true;

$tdatacm1_1_Convocatoria[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
					
;
$popupPagesLayoutNames["add"] = "add";
			;
$popupPagesLayoutNames["edit"] = "add";
						
	;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdatacm1_1_Convocatoria[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm1_1_Convocatoria[".fieldsForRegister"] = array();

$tdatacm1_1_Convocatoria[".listAjax"] = false;

	$tdatacm1_1_Convocatoria[".audit"] = false;

	$tdatacm1_1_Convocatoria[".locking"] = false;



$tdatacm1_1_Convocatoria[".list"] = true;





$tdatacm1_1_Convocatoria[".exportFormatting"] = 2;
$tdatacm1_1_Convocatoria[".exportDelimiter"] = ",";
		
$tdatacm1_1_Convocatoria[".view"] = true;


$tdatacm1_1_Convocatoria[".exportTo"] = true;

$tdatacm1_1_Convocatoria[".printFriendly"] = true;


$tdatacm1_1_Convocatoria[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm1_1_Convocatoria[".allowShowHideFields"] = true;
//

// Allow Fields Reordering in GRID
$tdatacm1_1_Convocatoria[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm1_1_Convocatoria[".searchSaving"] = false;
//

$tdatacm1_1_Convocatoria[".showSearchPanel"] = true;
		$tdatacm1_1_Convocatoria[".flexibleSearch"] = true;

$tdatacm1_1_Convocatoria[".isUseAjaxSuggest"] = true;

$tdatacm1_1_Convocatoria[".rowHighlite"] = true;





$tdatacm1_1_Convocatoria[".ajaxCodeSnippetAdded"] = false;

$tdatacm1_1_Convocatoria[".buttonsAdded"] = false;

$tdatacm1_1_Convocatoria[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm1_1_Convocatoria[".isUseTimeForSearch"] = false;



$tdatacm1_1_Convocatoria[".badgeColor"] = "4169E1";


$tdatacm1_1_Convocatoria[".allSearchFields"] = array();
$tdatacm1_1_Convocatoria[".filterFields"] = array();
$tdatacm1_1_Convocatoria[".requiredSearchFields"] = array();

$tdatacm1_1_Convocatoria[".allSearchFields"][] = "idConvocatoria";
	$tdatacm1_1_Convocatoria[".allSearchFields"][] = "Orden";
	$tdatacm1_1_Convocatoria[".allSearchFields"][] = "EsAsamblea";
	$tdatacm1_1_Convocatoria[".allSearchFields"][] = "Titulo";
	$tdatacm1_1_Convocatoria[".allSearchFields"][] = "Descripcion";
	

$tdatacm1_1_Convocatoria[".googleLikeFields"] = array();
$tdatacm1_1_Convocatoria[".googleLikeFields"][] = "idConvocatoria";
$tdatacm1_1_Convocatoria[".googleLikeFields"][] = "Orden";
$tdatacm1_1_Convocatoria[".googleLikeFields"][] = "EsAsamblea";
$tdatacm1_1_Convocatoria[".googleLikeFields"][] = "Titulo";
$tdatacm1_1_Convocatoria[".googleLikeFields"][] = "Descripcion";


$tdatacm1_1_Convocatoria[".advSearchFields"] = array();
$tdatacm1_1_Convocatoria[".advSearchFields"][] = "idConvocatoria";
$tdatacm1_1_Convocatoria[".advSearchFields"][] = "Orden";
$tdatacm1_1_Convocatoria[".advSearchFields"][] = "EsAsamblea";
$tdatacm1_1_Convocatoria[".advSearchFields"][] = "Titulo";
$tdatacm1_1_Convocatoria[".advSearchFields"][] = "Descripcion";

$tdatacm1_1_Convocatoria[".tableType"] = "list";

$tdatacm1_1_Convocatoria[".printerPageOrientation"] = 0;
$tdatacm1_1_Convocatoria[".nPrinterPageScale"] = 100;

$tdatacm1_1_Convocatoria[".nPrinterSplitRecords"] = 40;

$tdatacm1_1_Convocatoria[".nPrinterPDFSplitRecords"] = 40;



$tdatacm1_1_Convocatoria[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatacm1_1_Convocatoria[".pageSize"] = 8;

$tdatacm1_1_Convocatoria[".warnLeavingPages"] = true;



$tstrOrderBy = "order by Orden";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm1_1_Convocatoria[".strOrderBy"] = $tstrOrderBy;

$tdatacm1_1_Convocatoria[".orderindexes"] = array();
	$tdatacm1_1_Convocatoria[".orderindexes"][] = array(2, (1 ? "ASC" : "DESC"), "Orden");


$tdatacm1_1_Convocatoria[".sqlHead"] = "SELECT idConvocatoria,  	Orden,  	EsAsamblea,  	Titulo,  	Descripcion";
$tdatacm1_1_Convocatoria[".sqlFrom"] = "FROM convocatoria";
$tdatacm1_1_Convocatoria[".sqlWhereExpr"] = "";
$tdatacm1_1_Convocatoria[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm1_1_Convocatoria[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm1_1_Convocatoria[".arrGroupsPerPage"] = $arrGPP;

$tdatacm1_1_Convocatoria[".highlightSearchResults"] = true;

$tableKeyscm1_1_Convocatoria = array();
$tableKeyscm1_1_Convocatoria[] = "idConvocatoria";
$tdatacm1_1_Convocatoria[".Keys"] = $tableKeyscm1_1_Convocatoria;

$tdatacm1_1_Convocatoria[".listFields"] = array();
$tdatacm1_1_Convocatoria[".listFields"][] = "Titulo";
$tdatacm1_1_Convocatoria[".listFields"][] = "Descripcion";

$tdatacm1_1_Convocatoria[".hideMobileList"] = array();


$tdatacm1_1_Convocatoria[".viewFields"] = array();
$tdatacm1_1_Convocatoria[".viewFields"][] = "idConvocatoria";
$tdatacm1_1_Convocatoria[".viewFields"][] = "Orden";
$tdatacm1_1_Convocatoria[".viewFields"][] = "EsAsamblea";
$tdatacm1_1_Convocatoria[".viewFields"][] = "Titulo";
$tdatacm1_1_Convocatoria[".viewFields"][] = "Descripcion";

$tdatacm1_1_Convocatoria[".addFields"] = array();
$tdatacm1_1_Convocatoria[".addFields"][] = "Orden";
$tdatacm1_1_Convocatoria[".addFields"][] = "EsAsamblea";
$tdatacm1_1_Convocatoria[".addFields"][] = "Titulo";
$tdatacm1_1_Convocatoria[".addFields"][] = "Descripcion";

$tdatacm1_1_Convocatoria[".masterListFields"] = array();
$tdatacm1_1_Convocatoria[".masterListFields"][] = "idConvocatoria";
$tdatacm1_1_Convocatoria[".masterListFields"][] = "Orden";
$tdatacm1_1_Convocatoria[".masterListFields"][] = "EsAsamblea";
$tdatacm1_1_Convocatoria[".masterListFields"][] = "Titulo";
$tdatacm1_1_Convocatoria[".masterListFields"][] = "Descripcion";

$tdatacm1_1_Convocatoria[".inlineAddFields"] = array();

$tdatacm1_1_Convocatoria[".editFields"] = array();
$tdatacm1_1_Convocatoria[".editFields"][] = "Orden";
$tdatacm1_1_Convocatoria[".editFields"][] = "EsAsamblea";
$tdatacm1_1_Convocatoria[".editFields"][] = "Titulo";
$tdatacm1_1_Convocatoria[".editFields"][] = "Descripcion";

$tdatacm1_1_Convocatoria[".inlineEditFields"] = array();

$tdatacm1_1_Convocatoria[".updateSelectedFields"] = array();
$tdatacm1_1_Convocatoria[".updateSelectedFields"][] = "Orden";
$tdatacm1_1_Convocatoria[".updateSelectedFields"][] = "EsAsamblea";
$tdatacm1_1_Convocatoria[".updateSelectedFields"][] = "Titulo";
$tdatacm1_1_Convocatoria[".updateSelectedFields"][] = "Descripcion";


$tdatacm1_1_Convocatoria[".exportFields"] = array();
$tdatacm1_1_Convocatoria[".exportFields"][] = "idConvocatoria";
$tdatacm1_1_Convocatoria[".exportFields"][] = "Orden";
$tdatacm1_1_Convocatoria[".exportFields"][] = "EsAsamblea";
$tdatacm1_1_Convocatoria[".exportFields"][] = "Titulo";
$tdatacm1_1_Convocatoria[".exportFields"][] = "Descripcion";

$tdatacm1_1_Convocatoria[".importFields"] = array();
$tdatacm1_1_Convocatoria[".importFields"][] = "idConvocatoria";
$tdatacm1_1_Convocatoria[".importFields"][] = "Orden";
$tdatacm1_1_Convocatoria[".importFields"][] = "EsAsamblea";
$tdatacm1_1_Convocatoria[".importFields"][] = "Titulo";
$tdatacm1_1_Convocatoria[".importFields"][] = "Descripcion";

$tdatacm1_1_Convocatoria[".printFields"] = array();
$tdatacm1_1_Convocatoria[".printFields"][] = "idConvocatoria";
$tdatacm1_1_Convocatoria[".printFields"][] = "Orden";
$tdatacm1_1_Convocatoria[".printFields"][] = "EsAsamblea";
$tdatacm1_1_Convocatoria[".printFields"][] = "Titulo";
$tdatacm1_1_Convocatoria[".printFields"][] = "Descripcion";


//	idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idConvocatoria";
	$fdata["GoodName"] = "idConvocatoria";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm1_1_Convocatoria","idConvocatoria");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_Convocatoria["idConvocatoria"] = $fdata;
//	Orden
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Orden";
	$fdata["GoodName"] = "Orden";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm1_1_Convocatoria","Orden");
	$fdata["FieldType"] = 3;

	
	
	
			
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Orden";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Orden";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_Convocatoria["Orden"] = $fdata;
//	EsAsamblea
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "EsAsamblea";
	$fdata["GoodName"] = "EsAsamblea";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm1_1_Convocatoria","EsAsamblea");
	$fdata["FieldType"] = 200;

	
	
	
			
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "EsAsamblea";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "EsAsamblea";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_Convocatoria["EsAsamblea"] = $fdata;
//	Titulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Titulo";
	$fdata["GoodName"] = "Titulo";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm1_1_Convocatoria","Titulo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Titulo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=30";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_Convocatoria["Titulo"] = $fdata;
//	Descripcion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Descripcion";
	$fdata["GoodName"] = "Descripcion";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm1_1_Convocatoria","Descripcion");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Descripcion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Descripcion";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=500";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_1_Convocatoria["Descripcion"] = $fdata;


$tables_data["cm1_1_Convocatoria"]=&$tdatacm1_1_Convocatoria;
$field_labels["cm1_1_Convocatoria"] = &$fieldLabelscm1_1_Convocatoria;
$fieldToolTips["cm1_1_Convocatoria"] = &$fieldToolTipscm1_1_Convocatoria;
$placeHolders["cm1_1_Convocatoria"] = &$placeHolderscm1_1_Convocatoria;
$page_titles["cm1_1_Convocatoria"] = &$pageTitlescm1_1_Convocatoria;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm1_1_Convocatoria"] = array();
//	cm1_grafico_escanos
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm1_grafico_escanos";
		$detailsParam["dOriginalTable"] = "convocatoria";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm1_grafico_escanos";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm1_grafico_escanos");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm1_1_Convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm1_1_Convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm1_1_Convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["cm1_1_Convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm1_1_Convocatoria"][$dIndex]["detailKeys"][]="idConvocatoria";
//	cm1_1_v_elec_muni
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm1_1_v_elec_muni";
		$detailsParam["dOriginalTable"] = "v_elec_muni";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm1_1_v_elec_muni";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm1_1_v_elec_muni");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "1";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm1_1_Convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm1_1_Convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm1_1_Convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["cm1_1_Convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm1_1_Convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";
//	cm1_1_v_elec_distri
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm1_1_v_elec_distri";
		$detailsParam["dOriginalTable"] = "v_elec_distri";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm1_1_v_elec_distri";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm1_1_v_elec_distri");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm1_1_Convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm1_1_Convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm1_1_Convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["cm1_1_Convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm1_1_Convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";

// tables which are master tables for current table (detail)
$masterTablesData["cm1_1_Convocatoria"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm1_1_Convocatoria()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "idConvocatoria,  	Orden,  	EsAsamblea,  	Titulo,  	Descripcion";
$proto3["m_strFrom"] = "FROM convocatoria";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "order by Orden";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idConvocatoria",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm1_1_Convocatoria"
));

$proto9["m_sql"] = "idConvocatoria";
$proto9["m_srcTableName"] = "cm1_1_Convocatoria";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm1_1_Convocatoria"
));

$proto11["m_sql"] = "Orden";
$proto11["m_srcTableName"] = "cm1_1_Convocatoria";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "EsAsamblea",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm1_1_Convocatoria"
));

$proto13["m_sql"] = "EsAsamblea";
$proto13["m_srcTableName"] = "cm1_1_Convocatoria";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm1_1_Convocatoria"
));

$proto15["m_sql"] = "Titulo";
$proto15["m_srcTableName"] = "cm1_1_Convocatoria";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "Descripcion",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm1_1_Convocatoria"
));

$proto17["m_sql"] = "Descripcion";
$proto17["m_srcTableName"] = "cm1_1_Convocatoria";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto19=array();
$proto19["m_link"] = "SQLL_MAIN";
			$proto20=array();
$proto20["m_strName"] = "convocatoria";
$proto20["m_srcTableName"] = "cm1_1_Convocatoria";
$proto20["m_columns"] = array();
$proto20["m_columns"][] = "idConvocatoria";
$proto20["m_columns"][] = "Orden";
$proto20["m_columns"][] = "EsAsamblea";
$proto20["m_columns"][] = "Titulo";
$proto20["m_columns"][] = "Descripcion";
$obj = new SQLTable($proto20);

$proto19["m_table"] = $obj;
$proto19["m_sql"] = "convocatoria";
$proto19["m_alias"] = "";
$proto19["m_srcTableName"] = "cm1_1_Convocatoria";
$proto21=array();
$proto21["m_sql"] = "";
$proto21["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto21["m_column"]=$obj;
$proto21["m_contained"] = array();
$proto21["m_strCase"] = "";
$proto21["m_havingmode"] = false;
$proto21["m_inBrackets"] = false;
$proto21["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto21);

$proto19["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto19);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto23=array();
						$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm1_1_Convocatoria"
));

$proto23["m_column"]=$obj;
$proto23["m_bAsc"] = 1;
$proto23["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto23);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="cm1_1_Convocatoria";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_cm1_1_Convocatoria = createSqlQuery_cm1_1_Convocatoria();


	
		;

					

$tdatacm1_1_Convocatoria[".sqlquery"] = $queryData_cm1_1_Convocatoria;

$tableEvents["cm1_1_Convocatoria"] = new eventsBase;
$tdatacm1_1_Convocatoria[".hasEvents"] = false;

?>