<?php
require_once(getabspath("classes/cipherer.php"));




$tdatav_elec_muni = array();
	$tdatav_elec_muni[".truncateText"] = true;
	$tdatav_elec_muni[".NumberOfChars"] = 80;
	$tdatav_elec_muni[".ShortName"] = "v_elec_muni";
	$tdatav_elec_muni[".OwnerID"] = "";
	$tdatav_elec_muni[".OriginalTable"] = "v_elec_muni";

//	field labels
$fieldLabelsv_elec_muni = array();
$fieldToolTipsv_elec_muni = array();
$pageTitlesv_elec_muni = array();
$placeHoldersv_elec_muni = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelsv_elec_muni["Spanish"] = array();
	$fieldToolTipsv_elec_muni["Spanish"] = array();
	$placeHoldersv_elec_muni["Spanish"] = array();
	$pageTitlesv_elec_muni["Spanish"] = array();
	$fieldLabelsv_elec_muni["Spanish"]["NumeroMunicipio"] = "Numero Municipio";
	$fieldToolTipsv_elec_muni["Spanish"]["NumeroMunicipio"] = "";
	$placeHoldersv_elec_muni["Spanish"]["NumeroMunicipio"] = "";
	$fieldLabelsv_elec_muni["Spanish"]["NombreMunicipio"] = "Municipio";
	$fieldToolTipsv_elec_muni["Spanish"]["NombreMunicipio"] = "";
	$placeHoldersv_elec_muni["Spanish"]["NombreMunicipio"] = "";
	$fieldLabelsv_elec_muni["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldToolTipsv_elec_muni["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHoldersv_elec_muni["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelsv_elec_muni["Spanish"]["Censo"] = "Censo";
	$fieldToolTipsv_elec_muni["Spanish"]["Censo"] = "";
	$placeHoldersv_elec_muni["Spanish"]["Censo"] = "";
	$fieldLabelsv_elec_muni["Spanish"]["Certificacion"] = "Votos Certificación";
	$fieldToolTipsv_elec_muni["Spanish"]["Certificacion"] = "";
	$placeHoldersv_elec_muni["Spanish"]["Certificacion"] = "";
	$fieldLabelsv_elec_muni["Spanish"]["Interventores"] = "Votos Interventores";
	$fieldToolTipsv_elec_muni["Spanish"]["Interventores"] = "";
	$placeHoldersv_elec_muni["Spanish"]["Interventores"] = "";
	$fieldLabelsv_elec_muni["Spanish"]["Nulo"] = "Votos Nulos";
	$fieldToolTipsv_elec_muni["Spanish"]["Nulo"] = "";
	$placeHoldersv_elec_muni["Spanish"]["Nulo"] = "";
	$fieldLabelsv_elec_muni["Spanish"]["Blanco"] = "Votos Blancos";
	$fieldToolTipsv_elec_muni["Spanish"]["Blanco"] = "";
	$placeHoldersv_elec_muni["Spanish"]["Blanco"] = "";
	$fieldLabelsv_elec_muni["Spanish"]["Validos"] = "Votos Válidos";
	$fieldToolTipsv_elec_muni["Spanish"]["Validos"] = "";
	$placeHoldersv_elec_muni["Spanish"]["Validos"] = "";
	$pageTitlesv_elec_muni["Spanish"]["view"] = "Votos en el Municipio de {%NombreMunicipio}";
	$pageTitlesv_elec_muni["Spanish"]["masterlist"] = "Votos del Municipio de {%NombreMunicipio}";
	$pageTitlesv_elec_muni["Spanish"]["list"] = "<strong>{%master.Titulo}</strong>, Votos por Municipio";
	if (count($fieldToolTipsv_elec_muni["Spanish"]))
		$tdatav_elec_muni[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsv_elec_muni[""] = array();
	$fieldToolTipsv_elec_muni[""] = array();
	$placeHoldersv_elec_muni[""] = array();
	$pageTitlesv_elec_muni[""] = array();
	if (count($fieldToolTipsv_elec_muni[""]))
		$tdatav_elec_muni[".isUseToolTips"] = true;
}


	$tdatav_elec_muni[".NCSearch"] = true;



$tdatav_elec_muni[".shortTableName"] = "v_elec_muni";
$tdatav_elec_muni[".nSecOptions"] = 0;
$tdatav_elec_muni[".recsPerRowList"] = 1;
$tdatav_elec_muni[".recsPerRowPrint"] = 1;
$tdatav_elec_muni[".mainTableOwnerID"] = "";
$tdatav_elec_muni[".moveNext"] = 1;
$tdatav_elec_muni[".entityType"] = 0;

$tdatav_elec_muni[".strOriginalTableName"] = "v_elec_muni";

	



$tdatav_elec_muni[".showAddInPopup"] = false;

$tdatav_elec_muni[".showEditInPopup"] = false;

$tdatav_elec_muni[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
						
	;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdatav_elec_muni[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatav_elec_muni[".fieldsForRegister"] = array();

$tdatav_elec_muni[".listAjax"] = false;

	$tdatav_elec_muni[".audit"] = false;

	$tdatav_elec_muni[".locking"] = false;



$tdatav_elec_muni[".list"] = true;



$tdatav_elec_muni[".reorderRecordsByHeader"] = true;



$tdatav_elec_muni[".view"] = true;





$tdatav_elec_muni[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatav_elec_muni[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatav_elec_muni[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatav_elec_muni[".searchSaving"] = false;
//

$tdatav_elec_muni[".showSearchPanel"] = true;
		$tdatav_elec_muni[".flexibleSearch"] = true;

$tdatav_elec_muni[".isUseAjaxSuggest"] = true;

$tdatav_elec_muni[".rowHighlite"] = true;





$tdatav_elec_muni[".ajaxCodeSnippetAdded"] = false;

$tdatav_elec_muni[".buttonsAdded"] = false;

$tdatav_elec_muni[".addPageEvents"] = false;

// use timepicker for search panel
$tdatav_elec_muni[".isUseTimeForSearch"] = false;



$tdatav_elec_muni[".badgeColor"] = "6493ea";

$tdatav_elec_muni[".detailsLinksOnList"] = "1";

$tdatav_elec_muni[".allSearchFields"] = array();
$tdatav_elec_muni[".filterFields"] = array();
$tdatav_elec_muni[".requiredSearchFields"] = array();

$tdatav_elec_muni[".allSearchFields"][] = "NombreMunicipio";
	$tdatav_elec_muni[".allSearchFields"][] = "Convocatoria_idConvocatoria";
	$tdatav_elec_muni[".allSearchFields"][] = "Censo";
	$tdatav_elec_muni[".allSearchFields"][] = "Certificacion";
	$tdatav_elec_muni[".allSearchFields"][] = "Interventores";
	$tdatav_elec_muni[".allSearchFields"][] = "Nulo";
	$tdatav_elec_muni[".allSearchFields"][] = "Blanco";
	$tdatav_elec_muni[".allSearchFields"][] = "Validos";
	

$tdatav_elec_muni[".googleLikeFields"] = array();
$tdatav_elec_muni[".googleLikeFields"][] = "NombreMunicipio";


$tdatav_elec_muni[".advSearchFields"] = array();
$tdatav_elec_muni[".advSearchFields"][] = "NombreMunicipio";

$tdatav_elec_muni[".tableType"] = "list";

$tdatav_elec_muni[".printerPageOrientation"] = 0;
$tdatav_elec_muni[".nPrinterPageScale"] = 100;

$tdatav_elec_muni[".nPrinterSplitRecords"] = 40;

$tdatav_elec_muni[".nPrinterPDFSplitRecords"] = 40;



$tdatav_elec_muni[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatav_elec_muni[".pageSize"] = 5;

$tdatav_elec_muni[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatav_elec_muni[".strOrderBy"] = $tstrOrderBy;

$tdatav_elec_muni[".orderindexes"] = array();

$tdatav_elec_muni[".sqlHead"] = "SELECT NumeroMunicipio,  	NombreMunicipio,  	Convocatoria_idConvocatoria,  	Censo,  	Certificacion,  	Interventores,  	Nulo,  	Blanco,  	Validos";
$tdatav_elec_muni[".sqlFrom"] = "FROM v_elec_muni";
$tdatav_elec_muni[".sqlWhereExpr"] = "";
$tdatav_elec_muni[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatav_elec_muni[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatav_elec_muni[".arrGroupsPerPage"] = $arrGPP;

$tdatav_elec_muni[".highlightSearchResults"] = true;

$tableKeysv_elec_muni = array();
$tableKeysv_elec_muni[] = "NumeroMunicipio";
$tableKeysv_elec_muni[] = "Convocatoria_idConvocatoria";
$tdatav_elec_muni[".Keys"] = $tableKeysv_elec_muni;

$tdatav_elec_muni[".listFields"] = array();
$tdatav_elec_muni[".listFields"][] = "NombreMunicipio";
$tdatav_elec_muni[".listFields"][] = "Censo";
$tdatav_elec_muni[".listFields"][] = "Certificacion";
$tdatav_elec_muni[".listFields"][] = "Interventores";
$tdatav_elec_muni[".listFields"][] = "Nulo";
$tdatav_elec_muni[".listFields"][] = "Blanco";
$tdatav_elec_muni[".listFields"][] = "Validos";

$tdatav_elec_muni[".hideMobileList"] = array();


$tdatav_elec_muni[".viewFields"] = array();
$tdatav_elec_muni[".viewFields"][] = "Censo";
$tdatav_elec_muni[".viewFields"][] = "Certificacion";
$tdatav_elec_muni[".viewFields"][] = "Interventores";
$tdatav_elec_muni[".viewFields"][] = "Nulo";
$tdatav_elec_muni[".viewFields"][] = "Blanco";
$tdatav_elec_muni[".viewFields"][] = "Validos";

$tdatav_elec_muni[".addFields"] = array();

$tdatav_elec_muni[".masterListFields"] = array();
$tdatav_elec_muni[".masterListFields"][] = "Censo";
$tdatav_elec_muni[".masterListFields"][] = "Certificacion";
$tdatav_elec_muni[".masterListFields"][] = "Interventores";
$tdatav_elec_muni[".masterListFields"][] = "Nulo";
$tdatav_elec_muni[".masterListFields"][] = "Blanco";
$tdatav_elec_muni[".masterListFields"][] = "Validos";

$tdatav_elec_muni[".inlineAddFields"] = array();

$tdatav_elec_muni[".editFields"] = array();

$tdatav_elec_muni[".inlineEditFields"] = array();

$tdatav_elec_muni[".updateSelectedFields"] = array();


$tdatav_elec_muni[".exportFields"] = array();

$tdatav_elec_muni[".importFields"] = array();

$tdatav_elec_muni[".printFields"] = array();


//	NumeroMunicipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "NumeroMunicipio";
	$fdata["GoodName"] = "NumeroMunicipio";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("v_elec_muni","NumeroMunicipio");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "NumeroMunicipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NumeroMunicipio";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_elec_muni["NumeroMunicipio"] = $fdata;
//	NombreMunicipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "NombreMunicipio";
	$fdata["GoodName"] = "NombreMunicipio";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("v_elec_muni","NombreMunicipio");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "NombreMunicipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NombreMunicipio";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_muni["NombreMunicipio"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("v_elec_muni","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_muni["Convocatoria_idConvocatoria"] = $fdata;
//	Censo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Censo";
	$fdata["GoodName"] = "Censo";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("v_elec_muni","Censo");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Censo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Censo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_muni["Censo"] = $fdata;
//	Certificacion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Certificacion";
	$fdata["GoodName"] = "Certificacion";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("v_elec_muni","Certificacion");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Certificacion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Certificacion";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_muni["Certificacion"] = $fdata;
//	Interventores
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Interventores";
	$fdata["GoodName"] = "Interventores";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("v_elec_muni","Interventores");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Interventores";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Interventores";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_muni["Interventores"] = $fdata;
//	Nulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Nulo";
	$fdata["GoodName"] = "Nulo";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("v_elec_muni","Nulo");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Nulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Nulo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_muni["Nulo"] = $fdata;
//	Blanco
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "Blanco";
	$fdata["GoodName"] = "Blanco";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("v_elec_muni","Blanco");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Blanco";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Blanco";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_muni["Blanco"] = $fdata;
//	Validos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "Validos";
	$fdata["GoodName"] = "Validos";
	$fdata["ownerTable"] = "v_elec_muni";
	$fdata["Label"] = GetFieldLabel("v_elec_muni","Validos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Validos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Validos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatav_elec_muni["Validos"] = $fdata;


$tables_data["v_elec_muni"]=&$tdatav_elec_muni;
$field_labels["v_elec_muni"] = &$fieldLabelsv_elec_muni;
$fieldToolTips["v_elec_muni"] = &$fieldToolTipsv_elec_muni;
$placeHolders["v_elec_muni"] = &$placeHoldersv_elec_muni;
$page_titles["v_elec_muni"] = &$pageTitlesv_elec_muni;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["v_elec_muni"] = array();
//	v_elec_distri
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="v_elec_distri";
		$detailsParam["dOriginalTable"] = "v_elec_distri";
		$detailsParam["proceedLink"] = false;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "v_elec_distri";
	$detailsParam["dCaptionTable"] = GetTableCaption("v_elec_distri");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = true;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["v_elec_muni"][$dIndex] = $detailsParam;

	
		$detailsTablesData["v_elec_muni"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["v_elec_muni"][$dIndex]["masterKeys"][]="Convocatoria_idConvocatoria";

	$detailsTablesData["v_elec_muni"][$dIndex]["masterKeys"][]="NumeroMunicipio";

				$detailsTablesData["v_elec_muni"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["v_elec_muni"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";

		
	$detailsTablesData["v_elec_muni"][$dIndex]["detailKeys"][]="NumeroMunicipio";
//	cm1_municipio_voto Chart
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm1_municipio_voto Chart";
		$detailsParam["dOriginalTable"] = "v_municipio_voto";
		$detailsParam["proceedLink"] = false;
			$detailsParam["dType"]=PAGE_CHART;
		$detailsParam["dShortTable"] = "cm1_municipio_voto_Chart";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm1_municipio_voto_Chart");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "2";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 1;
		
	$detailsTablesData["v_elec_muni"][$dIndex] = $detailsParam;

	
		$detailsTablesData["v_elec_muni"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["v_elec_muni"][$dIndex]["masterKeys"][]="Convocatoria_idConvocatoria";

	$detailsTablesData["v_elec_muni"][$dIndex]["masterKeys"][]="NumeroMunicipio";

				$detailsTablesData["v_elec_muni"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["v_elec_muni"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";

		
	$detailsTablesData["v_elec_muni"][$dIndex]["detailKeys"][]="Municipio";

// tables which are master tables for current table (detail)
$masterTablesData["v_elec_muni"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
					
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["v_elec_muni"][0] = $masterParams;
				$masterTablesData["v_elec_muni"][0]["masterKeys"] = array();
	$masterTablesData["v_elec_muni"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["v_elec_muni"][0]["detailKeys"] = array();
	$masterTablesData["v_elec_muni"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_v_elec_muni()
{
$proto1=array();
$proto1["m_strHead"] = "SELECT";
$proto1["m_strFieldList"] = "NumeroMunicipio,  	NombreMunicipio,  	Convocatoria_idConvocatoria,  	Censo,  	Certificacion,  	Interventores,  	Nulo,  	Blanco,  	Validos";
$proto1["m_strFrom"] = "FROM v_elec_muni";
$proto1["m_strWhere"] = "";
$proto1["m_strOrderBy"] = "";
	
		;
			$proto1["cipherer"] = null;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto1["m_where"] = $obj;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto1["m_having"] = $obj;
$proto1["m_fieldlist"] = array();
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "NumeroMunicipio",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "v_elec_muni"
));

$proto7["m_sql"] = "NumeroMunicipio";
$proto7["m_srcTableName"] = "v_elec_muni";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto1["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "NombreMunicipio",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "v_elec_muni"
));

$proto9["m_sql"] = "NombreMunicipio";
$proto9["m_srcTableName"] = "v_elec_muni";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto1["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "v_elec_muni"
));

$proto11["m_sql"] = "Convocatoria_idConvocatoria";
$proto11["m_srcTableName"] = "v_elec_muni";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto1["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "Censo",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "v_elec_muni"
));

$proto13["m_sql"] = "Censo";
$proto13["m_srcTableName"] = "v_elec_muni";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto1["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Certificacion",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "v_elec_muni"
));

$proto15["m_sql"] = "Certificacion";
$proto15["m_srcTableName"] = "v_elec_muni";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto1["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "Interventores",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "v_elec_muni"
));

$proto17["m_sql"] = "Interventores";
$proto17["m_srcTableName"] = "v_elec_muni";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto1["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "Nulo",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "v_elec_muni"
));

$proto19["m_sql"] = "Nulo";
$proto19["m_srcTableName"] = "v_elec_muni";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto1["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "Blanco",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "v_elec_muni"
));

$proto21["m_sql"] = "Blanco";
$proto21["m_srcTableName"] = "v_elec_muni";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto1["m_fieldlist"][]=$obj;
						$proto23=array();
			$obj = new SQLField(array(
	"m_strName" => "Validos",
	"m_strTable" => "v_elec_muni",
	"m_srcTableName" => "v_elec_muni"
));

$proto23["m_sql"] = "Validos";
$proto23["m_srcTableName"] = "v_elec_muni";
$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "";
$obj = new SQLFieldListItem($proto23);

$proto1["m_fieldlist"][]=$obj;
$proto1["m_fromlist"] = array();
												$proto25=array();
$proto25["m_link"] = "SQLL_MAIN";
			$proto26=array();
$proto26["m_strName"] = "v_elec_muni";
$proto26["m_srcTableName"] = "v_elec_muni";
$proto26["m_columns"] = array();
$proto26["m_columns"][] = "NumeroMunicipio";
$proto26["m_columns"][] = "NombreMunicipio";
$proto26["m_columns"][] = "Convocatoria_idConvocatoria";
$proto26["m_columns"][] = "Censo";
$proto26["m_columns"][] = "Certificacion";
$proto26["m_columns"][] = "Interventores";
$proto26["m_columns"][] = "Nulo";
$proto26["m_columns"][] = "Blanco";
$proto26["m_columns"][] = "Validos";
$obj = new SQLTable($proto26);

$proto25["m_table"] = $obj;
$proto25["m_sql"] = "v_elec_muni";
$proto25["m_alias"] = "";
$proto25["m_srcTableName"] = "v_elec_muni";
$proto27=array();
$proto27["m_sql"] = "";
$proto27["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto27["m_column"]=$obj;
$proto27["m_contained"] = array();
$proto27["m_strCase"] = "";
$proto27["m_havingmode"] = false;
$proto27["m_inBrackets"] = false;
$proto27["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto27);

$proto25["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto25);

$proto1["m_fromlist"][]=$obj;
$proto1["m_groupby"] = array();
$proto1["m_orderby"] = array();
$proto1["m_srcTableName"]="v_elec_muni";		
$obj = new SQLQuery($proto1);

	return $obj;
}
$queryData_v_elec_muni = createSqlQuery_v_elec_muni();


	
		;

									

$tdatav_elec_muni[".sqlquery"] = $queryData_v_elec_muni;

$tableEvents["v_elec_muni"] = new eventsBase;
$tdatav_elec_muni[".hasEvents"] = false;

?>