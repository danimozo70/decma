<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm2_convocatoria = array();
	$tdatacm2_convocatoria[".truncateText"] = true;
	$tdatacm2_convocatoria[".NumberOfChars"] = 80;
	$tdatacm2_convocatoria[".ShortName"] = "cm2_convocatoria";
	$tdatacm2_convocatoria[".OwnerID"] = "";
	$tdatacm2_convocatoria[".OriginalTable"] = "convocatoria";

//	field labels
$fieldLabelscm2_convocatoria = array();
$fieldToolTipscm2_convocatoria = array();
$pageTitlescm2_convocatoria = array();
$placeHolderscm2_convocatoria = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm2_convocatoria["Spanish"] = array();
	$fieldToolTipscm2_convocatoria["Spanish"] = array();
	$placeHolderscm2_convocatoria["Spanish"] = array();
	$pageTitlescm2_convocatoria["Spanish"] = array();
	$fieldLabelscm2_convocatoria["Spanish"]["idConvocatoria"] = "Id Interno";
	$fieldToolTipscm2_convocatoria["Spanish"]["idConvocatoria"] = "";
	$placeHolderscm2_convocatoria["Spanish"]["idConvocatoria"] = "";
	$fieldLabelscm2_convocatoria["Spanish"]["Orden"] = "Orden";
	$fieldToolTipscm2_convocatoria["Spanish"]["Orden"] = "";
	$placeHolderscm2_convocatoria["Spanish"]["Orden"] = "";
	$fieldLabelscm2_convocatoria["Spanish"]["EsAsamblea"] = "Es Asamblea?";
	$fieldToolTipscm2_convocatoria["Spanish"]["EsAsamblea"] = "";
	$placeHolderscm2_convocatoria["Spanish"]["EsAsamblea"] = "";
	$fieldLabelscm2_convocatoria["Spanish"]["Titulo"] = "Código";
	$fieldToolTipscm2_convocatoria["Spanish"]["Titulo"] = "";
	$placeHolderscm2_convocatoria["Spanish"]["Titulo"] = "";
	$fieldLabelscm2_convocatoria["Spanish"]["Descripcion"] = "Descripción";
	$fieldToolTipscm2_convocatoria["Spanish"]["Descripcion"] = "";
	$placeHolderscm2_convocatoria["Spanish"]["Descripcion"] = "";
	if (count($fieldToolTipscm2_convocatoria["Spanish"]))
		$tdatacm2_convocatoria[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm2_convocatoria[""] = array();
	$fieldToolTipscm2_convocatoria[""] = array();
	$placeHolderscm2_convocatoria[""] = array();
	$pageTitlescm2_convocatoria[""] = array();
	if (count($fieldToolTipscm2_convocatoria[""]))
		$tdatacm2_convocatoria[".isUseToolTips"] = true;
}


	$tdatacm2_convocatoria[".NCSearch"] = true;



$tdatacm2_convocatoria[".shortTableName"] = "cm2_convocatoria";
$tdatacm2_convocatoria[".nSecOptions"] = 0;
$tdatacm2_convocatoria[".recsPerRowList"] = 1;
$tdatacm2_convocatoria[".recsPerRowPrint"] = 1;
$tdatacm2_convocatoria[".mainTableOwnerID"] = "";
$tdatacm2_convocatoria[".moveNext"] = 1;
$tdatacm2_convocatoria[".entityType"] = 1;

$tdatacm2_convocatoria[".strOriginalTableName"] = "convocatoria";

	



$tdatacm2_convocatoria[".showAddInPopup"] = true;

$tdatacm2_convocatoria[".showEditInPopup"] = true;

$tdatacm2_convocatoria[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["add"] = "view_bootstrap";
			;
$popupPagesLayoutNames["edit"] = "view_bootstrap";
			;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdatacm2_convocatoria[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm2_convocatoria[".fieldsForRegister"] = array();

$tdatacm2_convocatoria[".listAjax"] = false;

	$tdatacm2_convocatoria[".audit"] = false;

	$tdatacm2_convocatoria[".locking"] = false;



$tdatacm2_convocatoria[".list"] = true;











$tdatacm2_convocatoria[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm2_convocatoria[".allowShowHideFields"] = true;
//

// Allow Fields Reordering in GRID
$tdatacm2_convocatoria[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm2_convocatoria[".searchSaving"] = false;
//

$tdatacm2_convocatoria[".showSearchPanel"] = true;
		$tdatacm2_convocatoria[".flexibleSearch"] = true;

$tdatacm2_convocatoria[".isUseAjaxSuggest"] = true;

$tdatacm2_convocatoria[".rowHighlite"] = true;





$tdatacm2_convocatoria[".ajaxCodeSnippetAdded"] = false;

$tdatacm2_convocatoria[".buttonsAdded"] = false;

$tdatacm2_convocatoria[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm2_convocatoria[".isUseTimeForSearch"] = false;



$tdatacm2_convocatoria[".badgeColor"] = "9ACD32";


$tdatacm2_convocatoria[".allSearchFields"] = array();
$tdatacm2_convocatoria[".filterFields"] = array();
$tdatacm2_convocatoria[".requiredSearchFields"] = array();



$tdatacm2_convocatoria[".googleLikeFields"] = array();
$tdatacm2_convocatoria[".googleLikeFields"][] = "idConvocatoria";
$tdatacm2_convocatoria[".googleLikeFields"][] = "Orden";
$tdatacm2_convocatoria[".googleLikeFields"][] = "EsAsamblea";
$tdatacm2_convocatoria[".googleLikeFields"][] = "Titulo";
$tdatacm2_convocatoria[".googleLikeFields"][] = "Descripcion";



$tdatacm2_convocatoria[".tableType"] = "list";

$tdatacm2_convocatoria[".printerPageOrientation"] = 0;
$tdatacm2_convocatoria[".nPrinterPageScale"] = 100;

$tdatacm2_convocatoria[".nPrinterSplitRecords"] = 40;

$tdatacm2_convocatoria[".nPrinterPDFSplitRecords"] = 40;



$tdatacm2_convocatoria[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatacm2_convocatoria[".pageSize"] = 10;

$tdatacm2_convocatoria[".warnLeavingPages"] = true;



$tstrOrderBy = "order by Orden";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm2_convocatoria[".strOrderBy"] = $tstrOrderBy;

$tdatacm2_convocatoria[".orderindexes"] = array();
	$tdatacm2_convocatoria[".orderindexes"][] = array(2, (1 ? "ASC" : "DESC"), "Orden");


$tdatacm2_convocatoria[".sqlHead"] = "SELECT idConvocatoria,  	Orden,  	EsAsamblea,  	Titulo,  	Descripcion";
$tdatacm2_convocatoria[".sqlFrom"] = "FROM convocatoria";
$tdatacm2_convocatoria[".sqlWhereExpr"] = "";
$tdatacm2_convocatoria[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm2_convocatoria[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm2_convocatoria[".arrGroupsPerPage"] = $arrGPP;

$tdatacm2_convocatoria[".highlightSearchResults"] = true;

$tableKeyscm2_convocatoria = array();
$tableKeyscm2_convocatoria[] = "idConvocatoria";
$tdatacm2_convocatoria[".Keys"] = $tableKeyscm2_convocatoria;

$tdatacm2_convocatoria[".listFields"] = array();
$tdatacm2_convocatoria[".listFields"][] = "Titulo";
$tdatacm2_convocatoria[".listFields"][] = "Descripcion";

$tdatacm2_convocatoria[".hideMobileList"] = array();


$tdatacm2_convocatoria[".viewFields"] = array();

$tdatacm2_convocatoria[".addFields"] = array();

$tdatacm2_convocatoria[".masterListFields"] = array();

$tdatacm2_convocatoria[".inlineAddFields"] = array();

$tdatacm2_convocatoria[".editFields"] = array();

$tdatacm2_convocatoria[".inlineEditFields"] = array();

$tdatacm2_convocatoria[".updateSelectedFields"] = array();


$tdatacm2_convocatoria[".exportFields"] = array();

$tdatacm2_convocatoria[".importFields"] = array();

$tdatacm2_convocatoria[".printFields"] = array();


//	idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idConvocatoria";
	$fdata["GoodName"] = "idConvocatoria";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm2_convocatoria","idConvocatoria");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idConvocatoria";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_convocatoria["idConvocatoria"] = $fdata;
//	Orden
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Orden";
	$fdata["GoodName"] = "Orden";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm2_convocatoria","Orden");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Orden";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Orden";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_convocatoria["Orden"] = $fdata;
//	EsAsamblea
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "EsAsamblea";
	$fdata["GoodName"] = "EsAsamblea";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm2_convocatoria","EsAsamblea");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "EsAsamblea";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "EsAsamblea";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_convocatoria["EsAsamblea"] = $fdata;
//	Titulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Titulo";
	$fdata["GoodName"] = "Titulo";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm2_convocatoria","Titulo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Titulo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=30";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_convocatoria["Titulo"] = $fdata;
//	Descripcion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Descripcion";
	$fdata["GoodName"] = "Descripcion";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm2_convocatoria","Descripcion");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Descripcion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Descripcion";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=500";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_convocatoria["Descripcion"] = $fdata;


$tables_data["cm2_convocatoria"]=&$tdatacm2_convocatoria;
$field_labels["cm2_convocatoria"] = &$fieldLabelscm2_convocatoria;
$fieldToolTips["cm2_convocatoria"] = &$fieldToolTipscm2_convocatoria;
$placeHolders["cm2_convocatoria"] = &$placeHolderscm2_convocatoria;
$page_titles["cm2_convocatoria"] = &$pageTitlescm2_convocatoria;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm2_convocatoria"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["cm2_convocatoria"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm2_convocatoria()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "idConvocatoria,  	Orden,  	EsAsamblea,  	Titulo,  	Descripcion";
$proto3["m_strFrom"] = "FROM convocatoria";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "order by Orden";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idConvocatoria",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm2_convocatoria"
));

$proto9["m_sql"] = "idConvocatoria";
$proto9["m_srcTableName"] = "cm2_convocatoria";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm2_convocatoria"
));

$proto11["m_sql"] = "Orden";
$proto11["m_srcTableName"] = "cm2_convocatoria";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "EsAsamblea",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm2_convocatoria"
));

$proto13["m_sql"] = "EsAsamblea";
$proto13["m_srcTableName"] = "cm2_convocatoria";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm2_convocatoria"
));

$proto15["m_sql"] = "Titulo";
$proto15["m_srcTableName"] = "cm2_convocatoria";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "Descripcion",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm2_convocatoria"
));

$proto17["m_sql"] = "Descripcion";
$proto17["m_srcTableName"] = "cm2_convocatoria";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto19=array();
$proto19["m_link"] = "SQLL_MAIN";
			$proto20=array();
$proto20["m_strName"] = "convocatoria";
$proto20["m_srcTableName"] = "cm2_convocatoria";
$proto20["m_columns"] = array();
$proto20["m_columns"][] = "idConvocatoria";
$proto20["m_columns"][] = "Orden";
$proto20["m_columns"][] = "EsAsamblea";
$proto20["m_columns"][] = "Titulo";
$proto20["m_columns"][] = "Descripcion";
$obj = new SQLTable($proto20);

$proto19["m_table"] = $obj;
$proto19["m_sql"] = "convocatoria";
$proto19["m_alias"] = "";
$proto19["m_srcTableName"] = "cm2_convocatoria";
$proto21=array();
$proto21["m_sql"] = "";
$proto21["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto21["m_column"]=$obj;
$proto21["m_contained"] = array();
$proto21["m_strCase"] = "";
$proto21["m_havingmode"] = false;
$proto21["m_inBrackets"] = false;
$proto21["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto21);

$proto19["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto19);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto23=array();
						$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm2_convocatoria"
));

$proto23["m_column"]=$obj;
$proto23["m_bAsc"] = 1;
$proto23["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto23);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="cm2_convocatoria";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_cm2_convocatoria = createSqlQuery_cm2_convocatoria();


	
		;

					

$tdatacm2_convocatoria[".sqlquery"] = $queryData_cm2_convocatoria;

$tableEvents["cm2_convocatoria"] = new eventsBase;
$tdatacm2_convocatoria[".hasEvents"] = false;

?>