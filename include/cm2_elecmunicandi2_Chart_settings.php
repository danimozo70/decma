<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm2_elecmunicandi2_Chart = array();
	$tdatacm2_elecmunicandi2_Chart[".ShortName"] = "cm2_elecmunicandi2_Chart";
	$tdatacm2_elecmunicandi2_Chart[".OwnerID"] = "";
	$tdatacm2_elecmunicandi2_Chart[".OriginalTable"] = "elecmunicandi";

//	field labels
$fieldLabelscm2_elecmunicandi2_Chart = array();
$fieldToolTipscm2_elecmunicandi2_Chart = array();
$pageTitlescm2_elecmunicandi2_Chart = array();
$placeHolderscm2_elecmunicandi2_Chart = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm2_elecmunicandi2_Chart["Spanish"] = array();
	$fieldToolTipscm2_elecmunicandi2_Chart["Spanish"] = array();
	$placeHolderscm2_elecmunicandi2_Chart["Spanish"] = array();
	$pageTitlescm2_elecmunicandi2_Chart["Spanish"] = array();
	$fieldLabelscm2_elecmunicandi2_Chart["Spanish"]["idElecMuniCandi"] = "Id Elec Muni Candi";
	$fieldToolTipscm2_elecmunicandi2_Chart["Spanish"]["idElecMuniCandi"] = "";
	$placeHolderscm2_elecmunicandi2_Chart["Spanish"]["idElecMuniCandi"] = "";
	$fieldLabelscm2_elecmunicandi2_Chart["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria IdConvocatoria";
	$fieldToolTipscm2_elecmunicandi2_Chart["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderscm2_elecmunicandi2_Chart["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelscm2_elecmunicandi2_Chart["Spanish"]["Candidatura_idCandidatura"] = "Candidatura IdCandidatura";
	$fieldToolTipscm2_elecmunicandi2_Chart["Spanish"]["Candidatura_idCandidatura"] = "";
	$placeHolderscm2_elecmunicandi2_Chart["Spanish"]["Candidatura_idCandidatura"] = "";
	$fieldLabelscm2_elecmunicandi2_Chart["Spanish"]["Municipio"] = "Municipio";
	$fieldToolTipscm2_elecmunicandi2_Chart["Spanish"]["Municipio"] = "";
	$placeHolderscm2_elecmunicandi2_Chart["Spanish"]["Municipio"] = "";
	$fieldLabelscm2_elecmunicandi2_Chart["Spanish"]["Votos"] = "";
	$fieldToolTipscm2_elecmunicandi2_Chart["Spanish"]["Votos"] = "";
	$placeHolderscm2_elecmunicandi2_Chart["Spanish"]["Votos"] = "";
	$fieldLabelscm2_elecmunicandi2_Chart["Spanish"]["PorcVotos"] = "";
	$fieldToolTipscm2_elecmunicandi2_Chart["Spanish"]["PorcVotos"] = "";
	$placeHolderscm2_elecmunicandi2_Chart["Spanish"]["PorcVotos"] = "";
	$pageTitlescm2_elecmunicandi2_Chart["Spanish"]["chart"] = "Total votos del municipio seleccionado";
	if (count($fieldToolTipscm2_elecmunicandi2_Chart["Spanish"]))
		$tdatacm2_elecmunicandi2_Chart[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm2_elecmunicandi2_Chart[""] = array();
	$fieldToolTipscm2_elecmunicandi2_Chart[""] = array();
	$placeHolderscm2_elecmunicandi2_Chart[""] = array();
	$pageTitlescm2_elecmunicandi2_Chart[""] = array();
	if (count($fieldToolTipscm2_elecmunicandi2_Chart[""]))
		$tdatacm2_elecmunicandi2_Chart[".isUseToolTips"] = true;
}


	$tdatacm2_elecmunicandi2_Chart[".NCSearch"] = true;

	$tdatacm2_elecmunicandi2_Chart[".ChartRefreshTime"] = 0;


$tdatacm2_elecmunicandi2_Chart[".shortTableName"] = "cm2_elecmunicandi2_Chart";
$tdatacm2_elecmunicandi2_Chart[".nSecOptions"] = 0;
$tdatacm2_elecmunicandi2_Chart[".recsPerRowPrint"] = 1;
$tdatacm2_elecmunicandi2_Chart[".mainTableOwnerID"] = "";
$tdatacm2_elecmunicandi2_Chart[".moveNext"] = 1;
$tdatacm2_elecmunicandi2_Chart[".entityType"] = 3;

$tdatacm2_elecmunicandi2_Chart[".strOriginalTableName"] = "elecmunicandi";

	



$tdatacm2_elecmunicandi2_Chart[".showAddInPopup"] = false;

$tdatacm2_elecmunicandi2_Chart[".showEditInPopup"] = false;

$tdatacm2_elecmunicandi2_Chart[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacm2_elecmunicandi2_Chart[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm2_elecmunicandi2_Chart[".fieldsForRegister"] = array();

$tdatacm2_elecmunicandi2_Chart[".listAjax"] = false;

	$tdatacm2_elecmunicandi2_Chart[".audit"] = false;

	$tdatacm2_elecmunicandi2_Chart[".locking"] = false;

$tdatacm2_elecmunicandi2_Chart[".edit"] = true;
$tdatacm2_elecmunicandi2_Chart[".afterEditAction"] = 1;
$tdatacm2_elecmunicandi2_Chart[".closePopupAfterEdit"] = 1;
$tdatacm2_elecmunicandi2_Chart[".afterEditActionDetTable"] = "";

$tdatacm2_elecmunicandi2_Chart[".add"] = true;
$tdatacm2_elecmunicandi2_Chart[".afterAddAction"] = 1;
$tdatacm2_elecmunicandi2_Chart[".closePopupAfterAdd"] = 1;
$tdatacm2_elecmunicandi2_Chart[".afterAddActionDetTable"] = "";

$tdatacm2_elecmunicandi2_Chart[".list"] = true;



$tdatacm2_elecmunicandi2_Chart[".reorderRecordsByHeader"] = true;



$tdatacm2_elecmunicandi2_Chart[".view"] = true;




$tdatacm2_elecmunicandi2_Chart[".delete"] = true;

$tdatacm2_elecmunicandi2_Chart[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm2_elecmunicandi2_Chart[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm2_elecmunicandi2_Chart[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm2_elecmunicandi2_Chart[".searchSaving"] = false;
//

$tdatacm2_elecmunicandi2_Chart[".showSearchPanel"] = true;
		$tdatacm2_elecmunicandi2_Chart[".flexibleSearch"] = true;

$tdatacm2_elecmunicandi2_Chart[".isUseAjaxSuggest"] = true;






$tdatacm2_elecmunicandi2_Chart[".ajaxCodeSnippetAdded"] = false;

$tdatacm2_elecmunicandi2_Chart[".buttonsAdded"] = false;

$tdatacm2_elecmunicandi2_Chart[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm2_elecmunicandi2_Chart[".isUseTimeForSearch"] = false;



$tdatacm2_elecmunicandi2_Chart[".badgeColor"] = "4169e1";


$tdatacm2_elecmunicandi2_Chart[".allSearchFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".filterFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".requiredSearchFields"] = array();

$tdatacm2_elecmunicandi2_Chart[".allSearchFields"][] = "idElecMuniCandi";
	$tdatacm2_elecmunicandi2_Chart[".allSearchFields"][] = "Convocatoria_idConvocatoria";
	$tdatacm2_elecmunicandi2_Chart[".allSearchFields"][] = "Candidatura_idCandidatura";
	$tdatacm2_elecmunicandi2_Chart[".allSearchFields"][] = "Municipio";
	$tdatacm2_elecmunicandi2_Chart[".allSearchFields"][] = "Votos";
	$tdatacm2_elecmunicandi2_Chart[".allSearchFields"][] = "PorcVotos";
	

$tdatacm2_elecmunicandi2_Chart[".googleLikeFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".googleLikeFields"][] = "idElecMuniCandi";
$tdatacm2_elecmunicandi2_Chart[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".googleLikeFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".googleLikeFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".googleLikeFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".googleLikeFields"][] = "PorcVotos";


$tdatacm2_elecmunicandi2_Chart[".advSearchFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".advSearchFields"][] = "idElecMuniCandi";
$tdatacm2_elecmunicandi2_Chart[".advSearchFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".advSearchFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".advSearchFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".advSearchFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".advSearchFields"][] = "PorcVotos";

$tdatacm2_elecmunicandi2_Chart[".tableType"] = "chart";

$tdatacm2_elecmunicandi2_Chart[".printerPageOrientation"] = 0;
$tdatacm2_elecmunicandi2_Chart[".nPrinterPageScale"] = 100;

$tdatacm2_elecmunicandi2_Chart[".nPrinterSplitRecords"] = 40;

$tdatacm2_elecmunicandi2_Chart[".nPrinterPDFSplitRecords"] = 40;



$tdatacm2_elecmunicandi2_Chart[".geocodingEnabled"] = false;



// chart settings
$tdatacm2_elecmunicandi2_Chart[".chartType"] = "2DBar";
// end of chart settings


$tdatacm2_elecmunicandi2_Chart[".listGridLayout"] = 3;





// view page pdf

// print page pdf



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm2_elecmunicandi2_Chart[".strOrderBy"] = $tstrOrderBy;

$tdatacm2_elecmunicandi2_Chart[".orderindexes"] = array();

$tdatacm2_elecmunicandi2_Chart[".sqlHead"] = "SELECT idElecMuniCandi,  	Convocatoria_idConvocatoria,  	Candidatura_idCandidatura,  	Municipio,  	Votos,  	round(PorcVotos,1) PorcVotos";
$tdatacm2_elecmunicandi2_Chart[".sqlFrom"] = "FROM elecmunicandi";
$tdatacm2_elecmunicandi2_Chart[".sqlWhereExpr"] = "";
$tdatacm2_elecmunicandi2_Chart[".sqlTail"] = "";

//fill array of tabs for list page
$arrGridTabs = array();
$arrGridTabs[] = array(
	'tabId' => "",
	'name' => "All data",
	'nameType' => 'Text',
	'where' => "",	
	'showRowCount' => 0,
	'hideEmpty' => 0,	
);				  
$tdatacm2_elecmunicandi2_Chart[".arrGridTabs"] = $arrGridTabs;











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm2_elecmunicandi2_Chart[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm2_elecmunicandi2_Chart[".arrGroupsPerPage"] = $arrGPP;

$tdatacm2_elecmunicandi2_Chart[".highlightSearchResults"] = true;

$tableKeyscm2_elecmunicandi2_Chart = array();
$tableKeyscm2_elecmunicandi2_Chart[] = "idElecMuniCandi";
$tdatacm2_elecmunicandi2_Chart[".Keys"] = $tableKeyscm2_elecmunicandi2_Chart;

$tdatacm2_elecmunicandi2_Chart[".listFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".listFields"][] = "idElecMuniCandi";
$tdatacm2_elecmunicandi2_Chart[".listFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".listFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".listFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".listFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".listFields"][] = "PorcVotos";

$tdatacm2_elecmunicandi2_Chart[".hideMobileList"] = array();


$tdatacm2_elecmunicandi2_Chart[".viewFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".viewFields"][] = "idElecMuniCandi";
$tdatacm2_elecmunicandi2_Chart[".viewFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".viewFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".viewFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".viewFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".viewFields"][] = "PorcVotos";

$tdatacm2_elecmunicandi2_Chart[".addFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".addFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".addFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".addFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".addFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".addFields"][] = "PorcVotos";

$tdatacm2_elecmunicandi2_Chart[".masterListFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".masterListFields"][] = "idElecMuniCandi";
$tdatacm2_elecmunicandi2_Chart[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".masterListFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".masterListFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".masterListFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".masterListFields"][] = "PorcVotos";

$tdatacm2_elecmunicandi2_Chart[".inlineAddFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".inlineAddFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".inlineAddFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".inlineAddFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".inlineAddFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".inlineAddFields"][] = "PorcVotos";

$tdatacm2_elecmunicandi2_Chart[".editFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".editFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".editFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".editFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".editFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".editFields"][] = "PorcVotos";

$tdatacm2_elecmunicandi2_Chart[".inlineEditFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".inlineEditFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".inlineEditFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".inlineEditFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".inlineEditFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".inlineEditFields"][] = "PorcVotos";

$tdatacm2_elecmunicandi2_Chart[".updateSelectedFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".updateSelectedFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".updateSelectedFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".updateSelectedFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".updateSelectedFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".updateSelectedFields"][] = "PorcVotos";


$tdatacm2_elecmunicandi2_Chart[".exportFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".exportFields"][] = "idElecMuniCandi";
$tdatacm2_elecmunicandi2_Chart[".exportFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".exportFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".exportFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".exportFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".exportFields"][] = "PorcVotos";

$tdatacm2_elecmunicandi2_Chart[".importFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".importFields"][] = "idElecMuniCandi";
$tdatacm2_elecmunicandi2_Chart[".importFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".importFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".importFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".importFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".importFields"][] = "PorcVotos";

$tdatacm2_elecmunicandi2_Chart[".printFields"] = array();
$tdatacm2_elecmunicandi2_Chart[".printFields"][] = "idElecMuniCandi";
$tdatacm2_elecmunicandi2_Chart[".printFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_elecmunicandi2_Chart[".printFields"][] = "Candidatura_idCandidatura";
$tdatacm2_elecmunicandi2_Chart[".printFields"][] = "Municipio";
$tdatacm2_elecmunicandi2_Chart[".printFields"][] = "Votos";
$tdatacm2_elecmunicandi2_Chart[".printFields"][] = "PorcVotos";


//	idElecMuniCandi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idElecMuniCandi";
	$fdata["GoodName"] = "idElecMuniCandi";
	$fdata["ownerTable"] = "elecmunicandi";
	$fdata["Label"] = GetFieldLabel("cm2_elecmunicandi2_Chart","idElecMuniCandi");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idElecMuniCandi";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idElecMuniCandi";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_elecmunicandi2_Chart["idElecMuniCandi"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "elecmunicandi";
	$fdata["Label"] = GetFieldLabel("cm2_elecmunicandi2_Chart","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_elecmunicandi2_Chart["Convocatoria_idConvocatoria"] = $fdata;
//	Candidatura_idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Candidatura_idCandidatura";
	$fdata["GoodName"] = "Candidatura_idCandidatura";
	$fdata["ownerTable"] = "elecmunicandi";
	$fdata["Label"] = GetFieldLabel("cm2_elecmunicandi2_Chart","Candidatura_idCandidatura");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Candidatura_idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Candidatura_idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "candidatura";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idCandidatura";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "concat(Codigo,' - ',Titulo)";
	
	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_elecmunicandi2_Chart["Candidatura_idCandidatura"] = $fdata;
//	Municipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Municipio";
	$fdata["GoodName"] = "Municipio";
	$fdata["ownerTable"] = "elecmunicandi";
	$fdata["Label"] = GetFieldLabel("cm2_elecmunicandi2_Chart","Municipio");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Municipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Municipio";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "municipio";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "NumeroMunicipio";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "NombreMunicipio";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_elecmunicandi2_Chart["Municipio"] = $fdata;
//	Votos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Votos";
	$fdata["GoodName"] = "Votos";
	$fdata["ownerTable"] = "elecmunicandi";
	$fdata["Label"] = GetFieldLabel("cm2_elecmunicandi2_Chart","Votos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Votos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Votos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_elecmunicandi2_Chart["Votos"] = $fdata;
//	PorcVotos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "PorcVotos";
	$fdata["GoodName"] = "PorcVotos";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("cm2_elecmunicandi2_Chart","PorcVotos");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "PorcVotos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "round(PorcVotos,1)";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_elecmunicandi2_Chart["PorcVotos"] = $fdata;

	$tdatacm2_elecmunicandi2_Chart[".chartXml"] = '<chart>
		<attr value="tables">
			<attr value="0">cm2_elecmunicandi2 Chart</attr>
		</attr>
		<attr value="chart_type">
			<attr value="type">2d_bar</attr>
		</attr>

		<attr value="parameters">';
	$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '<attr value="0">
			<attr value="name">Votos</attr>';
	$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '</attr>';
	$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '<attr value="1">
		<attr value="name">Convocatoria_idConvocatoria</attr>
	</attr>';
	$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '</attr>
			<attr value="appearance">';


	$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '<attr value="head">'.xmlencode("Votos").'</attr>
<attr value="foot">'.xmlencode("Convocatorias").'</attr>
<attr value="y_axis_label">'.xmlencode("Convocatoria_idConvocatoria").'</attr>


<attr value="slegend">false</attr>
<attr value="sgrid">true</attr>
<attr value="sname">false</attr>
<attr value="sval">true</attr>
<attr value="sanim">true</attr>
<attr value="sstacked">false</attr>
<attr value="slog">true</attr>
<attr value="aqua">0</attr>
<attr value="cview">0</attr>
<attr value="is3d">0</attr>
<attr value="isstacked">0</attr>
<attr value="linestyle">0</attr>
<attr value="autoupdate">0</attr>
<attr value="autoupmin">60</attr>';
$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '</attr>

<attr value="fields">';
	$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '<attr value="0">
		<attr value="name">idElecMuniCandi</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm2_elecmunicandi2_Chart","idElecMuniCandi")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '<attr value="1">
		<attr value="name">Convocatoria_idConvocatoria</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm2_elecmunicandi2_Chart","Convocatoria_idConvocatoria")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '<attr value="2">
		<attr value="name">Candidatura_idCandidatura</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm2_elecmunicandi2_Chart","Candidatura_idCandidatura")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '<attr value="3">
		<attr value="name">Municipio</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm2_elecmunicandi2_Chart","Municipio")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '<attr value="4">
		<attr value="name">Votos</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm2_elecmunicandi2_Chart","Votos")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '<attr value="5">
		<attr value="name">PorcVotos</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm2_elecmunicandi2_Chart","PorcVotos")).'</attr>
		<attr value="search"></attr>
	</attr>';
$tdatacm2_elecmunicandi2_Chart[".chartXml"] .= '</attr>


<attr value="settings">
<attr value="name">cm2_elecmunicandi2 Chart</attr>
<attr value="short_table_name">cm2_elecmunicandi2_Chart</attr>
</attr>

</chart>';

$tables_data["cm2_elecmunicandi2 Chart"]=&$tdatacm2_elecmunicandi2_Chart;
$field_labels["cm2_elecmunicandi2_Chart"] = &$fieldLabelscm2_elecmunicandi2_Chart;
$fieldToolTips["cm2_elecmunicandi2_Chart"] = &$fieldToolTipscm2_elecmunicandi2_Chart;
$placeHolders["cm2_elecmunicandi2_Chart"] = &$placeHolderscm2_elecmunicandi2_Chart;
$page_titles["cm2_elecmunicandi2_Chart"] = &$pageTitlescm2_elecmunicandi2_Chart;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm2_elecmunicandi2 Chart"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["cm2_elecmunicandi2 Chart"] = array();


	
				$strOriginalDetailsTable="municipio";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm2_municipio";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm2_municipio";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm2_elecmunicandi2 Chart"][0] = $masterParams;
				$masterTablesData["cm2_elecmunicandi2 Chart"][0]["masterKeys"] = array();
	$masterTablesData["cm2_elecmunicandi2 Chart"][0]["masterKeys"][]="idCandidatura";
				$masterTablesData["cm2_elecmunicandi2 Chart"][0]["masterKeys"][]="NumeroMunicipio";
				$masterTablesData["cm2_elecmunicandi2 Chart"][0]["detailKeys"] = array();
	$masterTablesData["cm2_elecmunicandi2 Chart"][0]["detailKeys"][]="Candidatura_idCandidatura";
				$masterTablesData["cm2_elecmunicandi2 Chart"][0]["detailKeys"][]="Municipio";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm2_elecmunicandi2_Chart()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idElecMuniCandi,  	Convocatoria_idConvocatoria,  	Candidatura_idCandidatura,  	Municipio,  	Votos,  	round(PorcVotos,1) PorcVotos";
$proto0["m_strFrom"] = "FROM elecmunicandi";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idElecMuniCandi",
	"m_strTable" => "elecmunicandi",
	"m_srcTableName" => "cm2_elecmunicandi2 Chart"
));

$proto6["m_sql"] = "idElecMuniCandi";
$proto6["m_srcTableName"] = "cm2_elecmunicandi2 Chart";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "elecmunicandi",
	"m_srcTableName" => "cm2_elecmunicandi2 Chart"
));

$proto8["m_sql"] = "Convocatoria_idConvocatoria";
$proto8["m_srcTableName"] = "cm2_elecmunicandi2 Chart";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "elecmunicandi",
	"m_srcTableName" => "cm2_elecmunicandi2 Chart"
));

$proto10["m_sql"] = "Candidatura_idCandidatura";
$proto10["m_srcTableName"] = "cm2_elecmunicandi2 Chart";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "Municipio",
	"m_strTable" => "elecmunicandi",
	"m_srcTableName" => "cm2_elecmunicandi2 Chart"
));

$proto12["m_sql"] = "Municipio";
$proto12["m_srcTableName"] = "cm2_elecmunicandi2 Chart";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "elecmunicandi",
	"m_srcTableName" => "cm2_elecmunicandi2 Chart"
));

$proto14["m_sql"] = "Votos";
$proto14["m_srcTableName"] = "cm2_elecmunicandi2 Chart";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$proto17=array();
$proto17["m_functiontype"] = "SQLF_CUSTOM";
$proto17["m_arguments"] = array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "PorcVotos"
));

$proto17["m_arguments"][]=$obj;
						$obj = new SQLNonParsed(array(
	"m_sql" => "1"
));

$proto17["m_arguments"][]=$obj;
$proto17["m_strFunctionName"] = "round";
$obj = new SQLFunctionCall($proto17);

$proto16["m_sql"] = "round(PorcVotos,1)";
$proto16["m_srcTableName"] = "cm2_elecmunicandi2 Chart";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "PorcVotos";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "elecmunicandi";
$proto21["m_srcTableName"] = "cm2_elecmunicandi2 Chart";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "idElecMuniCandi";
$proto21["m_columns"][] = "Convocatoria_idConvocatoria";
$proto21["m_columns"][] = "Candidatura_idCandidatura";
$proto21["m_columns"][] = "Municipio";
$proto21["m_columns"][] = "Votos";
$proto21["m_columns"][] = "PorcVotos";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "elecmunicandi";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "cm2_elecmunicandi2 Chart";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="cm2_elecmunicandi2 Chart";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_cm2_elecmunicandi2_Chart = createSqlQuery_cm2_elecmunicandi2_Chart();


	
		;

						

$tdatacm2_elecmunicandi2_Chart[".sqlquery"] = $queryData_cm2_elecmunicandi2_Chart;

include_once(getabspath("include/cm2_elecmunicandi2_Chart_events.php"));
$tableEvents["cm2_elecmunicandi2 Chart"] = new eventclass_cm2_elecmunicandi2_Chart;
$tdatacm2_elecmunicandi2_Chart[".hasEvents"] = true;

?>