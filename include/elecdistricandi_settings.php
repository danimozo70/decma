<?php
require_once(getabspath("classes/cipherer.php"));




$tdataelecdistricandi = array();
	$tdataelecdistricandi[".truncateText"] = true;
	$tdataelecdistricandi[".NumberOfChars"] = 80;
	$tdataelecdistricandi[".ShortName"] = "elecdistricandi";
	$tdataelecdistricandi[".OwnerID"] = "";
	$tdataelecdistricandi[".OriginalTable"] = "elecdistricandi";

//	field labels
$fieldLabelselecdistricandi = array();
$fieldToolTipselecdistricandi = array();
$pageTitleselecdistricandi = array();
$placeHolderselecdistricandi = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelselecdistricandi["Spanish"] = array();
	$fieldToolTipselecdistricandi["Spanish"] = array();
	$placeHolderselecdistricandi["Spanish"] = array();
	$pageTitleselecdistricandi["Spanish"] = array();
	$fieldLabelselecdistricandi["Spanish"]["ElecCandidatura_idElecCandidatura"] = "Candidatura";
	$fieldToolTipselecdistricandi["Spanish"]["ElecCandidatura_idElecCandidatura"] = "";
	$placeHolderselecdistricandi["Spanish"]["ElecCandidatura_idElecCandidatura"] = "";
	$fieldLabelselecdistricandi["Spanish"]["idElecDistriCandi"] = "Id Interno";
	$fieldToolTipselecdistricandi["Spanish"]["idElecDistriCandi"] = "";
	$placeHolderselecdistricandi["Spanish"]["idElecDistriCandi"] = "";
	$fieldLabelselecdistricandi["Spanish"]["Municipio"] = "Municipio";
	$fieldToolTipselecdistricandi["Spanish"]["Municipio"] = "";
	$placeHolderselecdistricandi["Spanish"]["Municipio"] = "";
	$fieldLabelselecdistricandi["Spanish"]["Distrito"] = "Distrito";
	$fieldToolTipselecdistricandi["Spanish"]["Distrito"] = "";
	$placeHolderselecdistricandi["Spanish"]["Distrito"] = "";
	$fieldLabelselecdistricandi["Spanish"]["Votos"] = "Votos";
	$fieldToolTipselecdistricandi["Spanish"]["Votos"] = "";
	$placeHolderselecdistricandi["Spanish"]["Votos"] = "";
	$fieldLabelselecdistricandi["Spanish"]["PorcVotos"] = "%Votos";
	$fieldToolTipselecdistricandi["Spanish"]["PorcVotos"] = "";
	$placeHolderselecdistricandi["Spanish"]["PorcVotos"] = "";
	$fieldLabelselecdistricandi["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria IdConvocatoria";
	$fieldToolTipselecdistricandi["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderselecdistricandi["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelselecdistricandi["Spanish"]["Candidatura_idCandidatura"] = "Candidatura";
	$fieldToolTipselecdistricandi["Spanish"]["Candidatura_idCandidatura"] = "";
	$placeHolderselecdistricandi["Spanish"]["Candidatura_idCandidatura"] = "";
	if (count($fieldToolTipselecdistricandi["Spanish"]))
		$tdataelecdistricandi[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelselecdistricandi[""] = array();
	$fieldToolTipselecdistricandi[""] = array();
	$placeHolderselecdistricandi[""] = array();
	$pageTitleselecdistricandi[""] = array();
	if (count($fieldToolTipselecdistricandi[""]))
		$tdataelecdistricandi[".isUseToolTips"] = true;
}


	$tdataelecdistricandi[".NCSearch"] = true;



$tdataelecdistricandi[".shortTableName"] = "elecdistricandi";
$tdataelecdistricandi[".nSecOptions"] = 0;
$tdataelecdistricandi[".recsPerRowList"] = 1;
$tdataelecdistricandi[".recsPerRowPrint"] = 1;
$tdataelecdistricandi[".mainTableOwnerID"] = "";
$tdataelecdistricandi[".moveNext"] = 1;
$tdataelecdistricandi[".entityType"] = 0;

$tdataelecdistricandi[".strOriginalTableName"] = "elecdistricandi";

	



$tdataelecdistricandi[".showAddInPopup"] = true;

$tdataelecdistricandi[".showEditInPopup"] = true;

$tdataelecdistricandi[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["add"] = "add";
			;
$popupPagesLayoutNames["edit"] = "add";
						
	;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdataelecdistricandi[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataelecdistricandi[".fieldsForRegister"] = array();

$tdataelecdistricandi[".listAjax"] = false;

	$tdataelecdistricandi[".audit"] = false;

	$tdataelecdistricandi[".locking"] = false;



$tdataelecdistricandi[".list"] = true;






$tdataelecdistricandi[".view"] = true;





$tdataelecdistricandi[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdataelecdistricandi[".allowShowHideFields"] = true;
//

// Allow Fields Reordering in GRID
$tdataelecdistricandi[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdataelecdistricandi[".searchSaving"] = false;
//

$tdataelecdistricandi[".showSearchPanel"] = true;
		$tdataelecdistricandi[".flexibleSearch"] = true;

$tdataelecdistricandi[".isUseAjaxSuggest"] = true;

$tdataelecdistricandi[".rowHighlite"] = true;





$tdataelecdistricandi[".ajaxCodeSnippetAdded"] = false;

$tdataelecdistricandi[".buttonsAdded"] = false;

$tdataelecdistricandi[".addPageEvents"] = false;

// use timepicker for search panel
$tdataelecdistricandi[".isUseTimeForSearch"] = false;



$tdataelecdistricandi[".badgeColor"] = "00c2c5";


$tdataelecdistricandi[".allSearchFields"] = array();
$tdataelecdistricandi[".filterFields"] = array();
$tdataelecdistricandi[".requiredSearchFields"] = array();



$tdataelecdistricandi[".googleLikeFields"] = array();
$tdataelecdistricandi[".googleLikeFields"][] = "idElecDistriCandi";
$tdataelecdistricandi[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdataelecdistricandi[".googleLikeFields"][] = "Candidatura_idCandidatura";
$tdataelecdistricandi[".googleLikeFields"][] = "Municipio";
$tdataelecdistricandi[".googleLikeFields"][] = "Distrito";
$tdataelecdistricandi[".googleLikeFields"][] = "Votos";
$tdataelecdistricandi[".googleLikeFields"][] = "PorcVotos";



$tdataelecdistricandi[".tableType"] = "list";

$tdataelecdistricandi[".printerPageOrientation"] = 0;
$tdataelecdistricandi[".nPrinterPageScale"] = 100;

$tdataelecdistricandi[".nPrinterSplitRecords"] = 40;

$tdataelecdistricandi[".nPrinterPDFSplitRecords"] = 40;



$tdataelecdistricandi[".geocodingEnabled"] = false;










// view page pdf

// print page pdf

$tdataelecdistricandi[".totalsFields"] = array();
$tdataelecdistricandi[".totalsFields"][] = array(
	"fName" => "Votos",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');
$tdataelecdistricandi[".totalsFields"][] = array(
	"fName" => "PorcVotos",
	"numRows" => 0,
	"totalsType" => "TOTAL",
	"viewFormat" => 'Number');

$tdataelecdistricandi[".pageSize"] = 30;

$tdataelecdistricandi[".warnLeavingPages"] = true;



$tstrOrderBy = "order by Votos desc";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataelecdistricandi[".strOrderBy"] = $tstrOrderBy;

$tdataelecdistricandi[".orderindexes"] = array();
	$tdataelecdistricandi[".orderindexes"][] = array(6, (0 ? "ASC" : "DESC"), "Votos");


$tdataelecdistricandi[".sqlHead"] = "SELECT idElecDistriCandi,  	Convocatoria_idConvocatoria,  	Candidatura_idCandidatura,  	Municipio,  	Distrito,  	Votos,  	PorcVotos";
$tdataelecdistricandi[".sqlFrom"] = "FROM elecdistricandi";
$tdataelecdistricandi[".sqlWhereExpr"] = "";
$tdataelecdistricandi[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataelecdistricandi[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataelecdistricandi[".arrGroupsPerPage"] = $arrGPP;

$tdataelecdistricandi[".highlightSearchResults"] = true;

$tableKeyselecdistricandi = array();
$tableKeyselecdistricandi[] = "idElecDistriCandi";
$tdataelecdistricandi[".Keys"] = $tableKeyselecdistricandi;

$tdataelecdistricandi[".listFields"] = array();
$tdataelecdistricandi[".listFields"][] = "Candidatura_idCandidatura";
$tdataelecdistricandi[".listFields"][] = "Votos";
$tdataelecdistricandi[".listFields"][] = "PorcVotos";

$tdataelecdistricandi[".hideMobileList"] = array();


$tdataelecdistricandi[".viewFields"] = array();
$tdataelecdistricandi[".viewFields"][] = "idElecDistriCandi";
$tdataelecdistricandi[".viewFields"][] = "Convocatoria_idConvocatoria";
$tdataelecdistricandi[".viewFields"][] = "Candidatura_idCandidatura";
$tdataelecdistricandi[".viewFields"][] = "Municipio";
$tdataelecdistricandi[".viewFields"][] = "Distrito";
$tdataelecdistricandi[".viewFields"][] = "Votos";
$tdataelecdistricandi[".viewFields"][] = "PorcVotos";

$tdataelecdistricandi[".addFields"] = array();

$tdataelecdistricandi[".masterListFields"] = array();
$tdataelecdistricandi[".masterListFields"][] = "idElecDistriCandi";
$tdataelecdistricandi[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdataelecdistricandi[".masterListFields"][] = "Candidatura_idCandidatura";
$tdataelecdistricandi[".masterListFields"][] = "Municipio";
$tdataelecdistricandi[".masterListFields"][] = "Distrito";
$tdataelecdistricandi[".masterListFields"][] = "Votos";
$tdataelecdistricandi[".masterListFields"][] = "PorcVotos";

$tdataelecdistricandi[".inlineAddFields"] = array();

$tdataelecdistricandi[".editFields"] = array();

$tdataelecdistricandi[".inlineEditFields"] = array();

$tdataelecdistricandi[".updateSelectedFields"] = array();


$tdataelecdistricandi[".exportFields"] = array();

$tdataelecdistricandi[".importFields"] = array();

$tdataelecdistricandi[".printFields"] = array();


//	idElecDistriCandi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idElecDistriCandi";
	$fdata["GoodName"] = "idElecDistriCandi";
	$fdata["ownerTable"] = "elecdistricandi";
	$fdata["Label"] = GetFieldLabel("elecdistricandi","idElecDistriCandi");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "idElecDistriCandi";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idElecDistriCandi";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecdistricandi["idElecDistriCandi"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "elecdistricandi";
	$fdata["Label"] = GetFieldLabel("elecdistricandi","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecdistricandi["Convocatoria_idConvocatoria"] = $fdata;
//	Candidatura_idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Candidatura_idCandidatura";
	$fdata["GoodName"] = "Candidatura_idCandidatura";
	$fdata["ownerTable"] = "elecdistricandi";
	$fdata["Label"] = GetFieldLabel("elecdistricandi","Candidatura_idCandidatura");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "Candidatura_idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Candidatura_idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "candidatura";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idCandidatura";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "concat(Codigo,' - ',Titulo)";
	
	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecdistricandi["Candidatura_idCandidatura"] = $fdata;
//	Municipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Municipio";
	$fdata["GoodName"] = "Municipio";
	$fdata["ownerTable"] = "elecdistricandi";
	$fdata["Label"] = GetFieldLabel("elecdistricandi","Municipio");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "Municipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Municipio";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "municipio";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "NumeroMunicipio";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "NombreMunicipio";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;

				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "Distrito";

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecdistricandi["Municipio"] = $fdata;
//	Distrito
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Distrito";
	$fdata["GoodName"] = "Distrito";
	$fdata["ownerTable"] = "elecdistricandi";
	$fdata["Label"] = GetFieldLabel("elecdistricandi","Distrito");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "Distrito";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Distrito";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "distrito";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "NumeroDistrito";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "NombreDistrito";
	
	

	
	$edata["LookupOrderBy"] = "NombreDistrito";

	
		$edata["UseCategory"] = true;
	$edata["categoryFields"] = array();
	$edata["categoryFields"][] = array( "main" => "Municipio", "lookup" => "NumeroMunicipio" );

	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecdistricandi["Distrito"] = $fdata;
//	Votos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Votos";
	$fdata["GoodName"] = "Votos";
	$fdata["ownerTable"] = "elecdistricandi";
	$fdata["Label"] = GetFieldLabel("elecdistricandi","Votos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "Votos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Votos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecdistricandi["Votos"] = $fdata;
//	PorcVotos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "PorcVotos";
	$fdata["GoodName"] = "PorcVotos";
	$fdata["ownerTable"] = "elecdistricandi";
	$fdata["Label"] = GetFieldLabel("elecdistricandi","PorcVotos");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

	
	
	
		$fdata["strField"] = "PorcVotos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "PorcVotos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataelecdistricandi["PorcVotos"] = $fdata;


$tables_data["elecdistricandi"]=&$tdataelecdistricandi;
$field_labels["elecdistricandi"] = &$fieldLabelselecdistricandi;
$fieldToolTips["elecdistricandi"] = &$fieldToolTipselecdistricandi;
$placeHolders["elecdistricandi"] = &$placeHolderselecdistricandi;
$page_titles["elecdistricandi"] = &$pageTitleselecdistricandi;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["elecdistricandi"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["elecdistricandi"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 0;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["elecdistricandi"][0] = $masterParams;
				$masterTablesData["elecdistricandi"][0]["masterKeys"] = array();
	$masterTablesData["elecdistricandi"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["elecdistricandi"][0]["detailKeys"] = array();
	$masterTablesData["elecdistricandi"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
		
	
				$strOriginalDetailsTable="v_elec_distri";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm1_1_v_elec_distri";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm1_1_v_elec_distri";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 1;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["elecdistricandi"][1] = $masterParams;
				$masterTablesData["elecdistricandi"][1]["masterKeys"] = array();
	$masterTablesData["elecdistricandi"][1]["masterKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["elecdistricandi"][1]["masterKeys"][]="NumeroDistrito";
				$masterTablesData["elecdistricandi"][1]["masterKeys"][]="NumeroMunicipio";
				$masterTablesData["elecdistricandi"][1]["detailKeys"] = array();
	$masterTablesData["elecdistricandi"][1]["detailKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["elecdistricandi"][1]["detailKeys"][]="Distrito";
				$masterTablesData["elecdistricandi"][1]["detailKeys"][]="Municipio";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_elecdistricandi()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "idElecDistriCandi,  	Convocatoria_idConvocatoria,  	Candidatura_idCandidatura,  	Municipio,  	Distrito,  	Votos,  	PorcVotos";
$proto3["m_strFrom"] = "FROM elecdistricandi";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "order by Votos desc";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idElecDistriCandi",
	"m_strTable" => "elecdistricandi",
	"m_srcTableName" => "elecdistricandi"
));

$proto9["m_sql"] = "idElecDistriCandi";
$proto9["m_srcTableName"] = "elecdistricandi";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "elecdistricandi",
	"m_srcTableName" => "elecdistricandi"
));

$proto11["m_sql"] = "Convocatoria_idConvocatoria";
$proto11["m_srcTableName"] = "elecdistricandi";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "elecdistricandi",
	"m_srcTableName" => "elecdistricandi"
));

$proto13["m_sql"] = "Candidatura_idCandidatura";
$proto13["m_srcTableName"] = "elecdistricandi";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Municipio",
	"m_strTable" => "elecdistricandi",
	"m_srcTableName" => "elecdistricandi"
));

$proto15["m_sql"] = "Municipio";
$proto15["m_srcTableName"] = "elecdistricandi";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "Distrito",
	"m_strTable" => "elecdistricandi",
	"m_srcTableName" => "elecdistricandi"
));

$proto17["m_sql"] = "Distrito";
$proto17["m_srcTableName"] = "elecdistricandi";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "elecdistricandi",
	"m_srcTableName" => "elecdistricandi"
));

$proto19["m_sql"] = "Votos";
$proto19["m_srcTableName"] = "elecdistricandi";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto3["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "PorcVotos",
	"m_strTable" => "elecdistricandi",
	"m_srcTableName" => "elecdistricandi"
));

$proto21["m_sql"] = "PorcVotos";
$proto21["m_srcTableName"] = "elecdistricandi";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto23=array();
$proto23["m_link"] = "SQLL_MAIN";
			$proto24=array();
$proto24["m_strName"] = "elecdistricandi";
$proto24["m_srcTableName"] = "elecdistricandi";
$proto24["m_columns"] = array();
$proto24["m_columns"][] = "idElecDistriCandi";
$proto24["m_columns"][] = "Convocatoria_idConvocatoria";
$proto24["m_columns"][] = "Candidatura_idCandidatura";
$proto24["m_columns"][] = "Municipio";
$proto24["m_columns"][] = "Distrito";
$proto24["m_columns"][] = "Votos";
$proto24["m_columns"][] = "PorcVotos";
$obj = new SQLTable($proto24);

$proto23["m_table"] = $obj;
$proto23["m_sql"] = "elecdistricandi";
$proto23["m_alias"] = "";
$proto23["m_srcTableName"] = "elecdistricandi";
$proto25=array();
$proto25["m_sql"] = "";
$proto25["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto25["m_column"]=$obj;
$proto25["m_contained"] = array();
$proto25["m_strCase"] = "";
$proto25["m_havingmode"] = false;
$proto25["m_inBrackets"] = false;
$proto25["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto25);

$proto23["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto23);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto27=array();
						$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "elecdistricandi",
	"m_srcTableName" => "elecdistricandi"
));

$proto27["m_column"]=$obj;
$proto27["m_bAsc"] = 0;
$proto27["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto27);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="elecdistricandi";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_elecdistricandi = createSqlQuery_elecdistricandi();


	
		;

							

$tdataelecdistricandi[".sqlquery"] = $queryData_elecdistricandi;

$tableEvents["elecdistricandi"] = new eventsBase;
$tdataelecdistricandi[".hasEvents"] = false;

?>