<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm2_distrito = array();
	$tdatacm2_distrito[".truncateText"] = true;
	$tdatacm2_distrito[".NumberOfChars"] = 80;
	$tdatacm2_distrito[".ShortName"] = "cm2_distrito";
	$tdatacm2_distrito[".OwnerID"] = "";
	$tdatacm2_distrito[".OriginalTable"] = "distrito";

//	field labels
$fieldLabelscm2_distrito = array();
$fieldToolTipscm2_distrito = array();
$pageTitlescm2_distrito = array();
$placeHolderscm2_distrito = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm2_distrito["Spanish"] = array();
	$fieldToolTipscm2_distrito["Spanish"] = array();
	$placeHolderscm2_distrito["Spanish"] = array();
	$pageTitlescm2_distrito["Spanish"] = array();
	$fieldLabelscm2_distrito["Spanish"]["idDistrito"] = "Id Interno";
	$fieldToolTipscm2_distrito["Spanish"]["idDistrito"] = "";
	$placeHolderscm2_distrito["Spanish"]["idDistrito"] = "";
	$fieldLabelscm2_distrito["Spanish"]["NumeroMunicipio"] = "Municipio";
	$fieldToolTipscm2_distrito["Spanish"]["NumeroMunicipio"] = "";
	$placeHolderscm2_distrito["Spanish"]["NumeroMunicipio"] = "";
	$fieldLabelscm2_distrito["Spanish"]["NumeroDistrito"] = "Número Distrito";
	$fieldToolTipscm2_distrito["Spanish"]["NumeroDistrito"] = "";
	$placeHolderscm2_distrito["Spanish"]["NumeroDistrito"] = "";
	$fieldLabelscm2_distrito["Spanish"]["NombreDistrito"] = "Distrito";
	$fieldToolTipscm2_distrito["Spanish"]["NombreDistrito"] = "";
	$placeHolderscm2_distrito["Spanish"]["NombreDistrito"] = "";
	$fieldLabelscm2_distrito["Spanish"]["idCandidatura"] = "Id Candidatura";
	$fieldToolTipscm2_distrito["Spanish"]["idCandidatura"] = "";
	$placeHolderscm2_distrito["Spanish"]["idCandidatura"] = "";
	$fieldLabelscm2_distrito["Spanish"]["Codigo"] = "Codigo";
	$fieldToolTipscm2_distrito["Spanish"]["Codigo"] = "";
	$placeHolderscm2_distrito["Spanish"]["Codigo"] = "";
	$fieldLabelscm2_distrito["Spanish"]["Censo"] = "Censo (2015)";
	$fieldToolTipscm2_distrito["Spanish"]["Censo"] = "";
	$placeHolderscm2_distrito["Spanish"]["Censo"] = "";
	if (count($fieldToolTipscm2_distrito["Spanish"]))
		$tdatacm2_distrito[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm2_distrito[""] = array();
	$fieldToolTipscm2_distrito[""] = array();
	$placeHolderscm2_distrito[""] = array();
	$pageTitlescm2_distrito[""] = array();
	if (count($fieldToolTipscm2_distrito[""]))
		$tdatacm2_distrito[".isUseToolTips"] = true;
}


	$tdatacm2_distrito[".NCSearch"] = true;



$tdatacm2_distrito[".shortTableName"] = "cm2_distrito";
$tdatacm2_distrito[".nSecOptions"] = 0;
$tdatacm2_distrito[".recsPerRowList"] = 1;
$tdatacm2_distrito[".recsPerRowPrint"] = 1;
$tdatacm2_distrito[".mainTableOwnerID"] = "";
$tdatacm2_distrito[".moveNext"] = 1;
$tdatacm2_distrito[".entityType"] = 1;

$tdatacm2_distrito[".strOriginalTableName"] = "distrito";

	



$tdatacm2_distrito[".showAddInPopup"] = true;

$tdatacm2_distrito[".showEditInPopup"] = true;

$tdatacm2_distrito[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["add"] = "view_bootstrap";
			;
$popupPagesLayoutNames["edit"] = "view_bootstrap";
			;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdatacm2_distrito[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm2_distrito[".fieldsForRegister"] = array();

$tdatacm2_distrito[".listAjax"] = false;

	$tdatacm2_distrito[".audit"] = false;

	$tdatacm2_distrito[".locking"] = false;



$tdatacm2_distrito[".list"] = true;



$tdatacm2_distrito[".reorderRecordsByHeader"] = true;








$tdatacm2_distrito[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm2_distrito[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm2_distrito[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm2_distrito[".searchSaving"] = false;
//

$tdatacm2_distrito[".showSearchPanel"] = true;
		$tdatacm2_distrito[".flexibleSearch"] = true;

$tdatacm2_distrito[".isUseAjaxSuggest"] = true;

$tdatacm2_distrito[".rowHighlite"] = true;





$tdatacm2_distrito[".ajaxCodeSnippetAdded"] = false;

$tdatacm2_distrito[".buttonsAdded"] = false;

$tdatacm2_distrito[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm2_distrito[".isUseTimeForSearch"] = false;



$tdatacm2_distrito[".badgeColor"] = "edca00";


$tdatacm2_distrito[".allSearchFields"] = array();
$tdatacm2_distrito[".filterFields"] = array();
$tdatacm2_distrito[".requiredSearchFields"] = array();

$tdatacm2_distrito[".allSearchFields"][] = "idCandidatura";
	$tdatacm2_distrito[".allSearchFields"][] = "Codigo";
	$tdatacm2_distrito[".allSearchFields"][] = "idDistrito";
	$tdatacm2_distrito[".allSearchFields"][] = "NumeroMunicipio";
	$tdatacm2_distrito[".allSearchFields"][] = "NumeroDistrito";
	$tdatacm2_distrito[".allSearchFields"][] = "NombreDistrito";
	$tdatacm2_distrito[".allSearchFields"][] = "Censo";
	

$tdatacm2_distrito[".googleLikeFields"] = array();
$tdatacm2_distrito[".googleLikeFields"][] = "idCandidatura";
$tdatacm2_distrito[".googleLikeFields"][] = "Codigo";
$tdatacm2_distrito[".googleLikeFields"][] = "idDistrito";
$tdatacm2_distrito[".googleLikeFields"][] = "NumeroMunicipio";
$tdatacm2_distrito[".googleLikeFields"][] = "NumeroDistrito";
$tdatacm2_distrito[".googleLikeFields"][] = "NombreDistrito";
$tdatacm2_distrito[".googleLikeFields"][] = "Censo";


$tdatacm2_distrito[".advSearchFields"] = array();
$tdatacm2_distrito[".advSearchFields"][] = "idCandidatura";
$tdatacm2_distrito[".advSearchFields"][] = "Codigo";
$tdatacm2_distrito[".advSearchFields"][] = "idDistrito";
$tdatacm2_distrito[".advSearchFields"][] = "NumeroMunicipio";
$tdatacm2_distrito[".advSearchFields"][] = "NumeroDistrito";
$tdatacm2_distrito[".advSearchFields"][] = "NombreDistrito";
$tdatacm2_distrito[".advSearchFields"][] = "Censo";

$tdatacm2_distrito[".tableType"] = "list";

$tdatacm2_distrito[".printerPageOrientation"] = 0;
$tdatacm2_distrito[".nPrinterPageScale"] = 100;

$tdatacm2_distrito[".nPrinterSplitRecords"] = 40;

$tdatacm2_distrito[".nPrinterPDFSplitRecords"] = 40;



$tdatacm2_distrito[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatacm2_distrito[".pageSize"] = 6;

$tdatacm2_distrito[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY candidatura.idCandidatura, distrito.NombreDistrito";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm2_distrito[".strOrderBy"] = $tstrOrderBy;

$tdatacm2_distrito[".orderindexes"] = array();
	$tdatacm2_distrito[".orderindexes"][] = array(1, (1 ? "ASC" : "DESC"), "candidatura.idCandidatura");

	$tdatacm2_distrito[".orderindexes"][] = array(6, (1 ? "ASC" : "DESC"), "distrito.NombreDistrito");


$tdatacm2_distrito[".sqlHead"] = "SELECT candidatura.idCandidatura,  candidatura.Codigo,  distrito.idDistrito,  distrito.NumeroMunicipio,  distrito.NumeroDistrito,  distrito.NombreDistrito,  v_elec_distri.Censo";
$tdatacm2_distrito[".sqlFrom"] = "FROM candidatura  , distrito  INNER JOIN v_elec_distri ON distrito.NumeroMunicipio = v_elec_distri.NumeroMunicipio AND distrito.NumeroDistrito = v_elec_distri.NumeroDistrito";
$tdatacm2_distrito[".sqlWhereExpr"] = "(v_elec_distri.Convocatoria_idConvocatoria = 1)";
$tdatacm2_distrito[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm2_distrito[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm2_distrito[".arrGroupsPerPage"] = $arrGPP;

$tdatacm2_distrito[".highlightSearchResults"] = true;

$tableKeyscm2_distrito = array();
$tableKeyscm2_distrito[] = "idDistrito";
$tdatacm2_distrito[".Keys"] = $tableKeyscm2_distrito;

$tdatacm2_distrito[".listFields"] = array();
$tdatacm2_distrito[".listFields"][] = "NombreDistrito";
$tdatacm2_distrito[".listFields"][] = "Censo";

$tdatacm2_distrito[".hideMobileList"] = array();


$tdatacm2_distrito[".viewFields"] = array();

$tdatacm2_distrito[".addFields"] = array();

$tdatacm2_distrito[".masterListFields"] = array();
$tdatacm2_distrito[".masterListFields"][] = "idCandidatura";
$tdatacm2_distrito[".masterListFields"][] = "Codigo";
$tdatacm2_distrito[".masterListFields"][] = "idDistrito";
$tdatacm2_distrito[".masterListFields"][] = "NumeroMunicipio";
$tdatacm2_distrito[".masterListFields"][] = "NumeroDistrito";
$tdatacm2_distrito[".masterListFields"][] = "NombreDistrito";
$tdatacm2_distrito[".masterListFields"][] = "Censo";

$tdatacm2_distrito[".inlineAddFields"] = array();

$tdatacm2_distrito[".editFields"] = array();

$tdatacm2_distrito[".inlineEditFields"] = array();

$tdatacm2_distrito[".updateSelectedFields"] = array();


$tdatacm2_distrito[".exportFields"] = array();

$tdatacm2_distrito[".importFields"] = array();

$tdatacm2_distrito[".printFields"] = array();


//	idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idCandidatura";
	$fdata["GoodName"] = "idCandidatura";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm2_distrito","idCandidatura");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "candidatura.idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_distrito["idCandidatura"] = $fdata;
//	Codigo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Codigo";
	$fdata["GoodName"] = "Codigo";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm2_distrito","Codigo");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Codigo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "candidatura.Codigo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_distrito["Codigo"] = $fdata;
//	idDistrito
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "idDistrito";
	$fdata["GoodName"] = "idDistrito";
	$fdata["ownerTable"] = "distrito";
	$fdata["Label"] = GetFieldLabel("cm2_distrito","idDistrito");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "idDistrito";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "distrito.idDistrito";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_distrito["idDistrito"] = $fdata;
//	NumeroMunicipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "NumeroMunicipio";
	$fdata["GoodName"] = "NumeroMunicipio";
	$fdata["ownerTable"] = "distrito";
	$fdata["Label"] = GetFieldLabel("cm2_distrito","NumeroMunicipio");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "NumeroMunicipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "distrito.NumeroMunicipio";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "municipio";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "NumeroMunicipio";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "NombreMunicipio";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_distrito["NumeroMunicipio"] = $fdata;
//	NumeroDistrito
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "NumeroDistrito";
	$fdata["GoodName"] = "NumeroDistrito";
	$fdata["ownerTable"] = "distrito";
	$fdata["Label"] = GetFieldLabel("cm2_distrito","NumeroDistrito");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "NumeroDistrito";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "distrito.NumeroDistrito";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_distrito["NumeroDistrito"] = $fdata;
//	NombreDistrito
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "NombreDistrito";
	$fdata["GoodName"] = "NombreDistrito";
	$fdata["ownerTable"] = "distrito";
	$fdata["Label"] = GetFieldLabel("cm2_distrito","NombreDistrito");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "NombreDistrito";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "distrito.NombreDistrito";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_distrito["NombreDistrito"] = $fdata;
//	Censo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Censo";
	$fdata["GoodName"] = "Censo";
	$fdata["ownerTable"] = "v_elec_distri";
	$fdata["Label"] = GetFieldLabel("cm2_distrito","Censo");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Censo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "v_elec_distri.Censo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm2_distrito["Censo"] = $fdata;


$tables_data["cm2_distrito"]=&$tdatacm2_distrito;
$field_labels["cm2_distrito"] = &$fieldLabelscm2_distrito;
$fieldToolTips["cm2_distrito"] = &$fieldToolTipscm2_distrito;
$placeHolders["cm2_distrito"] = &$placeHolderscm2_distrito;
$page_titles["cm2_distrito"] = &$pageTitlescm2_distrito;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm2_distrito"] = array();
//	cm2_elecdistricandi Chart
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm2_elecdistricandi Chart";
		$detailsParam["dOriginalTable"] = "elecdistricandi";
		$detailsParam["proceedLink"] = true;
			$detailsParam["dType"]=PAGE_CHART;
		$detailsParam["dShortTable"] = "cm2_elecdistricandi_Chart";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm2_elecdistricandi_Chart");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "1";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm2_distrito"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm2_distrito"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm2_distrito"][$dIndex]["masterKeys"][]="idCandidatura";

	$detailsTablesData["cm2_distrito"][$dIndex]["masterKeys"][]="NumeroMunicipio";

	$detailsTablesData["cm2_distrito"][$dIndex]["masterKeys"][]="NumeroDistrito";

				$detailsTablesData["cm2_distrito"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm2_distrito"][$dIndex]["detailKeys"][]="Candidatura_idCandidatura";

		
	$detailsTablesData["cm2_distrito"][$dIndex]["detailKeys"][]="Municipio";

		
	$detailsTablesData["cm2_distrito"][$dIndex]["detailKeys"][]="Distrito";
//	cm2_elecdistricandi2 Chart
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm2_elecdistricandi2 Chart";
		$detailsParam["dOriginalTable"] = "elecdistricandi";
		$detailsParam["proceedLink"] = true;
			$detailsParam["dType"]=PAGE_CHART;
		$detailsParam["dShortTable"] = "cm2_elecdistricandi2_Chart";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm2_elecdistricandi2_Chart");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm2_distrito"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm2_distrito"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm2_distrito"][$dIndex]["masterKeys"][]="idCandidatura";

	$detailsTablesData["cm2_distrito"][$dIndex]["masterKeys"][]="NumeroMunicipio";

	$detailsTablesData["cm2_distrito"][$dIndex]["masterKeys"][]="NumeroDistrito";

				$detailsTablesData["cm2_distrito"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm2_distrito"][$dIndex]["detailKeys"][]="Candidatura_idCandidatura";

		
	$detailsTablesData["cm2_distrito"][$dIndex]["detailKeys"][]="Municipio";

		
	$detailsTablesData["cm2_distrito"][$dIndex]["detailKeys"][]="Distrito";

// tables which are master tables for current table (detail)
$masterTablesData["cm2_distrito"] = array();


	
				$strOriginalDetailsTable="candidatura";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm2_candidatura";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm2_candidatura";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm2_distrito"][0] = $masterParams;
				$masterTablesData["cm2_distrito"][0]["masterKeys"] = array();
	$masterTablesData["cm2_distrito"][0]["masterKeys"][]="idCandidatura";
				$masterTablesData["cm2_distrito"][0]["detailKeys"] = array();
	$masterTablesData["cm2_distrito"][0]["detailKeys"][]="idCandidatura";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm2_distrito()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "candidatura.idCandidatura,  candidatura.Codigo,  distrito.idDistrito,  distrito.NumeroMunicipio,  distrito.NumeroDistrito,  distrito.NombreDistrito,  v_elec_distri.Censo";
$proto3["m_strFrom"] = "FROM candidatura  , distrito  INNER JOIN v_elec_distri ON distrito.NumeroMunicipio = v_elec_distri.NumeroMunicipio AND distrito.NumeroDistrito = v_elec_distri.NumeroDistrito";
$proto3["m_strWhere"] = "(v_elec_distri.Convocatoria_idConvocatoria = 1)";
$proto3["m_strOrderBy"] = "ORDER BY candidatura.idCandidatura, distrito.NombreDistrito";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "v_elec_distri.Convocatoria_idConvocatoria = 1";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "cm2_distrito"
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "= 1";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idCandidatura",
	"m_strTable" => "candidatura",
	"m_srcTableName" => "cm2_distrito"
));

$proto9["m_sql"] = "candidatura.idCandidatura";
$proto9["m_srcTableName"] = "cm2_distrito";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Codigo",
	"m_strTable" => "candidatura",
	"m_srcTableName" => "cm2_distrito"
));

$proto11["m_sql"] = "candidatura.Codigo";
$proto11["m_srcTableName"] = "cm2_distrito";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "idDistrito",
	"m_strTable" => "distrito",
	"m_srcTableName" => "cm2_distrito"
));

$proto13["m_sql"] = "distrito.idDistrito";
$proto13["m_srcTableName"] = "cm2_distrito";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "NumeroMunicipio",
	"m_strTable" => "distrito",
	"m_srcTableName" => "cm2_distrito"
));

$proto15["m_sql"] = "distrito.NumeroMunicipio";
$proto15["m_srcTableName"] = "cm2_distrito";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "NumeroDistrito",
	"m_strTable" => "distrito",
	"m_srcTableName" => "cm2_distrito"
));

$proto17["m_sql"] = "distrito.NumeroDistrito";
$proto17["m_srcTableName"] = "cm2_distrito";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "NombreDistrito",
	"m_strTable" => "distrito",
	"m_srcTableName" => "cm2_distrito"
));

$proto19["m_sql"] = "distrito.NombreDistrito";
$proto19["m_srcTableName"] = "cm2_distrito";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto3["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "Censo",
	"m_strTable" => "v_elec_distri",
	"m_srcTableName" => "cm2_distrito"
));

$proto21["m_sql"] = "v_elec_distri.Censo";
$proto21["m_srcTableName"] = "cm2_distrito";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto23=array();
$proto23["m_link"] = "SQLL_MAIN";
			$proto24=array();
$proto24["m_strName"] = "candidatura";
$proto24["m_srcTableName"] = "cm2_distrito";
$proto24["m_columns"] = array();
$proto24["m_columns"][] = "idCandidatura";
$proto24["m_columns"][] = "Codigo";
$proto24["m_columns"][] = "Titulo";
$proto24["m_columns"][] = "Descripcion";
$proto24["m_columns"][] = "Color";
$proto24["m_columns"][] = "Logo";
$obj = new SQLTable($proto24);

$proto23["m_table"] = $obj;
$proto23["m_sql"] = "candidatura";
$proto23["m_alias"] = "";
$proto23["m_srcTableName"] = "cm2_distrito";
$proto25=array();
$proto25["m_sql"] = "";
$proto25["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto25["m_column"]=$obj;
$proto25["m_contained"] = array();
$proto25["m_strCase"] = "";
$proto25["m_havingmode"] = false;
$proto25["m_inBrackets"] = false;
$proto25["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto25);

$proto23["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto23);

$proto3["m_fromlist"][]=$obj;
												$proto27=array();
$proto27["m_link"] = "SQLL_CROSSJOIN";
			$proto28=array();
$proto28["m_strName"] = "distrito";
$proto28["m_srcTableName"] = "cm2_distrito";
$proto28["m_columns"] = array();
$proto28["m_columns"][] = "idDistrito";
$proto28["m_columns"][] = "NumeroMunicipio";
$proto28["m_columns"][] = "NumeroDistrito";
$proto28["m_columns"][] = "NombreDistrito";
$obj = new SQLTable($proto28);

$proto27["m_table"] = $obj;
$proto27["m_sql"] = "distrito";
$proto27["m_alias"] = "";
$proto27["m_srcTableName"] = "cm2_distrito";
$proto29=array();
$proto29["m_sql"] = "";
$proto29["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto29["m_column"]=$obj;
$proto29["m_contained"] = array();
$proto29["m_strCase"] = "";
$proto29["m_havingmode"] = false;
$proto29["m_inBrackets"] = false;
$proto29["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto29);

$proto27["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto27);

$proto3["m_fromlist"][]=$obj;
												$proto31=array();
$proto31["m_link"] = "SQLL_INNERJOIN";
			$proto32=array();
$proto32["m_strName"] = "v_elec_distri";
$proto32["m_srcTableName"] = "cm2_distrito";
$proto32["m_columns"] = array();
$proto32["m_columns"][] = "NumeroMunicipio";
$proto32["m_columns"][] = "NumeroDistrito";
$proto32["m_columns"][] = "NombreDistrito";
$proto32["m_columns"][] = "idElecDistriTotal";
$proto32["m_columns"][] = "Convocatoria_idConvocatoria";
$proto32["m_columns"][] = "Censo";
$proto32["m_columns"][] = "Certificacion";
$proto32["m_columns"][] = "Interventores";
$proto32["m_columns"][] = "Nulo";
$proto32["m_columns"][] = "Blanco";
$proto32["m_columns"][] = "Validos";
$obj = new SQLTable($proto32);

$proto31["m_table"] = $obj;
$proto31["m_sql"] = "INNER JOIN v_elec_distri ON distrito.NumeroMunicipio = v_elec_distri.NumeroMunicipio AND distrito.NumeroDistrito = v_elec_distri.NumeroDistrito";
$proto31["m_alias"] = "";
$proto31["m_srcTableName"] = "cm2_distrito";
$proto33=array();
$proto33["m_sql"] = "distrito.NumeroMunicipio = v_elec_distri.NumeroMunicipio AND distrito.NumeroDistrito = v_elec_distri.NumeroDistrito";
$proto33["m_uniontype"] = "SQLL_AND";
	$obj = new SQLNonParsed(array(
	"m_sql" => "distrito.NumeroMunicipio = v_elec_distri.NumeroMunicipio AND distrito.NumeroDistrito = v_elec_distri.NumeroDistrito"
));

$proto33["m_column"]=$obj;
$proto33["m_contained"] = array();
						$proto35=array();
$proto35["m_sql"] = "distrito.NumeroMunicipio = v_elec_distri.NumeroMunicipio";
$proto35["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "NumeroMunicipio",
	"m_strTable" => "distrito",
	"m_srcTableName" => "cm2_distrito"
));

$proto35["m_column"]=$obj;
$proto35["m_contained"] = array();
$proto35["m_strCase"] = "= v_elec_distri.NumeroMunicipio";
$proto35["m_havingmode"] = false;
$proto35["m_inBrackets"] = false;
$proto35["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto35);

			$proto33["m_contained"][]=$obj;
						$proto37=array();
$proto37["m_sql"] = "distrito.NumeroDistrito = v_elec_distri.NumeroDistrito";
$proto37["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "NumeroDistrito",
	"m_strTable" => "distrito",
	"m_srcTableName" => "cm2_distrito"
));

$proto37["m_column"]=$obj;
$proto37["m_contained"] = array();
$proto37["m_strCase"] = "= v_elec_distri.NumeroDistrito";
$proto37["m_havingmode"] = false;
$proto37["m_inBrackets"] = false;
$proto37["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto37);

			$proto33["m_contained"][]=$obj;
$proto33["m_strCase"] = "";
$proto33["m_havingmode"] = false;
$proto33["m_inBrackets"] = false;
$proto33["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto33);

$proto31["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto31);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto39=array();
						$obj = new SQLField(array(
	"m_strName" => "idCandidatura",
	"m_strTable" => "candidatura",
	"m_srcTableName" => "cm2_distrito"
));

$proto39["m_column"]=$obj;
$proto39["m_bAsc"] = 1;
$proto39["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto39);

$proto3["m_orderby"][]=$obj;					
												$proto41=array();
						$obj = new SQLField(array(
	"m_strName" => "NombreDistrito",
	"m_strTable" => "distrito",
	"m_srcTableName" => "cm2_distrito"
));

$proto41["m_column"]=$obj;
$proto41["m_bAsc"] = 1;
$proto41["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto41);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="cm2_distrito";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_cm2_distrito = createSqlQuery_cm2_distrito();


	
		;

							

$tdatacm2_distrito[".sqlquery"] = $queryData_cm2_distrito;

$tableEvents["cm2_distrito"] = new eventsBase;
$tdatacm2_distrito[".hasEvents"] = false;

?>