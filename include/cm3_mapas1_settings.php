<?php
require_once(getabspath("classes/cipherer.php"));



$tdatacm3_mapas1 = array();
$tdatacm3_mapas1[".ShortName"] = "cm3_mapas1";

//	field labels
$fieldLabelscm3_mapas1 = array();
$pageTitlescm3_mapas1 = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm3_mapas1["Spanish"] = array();
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm3_mapas1[""] = array();
}

//	search fields
$tdatacm3_mapas1[".searchFields"] = array();

// all search fields
$tdatacm3_mapas1[".allSearchFields"] = array();

// good like search fields
$tdatacm3_mapas1[".googleLikeFields"] = array();

$tdatacm3_mapas1[".dashElements"] = array();

	$dbelement = array( "elementName" => "cm3_convocatoria_list", "table" => "cm3_convocatoria", "type" => 0);
	$dbelement["cellName"] = "cell_0_0";

			$dbelement["width"] = 400;
			$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;


	$tdatacm3_mapas1[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "v_candidatura_mapa_list", "table" => "v_candidatura_mapa", "type" => 0);
	$dbelement["cellName"] = "cell_1_0";

			$dbelement["width"] = 400;
			$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "cm3_convocatoria";

	$tdatacm3_mapas1[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "v_candidatura_mapa_details", "table" => "v_candidatura_mapa", "type" => 5);
	$dbelement["cellName"] = "cell_0_1";

				$dbelement["notUsedDetailTables"] = array();
	$dbelement["initialTabDetailTable"] = "cm3_mapa_municipio";
	$dbelement["details"] = array();
	$dbelement["details"]["cm3_mapa_municipio"] = array(
		"add" => 0,
		"edit" => 0,
		"view" => 0,
		"delete" => 0
	);
	$dbelement["details"]["cm3_mapa_distrito_madrid"] = array(
		"add" => 0,
		"edit" => 0,
		"view" => 0,
		"delete" => 0
	);

$dbelement["masterTable"] = "cm3_convocatoria";

	$tdatacm3_mapas1[".dashElements"][] = $dbelement;

$tdatacm3_mapas1[".shortTableName"] = "cm3_mapas1";
$tdatacm3_mapas1[".entityType"] = 4;



include_once(getabspath("include/cm3_mapas1_events.php"));
$tableEvents["cm3_mapas1"] = new eventclass_cm3_mapas1;
$tdatacm3_mapas1[".hasEvents"] = true;


$tdatacm3_mapas1[".tableType"] = "dashboard";



$tdatacm3_mapas1[".addPageEvents"] = false;

$tables_data["cm3_mapas1"]=&$tdatacm3_mapas1;
$field_labels["cm3_mapas1"] = &$fieldLabelscm3_mapas1;
$page_titles["cm3_mapas1"] = &$pageTitlescm3_mapas1;

?>