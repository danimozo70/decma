<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm3_mapa_municipio = array();
	$tdatacm3_mapa_municipio[".truncateText"] = true;
	$tdatacm3_mapa_municipio[".NumberOfChars"] = 80;
	$tdatacm3_mapa_municipio[".ShortName"] = "cm3_mapa_municipio";
	$tdatacm3_mapa_municipio[".OwnerID"] = "";
	$tdatacm3_mapa_municipio[".OriginalTable"] = "eleccandidatura";

//	field labels
$fieldLabelscm3_mapa_municipio = array();
$fieldToolTipscm3_mapa_municipio = array();
$pageTitlescm3_mapa_municipio = array();
$placeHolderscm3_mapa_municipio = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm3_mapa_municipio["Spanish"] = array();
	$fieldToolTipscm3_mapa_municipio["Spanish"] = array();
	$placeHolderscm3_mapa_municipio["Spanish"] = array();
	$pageTitlescm3_mapa_municipio["Spanish"] = array();
	$fieldLabelscm3_mapa_municipio["Spanish"]["idElecCandidatura"] = "Id Interno";
	$fieldToolTipscm3_mapa_municipio["Spanish"]["idElecCandidatura"] = "";
	$placeHolderscm3_mapa_municipio["Spanish"]["idElecCandidatura"] = "";
	$fieldLabelscm3_mapa_municipio["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldToolTipscm3_mapa_municipio["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderscm3_mapa_municipio["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelscm3_mapa_municipio["Spanish"]["Orden"] = "Orden Presentación";
	$fieldToolTipscm3_mapa_municipio["Spanish"]["Orden"] = "";
	$placeHolderscm3_mapa_municipio["Spanish"]["Orden"] = "";
	$fieldLabelscm3_mapa_municipio["Spanish"]["idCandidatura"] = "Id Candidatura";
	$fieldToolTipscm3_mapa_municipio["Spanish"]["idCandidatura"] = "";
	$placeHolderscm3_mapa_municipio["Spanish"]["idCandidatura"] = "";
	$fieldLabelscm3_mapa_municipio["Spanish"]["Codigo"] = "Codigo";
	$fieldToolTipscm3_mapa_municipio["Spanish"]["Codigo"] = "";
	$placeHolderscm3_mapa_municipio["Spanish"]["Codigo"] = "";
	$fieldLabelscm3_mapa_municipio["Spanish"]["Titulo"] = "Titulo";
	$fieldToolTipscm3_mapa_municipio["Spanish"]["Titulo"] = "";
	$placeHolderscm3_mapa_municipio["Spanish"]["Titulo"] = "";
	$fieldLabelscm3_mapa_municipio["Spanish"]["Color"] = "Color";
	$fieldToolTipscm3_mapa_municipio["Spanish"]["Color"] = "";
	$placeHolderscm3_mapa_municipio["Spanish"]["Color"] = "";
	$fieldLabelscm3_mapa_municipio["Spanish"]["Descripcion"] = "Descripcion";
	$fieldToolTipscm3_mapa_municipio["Spanish"]["Descripcion"] = "";
	$placeHolderscm3_mapa_municipio["Spanish"]["Descripcion"] = "";
	$pageTitlescm3_mapa_municipio["Spanish"]["list"] = "Distribución del voto por municipios y candidatura";
	if (count($fieldToolTipscm3_mapa_municipio["Spanish"]))
		$tdatacm3_mapa_municipio[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm3_mapa_municipio[""] = array();
	$fieldToolTipscm3_mapa_municipio[""] = array();
	$placeHolderscm3_mapa_municipio[""] = array();
	$pageTitlescm3_mapa_municipio[""] = array();
	if (count($fieldToolTipscm3_mapa_municipio[""]))
		$tdatacm3_mapa_municipio[".isUseToolTips"] = true;
}


	$tdatacm3_mapa_municipio[".NCSearch"] = true;



$tdatacm3_mapa_municipio[".shortTableName"] = "cm3_mapa_municipio";
$tdatacm3_mapa_municipio[".nSecOptions"] = 0;
$tdatacm3_mapa_municipio[".recsPerRowPrint"] = 1;
$tdatacm3_mapa_municipio[".mainTableOwnerID"] = "";
$tdatacm3_mapa_municipio[".moveNext"] = 0;
$tdatacm3_mapa_municipio[".entityType"] = 1;

$tdatacm3_mapa_municipio[".strOriginalTableName"] = "eleccandidatura";

	



$tdatacm3_mapa_municipio[".showAddInPopup"] = false;

$tdatacm3_mapa_municipio[".showEditInPopup"] = false;

$tdatacm3_mapa_municipio[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacm3_mapa_municipio[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm3_mapa_municipio[".fieldsForRegister"] = array();

$tdatacm3_mapa_municipio[".listAjax"] = false;

	$tdatacm3_mapa_municipio[".audit"] = false;

	$tdatacm3_mapa_municipio[".locking"] = false;



$tdatacm3_mapa_municipio[".list"] = true;











$tdatacm3_mapa_municipio[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm3_mapa_municipio[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm3_mapa_municipio[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm3_mapa_municipio[".searchSaving"] = false;
//

	$tdatacm3_mapa_municipio[".showSearchPanel"] = false;

$tdatacm3_mapa_municipio[".isUseAjaxSuggest"] = true;

$tdatacm3_mapa_municipio[".rowHighlite"] = true;





$tdatacm3_mapa_municipio[".ajaxCodeSnippetAdded"] = false;

$tdatacm3_mapa_municipio[".buttonsAdded"] = false;

$tdatacm3_mapa_municipio[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm3_mapa_municipio[".isUseTimeForSearch"] = false;



$tdatacm3_mapa_municipio[".badgeColor"] = "4682b4";


$tdatacm3_mapa_municipio[".allSearchFields"] = array();
$tdatacm3_mapa_municipio[".filterFields"] = array();
$tdatacm3_mapa_municipio[".requiredSearchFields"] = array();



$tdatacm3_mapa_municipio[".googleLikeFields"] = array();
$tdatacm3_mapa_municipio[".googleLikeFields"][] = "Descripcion";



$tdatacm3_mapa_municipio[".tableType"] = "list";

$tdatacm3_mapa_municipio[".printerPageOrientation"] = 0;
$tdatacm3_mapa_municipio[".nPrinterPageScale"] = 100;

$tdatacm3_mapa_municipio[".nPrinterSplitRecords"] = 40;

$tdatacm3_mapa_municipio[".nPrinterPDFSplitRecords"] = 40;



$tdatacm3_mapa_municipio[".geocodingEnabled"] = false;





$tdatacm3_mapa_municipio[".listGridLayout"] = 2;





// view page pdf

// print page pdf


$tdatacm3_mapa_municipio[".pageSize"] = 1;

$tdatacm3_mapa_municipio[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm3_mapa_municipio[".strOrderBy"] = $tstrOrderBy;

$tdatacm3_mapa_municipio[".orderindexes"] = array();

$tdatacm3_mapa_municipio[".sqlHead"] = "SELECT can.`idElecCandidatura`,  can.`Convocatoria_idConvocatoria`,  co.`Descripcion`,  can.`Orden`,  ca.`idCandidatura`,  ca.`Codigo`,  ca.`Titulo`,  ca.`Color`";
$tdatacm3_mapa_municipio[".sqlFrom"] = "FROM eleccandidatura can  join (SELECT  distinct elem.`Convocatoria_idConvocatoria` FROM elecmunicandi elem) ele   on (can.`Convocatoria_idConvocatoria` = ele.`Convocatoria_idConvocatoria` )  join candidatura ca on (ca.`idCandidatura` = can.`Candidatura_idCandidatura`)  join convocatoria co on (can.`Convocatoria_idConvocatoria` = co.`idConvocatoria`)";
$tdatacm3_mapa_municipio[".sqlWhereExpr"] = "";
$tdatacm3_mapa_municipio[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm3_mapa_municipio[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm3_mapa_municipio[".arrGroupsPerPage"] = $arrGPP;

$tdatacm3_mapa_municipio[".highlightSearchResults"] = true;

$tableKeyscm3_mapa_municipio = array();
$tableKeyscm3_mapa_municipio[] = "idElecCandidatura";
$tdatacm3_mapa_municipio[".Keys"] = $tableKeyscm3_mapa_municipio;

$tdatacm3_mapa_municipio[".listFields"] = array();
$tdatacm3_mapa_municipio[".listFields"][] = "Codigo";

$tdatacm3_mapa_municipio[".hideMobileList"] = array();


$tdatacm3_mapa_municipio[".viewFields"] = array();

$tdatacm3_mapa_municipio[".addFields"] = array();

$tdatacm3_mapa_municipio[".masterListFields"] = array();
$tdatacm3_mapa_municipio[".masterListFields"][] = "idElecCandidatura";
$tdatacm3_mapa_municipio[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdatacm3_mapa_municipio[".masterListFields"][] = "Descripcion";
$tdatacm3_mapa_municipio[".masterListFields"][] = "Orden";
$tdatacm3_mapa_municipio[".masterListFields"][] = "idCandidatura";
$tdatacm3_mapa_municipio[".masterListFields"][] = "Codigo";
$tdatacm3_mapa_municipio[".masterListFields"][] = "Titulo";
$tdatacm3_mapa_municipio[".masterListFields"][] = "Color";

$tdatacm3_mapa_municipio[".inlineAddFields"] = array();

$tdatacm3_mapa_municipio[".editFields"] = array();

$tdatacm3_mapa_municipio[".inlineEditFields"] = array();

$tdatacm3_mapa_municipio[".updateSelectedFields"] = array();


$tdatacm3_mapa_municipio[".exportFields"] = array();

$tdatacm3_mapa_municipio[".importFields"] = array();

$tdatacm3_mapa_municipio[".printFields"] = array();


//	idElecCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idElecCandidatura";
	$fdata["GoodName"] = "idElecCandidatura";
	$fdata["ownerTable"] = "eleccandidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_municipio","idElecCandidatura");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idElecCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "can.`idElecCandidatura`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_municipio["idElecCandidatura"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "eleccandidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_municipio","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "can.`Convocatoria_idConvocatoria`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "cm3_convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Descripcion";
	
	

	
	$edata["LookupOrderBy"] = "Orden";

	
	
	
	
				//dependent dropdowns @deprecated data ?
	$edata["DependentLookups"] = array();
	$edata["DependentLookups"][] = "idCandidatura";

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_municipio["Convocatoria_idConvocatoria"] = $fdata;
//	Descripcion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Descripcion";
	$fdata["GoodName"] = "Descripcion";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_municipio","Descripcion");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Descripcion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "co.`Descripcion`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_municipio["Descripcion"] = $fdata;
//	Orden
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Orden";
	$fdata["GoodName"] = "Orden";
	$fdata["ownerTable"] = "eleccandidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_municipio","Orden");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Orden";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "can.`Orden`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_municipio["Orden"] = $fdata;
//	idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "idCandidatura";
	$fdata["GoodName"] = "idCandidatura";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_municipio","idCandidatura");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ca.`idCandidatura`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "cm3_eleccandidatura";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idCandidatura";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "Orden";

	
		$edata["UseCategory"] = true;
	$edata["categoryFields"] = array();
	$edata["categoryFields"][] = array( "main" => "Convocatoria_idConvocatoria", "lookup" => "Convocatoria_idConvocatoria" );

	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_municipio["idCandidatura"] = $fdata;
//	Codigo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Codigo";
	$fdata["GoodName"] = "Codigo";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_municipio","Codigo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Codigo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ca.`Codigo`";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_municipio["Codigo"] = $fdata;
//	Titulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Titulo";
	$fdata["GoodName"] = "Titulo";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_municipio","Titulo");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ca.`Titulo`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_municipio["Titulo"] = $fdata;
//	Color
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "Color";
	$fdata["GoodName"] = "Color";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_municipio","Color");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Color";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ca.`Color`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_municipio["Color"] = $fdata;


$tables_data["cm3_mapa_municipio"]=&$tdatacm3_mapa_municipio;
$field_labels["cm3_mapa_municipio"] = &$fieldLabelscm3_mapa_municipio;
$fieldToolTips["cm3_mapa_municipio"] = &$fieldToolTipscm3_mapa_municipio;
$placeHolders["cm3_mapa_municipio"] = &$placeHolderscm3_mapa_municipio;
$page_titles["cm3_mapa_municipio"] = &$pageTitlescm3_mapa_municipio;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm3_mapa_municipio"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["cm3_mapa_municipio"] = array();


	
				$strOriginalDetailsTable="v_candidatura_mapa";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="v_candidatura_mapa";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "v_candidatura_mapa";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
					
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 0;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm3_mapa_municipio"][0] = $masterParams;
				$masterTablesData["cm3_mapa_municipio"][0]["masterKeys"] = array();
	$masterTablesData["cm3_mapa_municipio"][0]["masterKeys"][]="idElecCandidatura";
				$masterTablesData["cm3_mapa_municipio"][0]["detailKeys"] = array();
	$masterTablesData["cm3_mapa_municipio"][0]["detailKeys"][]="idElecCandidatura";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm3_mapa_municipio()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "can.`idElecCandidatura`,  can.`Convocatoria_idConvocatoria`,  co.`Descripcion`,  can.`Orden`,  ca.`idCandidatura`,  ca.`Codigo`,  ca.`Titulo`,  ca.`Color`";
$proto0["m_strFrom"] = "FROM eleccandidatura can  join (SELECT  distinct elem.`Convocatoria_idConvocatoria` FROM elecmunicandi elem) ele   on (can.`Convocatoria_idConvocatoria` = ele.`Convocatoria_idConvocatoria` )  join candidatura ca on (ca.`idCandidatura` = can.`Candidatura_idCandidatura`)  join convocatoria co on (can.`Convocatoria_idConvocatoria` = co.`idConvocatoria`)";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idElecCandidatura",
	"m_strTable" => "can",
	"m_srcTableName" => "cm3_mapa_municipio"
));

$proto6["m_sql"] = "can.`idElecCandidatura`";
$proto6["m_srcTableName"] = "cm3_mapa_municipio";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "can",
	"m_srcTableName" => "cm3_mapa_municipio"
));

$proto8["m_sql"] = "can.`Convocatoria_idConvocatoria`";
$proto8["m_srcTableName"] = "cm3_mapa_municipio";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Descripcion",
	"m_strTable" => "co",
	"m_srcTableName" => "cm3_mapa_municipio"
));

$proto10["m_sql"] = "co.`Descripcion`";
$proto10["m_srcTableName"] = "cm3_mapa_municipio";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "can",
	"m_srcTableName" => "cm3_mapa_municipio"
));

$proto12["m_sql"] = "can.`Orden`";
$proto12["m_srcTableName"] = "cm3_mapa_municipio";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "idCandidatura",
	"m_strTable" => "ca",
	"m_srcTableName" => "cm3_mapa_municipio"
));

$proto14["m_sql"] = "ca.`idCandidatura`";
$proto14["m_srcTableName"] = "cm3_mapa_municipio";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Codigo",
	"m_strTable" => "ca",
	"m_srcTableName" => "cm3_mapa_municipio"
));

$proto16["m_sql"] = "ca.`Codigo`";
$proto16["m_srcTableName"] = "cm3_mapa_municipio";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "ca",
	"m_srcTableName" => "cm3_mapa_municipio"
));

$proto18["m_sql"] = "ca.`Titulo`";
$proto18["m_srcTableName"] = "cm3_mapa_municipio";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "Color",
	"m_strTable" => "ca",
	"m_srcTableName" => "cm3_mapa_municipio"
));

$proto20["m_sql"] = "ca.`Color`";
$proto20["m_srcTableName"] = "cm3_mapa_municipio";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto22=array();
$proto22["m_link"] = "SQLL_MAIN";
			$proto23=array();
$proto23["m_strName"] = "eleccandidatura";
$proto23["m_srcTableName"] = "cm3_mapa_municipio";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "idElecCandidatura";
$proto23["m_columns"][] = "Convocatoria_idConvocatoria";
$proto23["m_columns"][] = "Orden";
$proto23["m_columns"][] = "Candidatura_idCandidatura";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "eleccandidatura can";
$proto22["m_alias"] = "can";
$proto22["m_srcTableName"] = "cm3_mapa_municipio";
$proto24=array();
$proto24["m_sql"] = "";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
												$proto26=array();
$proto26["m_link"] = "SQLL_INNERJOIN";
			$proto27=array();
$proto27["m_strHead"] = "SELECT  distinct";
$proto27["m_strFieldList"] = "elem.`Convocatoria_idConvocatoria`";
$proto27["m_strFrom"] = "FROM elecmunicandi elem";
$proto27["m_strWhere"] = "";
$proto27["m_strOrderBy"] = "";
	
		;
			$proto27["cipherer"] = null;
$proto29=array();
$proto29["m_sql"] = "";
$proto29["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto29["m_column"]=$obj;
$proto29["m_contained"] = array();
$proto29["m_strCase"] = "";
$proto29["m_havingmode"] = false;
$proto29["m_inBrackets"] = false;
$proto29["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto29);

$proto27["m_where"] = $obj;
$proto31=array();
$proto31["m_sql"] = "";
$proto31["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto31["m_column"]=$obj;
$proto31["m_contained"] = array();
$proto31["m_strCase"] = "";
$proto31["m_havingmode"] = false;
$proto31["m_inBrackets"] = false;
$proto31["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto31);

$proto27["m_having"] = $obj;
$proto27["m_fieldlist"] = array();
						$proto33=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "elem",
	"m_srcTableName" => "cm3_mapa_municipio"
));

$proto33["m_sql"] = "elem.`Convocatoria_idConvocatoria`";
$proto33["m_srcTableName"] = "cm3_mapa_municipio";
$proto33["m_expr"]=$obj;
$proto33["m_alias"] = "";
$obj = new SQLFieldListItem($proto33);

$proto27["m_fieldlist"][]=$obj;
$proto27["m_fromlist"] = array();
												$proto35=array();
$proto35["m_link"] = "SQLL_MAIN";
			$proto36=array();
$proto36["m_strName"] = "elecmunicandi";
$proto36["m_srcTableName"] = "cm3_mapa_municipio";
$proto36["m_columns"] = array();
$proto36["m_columns"][] = "idElecMuniCandi";
$proto36["m_columns"][] = "Convocatoria_idConvocatoria";
$proto36["m_columns"][] = "Candidatura_idCandidatura";
$proto36["m_columns"][] = "Municipio";
$proto36["m_columns"][] = "Votos";
$proto36["m_columns"][] = "PorcVotos";
$obj = new SQLTable($proto36);

$proto35["m_table"] = $obj;
$proto35["m_sql"] = "elecmunicandi elem";
$proto35["m_alias"] = "elem";
$proto35["m_srcTableName"] = "cm3_mapa_municipio";
$proto37=array();
$proto37["m_sql"] = "";
$proto37["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto37["m_column"]=$obj;
$proto37["m_contained"] = array();
$proto37["m_strCase"] = "";
$proto37["m_havingmode"] = false;
$proto37["m_inBrackets"] = false;
$proto37["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto37);

$proto35["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto35);

$proto27["m_fromlist"][]=$obj;
$proto27["m_groupby"] = array();
$proto27["m_orderby"] = array();
$proto27["m_srcTableName"]="cm3_mapa_municipio";		
$obj = new SQLQuery($proto27);

$proto26["m_table"] = $obj;
$proto26["m_sql"] = "join (SELECT  distinct elem.`Convocatoria_idConvocatoria` FROM elecmunicandi elem) ele   on (can.`Convocatoria_idConvocatoria` = ele.`Convocatoria_idConvocatoria` )";
$proto26["m_alias"] = "ele";
$proto26["m_srcTableName"] = "cm3_mapa_municipio";
$proto39=array();
$proto39["m_sql"] = "can.`Convocatoria_idConvocatoria` = ele.`Convocatoria_idConvocatoria` ";
$proto39["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "can",
	"m_srcTableName" => "cm3_mapa_municipio"
));

$proto39["m_column"]=$obj;
$proto39["m_contained"] = array();
$proto39["m_strCase"] = "= ele.`Convocatoria_idConvocatoria`";
$proto39["m_havingmode"] = false;
$proto39["m_inBrackets"] = true;
$proto39["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto39);

$proto26["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto26);

$proto0["m_fromlist"][]=$obj;
												$proto41=array();
$proto41["m_link"] = "SQLL_INNERJOIN";
			$proto42=array();
$proto42["m_strName"] = "candidatura";
$proto42["m_srcTableName"] = "cm3_mapa_municipio";
$proto42["m_columns"] = array();
$proto42["m_columns"][] = "idCandidatura";
$proto42["m_columns"][] = "Codigo";
$proto42["m_columns"][] = "Titulo";
$proto42["m_columns"][] = "Descripcion";
$proto42["m_columns"][] = "Color";
$proto42["m_columns"][] = "Logo";
$obj = new SQLTable($proto42);

$proto41["m_table"] = $obj;
$proto41["m_sql"] = "join candidatura ca on (ca.`idCandidatura` = can.`Candidatura_idCandidatura`)";
$proto41["m_alias"] = "ca";
$proto41["m_srcTableName"] = "cm3_mapa_municipio";
$proto43=array();
$proto43["m_sql"] = "ca.`idCandidatura` = can.`Candidatura_idCandidatura`";
$proto43["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "idCandidatura",
	"m_strTable" => "ca",
	"m_srcTableName" => "cm3_mapa_municipio"
));

$proto43["m_column"]=$obj;
$proto43["m_contained"] = array();
$proto43["m_strCase"] = "= can.`Candidatura_idCandidatura`";
$proto43["m_havingmode"] = false;
$proto43["m_inBrackets"] = true;
$proto43["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto43);

$proto41["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto41);

$proto0["m_fromlist"][]=$obj;
												$proto45=array();
$proto45["m_link"] = "SQLL_INNERJOIN";
			$proto46=array();
$proto46["m_strName"] = "convocatoria";
$proto46["m_srcTableName"] = "cm3_mapa_municipio";
$proto46["m_columns"] = array();
$proto46["m_columns"][] = "idConvocatoria";
$proto46["m_columns"][] = "Orden";
$proto46["m_columns"][] = "EsAsamblea";
$proto46["m_columns"][] = "Titulo";
$proto46["m_columns"][] = "Descripcion";
$obj = new SQLTable($proto46);

$proto45["m_table"] = $obj;
$proto45["m_sql"] = "join convocatoria co on (can.`Convocatoria_idConvocatoria` = co.`idConvocatoria`)";
$proto45["m_alias"] = "co";
$proto45["m_srcTableName"] = "cm3_mapa_municipio";
$proto47=array();
$proto47["m_sql"] = "can.`Convocatoria_idConvocatoria` = co.`idConvocatoria`";
$proto47["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "can",
	"m_srcTableName" => "cm3_mapa_municipio"
));

$proto47["m_column"]=$obj;
$proto47["m_contained"] = array();
$proto47["m_strCase"] = "= co.`idConvocatoria`";
$proto47["m_havingmode"] = false;
$proto47["m_inBrackets"] = true;
$proto47["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto47);

$proto45["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto45);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="cm3_mapa_municipio";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_cm3_mapa_municipio = createSqlQuery_cm3_mapa_municipio();


	
		;

								

$tdatacm3_mapa_municipio[".sqlquery"] = $queryData_cm3_mapa_municipio;

$tableEvents["cm3_mapa_municipio"] = new eventsBase;
$tdatacm3_mapa_municipio[".hasEvents"] = false;

?>