<?php
require_once(getabspath("classes/cipherer.php"));




$tdataconvocatoria = array();
	$tdataconvocatoria[".truncateText"] = true;
	$tdataconvocatoria[".NumberOfChars"] = 80;
	$tdataconvocatoria[".ShortName"] = "convocatoria";
	$tdataconvocatoria[".OwnerID"] = "";
	$tdataconvocatoria[".OriginalTable"] = "convocatoria";

//	field labels
$fieldLabelsconvocatoria = array();
$fieldToolTipsconvocatoria = array();
$pageTitlesconvocatoria = array();
$placeHoldersconvocatoria = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelsconvocatoria["Spanish"] = array();
	$fieldToolTipsconvocatoria["Spanish"] = array();
	$placeHoldersconvocatoria["Spanish"] = array();
	$pageTitlesconvocatoria["Spanish"] = array();
	$fieldLabelsconvocatoria["Spanish"]["idConvocatoria"] = "Id Interno";
	$fieldToolTipsconvocatoria["Spanish"]["idConvocatoria"] = "";
	$placeHoldersconvocatoria["Spanish"]["idConvocatoria"] = "";
	$fieldLabelsconvocatoria["Spanish"]["Orden"] = "Orden";
	$fieldToolTipsconvocatoria["Spanish"]["Orden"] = "";
	$placeHoldersconvocatoria["Spanish"]["Orden"] = "";
	$fieldLabelsconvocatoria["Spanish"]["EsAsamblea"] = "Es Asamblea?";
	$fieldToolTipsconvocatoria["Spanish"]["EsAsamblea"] = "";
	$placeHoldersconvocatoria["Spanish"]["EsAsamblea"] = "";
	$fieldLabelsconvocatoria["Spanish"]["Titulo"] = "Título";
	$fieldToolTipsconvocatoria["Spanish"]["Titulo"] = "";
	$placeHoldersconvocatoria["Spanish"]["Titulo"] = "";
	$fieldLabelsconvocatoria["Spanish"]["Descripcion"] = "Descripción";
	$fieldToolTipsconvocatoria["Spanish"]["Descripcion"] = "";
	$placeHoldersconvocatoria["Spanish"]["Descripcion"] = "";
	if (count($fieldToolTipsconvocatoria["Spanish"]))
		$tdataconvocatoria[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsconvocatoria[""] = array();
	$fieldToolTipsconvocatoria[""] = array();
	$placeHoldersconvocatoria[""] = array();
	$pageTitlesconvocatoria[""] = array();
	if (count($fieldToolTipsconvocatoria[""]))
		$tdataconvocatoria[".isUseToolTips"] = true;
}


	$tdataconvocatoria[".NCSearch"] = true;



$tdataconvocatoria[".shortTableName"] = "convocatoria";
$tdataconvocatoria[".nSecOptions"] = 0;
$tdataconvocatoria[".recsPerRowList"] = 1;
$tdataconvocatoria[".recsPerRowPrint"] = 1;
$tdataconvocatoria[".mainTableOwnerID"] = "";
$tdataconvocatoria[".moveNext"] = 1;
$tdataconvocatoria[".entityType"] = 0;

$tdataconvocatoria[".strOriginalTableName"] = "convocatoria";

	



$tdataconvocatoria[".showAddInPopup"] = true;

$tdataconvocatoria[".showEditInPopup"] = true;

$tdataconvocatoria[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["add"] = "view_bootstrap";
			;
$popupPagesLayoutNames["edit"] = "view_bootstrap";
						
	;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdataconvocatoria[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataconvocatoria[".fieldsForRegister"] = array();

$tdataconvocatoria[".listAjax"] = false;

	$tdataconvocatoria[".audit"] = false;

	$tdataconvocatoria[".locking"] = false;



$tdataconvocatoria[".list"] = true;





$tdataconvocatoria[".exportFormatting"] = 2;
$tdataconvocatoria[".exportDelimiter"] = ",";
		
$tdataconvocatoria[".view"] = true;


$tdataconvocatoria[".exportTo"] = true;

$tdataconvocatoria[".printFriendly"] = true;


$tdataconvocatoria[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdataconvocatoria[".allowShowHideFields"] = true;
//

// Allow Fields Reordering in GRID
$tdataconvocatoria[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdataconvocatoria[".searchSaving"] = false;
//

$tdataconvocatoria[".showSearchPanel"] = true;
		$tdataconvocatoria[".flexibleSearch"] = true;

$tdataconvocatoria[".isUseAjaxSuggest"] = true;

$tdataconvocatoria[".rowHighlite"] = true;





$tdataconvocatoria[".ajaxCodeSnippetAdded"] = false;

$tdataconvocatoria[".buttonsAdded"] = false;

$tdataconvocatoria[".addPageEvents"] = false;

// use timepicker for search panel
$tdataconvocatoria[".isUseTimeForSearch"] = false;



$tdataconvocatoria[".badgeColor"] = "DC143C";


$tdataconvocatoria[".allSearchFields"] = array();
$tdataconvocatoria[".filterFields"] = array();
$tdataconvocatoria[".requiredSearchFields"] = array();

$tdataconvocatoria[".allSearchFields"][] = "idConvocatoria";
	$tdataconvocatoria[".allSearchFields"][] = "Orden";
	$tdataconvocatoria[".allSearchFields"][] = "EsAsamblea";
	$tdataconvocatoria[".allSearchFields"][] = "Titulo";
	$tdataconvocatoria[".allSearchFields"][] = "Descripcion";
	

$tdataconvocatoria[".googleLikeFields"] = array();
$tdataconvocatoria[".googleLikeFields"][] = "idConvocatoria";
$tdataconvocatoria[".googleLikeFields"][] = "Orden";
$tdataconvocatoria[".googleLikeFields"][] = "EsAsamblea";
$tdataconvocatoria[".googleLikeFields"][] = "Titulo";
$tdataconvocatoria[".googleLikeFields"][] = "Descripcion";


$tdataconvocatoria[".advSearchFields"] = array();
$tdataconvocatoria[".advSearchFields"][] = "idConvocatoria";
$tdataconvocatoria[".advSearchFields"][] = "Orden";
$tdataconvocatoria[".advSearchFields"][] = "EsAsamblea";
$tdataconvocatoria[".advSearchFields"][] = "Titulo";
$tdataconvocatoria[".advSearchFields"][] = "Descripcion";

$tdataconvocatoria[".tableType"] = "list";

$tdataconvocatoria[".printerPageOrientation"] = 0;
$tdataconvocatoria[".nPrinterPageScale"] = 100;

$tdataconvocatoria[".nPrinterSplitRecords"] = 40;

$tdataconvocatoria[".nPrinterPDFSplitRecords"] = 40;



$tdataconvocatoria[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdataconvocatoria[".pageSize"] = 6;

$tdataconvocatoria[".warnLeavingPages"] = true;



$tstrOrderBy = "order by Orden";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataconvocatoria[".strOrderBy"] = $tstrOrderBy;

$tdataconvocatoria[".orderindexes"] = array();
	$tdataconvocatoria[".orderindexes"][] = array(2, (1 ? "ASC" : "DESC"), "Orden");


$tdataconvocatoria[".sqlHead"] = "SELECT idConvocatoria,  	Orden,  	EsAsamblea,  	Titulo,  	Descripcion";
$tdataconvocatoria[".sqlFrom"] = "FROM convocatoria";
$tdataconvocatoria[".sqlWhereExpr"] = "";
$tdataconvocatoria[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataconvocatoria[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataconvocatoria[".arrGroupsPerPage"] = $arrGPP;

$tdataconvocatoria[".highlightSearchResults"] = true;

$tableKeysconvocatoria = array();
$tableKeysconvocatoria[] = "idConvocatoria";
$tdataconvocatoria[".Keys"] = $tableKeysconvocatoria;

$tdataconvocatoria[".listFields"] = array();
$tdataconvocatoria[".listFields"][] = "Titulo";
$tdataconvocatoria[".listFields"][] = "Descripcion";

$tdataconvocatoria[".hideMobileList"] = array();


$tdataconvocatoria[".viewFields"] = array();
$tdataconvocatoria[".viewFields"][] = "idConvocatoria";
$tdataconvocatoria[".viewFields"][] = "Orden";
$tdataconvocatoria[".viewFields"][] = "EsAsamblea";
$tdataconvocatoria[".viewFields"][] = "Titulo";
$tdataconvocatoria[".viewFields"][] = "Descripcion";

$tdataconvocatoria[".addFields"] = array();

$tdataconvocatoria[".masterListFields"] = array();
$tdataconvocatoria[".masterListFields"][] = "idConvocatoria";
$tdataconvocatoria[".masterListFields"][] = "Orden";
$tdataconvocatoria[".masterListFields"][] = "EsAsamblea";
$tdataconvocatoria[".masterListFields"][] = "Titulo";
$tdataconvocatoria[".masterListFields"][] = "Descripcion";

$tdataconvocatoria[".inlineAddFields"] = array();

$tdataconvocatoria[".editFields"] = array();

$tdataconvocatoria[".inlineEditFields"] = array();

$tdataconvocatoria[".updateSelectedFields"] = array();


$tdataconvocatoria[".exportFields"] = array();
$tdataconvocatoria[".exportFields"][] = "idConvocatoria";
$tdataconvocatoria[".exportFields"][] = "Orden";
$tdataconvocatoria[".exportFields"][] = "EsAsamblea";
$tdataconvocatoria[".exportFields"][] = "Titulo";
$tdataconvocatoria[".exportFields"][] = "Descripcion";

$tdataconvocatoria[".importFields"] = array();

$tdataconvocatoria[".printFields"] = array();
$tdataconvocatoria[".printFields"][] = "idConvocatoria";
$tdataconvocatoria[".printFields"][] = "Orden";
$tdataconvocatoria[".printFields"][] = "EsAsamblea";
$tdataconvocatoria[".printFields"][] = "Titulo";
$tdataconvocatoria[".printFields"][] = "Descripcion";


//	idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idConvocatoria";
	$fdata["GoodName"] = "idConvocatoria";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("convocatoria","idConvocatoria");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataconvocatoria["idConvocatoria"] = $fdata;
//	Orden
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Orden";
	$fdata["GoodName"] = "Orden";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("convocatoria","Orden");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Orden";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Orden";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataconvocatoria["Orden"] = $fdata;
//	EsAsamblea
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "EsAsamblea";
	$fdata["GoodName"] = "EsAsamblea";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("convocatoria","EsAsamblea");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "EsAsamblea";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "EsAsamblea";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataconvocatoria["EsAsamblea"] = $fdata;
//	Titulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Titulo";
	$fdata["GoodName"] = "Titulo";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("convocatoria","Titulo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Titulo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=30";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataconvocatoria["Titulo"] = $fdata;
//	Descripcion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Descripcion";
	$fdata["GoodName"] = "Descripcion";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("convocatoria","Descripcion");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Descripcion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Descripcion";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=500";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataconvocatoria["Descripcion"] = $fdata;


$tables_data["convocatoria"]=&$tdataconvocatoria;
$field_labels["convocatoria"] = &$fieldLabelsconvocatoria;
$fieldToolTips["convocatoria"] = &$fieldToolTipsconvocatoria;
$placeHolders["convocatoria"] = &$placeHoldersconvocatoria;
$page_titles["convocatoria"] = &$pageTitlesconvocatoria;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["convocatoria"] = array();
//	electos
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="electos";
		$detailsParam["dOriginalTable"] = "electos";
		$detailsParam["proceedLink"] = false;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "electos";
	$detailsParam["dCaptionTable"] = GetTableCaption("electos");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "1";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";
//	escanos
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="escanos";
		$detailsParam["dOriginalTable"] = "escanos";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "escanos";
	$detailsParam["dCaptionTable"] = GetTableCaption("escanos");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";
//	v_convocatoria_total
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="v_convocatoria_total";
		$detailsParam["dOriginalTable"] = "v_convocatoria_total";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm1_convocatoria_total";
	$detailsParam["dCaptionTable"] = GetTableCaption("v_convocatoria_total");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";
//	cm1_escanos Chart
	
	

		$dIndex = 3;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm1_escanos Chart";
		$detailsParam["dOriginalTable"] = "escanos";
		$detailsParam["proceedLink"] = true;
			$detailsParam["dType"]=PAGE_CHART;
		$detailsParam["dShortTable"] = "cm1_escanos_Chart";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm1_escanos_Chart");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";
//	v_elec_muni
	
	

		$dIndex = 4;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="v_elec_muni";
		$detailsParam["dOriginalTable"] = "v_elec_muni";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "v_elec_muni";
	$detailsParam["dCaptionTable"] = GetTableCaption("v_elec_muni");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";
//	cm1_votos Chart
	
	

		$dIndex = 5;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm1_votos Chart";
		$detailsParam["dOriginalTable"] = "escanos";
		$detailsParam["proceedLink"] = true;
			$detailsParam["dType"]=PAGE_CHART;
		$detailsParam["dShortTable"] = "cm1_votos_Chart";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm1_votos_Chart");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";
//	v_elec_distri
	
	

		$dIndex = 6;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="v_elec_distri";
		$detailsParam["dOriginalTable"] = "v_elec_distri";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "v_elec_distri";
	$detailsParam["dCaptionTable"] = GetTableCaption("v_elec_distri");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";
//	cm1_grafico_escanos
	
	

		$dIndex = 7;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm1_grafico_escanos";
		$detailsParam["dOriginalTable"] = "convocatoria";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm1_grafico_escanos";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm1_grafico_escanos");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["detailKeys"][]="idConvocatoria";
//	cm1_2_Candidaturas
	
	

		$dIndex = 8;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm1_2_Candidaturas";
		$detailsParam["dOriginalTable"] = "escanos";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm1_2_Candidaturas";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm1_2_Candidaturas");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";
//	cm1_2_Electos
	
	

		$dIndex = 9;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm1_2_Electos";
		$detailsParam["dOriginalTable"] = "electos";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm1_2_Electos";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm1_2_Electos");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";

// tables which are master tables for current table (detail)
$masterTablesData["convocatoria"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_convocatoria()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "idConvocatoria,  	Orden,  	EsAsamblea,  	Titulo,  	Descripcion";
$proto3["m_strFrom"] = "FROM convocatoria";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "order by Orden";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idConvocatoria",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "convocatoria"
));

$proto9["m_sql"] = "idConvocatoria";
$proto9["m_srcTableName"] = "convocatoria";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "convocatoria"
));

$proto11["m_sql"] = "Orden";
$proto11["m_srcTableName"] = "convocatoria";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "EsAsamblea",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "convocatoria"
));

$proto13["m_sql"] = "EsAsamblea";
$proto13["m_srcTableName"] = "convocatoria";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "convocatoria"
));

$proto15["m_sql"] = "Titulo";
$proto15["m_srcTableName"] = "convocatoria";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "Descripcion",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "convocatoria"
));

$proto17["m_sql"] = "Descripcion";
$proto17["m_srcTableName"] = "convocatoria";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto19=array();
$proto19["m_link"] = "SQLL_MAIN";
			$proto20=array();
$proto20["m_strName"] = "convocatoria";
$proto20["m_srcTableName"] = "convocatoria";
$proto20["m_columns"] = array();
$proto20["m_columns"][] = "idConvocatoria";
$proto20["m_columns"][] = "Orden";
$proto20["m_columns"][] = "EsAsamblea";
$proto20["m_columns"][] = "Titulo";
$proto20["m_columns"][] = "Descripcion";
$obj = new SQLTable($proto20);

$proto19["m_table"] = $obj;
$proto19["m_sql"] = "convocatoria";
$proto19["m_alias"] = "";
$proto19["m_srcTableName"] = "convocatoria";
$proto21=array();
$proto21["m_sql"] = "";
$proto21["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto21["m_column"]=$obj;
$proto21["m_contained"] = array();
$proto21["m_strCase"] = "";
$proto21["m_havingmode"] = false;
$proto21["m_inBrackets"] = false;
$proto21["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto21);

$proto19["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto19);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto23=array();
						$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "convocatoria"
));

$proto23["m_column"]=$obj;
$proto23["m_bAsc"] = 1;
$proto23["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto23);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="convocatoria";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_convocatoria = createSqlQuery_convocatoria();


	
		;

					

$tdataconvocatoria[".sqlquery"] = $queryData_convocatoria;

$tableEvents["convocatoria"] = new eventsBase;
$tdataconvocatoria[".hasEvents"] = false;

?>