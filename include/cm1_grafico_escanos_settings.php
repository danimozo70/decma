<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm1_grafico_escanos = array();
	$tdatacm1_grafico_escanos[".truncateText"] = true;
	$tdatacm1_grafico_escanos[".NumberOfChars"] = 80;
	$tdatacm1_grafico_escanos[".ShortName"] = "cm1_grafico_escanos";
	$tdatacm1_grafico_escanos[".OwnerID"] = "";
	$tdatacm1_grafico_escanos[".OriginalTable"] = "convocatoria";

//	field labels
$fieldLabelscm1_grafico_escanos = array();
$fieldToolTipscm1_grafico_escanos = array();
$pageTitlescm1_grafico_escanos = array();
$placeHolderscm1_grafico_escanos = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm1_grafico_escanos["Spanish"] = array();
	$fieldToolTipscm1_grafico_escanos["Spanish"] = array();
	$placeHolderscm1_grafico_escanos["Spanish"] = array();
	$pageTitlescm1_grafico_escanos["Spanish"] = array();
	$fieldLabelscm1_grafico_escanos["Spanish"]["idConvocatoria"] = "Id Interno";
	$fieldToolTipscm1_grafico_escanos["Spanish"]["idConvocatoria"] = "";
	$placeHolderscm1_grafico_escanos["Spanish"]["idConvocatoria"] = "";
	$fieldLabelscm1_grafico_escanos["Spanish"]["Orden"] = "Orden";
	$fieldToolTipscm1_grafico_escanos["Spanish"]["Orden"] = "";
	$placeHolderscm1_grafico_escanos["Spanish"]["Orden"] = "";
	$fieldLabelscm1_grafico_escanos["Spanish"]["EsAsamblea"] = "Es Asamblea?";
	$fieldToolTipscm1_grafico_escanos["Spanish"]["EsAsamblea"] = "";
	$placeHolderscm1_grafico_escanos["Spanish"]["EsAsamblea"] = "";
	$fieldLabelscm1_grafico_escanos["Spanish"]["Titulo"] = "Título";
	$fieldToolTipscm1_grafico_escanos["Spanish"]["Titulo"] = "";
	$placeHolderscm1_grafico_escanos["Spanish"]["Titulo"] = "";
	$fieldLabelscm1_grafico_escanos["Spanish"]["Descripcion"] = "Descripción";
	$fieldToolTipscm1_grafico_escanos["Spanish"]["Descripcion"] = "";
	$placeHolderscm1_grafico_escanos["Spanish"]["Descripcion"] = "";
	$pageTitlescm1_grafico_escanos["Spanish"]["list"] = "<strong>{%master.Titulo}</strong>, Distribución de escaños";
	if (count($fieldToolTipscm1_grafico_escanos["Spanish"]))
		$tdatacm1_grafico_escanos[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm1_grafico_escanos[""] = array();
	$fieldToolTipscm1_grafico_escanos[""] = array();
	$placeHolderscm1_grafico_escanos[""] = array();
	$pageTitlescm1_grafico_escanos[""] = array();
	if (count($fieldToolTipscm1_grafico_escanos[""]))
		$tdatacm1_grafico_escanos[".isUseToolTips"] = true;
}


	$tdatacm1_grafico_escanos[".NCSearch"] = true;



$tdatacm1_grafico_escanos[".shortTableName"] = "cm1_grafico_escanos";
$tdatacm1_grafico_escanos[".nSecOptions"] = 0;
$tdatacm1_grafico_escanos[".recsPerRowList"] = 1;
$tdatacm1_grafico_escanos[".recsPerRowPrint"] = 1;
$tdatacm1_grafico_escanos[".mainTableOwnerID"] = "";
$tdatacm1_grafico_escanos[".moveNext"] = 1;
$tdatacm1_grafico_escanos[".entityType"] = 1;

$tdatacm1_grafico_escanos[".strOriginalTableName"] = "convocatoria";

	



$tdatacm1_grafico_escanos[".showAddInPopup"] = false;

$tdatacm1_grafico_escanos[".showEditInPopup"] = false;

$tdatacm1_grafico_escanos[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacm1_grafico_escanos[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm1_grafico_escanos[".fieldsForRegister"] = array();

$tdatacm1_grafico_escanos[".listAjax"] = false;

	$tdatacm1_grafico_escanos[".audit"] = false;

	$tdatacm1_grafico_escanos[".locking"] = false;



$tdatacm1_grafico_escanos[".list"] = true;



$tdatacm1_grafico_escanos[".createSortByDropdown"] = true;
$tdatacm1_grafico_escanos[".strSortControlSettingsJSON"] = "";








$tdatacm1_grafico_escanos[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm1_grafico_escanos[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm1_grafico_escanos[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm1_grafico_escanos[".searchSaving"] = false;
//

	$tdatacm1_grafico_escanos[".showSearchPanel"] = false;

$tdatacm1_grafico_escanos[".isUseAjaxSuggest"] = true;

$tdatacm1_grafico_escanos[".rowHighlite"] = true;





$tdatacm1_grafico_escanos[".ajaxCodeSnippetAdded"] = false;

$tdatacm1_grafico_escanos[".buttonsAdded"] = false;

$tdatacm1_grafico_escanos[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm1_grafico_escanos[".isUseTimeForSearch"] = false;



$tdatacm1_grafico_escanos[".badgeColor"] = "b22222";


$tdatacm1_grafico_escanos[".allSearchFields"] = array();
$tdatacm1_grafico_escanos[".filterFields"] = array();
$tdatacm1_grafico_escanos[".requiredSearchFields"] = array();



$tdatacm1_grafico_escanos[".googleLikeFields"] = array();
$tdatacm1_grafico_escanos[".googleLikeFields"][] = "idConvocatoria";
$tdatacm1_grafico_escanos[".googleLikeFields"][] = "Orden";
$tdatacm1_grafico_escanos[".googleLikeFields"][] = "EsAsamblea";
$tdatacm1_grafico_escanos[".googleLikeFields"][] = "Titulo";
$tdatacm1_grafico_escanos[".googleLikeFields"][] = "Descripcion";



$tdatacm1_grafico_escanos[".tableType"] = "list";

$tdatacm1_grafico_escanos[".printerPageOrientation"] = 0;
$tdatacm1_grafico_escanos[".nPrinterPageScale"] = 100;

$tdatacm1_grafico_escanos[".nPrinterSplitRecords"] = 40;

$tdatacm1_grafico_escanos[".nPrinterPDFSplitRecords"] = 40;



$tdatacm1_grafico_escanos[".geocodingEnabled"] = false;





$tdatacm1_grafico_escanos[".listGridLayout"] = 1;





// view page pdf

// print page pdf


$tdatacm1_grafico_escanos[".pageSize"] = 1;

$tdatacm1_grafico_escanos[".warnLeavingPages"] = true;



$tstrOrderBy = "order by Orden";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm1_grafico_escanos[".strOrderBy"] = $tstrOrderBy;

$tdatacm1_grafico_escanos[".orderindexes"] = array();
	$tdatacm1_grafico_escanos[".orderindexes"][] = array(2, (1 ? "ASC" : "DESC"), "Orden");


$tdatacm1_grafico_escanos[".sqlHead"] = "SELECT idConvocatoria,  	Orden,  	EsAsamblea,  	Titulo,  	Descripcion";
$tdatacm1_grafico_escanos[".sqlFrom"] = "FROM convocatoria";
$tdatacm1_grafico_escanos[".sqlWhereExpr"] = "";
$tdatacm1_grafico_escanos[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm1_grafico_escanos[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm1_grafico_escanos[".arrGroupsPerPage"] = $arrGPP;

$tdatacm1_grafico_escanos[".highlightSearchResults"] = true;

$tableKeyscm1_grafico_escanos = array();
$tableKeyscm1_grafico_escanos[] = "idConvocatoria";
$tdatacm1_grafico_escanos[".Keys"] = $tableKeyscm1_grafico_escanos;

$tdatacm1_grafico_escanos[".listFields"] = array();
$tdatacm1_grafico_escanos[".listFields"][] = "idConvocatoria";

$tdatacm1_grafico_escanos[".hideMobileList"] = array();


$tdatacm1_grafico_escanos[".viewFields"] = array();

$tdatacm1_grafico_escanos[".addFields"] = array();

$tdatacm1_grafico_escanos[".masterListFields"] = array();
$tdatacm1_grafico_escanos[".masterListFields"][] = "idConvocatoria";
$tdatacm1_grafico_escanos[".masterListFields"][] = "Orden";
$tdatacm1_grafico_escanos[".masterListFields"][] = "EsAsamblea";
$tdatacm1_grafico_escanos[".masterListFields"][] = "Titulo";
$tdatacm1_grafico_escanos[".masterListFields"][] = "Descripcion";

$tdatacm1_grafico_escanos[".inlineAddFields"] = array();

$tdatacm1_grafico_escanos[".editFields"] = array();

$tdatacm1_grafico_escanos[".inlineEditFields"] = array();

$tdatacm1_grafico_escanos[".updateSelectedFields"] = array();


$tdatacm1_grafico_escanos[".exportFields"] = array();

$tdatacm1_grafico_escanos[".importFields"] = array();

$tdatacm1_grafico_escanos[".printFields"] = array();


//	idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idConvocatoria";
	$fdata["GoodName"] = "idConvocatoria";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm1_grafico_escanos","idConvocatoria");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm1_grafico_escanos["idConvocatoria"] = $fdata;
//	Orden
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Orden";
	$fdata["GoodName"] = "Orden";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm1_grafico_escanos","Orden");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Orden";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Orden";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm1_grafico_escanos["Orden"] = $fdata;
//	EsAsamblea
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "EsAsamblea";
	$fdata["GoodName"] = "EsAsamblea";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm1_grafico_escanos","EsAsamblea");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "EsAsamblea";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "EsAsamblea";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm1_grafico_escanos["EsAsamblea"] = $fdata;
//	Titulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Titulo";
	$fdata["GoodName"] = "Titulo";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm1_grafico_escanos","Titulo");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Titulo";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=30";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm1_grafico_escanos["Titulo"] = $fdata;
//	Descripcion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Descripcion";
	$fdata["GoodName"] = "Descripcion";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm1_grafico_escanos","Descripcion");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Descripcion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Descripcion";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=500";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm1_grafico_escanos["Descripcion"] = $fdata;


$tables_data["cm1_grafico_escanos"]=&$tdatacm1_grafico_escanos;
$field_labels["cm1_grafico_escanos"] = &$fieldLabelscm1_grafico_escanos;
$fieldToolTips["cm1_grafico_escanos"] = &$fieldToolTipscm1_grafico_escanos;
$placeHolders["cm1_grafico_escanos"] = &$placeHolderscm1_grafico_escanos;
$page_titles["cm1_grafico_escanos"] = &$pageTitlescm1_grafico_escanos;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm1_grafico_escanos"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["cm1_grafico_escanos"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm1_grafico_escanos"][0] = $masterParams;
				$masterTablesData["cm1_grafico_escanos"][0]["masterKeys"] = array();
	$masterTablesData["cm1_grafico_escanos"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["cm1_grafico_escanos"][0]["detailKeys"] = array();
	$masterTablesData["cm1_grafico_escanos"][0]["detailKeys"][]="idConvocatoria";
		
	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm1_1_Convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm1_1_Convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm1_grafico_escanos"][1] = $masterParams;
				$masterTablesData["cm1_grafico_escanos"][1]["masterKeys"] = array();
	$masterTablesData["cm1_grafico_escanos"][1]["masterKeys"][]="idConvocatoria";
				$masterTablesData["cm1_grafico_escanos"][1]["detailKeys"] = array();
	$masterTablesData["cm1_grafico_escanos"][1]["detailKeys"][]="idConvocatoria";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm1_grafico_escanos()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idConvocatoria,  	Orden,  	EsAsamblea,  	Titulo,  	Descripcion";
$proto0["m_strFrom"] = "FROM convocatoria";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "order by Orden";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idConvocatoria",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm1_grafico_escanos"
));

$proto6["m_sql"] = "idConvocatoria";
$proto6["m_srcTableName"] = "cm1_grafico_escanos";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm1_grafico_escanos"
));

$proto8["m_sql"] = "Orden";
$proto8["m_srcTableName"] = "cm1_grafico_escanos";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "EsAsamblea",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm1_grafico_escanos"
));

$proto10["m_sql"] = "EsAsamblea";
$proto10["m_srcTableName"] = "cm1_grafico_escanos";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm1_grafico_escanos"
));

$proto12["m_sql"] = "Titulo";
$proto12["m_srcTableName"] = "cm1_grafico_escanos";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "Descripcion",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm1_grafico_escanos"
));

$proto14["m_sql"] = "Descripcion";
$proto14["m_srcTableName"] = "cm1_grafico_escanos";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto16=array();
$proto16["m_link"] = "SQLL_MAIN";
			$proto17=array();
$proto17["m_strName"] = "convocatoria";
$proto17["m_srcTableName"] = "cm1_grafico_escanos";
$proto17["m_columns"] = array();
$proto17["m_columns"][] = "idConvocatoria";
$proto17["m_columns"][] = "Orden";
$proto17["m_columns"][] = "EsAsamblea";
$proto17["m_columns"][] = "Titulo";
$proto17["m_columns"][] = "Descripcion";
$obj = new SQLTable($proto17);

$proto16["m_table"] = $obj;
$proto16["m_sql"] = "convocatoria";
$proto16["m_alias"] = "";
$proto16["m_srcTableName"] = "cm1_grafico_escanos";
$proto18=array();
$proto18["m_sql"] = "";
$proto18["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto18["m_column"]=$obj;
$proto18["m_contained"] = array();
$proto18["m_strCase"] = "";
$proto18["m_havingmode"] = false;
$proto18["m_inBrackets"] = false;
$proto18["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto18);

$proto16["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto16);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto20=array();
						$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm1_grafico_escanos"
));

$proto20["m_column"]=$obj;
$proto20["m_bAsc"] = 1;
$proto20["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto20);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="cm1_grafico_escanos";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_cm1_grafico_escanos = createSqlQuery_cm1_grafico_escanos();


	
		;

					

$tdatacm1_grafico_escanos[".sqlquery"] = $queryData_cm1_grafico_escanos;

$tableEvents["cm1_grafico_escanos"] = new eventsBase;
$tdatacm1_grafico_escanos[".hasEvents"] = false;

?>