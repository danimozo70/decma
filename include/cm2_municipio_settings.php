<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm2_municipio = array();
	$tdatacm2_municipio[".truncateText"] = true;
	$tdatacm2_municipio[".NumberOfChars"] = 80;
	$tdatacm2_municipio[".ShortName"] = "cm2_municipio";
	$tdatacm2_municipio[".OwnerID"] = "";
	$tdatacm2_municipio[".OriginalTable"] = "municipio";

//	field labels
$fieldLabelscm2_municipio = array();
$fieldToolTipscm2_municipio = array();
$pageTitlescm2_municipio = array();
$placeHolderscm2_municipio = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm2_municipio["Spanish"] = array();
	$fieldToolTipscm2_municipio["Spanish"] = array();
	$placeHolderscm2_municipio["Spanish"] = array();
	$pageTitlescm2_municipio["Spanish"] = array();
	$fieldLabelscm2_municipio["Spanish"]["idMunicipio"] = "Id Interno";
	$fieldToolTipscm2_municipio["Spanish"]["idMunicipio"] = "";
	$placeHolderscm2_municipio["Spanish"]["idMunicipio"] = "";
	$fieldLabelscm2_municipio["Spanish"]["NumeroMunicipio"] = "Número Municipio";
	$fieldToolTipscm2_municipio["Spanish"]["NumeroMunicipio"] = "";
	$placeHolderscm2_municipio["Spanish"]["NumeroMunicipio"] = "";
	$fieldLabelscm2_municipio["Spanish"]["NombreMunicipio"] = "Municipio";
	$fieldToolTipscm2_municipio["Spanish"]["NombreMunicipio"] = "";
	$placeHolderscm2_municipio["Spanish"]["NombreMunicipio"] = "";
	$fieldLabelscm2_municipio["Spanish"]["idCandidatura"] = "Id Candidatura";
	$fieldToolTipscm2_municipio["Spanish"]["idCandidatura"] = "";
	$placeHolderscm2_municipio["Spanish"]["idCandidatura"] = "";
	$fieldLabelscm2_municipio["Spanish"]["Codigo"] = "Codigo";
	$fieldToolTipscm2_municipio["Spanish"]["Codigo"] = "";
	$placeHolderscm2_municipio["Spanish"]["Codigo"] = "";
	$fieldLabelscm2_municipio["Spanish"]["Censo"] = "Censo (2015)";
	$fieldToolTipscm2_municipio["Spanish"]["Censo"] = "";
	$placeHolderscm2_municipio["Spanish"]["Censo"] = "";
	if (count($fieldToolTipscm2_municipio["Spanish"]))
		$tdatacm2_municipio[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm2_municipio[""] = array();
	$fieldToolTipscm2_municipio[""] = array();
	$placeHolderscm2_municipio[""] = array();
	$pageTitlescm2_municipio[""] = array();
	if (count($fieldToolTipscm2_municipio[""]))
		$tdatacm2_municipio[".isUseToolTips"] = true;
}


	$tdatacm2_municipio[".NCSearch"] = true;



$tdatacm2_municipio[".shortTableName"] = "cm2_municipio";
$tdatacm2_municipio[".nSecOptions"] = 0;
$tdatacm2_municipio[".recsPerRowList"] = 1;
$tdatacm2_municipio[".recsPerRowPrint"] = 1;
$tdatacm2_municipio[".mainTableOwnerID"] = "";
$tdatacm2_municipio[".moveNext"] = 1;
$tdatacm2_municipio[".entityType"] = 1;

$tdatacm2_municipio[".strOriginalTableName"] = "municipio";

	



$tdatacm2_municipio[".showAddInPopup"] = true;

$tdatacm2_municipio[".showEditInPopup"] = true;

$tdatacm2_municipio[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["add"] = "add";
			;
$popupPagesLayoutNames["edit"] = "add";
			;
$popupPagesLayoutNames["view"] = "add";
$tdatacm2_municipio[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm2_municipio[".fieldsForRegister"] = array();

$tdatacm2_municipio[".listAjax"] = false;

	$tdatacm2_municipio[".audit"] = false;

	$tdatacm2_municipio[".locking"] = false;



$tdatacm2_municipio[".list"] = true;



$tdatacm2_municipio[".reorderRecordsByHeader"] = true;








$tdatacm2_municipio[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm2_municipio[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm2_municipio[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm2_municipio[".searchSaving"] = false;
//

$tdatacm2_municipio[".showSearchPanel"] = true;
		$tdatacm2_municipio[".flexibleSearch"] = true;

$tdatacm2_municipio[".isUseAjaxSuggest"] = true;

$tdatacm2_municipio[".rowHighlite"] = true;





$tdatacm2_municipio[".ajaxCodeSnippetAdded"] = false;

$tdatacm2_municipio[".buttonsAdded"] = false;

$tdatacm2_municipio[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm2_municipio[".isUseTimeForSearch"] = false;



$tdatacm2_municipio[".badgeColor"] = "cfae83";


$tdatacm2_municipio[".allSearchFields"] = array();
$tdatacm2_municipio[".filterFields"] = array();
$tdatacm2_municipio[".requiredSearchFields"] = array();



$tdatacm2_municipio[".googleLikeFields"] = array();
$tdatacm2_municipio[".googleLikeFields"][] = "idCandidatura";
$tdatacm2_municipio[".googleLikeFields"][] = "Codigo";
$tdatacm2_municipio[".googleLikeFields"][] = "idMunicipio";
$tdatacm2_municipio[".googleLikeFields"][] = "NumeroMunicipio";
$tdatacm2_municipio[".googleLikeFields"][] = "NombreMunicipio";
$tdatacm2_municipio[".googleLikeFields"][] = "Censo";



$tdatacm2_municipio[".tableType"] = "list";

$tdatacm2_municipio[".printerPageOrientation"] = 0;
$tdatacm2_municipio[".nPrinterPageScale"] = 100;

$tdatacm2_municipio[".nPrinterSplitRecords"] = 40;

$tdatacm2_municipio[".nPrinterPDFSplitRecords"] = 40;



$tdatacm2_municipio[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatacm2_municipio[".pageSize"] = 6;

$tdatacm2_municipio[".warnLeavingPages"] = true;



$tstrOrderBy = "order by `NombreMunicipio`";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm2_municipio[".strOrderBy"] = $tstrOrderBy;

$tdatacm2_municipio[".orderindexes"] = array();
	$tdatacm2_municipio[".orderindexes"][] = array(5, (1 ? "ASC" : "DESC"), "municipio.NombreMunicipio");


$tdatacm2_municipio[".sqlHead"] = "SELECT `idCandidatura`,  candidatura.`Codigo`,  `idMunicipio`,  `NumeroMunicipio`,  `NombreMunicipio`,  Censo";
$tdatacm2_municipio[".sqlFrom"] = "FROM  candidatura,  municipio  left join elecmunitotal on NumeroMunicipio=Municipio and Convocatoria_idConvocatoria = 1";
$tdatacm2_municipio[".sqlWhereExpr"] = "";
$tdatacm2_municipio[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm2_municipio[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm2_municipio[".arrGroupsPerPage"] = $arrGPP;

$tdatacm2_municipio[".highlightSearchResults"] = true;

$tableKeyscm2_municipio = array();
$tableKeyscm2_municipio[] = "idMunicipio";
$tdatacm2_municipio[".Keys"] = $tableKeyscm2_municipio;

$tdatacm2_municipio[".listFields"] = array();
$tdatacm2_municipio[".listFields"][] = "NombreMunicipio";
$tdatacm2_municipio[".listFields"][] = "Censo";

$tdatacm2_municipio[".hideMobileList"] = array();


$tdatacm2_municipio[".viewFields"] = array();

$tdatacm2_municipio[".addFields"] = array();

$tdatacm2_municipio[".masterListFields"] = array();
$tdatacm2_municipio[".masterListFields"][] = "idCandidatura";
$tdatacm2_municipio[".masterListFields"][] = "Codigo";
$tdatacm2_municipio[".masterListFields"][] = "idMunicipio";
$tdatacm2_municipio[".masterListFields"][] = "NumeroMunicipio";
$tdatacm2_municipio[".masterListFields"][] = "NombreMunicipio";
$tdatacm2_municipio[".masterListFields"][] = "Censo";

$tdatacm2_municipio[".inlineAddFields"] = array();

$tdatacm2_municipio[".editFields"] = array();

$tdatacm2_municipio[".inlineEditFields"] = array();

$tdatacm2_municipio[".updateSelectedFields"] = array();


$tdatacm2_municipio[".exportFields"] = array();

$tdatacm2_municipio[".importFields"] = array();

$tdatacm2_municipio[".printFields"] = array();


//	idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idCandidatura";
	$fdata["GoodName"] = "idCandidatura";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm2_municipio","idCandidatura");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`idCandidatura`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_municipio["idCandidatura"] = $fdata;
//	Codigo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Codigo";
	$fdata["GoodName"] = "Codigo";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm2_municipio","Codigo");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Codigo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "candidatura.`Codigo`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_municipio["Codigo"] = $fdata;
//	idMunicipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "idMunicipio";
	$fdata["GoodName"] = "idMunicipio";
	$fdata["ownerTable"] = "municipio";
	$fdata["Label"] = GetFieldLabel("cm2_municipio","idMunicipio");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idMunicipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`idMunicipio`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_municipio["idMunicipio"] = $fdata;
//	NumeroMunicipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "NumeroMunicipio";
	$fdata["GoodName"] = "NumeroMunicipio";
	$fdata["ownerTable"] = "municipio";
	$fdata["Label"] = GetFieldLabel("cm2_municipio","NumeroMunicipio");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "NumeroMunicipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`NumeroMunicipio`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_municipio["NumeroMunicipio"] = $fdata;
//	NombreMunicipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "NombreMunicipio";
	$fdata["GoodName"] = "NombreMunicipio";
	$fdata["ownerTable"] = "municipio";
	$fdata["Label"] = GetFieldLabel("cm2_municipio","NombreMunicipio");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "NombreMunicipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`NombreMunicipio`";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_municipio["NombreMunicipio"] = $fdata;
//	Censo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Censo";
	$fdata["GoodName"] = "Censo";
	$fdata["ownerTable"] = "elecmunitotal";
	$fdata["Label"] = GetFieldLabel("cm2_municipio","Censo");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Censo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Censo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_municipio["Censo"] = $fdata;


$tables_data["cm2_municipio"]=&$tdatacm2_municipio;
$field_labels["cm2_municipio"] = &$fieldLabelscm2_municipio;
$fieldToolTips["cm2_municipio"] = &$fieldToolTipscm2_municipio;
$placeHolders["cm2_municipio"] = &$placeHolderscm2_municipio;
$page_titles["cm2_municipio"] = &$pageTitlescm2_municipio;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm2_municipio"] = array();
//	cm2_elecmunicandi Chart
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm2_elecmunicandi Chart";
		$detailsParam["dOriginalTable"] = "elecmunicandi";
		$detailsParam["proceedLink"] = true;
			$detailsParam["dType"]=PAGE_CHART;
		$detailsParam["dShortTable"] = "cm2_elecmunicandi_Chart";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm2_elecmunicandi_Chart");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "1";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm2_municipio"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm2_municipio"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm2_municipio"][$dIndex]["masterKeys"][]="idCandidatura";

	$detailsTablesData["cm2_municipio"][$dIndex]["masterKeys"][]="NumeroMunicipio";

				$detailsTablesData["cm2_municipio"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm2_municipio"][$dIndex]["detailKeys"][]="Candidatura_idCandidatura";

		
	$detailsTablesData["cm2_municipio"][$dIndex]["detailKeys"][]="Municipio";
//	cm2_elecmunicandi2 Chart
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm2_elecmunicandi2 Chart";
		$detailsParam["dOriginalTable"] = "elecmunicandi";
		$detailsParam["proceedLink"] = true;
			$detailsParam["dType"]=PAGE_CHART;
		$detailsParam["dShortTable"] = "cm2_elecmunicandi2_Chart";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm2_elecmunicandi2_Chart");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm2_municipio"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm2_municipio"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm2_municipio"][$dIndex]["masterKeys"][]="idCandidatura";

	$detailsTablesData["cm2_municipio"][$dIndex]["masterKeys"][]="NumeroMunicipio";

				$detailsTablesData["cm2_municipio"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm2_municipio"][$dIndex]["detailKeys"][]="Candidatura_idCandidatura";

		
	$detailsTablesData["cm2_municipio"][$dIndex]["detailKeys"][]="Municipio";

// tables which are master tables for current table (detail)
$masterTablesData["cm2_municipio"] = array();


	
				$strOriginalDetailsTable="candidatura";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm2_candidatura";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm2_candidatura";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm2_municipio"][0] = $masterParams;
				$masterTablesData["cm2_municipio"][0]["masterKeys"] = array();
	$masterTablesData["cm2_municipio"][0]["masterKeys"][]="idCandidatura";
				$masterTablesData["cm2_municipio"][0]["detailKeys"] = array();
	$masterTablesData["cm2_municipio"][0]["detailKeys"][]="idCandidatura";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm2_municipio()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "`idCandidatura`,  candidatura.`Codigo`,  `idMunicipio`,  `NumeroMunicipio`,  `NombreMunicipio`,  Censo";
$proto3["m_strFrom"] = "FROM  candidatura,  municipio  left join elecmunitotal on NumeroMunicipio=Municipio and Convocatoria_idConvocatoria = 1";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "order by `NombreMunicipio`";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idCandidatura",
	"m_strTable" => "candidatura",
	"m_srcTableName" => "cm2_municipio"
));

$proto9["m_sql"] = "`idCandidatura`";
$proto9["m_srcTableName"] = "cm2_municipio";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Codigo",
	"m_strTable" => "candidatura",
	"m_srcTableName" => "cm2_municipio"
));

$proto11["m_sql"] = "candidatura.`Codigo`";
$proto11["m_srcTableName"] = "cm2_municipio";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "idMunicipio",
	"m_strTable" => "municipio",
	"m_srcTableName" => "cm2_municipio"
));

$proto13["m_sql"] = "`idMunicipio`";
$proto13["m_srcTableName"] = "cm2_municipio";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "NumeroMunicipio",
	"m_strTable" => "municipio",
	"m_srcTableName" => "cm2_municipio"
));

$proto15["m_sql"] = "`NumeroMunicipio`";
$proto15["m_srcTableName"] = "cm2_municipio";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "NombreMunicipio",
	"m_strTable" => "municipio",
	"m_srcTableName" => "cm2_municipio"
));

$proto17["m_sql"] = "`NombreMunicipio`";
$proto17["m_srcTableName"] = "cm2_municipio";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "Censo",
	"m_strTable" => "elecmunitotal",
	"m_srcTableName" => "cm2_municipio"
));

$proto19["m_sql"] = "Censo";
$proto19["m_srcTableName"] = "cm2_municipio";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto21=array();
$proto21["m_link"] = "SQLL_MAIN";
			$proto22=array();
$proto22["m_strName"] = "candidatura";
$proto22["m_srcTableName"] = "cm2_municipio";
$proto22["m_columns"] = array();
$proto22["m_columns"][] = "idCandidatura";
$proto22["m_columns"][] = "Codigo";
$proto22["m_columns"][] = "Titulo";
$proto22["m_columns"][] = "Descripcion";
$proto22["m_columns"][] = "Color";
$proto22["m_columns"][] = "Logo";
$obj = new SQLTable($proto22);

$proto21["m_table"] = $obj;
$proto21["m_sql"] = "candidatura";
$proto21["m_alias"] = "";
$proto21["m_srcTableName"] = "cm2_municipio";
$proto23=array();
$proto23["m_sql"] = "";
$proto23["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto23["m_column"]=$obj;
$proto23["m_contained"] = array();
$proto23["m_strCase"] = "";
$proto23["m_havingmode"] = false;
$proto23["m_inBrackets"] = false;
$proto23["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto23);

$proto21["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto21);

$proto3["m_fromlist"][]=$obj;
												$proto25=array();
$proto25["m_link"] = "SQLL_CROSSJOIN";
			$proto26=array();
$proto26["m_strName"] = "municipio";
$proto26["m_srcTableName"] = "cm2_municipio";
$proto26["m_columns"] = array();
$proto26["m_columns"][] = "idMunicipio";
$proto26["m_columns"][] = "NumeroMunicipio";
$proto26["m_columns"][] = "NombreMunicipio";
$obj = new SQLTable($proto26);

$proto25["m_table"] = $obj;
$proto25["m_sql"] = "municipio";
$proto25["m_alias"] = "";
$proto25["m_srcTableName"] = "cm2_municipio";
$proto27=array();
$proto27["m_sql"] = "";
$proto27["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto27["m_column"]=$obj;
$proto27["m_contained"] = array();
$proto27["m_strCase"] = "";
$proto27["m_havingmode"] = false;
$proto27["m_inBrackets"] = false;
$proto27["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto27);

$proto25["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto25);

$proto3["m_fromlist"][]=$obj;
												$proto29=array();
$proto29["m_link"] = "SQLL_LEFTJOIN";
			$proto30=array();
$proto30["m_strName"] = "elecmunitotal";
$proto30["m_srcTableName"] = "cm2_municipio";
$proto30["m_columns"] = array();
$proto30["m_columns"][] = "idElecMuniTotal";
$proto30["m_columns"][] = "Convocatoria_idConvocatoria";
$proto30["m_columns"][] = "Municipio";
$proto30["m_columns"][] = "Censo";
$proto30["m_columns"][] = "CensoOficial";
$proto30["m_columns"][] = "Certificacion";
$proto30["m_columns"][] = "CertificacionAlta";
$proto30["m_columns"][] = "CertificacionError";
$proto30["m_columns"][] = "Voto";
$proto30["m_columns"][] = "Interventores";
$proto30["m_columns"][] = "Nulo";
$proto30["m_columns"][] = "Blanco";
$proto30["m_columns"][] = "Validos";
$obj = new SQLTable($proto30);

$proto29["m_table"] = $obj;
$proto29["m_sql"] = "left join elecmunitotal on NumeroMunicipio=Municipio and Convocatoria_idConvocatoria = 1";
$proto29["m_alias"] = "";
$proto29["m_srcTableName"] = "cm2_municipio";
$proto31=array();
$proto31["m_sql"] = "NumeroMunicipio=Municipio and Convocatoria_idConvocatoria = 1";
$proto31["m_uniontype"] = "SQLL_AND";
	$obj = new SQLNonParsed(array(
	"m_sql" => "NumeroMunicipio=Municipio and Convocatoria_idConvocatoria = 1"
));

$proto31["m_column"]=$obj;
$proto31["m_contained"] = array();
						$proto33=array();
$proto33["m_sql"] = "NumeroMunicipio=Municipio";
$proto33["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "NumeroMunicipio",
	"m_strTable" => "municipio",
	"m_srcTableName" => "cm2_municipio"
));

$proto33["m_column"]=$obj;
$proto33["m_contained"] = array();
$proto33["m_strCase"] = "=Municipio";
$proto33["m_havingmode"] = false;
$proto33["m_inBrackets"] = false;
$proto33["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto33);

			$proto31["m_contained"][]=$obj;
						$proto35=array();
$proto35["m_sql"] = "Convocatoria_idConvocatoria = 1";
$proto35["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "elecmunitotal",
	"m_srcTableName" => "cm2_municipio"
));

$proto35["m_column"]=$obj;
$proto35["m_contained"] = array();
$proto35["m_strCase"] = "= 1";
$proto35["m_havingmode"] = false;
$proto35["m_inBrackets"] = false;
$proto35["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto35);

			$proto31["m_contained"][]=$obj;
$proto31["m_strCase"] = "";
$proto31["m_havingmode"] = false;
$proto31["m_inBrackets"] = false;
$proto31["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto31);

$proto29["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto29);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto37=array();
						$obj = new SQLField(array(
	"m_strName" => "NombreMunicipio",
	"m_strTable" => "municipio",
	"m_srcTableName" => "cm2_municipio"
));

$proto37["m_column"]=$obj;
$proto37["m_bAsc"] = 1;
$proto37["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto37);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="cm2_municipio";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_cm2_municipio = createSqlQuery_cm2_municipio();


	
		;

						

$tdatacm2_municipio[".sqlquery"] = $queryData_cm2_municipio;

$tableEvents["cm2_municipio"] = new eventsBase;
$tdatacm2_municipio[".hasEvents"] = false;

?>