<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm1_2_Candidaturas = array();
	$tdatacm1_2_Candidaturas[".truncateText"] = true;
	$tdatacm1_2_Candidaturas[".NumberOfChars"] = 80;
	$tdatacm1_2_Candidaturas[".ShortName"] = "cm1_2_Candidaturas";
	$tdatacm1_2_Candidaturas[".OwnerID"] = "";
	$tdatacm1_2_Candidaturas[".OriginalTable"] = "escanos";

//	field labels
$fieldLabelscm1_2_Candidaturas = array();
$fieldToolTipscm1_2_Candidaturas = array();
$pageTitlescm1_2_Candidaturas = array();
$placeHolderscm1_2_Candidaturas = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm1_2_Candidaturas["Spanish"] = array();
	$fieldToolTipscm1_2_Candidaturas["Spanish"] = array();
	$placeHolderscm1_2_Candidaturas["Spanish"] = array();
	$pageTitlescm1_2_Candidaturas["Spanish"] = array();
	$fieldLabelscm1_2_Candidaturas["Spanish"]["idEscanos"] = "Id Interno";
	$fieldToolTipscm1_2_Candidaturas["Spanish"]["idEscanos"] = "";
	$placeHolderscm1_2_Candidaturas["Spanish"]["idEscanos"] = "";
	$fieldLabelscm1_2_Candidaturas["Spanish"]["Escanos"] = "Escaños";
	$fieldToolTipscm1_2_Candidaturas["Spanish"]["Escanos"] = "";
	$placeHolderscm1_2_Candidaturas["Spanish"]["Escanos"] = "";
	$fieldLabelscm1_2_Candidaturas["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldToolTipscm1_2_Candidaturas["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderscm1_2_Candidaturas["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelscm1_2_Candidaturas["Spanish"]["Candidatura_idCandidatura"] = "Candidatura";
	$fieldToolTipscm1_2_Candidaturas["Spanish"]["Candidatura_idCandidatura"] = "";
	$placeHolderscm1_2_Candidaturas["Spanish"]["Candidatura_idCandidatura"] = "";
	$fieldLabelscm1_2_Candidaturas["Spanish"]["Color"] = "Color";
	$fieldToolTipscm1_2_Candidaturas["Spanish"]["Color"] = "";
	$placeHolderscm1_2_Candidaturas["Spanish"]["Color"] = "";
	$pageTitlescm1_2_Candidaturas["Spanish"]["list"] = "<strong>{%master.Titulo}</strong>, Escaños y filtro por Candidatura";
	if (count($fieldToolTipscm1_2_Candidaturas["Spanish"]))
		$tdatacm1_2_Candidaturas[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm1_2_Candidaturas[""] = array();
	$fieldToolTipscm1_2_Candidaturas[""] = array();
	$placeHolderscm1_2_Candidaturas[""] = array();
	$pageTitlescm1_2_Candidaturas[""] = array();
	if (count($fieldToolTipscm1_2_Candidaturas[""]))
		$tdatacm1_2_Candidaturas[".isUseToolTips"] = true;
}


	$tdatacm1_2_Candidaturas[".NCSearch"] = true;



$tdatacm1_2_Candidaturas[".shortTableName"] = "cm1_2_Candidaturas";
$tdatacm1_2_Candidaturas[".nSecOptions"] = 0;
$tdatacm1_2_Candidaturas[".recsPerRowList"] = 1;
$tdatacm1_2_Candidaturas[".recsPerRowPrint"] = 1;
$tdatacm1_2_Candidaturas[".mainTableOwnerID"] = "";
$tdatacm1_2_Candidaturas[".moveNext"] = 1;
$tdatacm1_2_Candidaturas[".entityType"] = 1;

$tdatacm1_2_Candidaturas[".strOriginalTableName"] = "escanos";

	



$tdatacm1_2_Candidaturas[".showAddInPopup"] = true;

$tdatacm1_2_Candidaturas[".showEditInPopup"] = true;

$tdatacm1_2_Candidaturas[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
					
;
$popupPagesLayoutNames["add"] = "add";
			;
$popupPagesLayoutNames["edit"] = "add";
			;
$popupPagesLayoutNames["view"] = "add";
$tdatacm1_2_Candidaturas[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm1_2_Candidaturas[".fieldsForRegister"] = array();

$tdatacm1_2_Candidaturas[".listAjax"] = false;

	$tdatacm1_2_Candidaturas[".audit"] = false;

	$tdatacm1_2_Candidaturas[".locking"] = false;



$tdatacm1_2_Candidaturas[".list"] = true;





$tdatacm1_2_Candidaturas[".exportFormatting"] = 2;
$tdatacm1_2_Candidaturas[".exportDelimiter"] = ",";
		


$tdatacm1_2_Candidaturas[".exportTo"] = true;



$tdatacm1_2_Candidaturas[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm1_2_Candidaturas[".allowShowHideFields"] = true;
//

// Allow Fields Reordering in GRID
$tdatacm1_2_Candidaturas[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm1_2_Candidaturas[".searchSaving"] = false;
//

$tdatacm1_2_Candidaturas[".showSearchPanel"] = true;
		$tdatacm1_2_Candidaturas[".flexibleSearch"] = true;

$tdatacm1_2_Candidaturas[".isUseAjaxSuggest"] = true;

$tdatacm1_2_Candidaturas[".rowHighlite"] = true;





$tdatacm1_2_Candidaturas[".ajaxCodeSnippetAdded"] = false;

$tdatacm1_2_Candidaturas[".buttonsAdded"] = false;

$tdatacm1_2_Candidaturas[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm1_2_Candidaturas[".isUseTimeForSearch"] = false;



$tdatacm1_2_Candidaturas[".badgeColor"] = "00c2c5";


$tdatacm1_2_Candidaturas[".allSearchFields"] = array();
$tdatacm1_2_Candidaturas[".filterFields"] = array();
$tdatacm1_2_Candidaturas[".requiredSearchFields"] = array();

$tdatacm1_2_Candidaturas[".allSearchFields"][] = "idEscanos";
	$tdatacm1_2_Candidaturas[".allSearchFields"][] = "Convocatoria_idConvocatoria";
	$tdatacm1_2_Candidaturas[".allSearchFields"][] = "Candidatura_idCandidatura";
	$tdatacm1_2_Candidaturas[".allSearchFields"][] = "Escanos";
	
$tdatacm1_2_Candidaturas[".filterFields"][] = "Convocatoria_idConvocatoria";

$tdatacm1_2_Candidaturas[".googleLikeFields"] = array();
$tdatacm1_2_Candidaturas[".googleLikeFields"][] = "idEscanos";
$tdatacm1_2_Candidaturas[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_2_Candidaturas[".googleLikeFields"][] = "Candidatura_idCandidatura";
$tdatacm1_2_Candidaturas[".googleLikeFields"][] = "Escanos";


$tdatacm1_2_Candidaturas[".advSearchFields"] = array();
$tdatacm1_2_Candidaturas[".advSearchFields"][] = "idEscanos";
$tdatacm1_2_Candidaturas[".advSearchFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_2_Candidaturas[".advSearchFields"][] = "Candidatura_idCandidatura";
$tdatacm1_2_Candidaturas[".advSearchFields"][] = "Escanos";

$tdatacm1_2_Candidaturas[".tableType"] = "list";

$tdatacm1_2_Candidaturas[".printerPageOrientation"] = 0;
$tdatacm1_2_Candidaturas[".nPrinterPageScale"] = 100;

$tdatacm1_2_Candidaturas[".nPrinterSplitRecords"] = 40;

$tdatacm1_2_Candidaturas[".nPrinterPDFSplitRecords"] = 40;



$tdatacm1_2_Candidaturas[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatacm1_2_Candidaturas[".pageSize"] = 5;

$tdatacm1_2_Candidaturas[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY Escanos DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm1_2_Candidaturas[".strOrderBy"] = $tstrOrderBy;

$tdatacm1_2_Candidaturas[".orderindexes"] = array();
	$tdatacm1_2_Candidaturas[".orderindexes"][] = array(5, (0 ? "ASC" : "DESC"), "Escanos");


$tdatacm1_2_Candidaturas[".sqlHead"] = "SELECT idEscanos,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Candidatura_idCandidatura AS Color,  Escanos";
$tdatacm1_2_Candidaturas[".sqlFrom"] = "FROM escanos";
$tdatacm1_2_Candidaturas[".sqlWhereExpr"] = "(Escanos > 0)";
$tdatacm1_2_Candidaturas[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm1_2_Candidaturas[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm1_2_Candidaturas[".arrGroupsPerPage"] = $arrGPP;

$tdatacm1_2_Candidaturas[".highlightSearchResults"] = true;

$tableKeyscm1_2_Candidaturas = array();
$tableKeyscm1_2_Candidaturas[] = "idEscanos";
$tdatacm1_2_Candidaturas[".Keys"] = $tableKeyscm1_2_Candidaturas;

$tdatacm1_2_Candidaturas[".listFields"] = array();
$tdatacm1_2_Candidaturas[".listFields"][] = "Candidatura_idCandidatura";
$tdatacm1_2_Candidaturas[".listFields"][] = "Color";
$tdatacm1_2_Candidaturas[".listFields"][] = "Escanos";

$tdatacm1_2_Candidaturas[".hideMobileList"] = array();


$tdatacm1_2_Candidaturas[".viewFields"] = array();

$tdatacm1_2_Candidaturas[".addFields"] = array();

$tdatacm1_2_Candidaturas[".masterListFields"] = array();
$tdatacm1_2_Candidaturas[".masterListFields"][] = "idEscanos";
$tdatacm1_2_Candidaturas[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_2_Candidaturas[".masterListFields"][] = "Candidatura_idCandidatura";
$tdatacm1_2_Candidaturas[".masterListFields"][] = "Color";
$tdatacm1_2_Candidaturas[".masterListFields"][] = "Escanos";

$tdatacm1_2_Candidaturas[".inlineAddFields"] = array();

$tdatacm1_2_Candidaturas[".editFields"] = array();

$tdatacm1_2_Candidaturas[".inlineEditFields"] = array();

$tdatacm1_2_Candidaturas[".updateSelectedFields"] = array();


$tdatacm1_2_Candidaturas[".exportFields"] = array();
$tdatacm1_2_Candidaturas[".exportFields"][] = "idEscanos";
$tdatacm1_2_Candidaturas[".exportFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_2_Candidaturas[".exportFields"][] = "Candidatura_idCandidatura";
$tdatacm1_2_Candidaturas[".exportFields"][] = "Escanos";

$tdatacm1_2_Candidaturas[".importFields"] = array();

$tdatacm1_2_Candidaturas[".printFields"] = array();


//	idEscanos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idEscanos";
	$fdata["GoodName"] = "idEscanos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm1_2_Candidaturas","idEscanos");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idEscanos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idEscanos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_2_Candidaturas["idEscanos"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm1_2_Candidaturas","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 2;
		$fdata["filterTotalFields"] = "idEscanos";
		$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = true;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 0;

			
	
	
//end of Filters settings


	$tdatacm1_2_Candidaturas["Convocatoria_idConvocatoria"] = $fdata;
//	Candidatura_idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Candidatura_idCandidatura";
	$fdata["GoodName"] = "Candidatura_idCandidatura";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm1_2_Candidaturas","Candidatura_idCandidatura");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Candidatura_idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Candidatura_idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "candidatura";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idCandidatura";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "concat(Codigo,' -  ',Titulo)";
	
	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_2_Candidaturas["Candidatura_idCandidatura"] = $fdata;
//	Color
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Color";
	$fdata["GoodName"] = "Color";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm1_2_Candidaturas","Color");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Candidatura_idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Candidatura_idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "candidatura";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idCandidatura";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Color";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm1_2_Candidaturas["Color"] = $fdata;
//	Escanos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Escanos";
	$fdata["GoodName"] = "Escanos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm1_2_Candidaturas","Escanos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Escanos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Escanos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_2_Candidaturas["Escanos"] = $fdata;


$tables_data["cm1_2_Candidaturas"]=&$tdatacm1_2_Candidaturas;
$field_labels["cm1_2_Candidaturas"] = &$fieldLabelscm1_2_Candidaturas;
$fieldToolTips["cm1_2_Candidaturas"] = &$fieldToolTipscm1_2_Candidaturas;
$placeHolders["cm1_2_Candidaturas"] = &$placeHolderscm1_2_Candidaturas;
$page_titles["cm1_2_Candidaturas"] = &$pageTitlescm1_2_Candidaturas;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm1_2_Candidaturas"] = array();
//	electos
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="electos";
		$detailsParam["dOriginalTable"] = "electos";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "electos";
	$detailsParam["dCaptionTable"] = GetTableCaption("electos");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "1";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm1_2_Candidaturas"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm1_2_Candidaturas"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm1_2_Candidaturas"][$dIndex]["masterKeys"][]="Convocatoria_idConvocatoria";

	$detailsTablesData["cm1_2_Candidaturas"][$dIndex]["masterKeys"][]="Candidatura_idCandidatura";

				$detailsTablesData["cm1_2_Candidaturas"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm1_2_Candidaturas"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";

		
	$detailsTablesData["cm1_2_Candidaturas"][$dIndex]["detailKeys"][]="Candidatura_idCandidatura";
//	cm1_2_Electos
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm1_2_Electos";
		$detailsParam["dOriginalTable"] = "electos";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm1_2_Electos";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm1_2_Electos");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm1_2_Candidaturas"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm1_2_Candidaturas"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm1_2_Candidaturas"][$dIndex]["masterKeys"][]="Convocatoria_idConvocatoria";

	$detailsTablesData["cm1_2_Candidaturas"][$dIndex]["masterKeys"][]="Candidatura_idCandidatura";

				$detailsTablesData["cm1_2_Candidaturas"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm1_2_Candidaturas"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";

		
	$detailsTablesData["cm1_2_Candidaturas"][$dIndex]["detailKeys"][]="Candidatura_idCandidatura";

// tables which are master tables for current table (detail)
$masterTablesData["cm1_2_Candidaturas"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm1_2_Candidaturas"][0] = $masterParams;
				$masterTablesData["cm1_2_Candidaturas"][0]["masterKeys"] = array();
	$masterTablesData["cm1_2_Candidaturas"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["cm1_2_Candidaturas"][0]["detailKeys"] = array();
	$masterTablesData["cm1_2_Candidaturas"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
		
	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm1_2_Convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm1_2_Convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm1_2_Candidaturas"][1] = $masterParams;
				$masterTablesData["cm1_2_Candidaturas"][1]["masterKeys"] = array();
	$masterTablesData["cm1_2_Candidaturas"][1]["masterKeys"][]="idConvocatoria";
				$masterTablesData["cm1_2_Candidaturas"][1]["detailKeys"] = array();
	$masterTablesData["cm1_2_Candidaturas"][1]["detailKeys"][]="Convocatoria_idConvocatoria";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm1_2_Candidaturas()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "idEscanos,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Candidatura_idCandidatura AS Color,  Escanos";
$proto3["m_strFrom"] = "FROM escanos";
$proto3["m_strWhere"] = "(Escanos > 0)";
$proto3["m_strOrderBy"] = "ORDER BY Escanos DESC";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "Escanos > 0";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm1_2_Candidaturas"
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "> 0";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idEscanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm1_2_Candidaturas"
));

$proto9["m_sql"] = "idEscanos";
$proto9["m_srcTableName"] = "cm1_2_Candidaturas";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm1_2_Candidaturas"
));

$proto11["m_sql"] = "Convocatoria_idConvocatoria";
$proto11["m_srcTableName"] = "cm1_2_Candidaturas";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm1_2_Candidaturas"
));

$proto13["m_sql"] = "Candidatura_idCandidatura";
$proto13["m_srcTableName"] = "cm1_2_Candidaturas";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm1_2_Candidaturas"
));

$proto15["m_sql"] = "Candidatura_idCandidatura";
$proto15["m_srcTableName"] = "cm1_2_Candidaturas";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "Color";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm1_2_Candidaturas"
));

$proto17["m_sql"] = "Escanos";
$proto17["m_srcTableName"] = "cm1_2_Candidaturas";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto19=array();
$proto19["m_link"] = "SQLL_MAIN";
			$proto20=array();
$proto20["m_strName"] = "escanos";
$proto20["m_srcTableName"] = "cm1_2_Candidaturas";
$proto20["m_columns"] = array();
$proto20["m_columns"][] = "idEscanos";
$proto20["m_columns"][] = "Convocatoria_idConvocatoria";
$proto20["m_columns"][] = "Candidatura_idCandidatura";
$proto20["m_columns"][] = "Votos";
$proto20["m_columns"][] = "PorcVotos";
$proto20["m_columns"][] = "Escanos";
$obj = new SQLTable($proto20);

$proto19["m_table"] = $obj;
$proto19["m_sql"] = "escanos";
$proto19["m_alias"] = "";
$proto19["m_srcTableName"] = "cm1_2_Candidaturas";
$proto21=array();
$proto21["m_sql"] = "";
$proto21["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto21["m_column"]=$obj;
$proto21["m_contained"] = array();
$proto21["m_strCase"] = "";
$proto21["m_havingmode"] = false;
$proto21["m_inBrackets"] = false;
$proto21["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto21);

$proto19["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto19);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto23=array();
						$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm1_2_Candidaturas"
));

$proto23["m_column"]=$obj;
$proto23["m_bAsc"] = 0;
$proto23["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto23);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="cm1_2_Candidaturas";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_cm1_2_Candidaturas = createSqlQuery_cm1_2_Candidaturas();


	
		;

					

$tdatacm1_2_Candidaturas[".sqlquery"] = $queryData_cm1_2_Candidaturas;

$tableEvents["cm1_2_Candidaturas"] = new eventsBase;
$tdatacm1_2_Candidaturas[".hasEvents"] = false;

?>