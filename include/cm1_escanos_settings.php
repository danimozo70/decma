<?php
require_once(getabspath("classes/cipherer.php"));



$tdatacm1_escanos = array();
$tdatacm1_escanos[".ShortName"] = "cm1_escanos";

//	field labels
$fieldLabelscm1_escanos = array();
$pageTitlescm1_escanos = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm1_escanos["Spanish"] = array();
	$fieldLabelscm1_escanos["Spanish"]["convocatoria_idConvocatoria"] = "Id Interno";
	$fieldLabelscm1_escanos["Spanish"]["convocatoria_Orden"] = "Orden";
	$fieldLabelscm1_escanos["Spanish"]["convocatoria_EsAsamblea"] = "Es Asamblea?";
	$fieldLabelscm1_escanos["Spanish"]["convocatoria_Titulo"] = "Título";
	$fieldLabelscm1_escanos["Spanish"]["convocatoria_Descripcion"] = "Descripción";
	$fieldLabelscm1_escanos["Spanish"]["escanos_idEscanos"] = "Id Interno";
	$fieldLabelscm1_escanos["Spanish"]["escanos_Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldLabelscm1_escanos["Spanish"]["escanos_Candidatura_idCandidatura"] = "Candidatura";
	$fieldLabelscm1_escanos["Spanish"]["escanos_Votos"] = "Votos";
	$fieldLabelscm1_escanos["Spanish"]["escanos_PorcVotos"] = "%Votos";
	$fieldLabelscm1_escanos["Spanish"]["escanos_Escanos"] = "Escaños";
	$fieldLabelscm1_escanos["Spanish"]["v_convocatoria_total_Censo"] = "Censo";
	$fieldLabelscm1_escanos["Spanish"]["v_convocatoria_total_Certificacion"] = "Votos Certificación";
	$fieldLabelscm1_escanos["Spanish"]["v_convocatoria_total_Interventores"] = "Votos Interventores";
	$fieldLabelscm1_escanos["Spanish"]["v_convocatoria_total_Nulo"] = "Votos Nulos";
	$fieldLabelscm1_escanos["Spanish"]["v_convocatoria_total_Blanco"] = "Votos Blancos";
	$fieldLabelscm1_escanos["Spanish"]["v_convocatoria_total_Validos"] = "Votos Válidos";
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm1_escanos[""] = array();
}

//	search fields
$tdatacm1_escanos[".searchFields"] = array();

// all search fields
$tdatacm1_escanos[".allSearchFields"] = array();

// good like search fields
$tdatacm1_escanos[".googleLikeFields"] = array();

$tdatacm1_escanos[".dashElements"] = array();

	$dbelement = array( "elementName" => "convocatoria_list", "table" => "convocatoria", "type" => 0);
	$dbelement["cellName"] = "cell_0_0";

			$dbelement["width"] = 400;
			$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;


	$tdatacm1_escanos[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "escanos_list", "table" => "escanos", "type" => 0);
	$dbelement["cellName"] = "cell_1_1";

					$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "convocatoria";

	$tdatacm1_escanos[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "v_convocatoria_total_list", "table" => "v_convocatoria_total", "type" => 0);
	$dbelement["cellName"] = "cell_0_1";

					$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "convocatoria";

	$tdatacm1_escanos[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm1_grafico_escanos_list", "table" => "cm1_grafico_escanos", "type" => 0);
	$dbelement["cellName"] = "cell_2_0";

					$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "convocatoria";

	$tdatacm1_escanos[".dashElements"][] = $dbelement;

$tdatacm1_escanos[".shortTableName"] = "cm1_escanos";
$tdatacm1_escanos[".entityType"] = 4;



include_once(getabspath("include/cm1_escanos_events.php"));
$tableEvents["cm1_escanos"] = new eventclass_cm1_escanos;
$tdatacm1_escanos[".hasEvents"] = true;


$tdatacm1_escanos[".tableType"] = "dashboard";



$tdatacm1_escanos[".addPageEvents"] = false;

$tables_data["cm1_escanos"]=&$tdatacm1_escanos;
$field_labels["cm1_escanos"] = &$fieldLabelscm1_escanos;
$page_titles["cm1_escanos"] = &$pageTitlescm1_escanos;

?>