<?php

/**
* getLookupMainTableSettings - tests whether the lookup link exists between the tables
*
*  returns array with ProjectSettings class for main table if the link exists in project settings.
*  returns NULL otherwise
*/
function getLookupMainTableSettings($lookupTable, $mainTableShortName, $mainField, $desiredPage = "")
{
	global $lookupTableLinks;
	if(!isset($lookupTableLinks[$lookupTable]))
		return null;
	if(!isset($lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField]))
		return null;
	$arr = &$lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField];
	$effectivePage = $desiredPage;
	if(!isset($arr[$effectivePage]))
	{
		$effectivePage = PAGE_EDIT;
		if(!isset($arr[$effectivePage]))
		{
			if($desiredPage == "" && 0 < count($arr))
			{
				$effectivePage = $arr[0];
			}
			else
				return null;
		}
	}
	return new ProjectSettings($arr[$effectivePage]["table"], $effectivePage);
}

/** 
* $lookupTableLinks array stores all lookup links between tables in the project
*/
function InitLookupLinks()
{
	global $lookupTableLinks;

	$lookupTableLinks = array();

	$lookupTableLinks["convocatoria"]["electos.Convocatoria_idConvocatoria"]["edit"] = array("table" => "electos", "field" => "Convocatoria_idConvocatoria", "page" => "edit");
	$lookupTableLinks["candidatura"]["electos.Candidatura_idCandidatura"]["edit"] = array("table" => "electos", "field" => "Candidatura_idCandidatura", "page" => "edit");
	$lookupTableLinks["convocatoria"]["escanos.Convocatoria_idConvocatoria"]["edit"] = array("table" => "escanos", "field" => "Convocatoria_idConvocatoria", "page" => "edit");
	$lookupTableLinks["candidatura"]["escanos.Candidatura_idCandidatura"]["edit"] = array("table" => "escanos", "field" => "Candidatura_idCandidatura", "page" => "edit");
	$lookupTableLinks["candidatura"]["escanos.Color"]["edit"] = array("table" => "escanos", "field" => "Color", "page" => "edit");
	$lookupTableLinks["convocatoria"]["cm1_convocatoria_total.Convocatoria_idConvocatoria"]["edit"] = array("table" => "v_convocatoria_total", "field" => "Convocatoria_idConvocatoria", "page" => "edit");
	$lookupTableLinks["convocatoria"]["cm1_escanos_Chart.Convocatoria_idConvocatoria"]["search"] = array("table" => "cm1_escanos Chart", "field" => "Convocatoria_idConvocatoria", "page" => "search");
	$lookupTableLinks["convocatoria"]["cm1_votos_Chart.Convocatoria_idConvocatoria"]["search"] = array("table" => "cm1_votos Chart", "field" => "Convocatoria_idConvocatoria", "page" => "search");
	$lookupTableLinks["convocatoria"]["cm2_escanos_chart.Convocatoria_idConvocatoria"]["search"] = array("table" => "cm2_escanos_chart", "field" => "Convocatoria_idConvocatoria", "page" => "search");
	$lookupTableLinks["candidatura"]["cm2_escanos_chart.Candidatura_idCandidatura"]["search"] = array("table" => "cm2_escanos_chart", "field" => "Candidatura_idCandidatura", "page" => "search");
	$lookupTableLinks["convocatoria"]["cm2_escanos_Report.Convocatoria_idConvocatoria"]["search"] = array("table" => "cm2_escanos Report", "field" => "Convocatoria_idConvocatoria", "page" => "search");
	$lookupTableLinks["candidatura"]["cm2_escanos_Report.Candidatura_idCandidatura"]["search"] = array("table" => "cm2_escanos Report", "field" => "Candidatura_idCandidatura", "page" => "search");
	$lookupTableLinks["convocatoria"]["cm2_elecmunicandi_Chart.Convocatoria_idConvocatoria"]["search"] = array("table" => "cm2_elecmunicandi Chart", "field" => "Convocatoria_idConvocatoria", "page" => "search");
	$lookupTableLinks["candidatura"]["cm2_elecmunicandi_Chart.Candidatura_idCandidatura"]["search"] = array("table" => "cm2_elecmunicandi Chart", "field" => "Candidatura_idCandidatura", "page" => "search");
	$lookupTableLinks["municipio"]["cm2_elecmunicandi_Chart.Municipio"]["search"] = array("table" => "cm2_elecmunicandi Chart", "field" => "Municipio", "page" => "search");
	$lookupTableLinks["convocatoria"]["cm2_elecmunicandi2_Chart.Convocatoria_idConvocatoria"]["search"] = array("table" => "cm2_elecmunicandi2 Chart", "field" => "Convocatoria_idConvocatoria", "page" => "search");
	$lookupTableLinks["candidatura"]["cm2_elecmunicandi2_Chart.Candidatura_idCandidatura"]["search"] = array("table" => "cm2_elecmunicandi2 Chart", "field" => "Candidatura_idCandidatura", "page" => "search");
	$lookupTableLinks["municipio"]["cm2_elecmunicandi2_Chart.Municipio"]["search"] = array("table" => "cm2_elecmunicandi2 Chart", "field" => "Municipio", "page" => "search");
	$lookupTableLinks["convocatoria"]["cm2_escanos2_Chart.Convocatoria_idConvocatoria"]["search"] = array("table" => "cm2_escanos2 Chart", "field" => "Convocatoria_idConvocatoria", "page" => "search");
	$lookupTableLinks["candidatura"]["cm2_escanos2_Chart.Candidatura_idCandidatura"]["search"] = array("table" => "cm2_escanos2 Chart", "field" => "Candidatura_idCandidatura", "page" => "search");
	$lookupTableLinks["municipio"]["cm2_distrito.NumeroMunicipio"]["edit"] = array("table" => "cm2_distrito", "field" => "NumeroMunicipio", "page" => "edit");
	$lookupTableLinks["convocatoria"]["cm2_elecdistricandi_Chart.Convocatoria_idConvocatoria"]["search"] = array("table" => "cm2_elecdistricandi Chart", "field" => "Convocatoria_idConvocatoria", "page" => "search");
	$lookupTableLinks["candidatura"]["cm2_elecdistricandi_Chart.Candidatura_idCandidatura"]["search"] = array("table" => "cm2_elecdistricandi Chart", "field" => "Candidatura_idCandidatura", "page" => "search");
	$lookupTableLinks["municipio"]["cm2_elecdistricandi_Chart.Municipio"]["search"] = array("table" => "cm2_elecdistricandi Chart", "field" => "Municipio", "page" => "search");
	$lookupTableLinks["distrito"]["cm2_elecdistricandi_Chart.Distrito"]["search"] = array("table" => "cm2_elecdistricandi Chart", "field" => "Distrito", "page" => "search");
	$lookupTableLinks["convocatoria"]["cm2_elecdistricandi2_Chart.Convocatoria_idConvocatoria"]["search"] = array("table" => "cm2_elecdistricandi2 Chart", "field" => "Convocatoria_idConvocatoria", "page" => "search");
	$lookupTableLinks["candidatura"]["cm2_elecdistricandi2_Chart.Candidatura_idCandidatura"]["search"] = array("table" => "cm2_elecdistricandi2 Chart", "field" => "Candidatura_idCandidatura", "page" => "search");
	$lookupTableLinks["municipio"]["cm2_elecdistricandi2_Chart.Municipio"]["search"] = array("table" => "cm2_elecdistricandi2 Chart", "field" => "Municipio", "page" => "search");
	$lookupTableLinks["distrito"]["cm2_elecdistricandi2_Chart.Distrito"]["search"] = array("table" => "cm2_elecdistricandi2 Chart", "field" => "Distrito", "page" => "search");
	$lookupTableLinks["cm3_convocatoria"]["cm3_mapa_municipio.Convocatoria_idConvocatoria"]["edit"] = array("table" => "cm3_mapa_municipio", "field" => "Convocatoria_idConvocatoria", "page" => "edit");
	$lookupTableLinks["cm3_eleccandidatura"]["cm3_mapa_municipio.idCandidatura"]["edit"] = array("table" => "cm3_mapa_municipio", "field" => "idCandidatura", "page" => "edit");
	$lookupTableLinks["convocatoria"]["cm3_mapa_distrito_madrid.Convocatoria_idConvocatoria"]["edit"] = array("table" => "cm3_mapa_distrito_madrid", "field" => "Convocatoria_idConvocatoria", "page" => "edit");
	$lookupTableLinks["convocatoria"]["cm1_2_Candidaturas.Convocatoria_idConvocatoria"]["edit"] = array("table" => "cm1_2_Candidaturas", "field" => "Convocatoria_idConvocatoria", "page" => "edit");
	$lookupTableLinks["candidatura"]["cm1_2_Candidaturas.Candidatura_idCandidatura"]["edit"] = array("table" => "cm1_2_Candidaturas", "field" => "Candidatura_idCandidatura", "page" => "edit");
	$lookupTableLinks["candidatura"]["cm1_2_Candidaturas.Color"]["edit"] = array("table" => "cm1_2_Candidaturas", "field" => "Color", "page" => "edit");
	$lookupTableLinks["convocatoria"]["cm1_2_Electos.Convocatoria_idConvocatoria"]["edit"] = array("table" => "cm1_2_Electos", "field" => "Convocatoria_idConvocatoria", "page" => "edit");
	$lookupTableLinks["candidatura"]["cm1_2_Electos.Candidatura_idCandidatura"]["edit"] = array("table" => "cm1_2_Electos", "field" => "Candidatura_idCandidatura", "page" => "edit");
	$lookupTableLinks["convocatoria"]["elecmunicandi.Convocatoria_idConvocatoria"]["edit"] = array("table" => "elecmunicandi", "field" => "Convocatoria_idConvocatoria", "page" => "edit");
	$lookupTableLinks["candidatura"]["elecmunicandi.Candidatura_idCandidatura"]["edit"] = array("table" => "elecmunicandi", "field" => "Candidatura_idCandidatura", "page" => "edit");
	$lookupTableLinks["municipio"]["elecmunicandi.Municipio"]["edit"] = array("table" => "elecmunicandi", "field" => "Municipio", "page" => "edit");
	$lookupTableLinks["convocatoria"]["elecdistricandi.Convocatoria_idConvocatoria"]["edit"] = array("table" => "elecdistricandi", "field" => "Convocatoria_idConvocatoria", "page" => "edit");
	$lookupTableLinks["candidatura"]["elecdistricandi.Candidatura_idCandidatura"]["edit"] = array("table" => "elecdistricandi", "field" => "Candidatura_idCandidatura", "page" => "edit");
	$lookupTableLinks["municipio"]["elecdistricandi.Municipio"]["edit"] = array("table" => "elecdistricandi", "field" => "Municipio", "page" => "edit");
	$lookupTableLinks["distrito"]["elecdistricandi.Distrito"]["edit"] = array("table" => "elecdistricandi", "field" => "Distrito", "page" => "edit");
}

?>