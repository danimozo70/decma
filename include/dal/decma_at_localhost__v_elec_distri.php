<?php
$dalTablev_elec_distri = array();
$dalTablev_elec_distri["NumeroMunicipio"] = array("type"=>3,"varname"=>"NumeroMunicipio", "name" => "NumeroMunicipio");
$dalTablev_elec_distri["NumeroDistrito"] = array("type"=>3,"varname"=>"NumeroDistrito", "name" => "NumeroDistrito");
$dalTablev_elec_distri["NombreDistrito"] = array("type"=>200,"varname"=>"NombreDistrito", "name" => "NombreDistrito");
$dalTablev_elec_distri["idElecDistriTotal"] = array("type"=>3,"varname"=>"idElecDistriTotal", "name" => "idElecDistriTotal");
$dalTablev_elec_distri["Convocatoria_idConvocatoria"] = array("type"=>3,"varname"=>"Convocatoria_idConvocatoria", "name" => "Convocatoria_idConvocatoria");
$dalTablev_elec_distri["Censo"] = array("type"=>3,"varname"=>"Censo", "name" => "Censo");
$dalTablev_elec_distri["Certificacion"] = array("type"=>3,"varname"=>"Certificacion", "name" => "Certificacion");
$dalTablev_elec_distri["Interventores"] = array("type"=>3,"varname"=>"Interventores", "name" => "Interventores");
$dalTablev_elec_distri["Nulo"] = array("type"=>3,"varname"=>"Nulo", "name" => "Nulo");
$dalTablev_elec_distri["Blanco"] = array("type"=>3,"varname"=>"Blanco", "name" => "Blanco");
$dalTablev_elec_distri["Validos"] = array("type"=>3,"varname"=>"Validos", "name" => "Validos");

$dal_info["decma_at_localhost__v_elec_distri"] = &$dalTablev_elec_distri;
?>