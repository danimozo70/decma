<?php
$dalTablev_distrito_voto = array();
$dalTablev_distrito_voto["Convocatoria_idConvocatoria"] = array("type"=>3,"varname"=>"Convocatoria_idConvocatoria", "name" => "Convocatoria_idConvocatoria");
$dalTablev_distrito_voto["Municipio"] = array("type"=>3,"varname"=>"Municipio", "name" => "Municipio");
$dalTablev_distrito_voto["Distrito"] = array("type"=>3,"varname"=>"Distrito", "name" => "Distrito");
$dalTablev_distrito_voto["idCandidatura"] = array("type"=>3,"varname"=>"idCandidatura", "name" => "idCandidatura");
$dalTablev_distrito_voto["Codigo"] = array("type"=>200,"varname"=>"Codigo", "name" => "Codigo");
$dalTablev_distrito_voto["Titulo"] = array("type"=>200,"varname"=>"Titulo", "name" => "Titulo");
$dalTablev_distrito_voto["Color"] = array("type"=>200,"varname"=>"Color", "name" => "Color");
$dalTablev_distrito_voto["Logo"] = array("type"=>200,"varname"=>"Logo", "name" => "Logo");
$dalTablev_distrito_voto["Votos"] = array("type"=>3,"varname"=>"Votos", "name" => "Votos");
$dalTablev_distrito_voto["PorcVotos"] = array("type"=>14,"varname"=>"PorcVotos", "name" => "PorcVotos");

$dal_info["decma_at_localhost__v_distrito_voto"] = &$dalTablev_distrito_voto;
?>