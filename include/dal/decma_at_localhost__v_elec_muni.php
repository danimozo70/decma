<?php
$dalTablev_elec_muni = array();
$dalTablev_elec_muni["NumeroMunicipio"] = array("type"=>3,"varname"=>"NumeroMunicipio", "name" => "NumeroMunicipio");
$dalTablev_elec_muni["NombreMunicipio"] = array("type"=>200,"varname"=>"NombreMunicipio", "name" => "NombreMunicipio");
$dalTablev_elec_muni["Convocatoria_idConvocatoria"] = array("type"=>3,"varname"=>"Convocatoria_idConvocatoria", "name" => "Convocatoria_idConvocatoria");
$dalTablev_elec_muni["Censo"] = array("type"=>3,"varname"=>"Censo", "name" => "Censo");
$dalTablev_elec_muni["Certificacion"] = array("type"=>3,"varname"=>"Certificacion", "name" => "Certificacion");
$dalTablev_elec_muni["Interventores"] = array("type"=>3,"varname"=>"Interventores", "name" => "Interventores");
$dalTablev_elec_muni["Nulo"] = array("type"=>3,"varname"=>"Nulo", "name" => "Nulo");
$dalTablev_elec_muni["Blanco"] = array("type"=>3,"varname"=>"Blanco", "name" => "Blanco");
$dalTablev_elec_muni["Validos"] = array("type"=>3,"varname"=>"Validos", "name" => "Validos");

$dal_info["decma_at_localhost__v_elec_muni"] = &$dalTablev_elec_muni;
?>