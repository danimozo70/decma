<?php
$dalTablev_convocatoria_total = array();
$dalTablev_convocatoria_total["Convocatoria_idConvocatoria"] = array("type"=>3,"varname"=>"Convocatoria_idConvocatoria", "name" => "Convocatoria_idConvocatoria");
$dalTablev_convocatoria_total["Censo"] = array("type"=>14,"varname"=>"Censo", "name" => "Censo");
$dalTablev_convocatoria_total["CensoOficial"] = array("type"=>14,"varname"=>"CensoOficial", "name" => "CensoOficial");
$dalTablev_convocatoria_total["Certificacion"] = array("type"=>14,"varname"=>"Certificacion", "name" => "Certificacion");
$dalTablev_convocatoria_total["CertificacionAlta"] = array("type"=>14,"varname"=>"CertificacionAlta", "name" => "CertificacionAlta");
$dalTablev_convocatoria_total["CertificacionError"] = array("type"=>14,"varname"=>"CertificacionError", "name" => "CertificacionError");
$dalTablev_convocatoria_total["Voto"] = array("type"=>14,"varname"=>"Voto", "name" => "Voto");
$dalTablev_convocatoria_total["Interventores"] = array("type"=>14,"varname"=>"Interventores", "name" => "Interventores");
$dalTablev_convocatoria_total["Nulo"] = array("type"=>14,"varname"=>"Nulo", "name" => "Nulo");
$dalTablev_convocatoria_total["Blanco"] = array("type"=>14,"varname"=>"Blanco", "name" => "Blanco");
$dalTablev_convocatoria_total["Validos"] = array("type"=>14,"varname"=>"Validos", "name" => "Validos");

$dal_info["decma_at_localhost__v_convocatoria_total"] = &$dalTablev_convocatoria_total;
?>