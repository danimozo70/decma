<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm2_escanos_Report = array();
	$tdatacm2_escanos_Report[".truncateText"] = true;
	$tdatacm2_escanos_Report[".NumberOfChars"] = 80;
	$tdatacm2_escanos_Report[".ShortName"] = "cm2_escanos_Report";
	$tdatacm2_escanos_Report[".OwnerID"] = "";
	$tdatacm2_escanos_Report[".OriginalTable"] = "escanos";

//	field labels
$fieldLabelscm2_escanos_Report = array();
$fieldToolTipscm2_escanos_Report = array();
$pageTitlescm2_escanos_Report = array();
$placeHolderscm2_escanos_Report = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm2_escanos_Report["Spanish"] = array();
	$fieldToolTipscm2_escanos_Report["Spanish"] = array();
	$placeHolderscm2_escanos_Report["Spanish"] = array();
	$pageTitlescm2_escanos_Report["Spanish"] = array();
	$fieldLabelscm2_escanos_Report["Spanish"]["idEscanos"] = "Id Escanos";
	$fieldToolTipscm2_escanos_Report["Spanish"]["idEscanos"] = "";
	$placeHolderscm2_escanos_Report["Spanish"]["idEscanos"] = "";
	$fieldLabelscm2_escanos_Report["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria IdConvocatoria";
	$fieldToolTipscm2_escanos_Report["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderscm2_escanos_Report["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelscm2_escanos_Report["Spanish"]["Candidatura_idCandidatura"] = "Candidatura IdCandidatura";
	$fieldToolTipscm2_escanos_Report["Spanish"]["Candidatura_idCandidatura"] = "";
	$placeHolderscm2_escanos_Report["Spanish"]["Candidatura_idCandidatura"] = "";
	$fieldLabelscm2_escanos_Report["Spanish"]["Votos"] = "Votos";
	$fieldToolTipscm2_escanos_Report["Spanish"]["Votos"] = "";
	$placeHolderscm2_escanos_Report["Spanish"]["Votos"] = "";
	$fieldLabelscm2_escanos_Report["Spanish"]["PorcVotos"] = "Porc Votos";
	$fieldToolTipscm2_escanos_Report["Spanish"]["PorcVotos"] = "";
	$placeHolderscm2_escanos_Report["Spanish"]["PorcVotos"] = "";
	$fieldLabelscm2_escanos_Report["Spanish"]["Escanos"] = "Escanos";
	$fieldToolTipscm2_escanos_Report["Spanish"]["Escanos"] = "";
	$placeHolderscm2_escanos_Report["Spanish"]["Escanos"] = "";
	if (count($fieldToolTipscm2_escanos_Report["Spanish"]))
		$tdatacm2_escanos_Report[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm2_escanos_Report[""] = array();
	$fieldToolTipscm2_escanos_Report[""] = array();
	$placeHolderscm2_escanos_Report[""] = array();
	$pageTitlescm2_escanos_Report[""] = array();
	if (count($fieldToolTipscm2_escanos_Report[""]))
		$tdatacm2_escanos_Report[".isUseToolTips"] = true;
}


	$tdatacm2_escanos_Report[".NCSearch"] = true;



$tdatacm2_escanos_Report[".shortTableName"] = "cm2_escanos_Report";
$tdatacm2_escanos_Report[".nSecOptions"] = 0;
$tdatacm2_escanos_Report[".recsPerRowPrint"] = 1;
$tdatacm2_escanos_Report[".mainTableOwnerID"] = "";
$tdatacm2_escanos_Report[".moveNext"] = 1;
$tdatacm2_escanos_Report[".entityType"] = 2;

$tdatacm2_escanos_Report[".strOriginalTableName"] = "escanos";

	



$tdatacm2_escanos_Report[".showAddInPopup"] = false;

$tdatacm2_escanos_Report[".showEditInPopup"] = false;

$tdatacm2_escanos_Report[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacm2_escanos_Report[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm2_escanos_Report[".fieldsForRegister"] = array();

$tdatacm2_escanos_Report[".listAjax"] = false;

	$tdatacm2_escanos_Report[".audit"] = false;

	$tdatacm2_escanos_Report[".locking"] = false;

$tdatacm2_escanos_Report[".edit"] = true;
$tdatacm2_escanos_Report[".afterEditAction"] = 1;
$tdatacm2_escanos_Report[".closePopupAfterEdit"] = 1;
$tdatacm2_escanos_Report[".afterEditActionDetTable"] = "";

$tdatacm2_escanos_Report[".add"] = true;
$tdatacm2_escanos_Report[".afterAddAction"] = 1;
$tdatacm2_escanos_Report[".closePopupAfterAdd"] = 1;
$tdatacm2_escanos_Report[".afterAddActionDetTable"] = "";

$tdatacm2_escanos_Report[".list"] = true;



$tdatacm2_escanos_Report[".reorderRecordsByHeader"] = true;


$tdatacm2_escanos_Report[".exportFormatting"] = 2;
$tdatacm2_escanos_Report[".exportDelimiter"] = ",";
		
$tdatacm2_escanos_Report[".view"] = true;


$tdatacm2_escanos_Report[".exportTo"] = true;


$tdatacm2_escanos_Report[".delete"] = true;

$tdatacm2_escanos_Report[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm2_escanos_Report[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm2_escanos_Report[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm2_escanos_Report[".searchSaving"] = false;
//

$tdatacm2_escanos_Report[".showSearchPanel"] = true;
		$tdatacm2_escanos_Report[".flexibleSearch"] = true;

$tdatacm2_escanos_Report[".isUseAjaxSuggest"] = true;






$tdatacm2_escanos_Report[".ajaxCodeSnippetAdded"] = false;

$tdatacm2_escanos_Report[".buttonsAdded"] = false;

$tdatacm2_escanos_Report[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm2_escanos_Report[".isUseTimeForSearch"] = false;



$tdatacm2_escanos_Report[".badgeColor"] = "b22222";


$tdatacm2_escanos_Report[".allSearchFields"] = array();
$tdatacm2_escanos_Report[".filterFields"] = array();
$tdatacm2_escanos_Report[".requiredSearchFields"] = array();



$tdatacm2_escanos_Report[".googleLikeFields"] = array();
$tdatacm2_escanos_Report[".googleLikeFields"][] = "idEscanos";
$tdatacm2_escanos_Report[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_Report[".googleLikeFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_Report[".googleLikeFields"][] = "Votos";
$tdatacm2_escanos_Report[".googleLikeFields"][] = "PorcVotos";
$tdatacm2_escanos_Report[".googleLikeFields"][] = "Escanos";



$tdatacm2_escanos_Report[".tableType"] = "report";

$tdatacm2_escanos_Report[".printerPageOrientation"] = 0;
$tdatacm2_escanos_Report[".nPrinterPageScale"] = 100;

$tdatacm2_escanos_Report[".nPrinterSplitRecords"] = 40;

$tdatacm2_escanos_Report[".nPrinterPDFSplitRecords"] = 40;



$tdatacm2_escanos_Report[".geocodingEnabled"] = false;

//report settings
$tdatacm2_escanos_Report[".crossTabReport"] = true;

$tdatacm2_escanos_Report[".reportGroupFields"] = true;
$tdatacm2_escanos_Report[".pageSize"] = 1;
$tdatacm2_escanos_Report[".showGroupSummaryCount"] = true;
$reportGroupFields = array();
	$rgroupField = array();
	$rgroupField['strGroupField'] = "Candidatura_idCandidatura";
	$rgroupField['groupInterval'] = 0;
	$rgroupField['groupOrder'] = 1;
	$rgroupField['showGroupSummary'] = "1";
	$rgroupField['crossTabAxis'] = "0";
	$reportGroupFields[] = $rgroupField;
	$rgroupField = array();
	$rgroupField['strGroupField'] = "Convocatoria_idConvocatoria";
	$rgroupField['groupInterval'] = 0;
	$rgroupField['groupOrder'] = 2;
	$rgroupField['showGroupSummary'] = "1";
	$rgroupField['crossTabAxis'] = "1";
	$reportGroupFields[] = $rgroupField;
$tdatacm2_escanos_Report[".reportGroupFieldsData"] = $reportGroupFields;


$tdatacm2_escanos_Report[".isExistTotalFields"] = true;




$tdatacm2_escanos_Report[".repShowDet"] = true;

$tdatacm2_escanos_Report[".reportLayout"] = 0;

//end of report settings




$tdatacm2_escanos_Report[".listGridLayout"] = 3;





// view page pdf

// print page pdf



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm2_escanos_Report[".strOrderBy"] = $tstrOrderBy;

$tdatacm2_escanos_Report[".orderindexes"] = array();

$tdatacm2_escanos_Report[".sqlHead"] = "SELECT idEscanos,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Votos,  PorcVotos,  Escanos";
$tdatacm2_escanos_Report[".sqlFrom"] = "FROM escanos";
$tdatacm2_escanos_Report[".sqlWhereExpr"] = "(Escanos <> 0)";
$tdatacm2_escanos_Report[".sqlTail"] = "";

//fill array of tabs for list page
$arrGridTabs = array();
$arrGridTabs[] = array(
	'tabId' => "",
	'name' => "All data",
	'nameType' => 'Text',
	'where' => "",	
	'showRowCount' => 0,
	'hideEmpty' => 0,	
);				  
$tdatacm2_escanos_Report[".arrGridTabs"] = $arrGridTabs;











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm2_escanos_Report[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm2_escanos_Report[".arrGroupsPerPage"] = $arrGPP;

$tdatacm2_escanos_Report[".highlightSearchResults"] = true;

$tableKeyscm2_escanos_Report = array();
$tableKeyscm2_escanos_Report[] = "idEscanos";
$tdatacm2_escanos_Report[".Keys"] = $tableKeyscm2_escanos_Report;

$tdatacm2_escanos_Report[".listFields"] = array();
$tdatacm2_escanos_Report[".listFields"][] = "idEscanos";
$tdatacm2_escanos_Report[".listFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_Report[".listFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_Report[".listFields"][] = "Votos";
$tdatacm2_escanos_Report[".listFields"][] = "PorcVotos";
$tdatacm2_escanos_Report[".listFields"][] = "Escanos";

$tdatacm2_escanos_Report[".hideMobileList"] = array();


$tdatacm2_escanos_Report[".viewFields"] = array();
$tdatacm2_escanos_Report[".viewFields"][] = "idEscanos";
$tdatacm2_escanos_Report[".viewFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_Report[".viewFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_Report[".viewFields"][] = "Votos";
$tdatacm2_escanos_Report[".viewFields"][] = "PorcVotos";
$tdatacm2_escanos_Report[".viewFields"][] = "Escanos";

$tdatacm2_escanos_Report[".addFields"] = array();
$tdatacm2_escanos_Report[".addFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_Report[".addFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_Report[".addFields"][] = "Votos";
$tdatacm2_escanos_Report[".addFields"][] = "PorcVotos";
$tdatacm2_escanos_Report[".addFields"][] = "Escanos";

$tdatacm2_escanos_Report[".masterListFields"] = array();
$tdatacm2_escanos_Report[".masterListFields"][] = "idEscanos";
$tdatacm2_escanos_Report[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_Report[".masterListFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_Report[".masterListFields"][] = "Votos";
$tdatacm2_escanos_Report[".masterListFields"][] = "PorcVotos";
$tdatacm2_escanos_Report[".masterListFields"][] = "Escanos";

$tdatacm2_escanos_Report[".inlineAddFields"] = array();
$tdatacm2_escanos_Report[".inlineAddFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_Report[".inlineAddFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_Report[".inlineAddFields"][] = "Votos";
$tdatacm2_escanos_Report[".inlineAddFields"][] = "PorcVotos";
$tdatacm2_escanos_Report[".inlineAddFields"][] = "Escanos";

$tdatacm2_escanos_Report[".editFields"] = array();
$tdatacm2_escanos_Report[".editFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_Report[".editFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_Report[".editFields"][] = "Votos";
$tdatacm2_escanos_Report[".editFields"][] = "PorcVotos";
$tdatacm2_escanos_Report[".editFields"][] = "Escanos";

$tdatacm2_escanos_Report[".inlineEditFields"] = array();
$tdatacm2_escanos_Report[".inlineEditFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_Report[".inlineEditFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_Report[".inlineEditFields"][] = "Votos";
$tdatacm2_escanos_Report[".inlineEditFields"][] = "PorcVotos";
$tdatacm2_escanos_Report[".inlineEditFields"][] = "Escanos";

$tdatacm2_escanos_Report[".updateSelectedFields"] = array();
$tdatacm2_escanos_Report[".updateSelectedFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_Report[".updateSelectedFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_Report[".updateSelectedFields"][] = "Votos";
$tdatacm2_escanos_Report[".updateSelectedFields"][] = "PorcVotos";
$tdatacm2_escanos_Report[".updateSelectedFields"][] = "Escanos";


$tdatacm2_escanos_Report[".exportFields"] = array();
$tdatacm2_escanos_Report[".exportFields"][] = "idEscanos";
$tdatacm2_escanos_Report[".exportFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_Report[".exportFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_Report[".exportFields"][] = "Votos";
$tdatacm2_escanos_Report[".exportFields"][] = "PorcVotos";
$tdatacm2_escanos_Report[".exportFields"][] = "Escanos";

$tdatacm2_escanos_Report[".importFields"] = array();
$tdatacm2_escanos_Report[".importFields"][] = "idEscanos";
$tdatacm2_escanos_Report[".importFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_Report[".importFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_Report[".importFields"][] = "Votos";
$tdatacm2_escanos_Report[".importFields"][] = "PorcVotos";
$tdatacm2_escanos_Report[".importFields"][] = "Escanos";

$tdatacm2_escanos_Report[".printFields"] = array();
$tdatacm2_escanos_Report[".printFields"][] = "idEscanos";
$tdatacm2_escanos_Report[".printFields"][] = "Convocatoria_idConvocatoria";
$tdatacm2_escanos_Report[".printFields"][] = "Candidatura_idCandidatura";
$tdatacm2_escanos_Report[".printFields"][] = "Votos";
$tdatacm2_escanos_Report[".printFields"][] = "PorcVotos";
$tdatacm2_escanos_Report[".printFields"][] = "Escanos";


//	idEscanos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idEscanos";
	$fdata["GoodName"] = "idEscanos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm2_escanos_Report","idEscanos");
	$fdata["FieldType"] = 3;

		// report field settings
					// end of report field settings

		$fdata["AutoInc"] = true;

	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idEscanos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idEscanos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_escanos_Report["idEscanos"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm2_escanos_Report","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

		// report field settings
					// end of report field settings

	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_escanos_Report["Convocatoria_idConvocatoria"] = $fdata;
//	Candidatura_idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Candidatura_idCandidatura";
	$fdata["GoodName"] = "Candidatura_idCandidatura";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm2_escanos_Report","Candidatura_idCandidatura");
	$fdata["FieldType"] = 3;

		// report field settings
					// end of report field settings

	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Candidatura_idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Candidatura_idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "candidatura";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idCandidatura";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Codigo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_escanos_Report["Candidatura_idCandidatura"] = $fdata;
//	Votos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Votos";
	$fdata["GoodName"] = "Votos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm2_escanos_Report","Votos");
	$fdata["FieldType"] = 3;

		// report field settings
					// end of report field settings

	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Votos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Votos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_escanos_Report["Votos"] = $fdata;
//	PorcVotos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "PorcVotos";
	$fdata["GoodName"] = "PorcVotos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm2_escanos_Report","PorcVotos");
	$fdata["FieldType"] = 14;

		// report field settings
					// end of report field settings

	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "PorcVotos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "PorcVotos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_escanos_Report["PorcVotos"] = $fdata;
//	Escanos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Escanos";
	$fdata["GoodName"] = "Escanos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm2_escanos_Report","Escanos");
	$fdata["FieldType"] = 3;

		// report field settings
					$fdata["isTotalSum"] = true;
	// end of report field settings

	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Escanos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Escanos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["report"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm2_escanos_Report["Escanos"] = $fdata;


$tables_data["cm2_escanos Report"]=&$tdatacm2_escanos_Report;
$field_labels["cm2_escanos_Report"] = &$fieldLabelscm2_escanos_Report;
$fieldToolTips["cm2_escanos_Report"] = &$fieldToolTipscm2_escanos_Report;
$placeHolders["cm2_escanos_Report"] = &$placeHolderscm2_escanos_Report;
$page_titles["cm2_escanos_Report"] = &$pageTitlescm2_escanos_Report;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)

// tables which are master tables for current table (detail)
$masterTablesData["cm2_escanos Report"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm2_escanos_Report()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idEscanos,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Votos,  PorcVotos,  Escanos";
$proto0["m_strFrom"] = "FROM escanos";
$proto0["m_strWhere"] = "(Escanos <> 0)";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "Escanos <> 0";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos Report"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "<> 0";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idEscanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos Report"
));

$proto6["m_sql"] = "idEscanos";
$proto6["m_srcTableName"] = "cm2_escanos Report";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos Report"
));

$proto8["m_sql"] = "Convocatoria_idConvocatoria";
$proto8["m_srcTableName"] = "cm2_escanos Report";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos Report"
));

$proto10["m_sql"] = "Candidatura_idCandidatura";
$proto10["m_srcTableName"] = "cm2_escanos Report";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos Report"
));

$proto12["m_sql"] = "Votos";
$proto12["m_srcTableName"] = "cm2_escanos Report";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "PorcVotos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos Report"
));

$proto14["m_sql"] = "PorcVotos";
$proto14["m_srcTableName"] = "cm2_escanos Report";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "cm2_escanos Report"
));

$proto16["m_sql"] = "Escanos";
$proto16["m_srcTableName"] = "cm2_escanos Report";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "escanos";
$proto19["m_srcTableName"] = "cm2_escanos Report";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "idEscanos";
$proto19["m_columns"][] = "Convocatoria_idConvocatoria";
$proto19["m_columns"][] = "Candidatura_idCandidatura";
$proto19["m_columns"][] = "Votos";
$proto19["m_columns"][] = "PorcVotos";
$proto19["m_columns"][] = "Escanos";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "escanos";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "cm2_escanos Report";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="cm2_escanos Report";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_cm2_escanos_Report = createSqlQuery_cm2_escanos_Report();


	
		;

						

$tdatacm2_escanos_Report[".sqlquery"] = $queryData_cm2_escanos_Report;

$tableEvents["cm2_escanos Report"] = new eventsBase;
$tdatacm2_escanos_Report[".hasEvents"] = false;

?>