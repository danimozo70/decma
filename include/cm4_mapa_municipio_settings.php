<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm4_mapa_municipio = array();
	$tdatacm4_mapa_municipio[".truncateText"] = true;
	$tdatacm4_mapa_municipio[".NumberOfChars"] = 80;
	$tdatacm4_mapa_municipio[".ShortName"] = "cm4_mapa_municipio";
	$tdatacm4_mapa_municipio[".OwnerID"] = "";
	$tdatacm4_mapa_municipio[".OriginalTable"] = "convocatoria";

//	field labels
$fieldLabelscm4_mapa_municipio = array();
$fieldToolTipscm4_mapa_municipio = array();
$pageTitlescm4_mapa_municipio = array();
$placeHolderscm4_mapa_municipio = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm4_mapa_municipio["Spanish"] = array();
	$fieldToolTipscm4_mapa_municipio["Spanish"] = array();
	$placeHolderscm4_mapa_municipio["Spanish"] = array();
	$pageTitlescm4_mapa_municipio["Spanish"] = array();
	$fieldLabelscm4_mapa_municipio["Spanish"]["idConvocatoria"] = "Id Interno";
	$fieldToolTipscm4_mapa_municipio["Spanish"]["idConvocatoria"] = "";
	$placeHolderscm4_mapa_municipio["Spanish"]["idConvocatoria"] = "";
	$fieldLabelscm4_mapa_municipio["Spanish"]["Codigo"] = "Codigo";
	$fieldToolTipscm4_mapa_municipio["Spanish"]["Codigo"] = "";
	$placeHolderscm4_mapa_municipio["Spanish"]["Codigo"] = "";
	$fieldLabelscm4_mapa_municipio["Spanish"]["Descripcion"] = "Descripción";
	$fieldToolTipscm4_mapa_municipio["Spanish"]["Descripcion"] = "";
	$placeHolderscm4_mapa_municipio["Spanish"]["Descripcion"] = "";
	$pageTitlescm4_mapa_municipio["Spanish"]["list"] = "Candidatura mas votada por municipios";
	if (count($fieldToolTipscm4_mapa_municipio["Spanish"]))
		$tdatacm4_mapa_municipio[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm4_mapa_municipio[""] = array();
	$fieldToolTipscm4_mapa_municipio[""] = array();
	$placeHolderscm4_mapa_municipio[""] = array();
	$pageTitlescm4_mapa_municipio[""] = array();
	$fieldLabelscm4_mapa_municipio[""]["Codigo"] = "Codigo";
	$fieldToolTipscm4_mapa_municipio[""]["Codigo"] = "";
	$placeHolderscm4_mapa_municipio[""]["Codigo"] = "";
	$fieldLabelscm4_mapa_municipio[""]["Descripcion"] = "Descripcion";
	$fieldToolTipscm4_mapa_municipio[""]["Descripcion"] = "";
	$placeHolderscm4_mapa_municipio[""]["Descripcion"] = "";
	if (count($fieldToolTipscm4_mapa_municipio[""]))
		$tdatacm4_mapa_municipio[".isUseToolTips"] = true;
}


	$tdatacm4_mapa_municipio[".NCSearch"] = true;



$tdatacm4_mapa_municipio[".shortTableName"] = "cm4_mapa_municipio";
$tdatacm4_mapa_municipio[".nSecOptions"] = 0;
$tdatacm4_mapa_municipio[".recsPerRowPrint"] = 1;
$tdatacm4_mapa_municipio[".mainTableOwnerID"] = "";
$tdatacm4_mapa_municipio[".moveNext"] = 0;
$tdatacm4_mapa_municipio[".entityType"] = 1;

$tdatacm4_mapa_municipio[".strOriginalTableName"] = "convocatoria";

	



$tdatacm4_mapa_municipio[".showAddInPopup"] = false;

$tdatacm4_mapa_municipio[".showEditInPopup"] = true;

$tdatacm4_mapa_municipio[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["edit"] = "view_bootstrap";
			;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdatacm4_mapa_municipio[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm4_mapa_municipio[".fieldsForRegister"] = array();

$tdatacm4_mapa_municipio[".listAjax"] = false;

	$tdatacm4_mapa_municipio[".audit"] = false;

	$tdatacm4_mapa_municipio[".locking"] = false;



$tdatacm4_mapa_municipio[".list"] = true;



$tdatacm4_mapa_municipio[".createSortByDropdown"] = true;
$tdatacm4_mapa_municipio[".strSortControlSettingsJSON"] = "";








$tdatacm4_mapa_municipio[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm4_mapa_municipio[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm4_mapa_municipio[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm4_mapa_municipio[".searchSaving"] = false;
//

	$tdatacm4_mapa_municipio[".showSearchPanel"] = false;

$tdatacm4_mapa_municipio[".isUseAjaxSuggest"] = true;

$tdatacm4_mapa_municipio[".rowHighlite"] = true;





$tdatacm4_mapa_municipio[".ajaxCodeSnippetAdded"] = false;

$tdatacm4_mapa_municipio[".buttonsAdded"] = false;

$tdatacm4_mapa_municipio[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm4_mapa_municipio[".isUseTimeForSearch"] = false;



$tdatacm4_mapa_municipio[".badgeColor"] = "cd853f";

$tdatacm4_mapa_municipio[".detailsLinksOnList"] = "1";

$tdatacm4_mapa_municipio[".allSearchFields"] = array();
$tdatacm4_mapa_municipio[".filterFields"] = array();
$tdatacm4_mapa_municipio[".requiredSearchFields"] = array();



$tdatacm4_mapa_municipio[".googleLikeFields"] = array();
$tdatacm4_mapa_municipio[".googleLikeFields"][] = "idConvocatoria";
$tdatacm4_mapa_municipio[".googleLikeFields"][] = "Codigo";
$tdatacm4_mapa_municipio[".googleLikeFields"][] = "Descripcion";



$tdatacm4_mapa_municipio[".tableType"] = "list";

$tdatacm4_mapa_municipio[".printerPageOrientation"] = 0;
$tdatacm4_mapa_municipio[".nPrinterPageScale"] = 100;

$tdatacm4_mapa_municipio[".nPrinterSplitRecords"] = 40;

$tdatacm4_mapa_municipio[".nPrinterPDFSplitRecords"] = 40;



$tdatacm4_mapa_municipio[".geocodingEnabled"] = false;





$tdatacm4_mapa_municipio[".listGridLayout"] = 2;





// view page pdf

// print page pdf


$tdatacm4_mapa_municipio[".pageSize"] = 1;

$tdatacm4_mapa_municipio[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm4_mapa_municipio[".strOrderBy"] = $tstrOrderBy;

$tdatacm4_mapa_municipio[".orderindexes"] = array();

$tdatacm4_mapa_municipio[".sqlHead"] = "SELECT idConvocatoria,  Titulo AS Codigo,  Descripcion";
$tdatacm4_mapa_municipio[".sqlFrom"] = "FROM convocatoria";
$tdatacm4_mapa_municipio[".sqlWhereExpr"] = "";
$tdatacm4_mapa_municipio[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm4_mapa_municipio[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm4_mapa_municipio[".arrGroupsPerPage"] = $arrGPP;

$tdatacm4_mapa_municipio[".highlightSearchResults"] = true;

$tableKeyscm4_mapa_municipio = array();
$tableKeyscm4_mapa_municipio[] = "idConvocatoria";
$tdatacm4_mapa_municipio[".Keys"] = $tableKeyscm4_mapa_municipio;

$tdatacm4_mapa_municipio[".listFields"] = array();
$tdatacm4_mapa_municipio[".listFields"][] = "Codigo";

$tdatacm4_mapa_municipio[".hideMobileList"] = array();


$tdatacm4_mapa_municipio[".viewFields"] = array();

$tdatacm4_mapa_municipio[".addFields"] = array();

$tdatacm4_mapa_municipio[".masterListFields"] = array();
$tdatacm4_mapa_municipio[".masterListFields"][] = "idConvocatoria";
$tdatacm4_mapa_municipio[".masterListFields"][] = "Codigo";
$tdatacm4_mapa_municipio[".masterListFields"][] = "Descripcion";

$tdatacm4_mapa_municipio[".inlineAddFields"] = array();

$tdatacm4_mapa_municipio[".editFields"] = array();

$tdatacm4_mapa_municipio[".inlineEditFields"] = array();

$tdatacm4_mapa_municipio[".updateSelectedFields"] = array();


$tdatacm4_mapa_municipio[".exportFields"] = array();

$tdatacm4_mapa_municipio[".importFields"] = array();

$tdatacm4_mapa_municipio[".printFields"] = array();


//	idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idConvocatoria";
	$fdata["GoodName"] = "idConvocatoria";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm4_mapa_municipio","idConvocatoria");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idConvocatoria";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm4_mapa_municipio["idConvocatoria"] = $fdata;
//	Codigo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Codigo";
	$fdata["GoodName"] = "Codigo";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm4_mapa_municipio","Codigo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Titulo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm4_mapa_municipio["Codigo"] = $fdata;
//	Descripcion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Descripcion";
	$fdata["GoodName"] = "Descripcion";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm4_mapa_municipio","Descripcion");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Descripcion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Descripcion";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=500";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm4_mapa_municipio["Descripcion"] = $fdata;


$tables_data["cm4_mapa_municipio"]=&$tdatacm4_mapa_municipio;
$field_labels["cm4_mapa_municipio"] = &$fieldLabelscm4_mapa_municipio;
$fieldToolTips["cm4_mapa_municipio"] = &$fieldToolTipscm4_mapa_municipio;
$placeHolders["cm4_mapa_municipio"] = &$placeHolderscm4_mapa_municipio;
$page_titles["cm4_mapa_municipio"] = &$pageTitlescm4_mapa_municipio;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm4_mapa_municipio"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["cm4_mapa_municipio"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm4_convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm4_convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm4_mapa_municipio"][0] = $masterParams;
				$masterTablesData["cm4_mapa_municipio"][0]["masterKeys"] = array();
	$masterTablesData["cm4_mapa_municipio"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["cm4_mapa_municipio"][0]["detailKeys"] = array();
	$masterTablesData["cm4_mapa_municipio"][0]["detailKeys"][]="idConvocatoria";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm4_mapa_municipio()
{
$proto2=array();
$proto2["m_strHead"] = "SELECT";
$proto2["m_strFieldList"] = "idConvocatoria,  Titulo AS Codigo,  Descripcion";
$proto2["m_strFrom"] = "FROM convocatoria";
$proto2["m_strWhere"] = "";
$proto2["m_strOrderBy"] = "";
	
		;
			$proto2["cipherer"] = null;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto2["m_where"] = $obj;
$proto6=array();
$proto6["m_sql"] = "";
$proto6["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto6["m_column"]=$obj;
$proto6["m_contained"] = array();
$proto6["m_strCase"] = "";
$proto6["m_havingmode"] = false;
$proto6["m_inBrackets"] = false;
$proto6["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto6);

$proto2["m_having"] = $obj;
$proto2["m_fieldlist"] = array();
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "idConvocatoria",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm4_mapa_municipio"
));

$proto8["m_sql"] = "idConvocatoria";
$proto8["m_srcTableName"] = "cm4_mapa_municipio";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto2["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm4_mapa_municipio"
));

$proto10["m_sql"] = "Titulo";
$proto10["m_srcTableName"] = "cm4_mapa_municipio";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "Codigo";
$obj = new SQLFieldListItem($proto10);

$proto2["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "Descripcion",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm4_mapa_municipio"
));

$proto12["m_sql"] = "Descripcion";
$proto12["m_srcTableName"] = "cm4_mapa_municipio";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto2["m_fieldlist"][]=$obj;
$proto2["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "convocatoria";
$proto15["m_srcTableName"] = "cm4_mapa_municipio";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "idConvocatoria";
$proto15["m_columns"][] = "Orden";
$proto15["m_columns"][] = "EsAsamblea";
$proto15["m_columns"][] = "Titulo";
$proto15["m_columns"][] = "Descripcion";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "convocatoria";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "cm4_mapa_municipio";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto2["m_fromlist"][]=$obj;
$proto2["m_groupby"] = array();
$proto2["m_orderby"] = array();
$proto2["m_srcTableName"]="cm4_mapa_municipio";		
$obj = new SQLQuery($proto2);

	return $obj;
}
$queryData_cm4_mapa_municipio = createSqlQuery_cm4_mapa_municipio();


	
		;

			

$tdatacm4_mapa_municipio[".sqlquery"] = $queryData_cm4_mapa_municipio;

$tableEvents["cm4_mapa_municipio"] = new eventsBase;
$tdatacm4_mapa_municipio[".hasEvents"] = false;

?>