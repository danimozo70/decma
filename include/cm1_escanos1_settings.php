<?php
require_once(getabspath("classes/cipherer.php"));



$tdatacm1_escanos1 = array();
$tdatacm1_escanos1[".ShortName"] = "cm1_escanos1";

//	field labels
$fieldLabelscm1_escanos1 = array();
$pageTitlescm1_escanos1 = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm1_escanos1["Spanish"] = array();
	$fieldLabelscm1_escanos1["Spanish"]["cm1_1_Convocatoria_idConvocatoria"] = "Id Interno";
	$fieldLabelscm1_escanos1["Spanish"]["cm1_1_Convocatoria_Orden"] = "Orden";
	$fieldLabelscm1_escanos1["Spanish"]["cm1_1_Convocatoria_EsAsamblea"] = "Es Asamblea?";
	$fieldLabelscm1_escanos1["Spanish"]["cm1_1_Convocatoria_Titulo"] = "Título";
	$fieldLabelscm1_escanos1["Spanish"]["cm1_1_Convocatoria_Descripcion"] = "Descripción";
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm1_escanos1[""] = array();
}

//	search fields
$tdatacm1_escanos1[".searchFields"] = array();
$dashField = array();
$dashField[] = array( "table"=>"cm1_1_Convocatoria", "field"=>"idConvocatoria" );
$tdatacm1_escanos1[".searchFields"]["cm1_1_Convocatoria_idConvocatoria"] = $dashField;
$dashField = array();
$dashField[] = array( "table"=>"cm1_1_Convocatoria", "field"=>"Orden" );
$tdatacm1_escanos1[".searchFields"]["cm1_1_Convocatoria_Orden"] = $dashField;
$dashField = array();
$dashField[] = array( "table"=>"cm1_1_Convocatoria", "field"=>"EsAsamblea" );
$tdatacm1_escanos1[".searchFields"]["cm1_1_Convocatoria_EsAsamblea"] = $dashField;
$dashField = array();
$dashField[] = array( "table"=>"cm1_1_Convocatoria", "field"=>"Titulo" );
$tdatacm1_escanos1[".searchFields"]["cm1_1_Convocatoria_Titulo"] = $dashField;
$dashField = array();
$dashField[] = array( "table"=>"cm1_1_Convocatoria", "field"=>"Descripcion" );
$tdatacm1_escanos1[".searchFields"]["cm1_1_Convocatoria_Descripcion"] = $dashField;

// all search fields
$tdatacm1_escanos1[".allSearchFields"] = array();
$tdatacm1_escanos1[".allSearchFields"][] = "cm1_1_Convocatoria_idConvocatoria";
$tdatacm1_escanos1[".allSearchFields"][] = "cm1_1_Convocatoria_Orden";
$tdatacm1_escanos1[".allSearchFields"][] = "cm1_1_Convocatoria_EsAsamblea";
$tdatacm1_escanos1[".allSearchFields"][] = "cm1_1_Convocatoria_Titulo";
$tdatacm1_escanos1[".allSearchFields"][] = "cm1_1_Convocatoria_Descripcion";

// good like search fields
$tdatacm1_escanos1[".googleLikeFields"] = array();
$tdatacm1_escanos1[".googleLikeFields"][] = "cm1_1_Convocatoria_idConvocatoria";
$tdatacm1_escanos1[".googleLikeFields"][] = "cm1_1_Convocatoria_Orden";
$tdatacm1_escanos1[".googleLikeFields"][] = "cm1_1_Convocatoria_EsAsamblea";
$tdatacm1_escanos1[".googleLikeFields"][] = "cm1_1_Convocatoria_Titulo";
$tdatacm1_escanos1[".googleLikeFields"][] = "cm1_1_Convocatoria_Descripcion";

$tdatacm1_escanos1[".dashElements"] = array();

	$dbelement = array( "elementName" => "cm1_1_Convocatoria_list", "table" => "cm1_1_Convocatoria", "type" => 0);
	$dbelement["cellName"] = "cell_0_0";

					$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;


	$tdatacm1_escanos1[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm1_1_Convocatoria_details", "table" => "cm1_1_Convocatoria", "type" => 5);
	$dbelement["cellName"] = "cell_0_1";

				$dbelement["notUsedDetailTables"] = array();
	$dbelement["initialTabDetailTable"] = "v_elec_distri";
	$dbelement["details"] = array();
	$dbelement["details"]["cm1_grafico_escanos"] = array(
		"add" => 0,
		"edit" => 0,
		"view" => 0,
		"delete" => 0
	);
	$dbelement["details"]["cm1_1_v_elec_muni"] = array(
		"add" => 0,
		"edit" => 0,
		"view" => 1,
		"delete" => 0
	);
	$dbelement["details"]["cm1_1_v_elec_distri"] = array(
		"add" => 0,
		"edit" => 0,
		"view" => 1,
		"delete" => 0
	);
	$dbelement["notUsedDetailTables"][] = "cm1_escanos Chart";
	$dbelement["notUsedDetailTables"][] = "cm1_grafico_escanos";


	$tdatacm1_escanos1[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm1_grafico_escanos_list", "table" => "cm1_grafico_escanos", "type" => 0);
	$dbelement["cellName"] = "cell_1_0";

					$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "cm1_1_Convocatoria";

	$tdatacm1_escanos1[".dashElements"][] = $dbelement;

$tdatacm1_escanos1[".shortTableName"] = "cm1_escanos1";
$tdatacm1_escanos1[".entityType"] = 4;



include_once(getabspath("include/cm1_escanos1_events.php"));
$tableEvents["cm1_escanos1"] = new eventclass_cm1_escanos1;
$tdatacm1_escanos1[".hasEvents"] = true;


$tdatacm1_escanos1[".tableType"] = "dashboard";



$tdatacm1_escanos1[".addPageEvents"] = false;

$tables_data["cm1_escanos1"]=&$tdatacm1_escanos1;
$field_labels["cm1_escanos1"] = &$fieldLabelscm1_escanos1;
$page_titles["cm1_escanos1"] = &$pageTitlescm1_escanos1;

?>