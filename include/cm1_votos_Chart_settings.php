<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm1_votos_Chart = array();
	$tdatacm1_votos_Chart[".ShortName"] = "cm1_votos_Chart";
	$tdatacm1_votos_Chart[".OwnerID"] = "";
	$tdatacm1_votos_Chart[".OriginalTable"] = "escanos";

//	field labels
$fieldLabelscm1_votos_Chart = array();
$fieldToolTipscm1_votos_Chart = array();
$pageTitlescm1_votos_Chart = array();
$placeHolderscm1_votos_Chart = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm1_votos_Chart["Spanish"] = array();
	$fieldToolTipscm1_votos_Chart["Spanish"] = array();
	$placeHolderscm1_votos_Chart["Spanish"] = array();
	$pageTitlescm1_votos_Chart["Spanish"] = array();
	$fieldLabelscm1_votos_Chart["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria IdConvocatoria";
	$fieldToolTipscm1_votos_Chart["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderscm1_votos_Chart["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelscm1_votos_Chart["Spanish"]["Votos"] = "Votos";
	$fieldToolTipscm1_votos_Chart["Spanish"]["Votos"] = "";
	$placeHolderscm1_votos_Chart["Spanish"]["Votos"] = "";
	$fieldLabelscm1_votos_Chart["Spanish"]["PorcVotos"] = "Porc Votos";
	$fieldToolTipscm1_votos_Chart["Spanish"]["PorcVotos"] = "";
	$placeHolderscm1_votos_Chart["Spanish"]["PorcVotos"] = "";
	$fieldLabelscm1_votos_Chart["Spanish"]["Escanos"] = "Escaños";
	$fieldToolTipscm1_votos_Chart["Spanish"]["Escanos"] = "";
	$placeHolderscm1_votos_Chart["Spanish"]["Escanos"] = "";
	$fieldLabelscm1_votos_Chart["Spanish"]["idCandidatura"] = "Id Candidatura";
	$fieldToolTipscm1_votos_Chart["Spanish"]["idCandidatura"] = "";
	$placeHolderscm1_votos_Chart["Spanish"]["idCandidatura"] = "";
	$fieldLabelscm1_votos_Chart["Spanish"]["Codigo"] = "Codigo";
	$fieldToolTipscm1_votos_Chart["Spanish"]["Codigo"] = "";
	$placeHolderscm1_votos_Chart["Spanish"]["Codigo"] = "";
	$fieldLabelscm1_votos_Chart["Spanish"]["Titulo"] = "Titulo";
	$fieldToolTipscm1_votos_Chart["Spanish"]["Titulo"] = "";
	$placeHolderscm1_votos_Chart["Spanish"]["Titulo"] = "";
	$fieldLabelscm1_votos_Chart["Spanish"]["Descripcion"] = "Descripcion";
	$fieldToolTipscm1_votos_Chart["Spanish"]["Descripcion"] = "";
	$placeHolderscm1_votos_Chart["Spanish"]["Descripcion"] = "";
	$fieldLabelscm1_votos_Chart["Spanish"]["Color"] = "Color";
	$fieldToolTipscm1_votos_Chart["Spanish"]["Color"] = "";
	$placeHolderscm1_votos_Chart["Spanish"]["Color"] = "";
	$fieldLabelscm1_votos_Chart["Spanish"]["Logo"] = "Logo";
	$fieldToolTipscm1_votos_Chart["Spanish"]["Logo"] = "";
	$placeHolderscm1_votos_Chart["Spanish"]["Logo"] = "";
	$pageTitlescm1_votos_Chart["Spanish"]["chart"] = "Candidaturas de + 35.000 Votos";
	if (count($fieldToolTipscm1_votos_Chart["Spanish"]))
		$tdatacm1_votos_Chart[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm1_votos_Chart[""] = array();
	$fieldToolTipscm1_votos_Chart[""] = array();
	$placeHolderscm1_votos_Chart[""] = array();
	$pageTitlescm1_votos_Chart[""] = array();
	if (count($fieldToolTipscm1_votos_Chart[""]))
		$tdatacm1_votos_Chart[".isUseToolTips"] = true;
}


	$tdatacm1_votos_Chart[".NCSearch"] = true;

	$tdatacm1_votos_Chart[".ChartRefreshTime"] = 0;


$tdatacm1_votos_Chart[".shortTableName"] = "cm1_votos_Chart";
$tdatacm1_votos_Chart[".nSecOptions"] = 0;
$tdatacm1_votos_Chart[".recsPerRowPrint"] = 1;
$tdatacm1_votos_Chart[".mainTableOwnerID"] = "";
$tdatacm1_votos_Chart[".moveNext"] = 1;
$tdatacm1_votos_Chart[".entityType"] = 3;

$tdatacm1_votos_Chart[".strOriginalTableName"] = "escanos";

	



$tdatacm1_votos_Chart[".showAddInPopup"] = false;

$tdatacm1_votos_Chart[".showEditInPopup"] = false;

$tdatacm1_votos_Chart[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacm1_votos_Chart[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm1_votos_Chart[".fieldsForRegister"] = array();

$tdatacm1_votos_Chart[".listAjax"] = false;

	$tdatacm1_votos_Chart[".audit"] = false;

	$tdatacm1_votos_Chart[".locking"] = false;


$tdatacm1_votos_Chart[".add"] = true;
$tdatacm1_votos_Chart[".afterAddAction"] = 1;
$tdatacm1_votos_Chart[".closePopupAfterAdd"] = 1;
$tdatacm1_votos_Chart[".afterAddActionDetTable"] = "";

$tdatacm1_votos_Chart[".list"] = true;



$tdatacm1_votos_Chart[".reorderRecordsByHeader"] = true;








$tdatacm1_votos_Chart[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm1_votos_Chart[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm1_votos_Chart[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm1_votos_Chart[".searchSaving"] = false;
//

$tdatacm1_votos_Chart[".showSearchPanel"] = true;
		$tdatacm1_votos_Chart[".flexibleSearch"] = true;

$tdatacm1_votos_Chart[".isUseAjaxSuggest"] = true;






$tdatacm1_votos_Chart[".ajaxCodeSnippetAdded"] = false;

$tdatacm1_votos_Chart[".buttonsAdded"] = false;

$tdatacm1_votos_Chart[".addPageEvents"] = true;

// use timepicker for search panel
$tdatacm1_votos_Chart[".isUseTimeForSearch"] = false;



$tdatacm1_votos_Chart[".badgeColor"] = "db7093";


$tdatacm1_votos_Chart[".allSearchFields"] = array();
$tdatacm1_votos_Chart[".filterFields"] = array();
$tdatacm1_votos_Chart[".requiredSearchFields"] = array();

$tdatacm1_votos_Chart[".allSearchFields"][] = "Convocatoria_idConvocatoria";
	$tdatacm1_votos_Chart[".allSearchFields"][] = "idCandidatura";
	$tdatacm1_votos_Chart[".allSearchFields"][] = "Codigo";
	$tdatacm1_votos_Chart[".allSearchFields"][] = "Titulo";
	$tdatacm1_votos_Chart[".allSearchFields"][] = "Descripcion";
	$tdatacm1_votos_Chart[".allSearchFields"][] = "Color";
	$tdatacm1_votos_Chart[".allSearchFields"][] = "Logo";
	$tdatacm1_votos_Chart[".allSearchFields"][] = "Escanos";
	$tdatacm1_votos_Chart[".allSearchFields"][] = "Votos";
	$tdatacm1_votos_Chart[".allSearchFields"][] = "PorcVotos";
	

$tdatacm1_votos_Chart[".googleLikeFields"] = array();
$tdatacm1_votos_Chart[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".googleLikeFields"][] = "idCandidatura";
$tdatacm1_votos_Chart[".googleLikeFields"][] = "Codigo";
$tdatacm1_votos_Chart[".googleLikeFields"][] = "Titulo";
$tdatacm1_votos_Chart[".googleLikeFields"][] = "Descripcion";
$tdatacm1_votos_Chart[".googleLikeFields"][] = "Color";
$tdatacm1_votos_Chart[".googleLikeFields"][] = "Logo";
$tdatacm1_votos_Chart[".googleLikeFields"][] = "Escanos";
$tdatacm1_votos_Chart[".googleLikeFields"][] = "Votos";
$tdatacm1_votos_Chart[".googleLikeFields"][] = "PorcVotos";


$tdatacm1_votos_Chart[".advSearchFields"] = array();
$tdatacm1_votos_Chart[".advSearchFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".advSearchFields"][] = "idCandidatura";
$tdatacm1_votos_Chart[".advSearchFields"][] = "Codigo";
$tdatacm1_votos_Chart[".advSearchFields"][] = "Titulo";
$tdatacm1_votos_Chart[".advSearchFields"][] = "Descripcion";
$tdatacm1_votos_Chart[".advSearchFields"][] = "Color";
$tdatacm1_votos_Chart[".advSearchFields"][] = "Logo";
$tdatacm1_votos_Chart[".advSearchFields"][] = "Escanos";
$tdatacm1_votos_Chart[".advSearchFields"][] = "Votos";
$tdatacm1_votos_Chart[".advSearchFields"][] = "PorcVotos";

$tdatacm1_votos_Chart[".tableType"] = "chart";

$tdatacm1_votos_Chart[".printerPageOrientation"] = 0;
$tdatacm1_votos_Chart[".nPrinterPageScale"] = 100;

$tdatacm1_votos_Chart[".nPrinterSplitRecords"] = 40;

$tdatacm1_votos_Chart[".nPrinterPDFSplitRecords"] = 40;



$tdatacm1_votos_Chart[".geocodingEnabled"] = false;



// chart settings
$tdatacm1_votos_Chart[".chartType"] = "2DBar";
// end of chart settings


$tdatacm1_votos_Chart[".listGridLayout"] = 3;





// view page pdf

// print page pdf



$tstrOrderBy = "ORDER BY e.Votos DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm1_votos_Chart[".strOrderBy"] = $tstrOrderBy;

$tdatacm1_votos_Chart[".orderindexes"] = array();
	$tdatacm1_votos_Chart[".orderindexes"][] = array(9, (0 ? "ASC" : "DESC"), "e.Votos");


$tdatacm1_votos_Chart[".sqlHead"] = "SELECT e.Convocatoria_idConvocatoria,  c.idCandidatura,  c.Codigo,  c.Titulo,  c.Descripcion,  c.Color,  c.Logo,  e.Escanos,  e.Votos,  e.PorcVotos";
$tdatacm1_votos_Chart[".sqlFrom"] = "FROM escanos AS e  INNER JOIN candidatura AS c ON e.Candidatura_idCandidatura = c.idCandidatura";
$tdatacm1_votos_Chart[".sqlWhereExpr"] = "e.Votos > 35000";
$tdatacm1_votos_Chart[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm1_votos_Chart[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm1_votos_Chart[".arrGroupsPerPage"] = $arrGPP;

$tdatacm1_votos_Chart[".highlightSearchResults"] = true;

$tableKeyscm1_votos_Chart = array();
$tdatacm1_votos_Chart[".Keys"] = $tableKeyscm1_votos_Chart;

$tdatacm1_votos_Chart[".listFields"] = array();
$tdatacm1_votos_Chart[".listFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".listFields"][] = "idCandidatura";
$tdatacm1_votos_Chart[".listFields"][] = "Codigo";
$tdatacm1_votos_Chart[".listFields"][] = "Titulo";
$tdatacm1_votos_Chart[".listFields"][] = "Descripcion";
$tdatacm1_votos_Chart[".listFields"][] = "Color";
$tdatacm1_votos_Chart[".listFields"][] = "Logo";
$tdatacm1_votos_Chart[".listFields"][] = "Escanos";
$tdatacm1_votos_Chart[".listFields"][] = "Votos";
$tdatacm1_votos_Chart[".listFields"][] = "PorcVotos";

$tdatacm1_votos_Chart[".hideMobileList"] = array();


$tdatacm1_votos_Chart[".viewFields"] = array();
$tdatacm1_votos_Chart[".viewFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".viewFields"][] = "idCandidatura";
$tdatacm1_votos_Chart[".viewFields"][] = "Codigo";
$tdatacm1_votos_Chart[".viewFields"][] = "Titulo";
$tdatacm1_votos_Chart[".viewFields"][] = "Descripcion";
$tdatacm1_votos_Chart[".viewFields"][] = "Color";
$tdatacm1_votos_Chart[".viewFields"][] = "Logo";
$tdatacm1_votos_Chart[".viewFields"][] = "Escanos";
$tdatacm1_votos_Chart[".viewFields"][] = "Votos";
$tdatacm1_votos_Chart[".viewFields"][] = "PorcVotos";

$tdatacm1_votos_Chart[".addFields"] = array();
$tdatacm1_votos_Chart[".addFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".addFields"][] = "Escanos";
$tdatacm1_votos_Chart[".addFields"][] = "Votos";
$tdatacm1_votos_Chart[".addFields"][] = "PorcVotos";

$tdatacm1_votos_Chart[".masterListFields"] = array();
$tdatacm1_votos_Chart[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".masterListFields"][] = "idCandidatura";
$tdatacm1_votos_Chart[".masterListFields"][] = "Codigo";
$tdatacm1_votos_Chart[".masterListFields"][] = "Titulo";
$tdatacm1_votos_Chart[".masterListFields"][] = "Descripcion";
$tdatacm1_votos_Chart[".masterListFields"][] = "Color";
$tdatacm1_votos_Chart[".masterListFields"][] = "Logo";
$tdatacm1_votos_Chart[".masterListFields"][] = "Escanos";
$tdatacm1_votos_Chart[".masterListFields"][] = "Votos";
$tdatacm1_votos_Chart[".masterListFields"][] = "PorcVotos";

$tdatacm1_votos_Chart[".inlineAddFields"] = array();
$tdatacm1_votos_Chart[".inlineAddFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".inlineAddFields"][] = "Escanos";
$tdatacm1_votos_Chart[".inlineAddFields"][] = "Votos";
$tdatacm1_votos_Chart[".inlineAddFields"][] = "PorcVotos";

$tdatacm1_votos_Chart[".editFields"] = array();
$tdatacm1_votos_Chart[".editFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".editFields"][] = "Escanos";
$tdatacm1_votos_Chart[".editFields"][] = "Votos";
$tdatacm1_votos_Chart[".editFields"][] = "PorcVotos";

$tdatacm1_votos_Chart[".inlineEditFields"] = array();
$tdatacm1_votos_Chart[".inlineEditFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".inlineEditFields"][] = "Escanos";
$tdatacm1_votos_Chart[".inlineEditFields"][] = "Votos";
$tdatacm1_votos_Chart[".inlineEditFields"][] = "PorcVotos";

$tdatacm1_votos_Chart[".updateSelectedFields"] = array();
$tdatacm1_votos_Chart[".updateSelectedFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".updateSelectedFields"][] = "Escanos";
$tdatacm1_votos_Chart[".updateSelectedFields"][] = "Votos";
$tdatacm1_votos_Chart[".updateSelectedFields"][] = "PorcVotos";


$tdatacm1_votos_Chart[".exportFields"] = array();
$tdatacm1_votos_Chart[".exportFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".exportFields"][] = "idCandidatura";
$tdatacm1_votos_Chart[".exportFields"][] = "Codigo";
$tdatacm1_votos_Chart[".exportFields"][] = "Titulo";
$tdatacm1_votos_Chart[".exportFields"][] = "Descripcion";
$tdatacm1_votos_Chart[".exportFields"][] = "Color";
$tdatacm1_votos_Chart[".exportFields"][] = "Logo";
$tdatacm1_votos_Chart[".exportFields"][] = "Escanos";
$tdatacm1_votos_Chart[".exportFields"][] = "Votos";
$tdatacm1_votos_Chart[".exportFields"][] = "PorcVotos";

$tdatacm1_votos_Chart[".importFields"] = array();
$tdatacm1_votos_Chart[".importFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".importFields"][] = "idCandidatura";
$tdatacm1_votos_Chart[".importFields"][] = "Codigo";
$tdatacm1_votos_Chart[".importFields"][] = "Titulo";
$tdatacm1_votos_Chart[".importFields"][] = "Descripcion";
$tdatacm1_votos_Chart[".importFields"][] = "Color";
$tdatacm1_votos_Chart[".importFields"][] = "Logo";
$tdatacm1_votos_Chart[".importFields"][] = "Escanos";
$tdatacm1_votos_Chart[".importFields"][] = "Votos";
$tdatacm1_votos_Chart[".importFields"][] = "PorcVotos";

$tdatacm1_votos_Chart[".printFields"] = array();
$tdatacm1_votos_Chart[".printFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_votos_Chart[".printFields"][] = "idCandidatura";
$tdatacm1_votos_Chart[".printFields"][] = "Codigo";
$tdatacm1_votos_Chart[".printFields"][] = "Titulo";
$tdatacm1_votos_Chart[".printFields"][] = "Descripcion";
$tdatacm1_votos_Chart[".printFields"][] = "Color";
$tdatacm1_votos_Chart[".printFields"][] = "Logo";
$tdatacm1_votos_Chart[".printFields"][] = "Escanos";
$tdatacm1_votos_Chart[".printFields"][] = "Votos";
$tdatacm1_votos_Chart[".printFields"][] = "PorcVotos";


//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm1_votos_Chart","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "e.Convocatoria_idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_votos_Chart["Convocatoria_idConvocatoria"] = $fdata;
//	idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "idCandidatura";
	$fdata["GoodName"] = "idCandidatura";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm1_votos_Chart","idCandidatura");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "c.idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_votos_Chart["idCandidatura"] = $fdata;
//	Codigo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Codigo";
	$fdata["GoodName"] = "Codigo";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm1_votos_Chart","Codigo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Codigo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "c.Codigo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_votos_Chart["Codigo"] = $fdata;
//	Titulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Titulo";
	$fdata["GoodName"] = "Titulo";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm1_votos_Chart","Titulo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "c.Titulo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_votos_Chart["Titulo"] = $fdata;
//	Descripcion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Descripcion";
	$fdata["GoodName"] = "Descripcion";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm1_votos_Chart","Descripcion");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Descripcion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "c.Descripcion";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_votos_Chart["Descripcion"] = $fdata;
//	Color
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Color";
	$fdata["GoodName"] = "Color";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm1_votos_Chart","Color");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Color";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "c.Color";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_votos_Chart["Color"] = $fdata;
//	Logo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Logo";
	$fdata["GoodName"] = "Logo";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm1_votos_Chart","Logo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Logo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "c.Logo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_votos_Chart["Logo"] = $fdata;
//	Escanos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "Escanos";
	$fdata["GoodName"] = "Escanos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm1_votos_Chart","Escanos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Escanos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "e.Escanos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_votos_Chart["Escanos"] = $fdata;
//	Votos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "Votos";
	$fdata["GoodName"] = "Votos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm1_votos_Chart","Votos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Votos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "e.Votos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_votos_Chart["Votos"] = $fdata;
//	PorcVotos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "PorcVotos";
	$fdata["GoodName"] = "PorcVotos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("cm1_votos_Chart","PorcVotos");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "PorcVotos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "e.PorcVotos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_votos_Chart["PorcVotos"] = $fdata;

	$tdatacm1_votos_Chart[".chartXml"] = '<chart>
		<attr value="tables">
			<attr value="0">cm1_votos Chart</attr>
		</attr>
		<attr value="chart_type">
			<attr value="type">2d_bar</attr>
		</attr>

		<attr value="parameters">';
	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="0">
			<attr value="name">Votos</attr>';
	$tdatacm1_votos_Chart[".chartXml"] .= '</attr>';
	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="1">
		<attr value="name">Codigo</attr>
	</attr>';
	$tdatacm1_votos_Chart[".chartXml"] .= '</attr>
			<attr value="appearance">';


	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="head">'.xmlencode("Votos por Candidatura").'</attr>
<attr value="foot">'.xmlencode("Candidaturas").'</attr>
<attr value="y_axis_label">'.xmlencode("Convocatoria_idConvocatoria").'</attr>


<attr value="slegend">true</attr>
<attr value="sgrid">true</attr>
<attr value="sname">true</attr>
<attr value="sval">true</attr>
<attr value="sanim">true</attr>
<attr value="sstacked">false</attr>
<attr value="slog">false</attr>
<attr value="aqua">0</attr>
<attr value="cview">0</attr>
<attr value="is3d">0</attr>
<attr value="isstacked">0</attr>
<attr value="linestyle">0</attr>
<attr value="autoupdate">0</attr>
<attr value="autoupmin">60</attr>';
$tdatacm1_votos_Chart[".chartXml"] .= '</attr>

<attr value="fields">';
	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="0">
		<attr value="name">Convocatoria_idConvocatoria</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_votos_Chart","Convocatoria_idConvocatoria")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="1">
		<attr value="name">idCandidatura</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_votos_Chart","idCandidatura")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="2">
		<attr value="name">Codigo</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_votos_Chart","Codigo")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="3">
		<attr value="name">Titulo</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_votos_Chart","Titulo")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="4">
		<attr value="name">Descripcion</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_votos_Chart","Descripcion")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="5">
		<attr value="name">Color</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_votos_Chart","Color")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="6">
		<attr value="name">Logo</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_votos_Chart","Logo")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="7">
		<attr value="name">Escanos</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_votos_Chart","Escanos")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="8">
		<attr value="name">Votos</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_votos_Chart","Votos")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_votos_Chart[".chartXml"] .= '<attr value="9">
		<attr value="name">PorcVotos</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_votos_Chart","PorcVotos")).'</attr>
		<attr value="search"></attr>
	</attr>';
$tdatacm1_votos_Chart[".chartXml"] .= '</attr>


<attr value="settings">
<attr value="name">cm1_votos Chart</attr>
<attr value="short_table_name">cm1_votos_Chart</attr>
</attr>

</chart>';

$tables_data["cm1_votos Chart"]=&$tdatacm1_votos_Chart;
$field_labels["cm1_votos_Chart"] = &$fieldLabelscm1_votos_Chart;
$fieldToolTips["cm1_votos_Chart"] = &$fieldToolTipscm1_votos_Chart;
$placeHolders["cm1_votos_Chart"] = &$placeHolderscm1_votos_Chart;
$page_titles["cm1_votos_Chart"] = &$pageTitlescm1_votos_Chart;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm1_votos Chart"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["cm1_votos Chart"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm1_votos Chart"][0] = $masterParams;
				$masterTablesData["cm1_votos Chart"][0]["masterKeys"] = array();
	$masterTablesData["cm1_votos Chart"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["cm1_votos Chart"][0]["detailKeys"] = array();
	$masterTablesData["cm1_votos Chart"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm1_votos_Chart()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "e.Convocatoria_idConvocatoria,  c.idCandidatura,  c.Codigo,  c.Titulo,  c.Descripcion,  c.Color,  c.Logo,  e.Escanos,  e.Votos,  e.PorcVotos";
$proto0["m_strFrom"] = "FROM escanos AS e  INNER JOIN candidatura AS c ON e.Candidatura_idCandidatura = c.idCandidatura";
$proto0["m_strWhere"] = "e.Votos > 35000";
$proto0["m_strOrderBy"] = "ORDER BY e.Votos DESC";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "e.Votos > 35000";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "e",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "> 35000";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "e",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto6["m_sql"] = "e.Convocatoria_idConvocatoria";
$proto6["m_srcTableName"] = "cm1_votos Chart";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "idCandidatura",
	"m_strTable" => "c",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto8["m_sql"] = "c.idCandidatura";
$proto8["m_srcTableName"] = "cm1_votos Chart";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Codigo",
	"m_strTable" => "c",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto10["m_sql"] = "c.Codigo";
$proto10["m_srcTableName"] = "cm1_votos Chart";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "c",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto12["m_sql"] = "c.Titulo";
$proto12["m_srcTableName"] = "cm1_votos Chart";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "Descripcion",
	"m_strTable" => "c",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto14["m_sql"] = "c.Descripcion";
$proto14["m_srcTableName"] = "cm1_votos Chart";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Color",
	"m_strTable" => "c",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto16["m_sql"] = "c.Color";
$proto16["m_srcTableName"] = "cm1_votos Chart";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "Logo",
	"m_strTable" => "c",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto18["m_sql"] = "c.Logo";
$proto18["m_srcTableName"] = "cm1_votos Chart";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "e",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto20["m_sql"] = "e.Escanos";
$proto20["m_srcTableName"] = "cm1_votos Chart";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "e",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto22["m_sql"] = "e.Votos";
$proto22["m_srcTableName"] = "cm1_votos Chart";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "PorcVotos",
	"m_strTable" => "e",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto24["m_sql"] = "e.PorcVotos";
$proto24["m_srcTableName"] = "cm1_votos Chart";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto26=array();
$proto26["m_link"] = "SQLL_MAIN";
			$proto27=array();
$proto27["m_strName"] = "escanos";
$proto27["m_srcTableName"] = "cm1_votos Chart";
$proto27["m_columns"] = array();
$proto27["m_columns"][] = "idEscanos";
$proto27["m_columns"][] = "Convocatoria_idConvocatoria";
$proto27["m_columns"][] = "Candidatura_idCandidatura";
$proto27["m_columns"][] = "Votos";
$proto27["m_columns"][] = "PorcVotos";
$proto27["m_columns"][] = "Escanos";
$obj = new SQLTable($proto27);

$proto26["m_table"] = $obj;
$proto26["m_sql"] = "escanos AS e";
$proto26["m_alias"] = "e";
$proto26["m_srcTableName"] = "cm1_votos Chart";
$proto28=array();
$proto28["m_sql"] = "";
$proto28["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto28["m_column"]=$obj;
$proto28["m_contained"] = array();
$proto28["m_strCase"] = "";
$proto28["m_havingmode"] = false;
$proto28["m_inBrackets"] = false;
$proto28["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto28);

$proto26["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto26);

$proto0["m_fromlist"][]=$obj;
												$proto30=array();
$proto30["m_link"] = "SQLL_INNERJOIN";
			$proto31=array();
$proto31["m_strName"] = "candidatura";
$proto31["m_srcTableName"] = "cm1_votos Chart";
$proto31["m_columns"] = array();
$proto31["m_columns"][] = "idCandidatura";
$proto31["m_columns"][] = "Codigo";
$proto31["m_columns"][] = "Titulo";
$proto31["m_columns"][] = "Descripcion";
$proto31["m_columns"][] = "Color";
$proto31["m_columns"][] = "Logo";
$obj = new SQLTable($proto31);

$proto30["m_table"] = $obj;
$proto30["m_sql"] = "INNER JOIN candidatura AS c ON e.Candidatura_idCandidatura = c.idCandidatura";
$proto30["m_alias"] = "c";
$proto30["m_srcTableName"] = "cm1_votos Chart";
$proto32=array();
$proto32["m_sql"] = "e.Candidatura_idCandidatura = c.idCandidatura";
$proto32["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "e",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto32["m_column"]=$obj;
$proto32["m_contained"] = array();
$proto32["m_strCase"] = "= c.idCandidatura";
$proto32["m_havingmode"] = false;
$proto32["m_inBrackets"] = false;
$proto32["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto32);

$proto30["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto30);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto34=array();
						$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "e",
	"m_srcTableName" => "cm1_votos Chart"
));

$proto34["m_column"]=$obj;
$proto34["m_bAsc"] = 0;
$proto34["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto34);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="cm1_votos Chart";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_cm1_votos_Chart = createSqlQuery_cm1_votos_Chart();


	
		;

										

$tdatacm1_votos_Chart[".sqlquery"] = $queryData_cm1_votos_Chart;

include_once(getabspath("include/cm1_votos_Chart_events.php"));
$tableEvents["cm1_votos Chart"] = new eventclass_cm1_votos_Chart;
$tdatacm1_votos_Chart[".hasEvents"] = true;

?>