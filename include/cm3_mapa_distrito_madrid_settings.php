<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm3_mapa_distrito_madrid = array();
	$tdatacm3_mapa_distrito_madrid[".truncateText"] = true;
	$tdatacm3_mapa_distrito_madrid[".NumberOfChars"] = 80;
	$tdatacm3_mapa_distrito_madrid[".ShortName"] = "cm3_mapa_distrito_madrid";
	$tdatacm3_mapa_distrito_madrid[".OwnerID"] = "";
	$tdatacm3_mapa_distrito_madrid[".OriginalTable"] = "candidatura";

//	field labels
$fieldLabelscm3_mapa_distrito_madrid = array();
$fieldToolTipscm3_mapa_distrito_madrid = array();
$pageTitlescm3_mapa_distrito_madrid = array();
$placeHolderscm3_mapa_distrito_madrid = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm3_mapa_distrito_madrid["Spanish"] = array();
	$fieldToolTipscm3_mapa_distrito_madrid["Spanish"] = array();
	$placeHolderscm3_mapa_distrito_madrid["Spanish"] = array();
	$pageTitlescm3_mapa_distrito_madrid["Spanish"] = array();
	$fieldLabelscm3_mapa_distrito_madrid["Spanish"]["idElecCandidatura"] = "Id Interno";
	$fieldToolTipscm3_mapa_distrito_madrid["Spanish"]["idElecCandidatura"] = "";
	$placeHolderscm3_mapa_distrito_madrid["Spanish"]["idElecCandidatura"] = "";
	$fieldLabelscm3_mapa_distrito_madrid["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldToolTipscm3_mapa_distrito_madrid["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderscm3_mapa_distrito_madrid["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelscm3_mapa_distrito_madrid["Spanish"]["Descripcion"] = "Descripcion";
	$fieldToolTipscm3_mapa_distrito_madrid["Spanish"]["Descripcion"] = "";
	$placeHolderscm3_mapa_distrito_madrid["Spanish"]["Descripcion"] = "";
	$fieldLabelscm3_mapa_distrito_madrid["Spanish"]["idCandidatura"] = "Id Candidatura";
	$fieldToolTipscm3_mapa_distrito_madrid["Spanish"]["idCandidatura"] = "";
	$placeHolderscm3_mapa_distrito_madrid["Spanish"]["idCandidatura"] = "";
	$fieldLabelscm3_mapa_distrito_madrid["Spanish"]["Codigo"] = "Codigo";
	$fieldToolTipscm3_mapa_distrito_madrid["Spanish"]["Codigo"] = "";
	$placeHolderscm3_mapa_distrito_madrid["Spanish"]["Codigo"] = "";
	$fieldLabelscm3_mapa_distrito_madrid["Spanish"]["Titulo"] = "Titulo";
	$fieldToolTipscm3_mapa_distrito_madrid["Spanish"]["Titulo"] = "";
	$placeHolderscm3_mapa_distrito_madrid["Spanish"]["Titulo"] = "";
	$fieldLabelscm3_mapa_distrito_madrid["Spanish"]["Color"] = "Color";
	$fieldToolTipscm3_mapa_distrito_madrid["Spanish"]["Color"] = "";
	$placeHolderscm3_mapa_distrito_madrid["Spanish"]["Color"] = "";
	$pageTitlescm3_mapa_distrito_madrid["Spanish"]["list"] = "Distribución del voto por distritos de Madrid y candidatura";
	if (count($fieldToolTipscm3_mapa_distrito_madrid["Spanish"]))
		$tdatacm3_mapa_distrito_madrid[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm3_mapa_distrito_madrid[""] = array();
	$fieldToolTipscm3_mapa_distrito_madrid[""] = array();
	$placeHolderscm3_mapa_distrito_madrid[""] = array();
	$pageTitlescm3_mapa_distrito_madrid[""] = array();
	if (count($fieldToolTipscm3_mapa_distrito_madrid[""]))
		$tdatacm3_mapa_distrito_madrid[".isUseToolTips"] = true;
}


	$tdatacm3_mapa_distrito_madrid[".NCSearch"] = true;



$tdatacm3_mapa_distrito_madrid[".shortTableName"] = "cm3_mapa_distrito_madrid";
$tdatacm3_mapa_distrito_madrid[".nSecOptions"] = 0;
$tdatacm3_mapa_distrito_madrid[".recsPerRowPrint"] = 1;
$tdatacm3_mapa_distrito_madrid[".mainTableOwnerID"] = "";
$tdatacm3_mapa_distrito_madrid[".moveNext"] = 1;
$tdatacm3_mapa_distrito_madrid[".entityType"] = 1;

$tdatacm3_mapa_distrito_madrid[".strOriginalTableName"] = "candidatura";

	



$tdatacm3_mapa_distrito_madrid[".showAddInPopup"] = true;

$tdatacm3_mapa_distrito_madrid[".showEditInPopup"] = true;

$tdatacm3_mapa_distrito_madrid[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["add"] = "view_bootstrap";
			;
$popupPagesLayoutNames["edit"] = "view_bootstrap";
			;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdatacm3_mapa_distrito_madrid[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm3_mapa_distrito_madrid[".fieldsForRegister"] = array();

$tdatacm3_mapa_distrito_madrid[".listAjax"] = false;

	$tdatacm3_mapa_distrito_madrid[".audit"] = false;

	$tdatacm3_mapa_distrito_madrid[".locking"] = false;



$tdatacm3_mapa_distrito_madrid[".list"] = true;











$tdatacm3_mapa_distrito_madrid[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm3_mapa_distrito_madrid[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm3_mapa_distrito_madrid[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm3_mapa_distrito_madrid[".searchSaving"] = false;
//

	$tdatacm3_mapa_distrito_madrid[".showSearchPanel"] = false;

$tdatacm3_mapa_distrito_madrid[".isUseAjaxSuggest"] = true;

$tdatacm3_mapa_distrito_madrid[".rowHighlite"] = true;





$tdatacm3_mapa_distrito_madrid[".ajaxCodeSnippetAdded"] = false;

$tdatacm3_mapa_distrito_madrid[".buttonsAdded"] = false;

$tdatacm3_mapa_distrito_madrid[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm3_mapa_distrito_madrid[".isUseTimeForSearch"] = false;



$tdatacm3_mapa_distrito_madrid[".badgeColor"] = "5f9ea0";


$tdatacm3_mapa_distrito_madrid[".allSearchFields"] = array();
$tdatacm3_mapa_distrito_madrid[".filterFields"] = array();
$tdatacm3_mapa_distrito_madrid[".requiredSearchFields"] = array();



$tdatacm3_mapa_distrito_madrid[".googleLikeFields"] = array();
$tdatacm3_mapa_distrito_madrid[".googleLikeFields"][] = "idElecCandidatura";
$tdatacm3_mapa_distrito_madrid[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdatacm3_mapa_distrito_madrid[".googleLikeFields"][] = "Descripcion";
$tdatacm3_mapa_distrito_madrid[".googleLikeFields"][] = "idCandidatura";
$tdatacm3_mapa_distrito_madrid[".googleLikeFields"][] = "Codigo";
$tdatacm3_mapa_distrito_madrid[".googleLikeFields"][] = "Titulo";
$tdatacm3_mapa_distrito_madrid[".googleLikeFields"][] = "Color";



$tdatacm3_mapa_distrito_madrid[".tableType"] = "list";

$tdatacm3_mapa_distrito_madrid[".printerPageOrientation"] = 0;
$tdatacm3_mapa_distrito_madrid[".nPrinterPageScale"] = 100;

$tdatacm3_mapa_distrito_madrid[".nPrinterSplitRecords"] = 40;

$tdatacm3_mapa_distrito_madrid[".nPrinterPDFSplitRecords"] = 40;



$tdatacm3_mapa_distrito_madrid[".geocodingEnabled"] = false;





$tdatacm3_mapa_distrito_madrid[".listGridLayout"] = 2;





// view page pdf

// print page pdf


$tdatacm3_mapa_distrito_madrid[".pageSize"] = 1;

$tdatacm3_mapa_distrito_madrid[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm3_mapa_distrito_madrid[".strOrderBy"] = $tstrOrderBy;

$tdatacm3_mapa_distrito_madrid[".orderindexes"] = array();

$tdatacm3_mapa_distrito_madrid[".sqlHead"] = "SELECT can.`idElecCandidatura`,  can.`Convocatoria_idConvocatoria`,  co.`Descripcion`,  ca.`idCandidatura`,  ca.`Codigo`,  ca.`Titulo`,  ca.`Color`";
$tdatacm3_mapa_distrito_madrid[".sqlFrom"] = "FROM eleccandidatura can  join (SELECT  distinct elem.`Convocatoria_idConvocatoria` FROM elecmunicandi elem) ele   on (can.`Convocatoria_idConvocatoria` = ele.`Convocatoria_idConvocatoria` )  join candidatura ca on (ca.`idCandidatura` = can.`Candidatura_idCandidatura`)  join convocatoria co on (can.`Convocatoria_idConvocatoria` = co.`idConvocatoria`)";
$tdatacm3_mapa_distrito_madrid[".sqlWhereExpr"] = "";
$tdatacm3_mapa_distrito_madrid[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm3_mapa_distrito_madrid[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm3_mapa_distrito_madrid[".arrGroupsPerPage"] = $arrGPP;

$tdatacm3_mapa_distrito_madrid[".highlightSearchResults"] = true;

$tableKeyscm3_mapa_distrito_madrid = array();
$tdatacm3_mapa_distrito_madrid[".Keys"] = $tableKeyscm3_mapa_distrito_madrid;

$tdatacm3_mapa_distrito_madrid[".listFields"] = array();
$tdatacm3_mapa_distrito_madrid[".listFields"][] = "Color";

$tdatacm3_mapa_distrito_madrid[".hideMobileList"] = array();


$tdatacm3_mapa_distrito_madrid[".viewFields"] = array();

$tdatacm3_mapa_distrito_madrid[".addFields"] = array();

$tdatacm3_mapa_distrito_madrid[".masterListFields"] = array();
$tdatacm3_mapa_distrito_madrid[".masterListFields"][] = "idElecCandidatura";
$tdatacm3_mapa_distrito_madrid[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdatacm3_mapa_distrito_madrid[".masterListFields"][] = "Descripcion";
$tdatacm3_mapa_distrito_madrid[".masterListFields"][] = "idCandidatura";
$tdatacm3_mapa_distrito_madrid[".masterListFields"][] = "Codigo";
$tdatacm3_mapa_distrito_madrid[".masterListFields"][] = "Titulo";
$tdatacm3_mapa_distrito_madrid[".masterListFields"][] = "Color";

$tdatacm3_mapa_distrito_madrid[".inlineAddFields"] = array();

$tdatacm3_mapa_distrito_madrid[".editFields"] = array();

$tdatacm3_mapa_distrito_madrid[".inlineEditFields"] = array();

$tdatacm3_mapa_distrito_madrid[".updateSelectedFields"] = array();


$tdatacm3_mapa_distrito_madrid[".exportFields"] = array();

$tdatacm3_mapa_distrito_madrid[".importFields"] = array();

$tdatacm3_mapa_distrito_madrid[".printFields"] = array();


//	idElecCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idElecCandidatura";
	$fdata["GoodName"] = "idElecCandidatura";
	$fdata["ownerTable"] = "eleccandidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_distrito_madrid","idElecCandidatura");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idElecCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "can.`idElecCandidatura`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_distrito_madrid["idElecCandidatura"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "eleccandidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_distrito_madrid","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "can.`Convocatoria_idConvocatoria`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_distrito_madrid["Convocatoria_idConvocatoria"] = $fdata;
//	Descripcion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Descripcion";
	$fdata["GoodName"] = "Descripcion";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_distrito_madrid","Descripcion");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Descripcion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "co.`Descripcion`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_distrito_madrid["Descripcion"] = $fdata;
//	idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "idCandidatura";
	$fdata["GoodName"] = "idCandidatura";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_distrito_madrid","idCandidatura");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ca.`idCandidatura`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_distrito_madrid["idCandidatura"] = $fdata;
//	Codigo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Codigo";
	$fdata["GoodName"] = "Codigo";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_distrito_madrid","Codigo");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Codigo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ca.`Codigo`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_distrito_madrid["Codigo"] = $fdata;
//	Titulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Titulo";
	$fdata["GoodName"] = "Titulo";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_distrito_madrid","Titulo");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ca.`Titulo`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_distrito_madrid["Titulo"] = $fdata;
//	Color
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Color";
	$fdata["GoodName"] = "Color";
	$fdata["ownerTable"] = "candidatura";
	$fdata["Label"] = GetFieldLabel("cm3_mapa_distrito_madrid","Color");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Color";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ca.`Color`";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacm3_mapa_distrito_madrid["Color"] = $fdata;


$tables_data["cm3_mapa_distrito_madrid"]=&$tdatacm3_mapa_distrito_madrid;
$field_labels["cm3_mapa_distrito_madrid"] = &$fieldLabelscm3_mapa_distrito_madrid;
$fieldToolTips["cm3_mapa_distrito_madrid"] = &$fieldToolTipscm3_mapa_distrito_madrid;
$placeHolders["cm3_mapa_distrito_madrid"] = &$placeHolderscm3_mapa_distrito_madrid;
$page_titles["cm3_mapa_distrito_madrid"] = &$pageTitlescm3_mapa_distrito_madrid;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm3_mapa_distrito_madrid"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["cm3_mapa_distrito_madrid"] = array();


	
				$strOriginalDetailsTable="v_candidatura_mapa";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="v_candidatura_mapa";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "v_candidatura_mapa";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
					
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 0;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm3_mapa_distrito_madrid"][0] = $masterParams;
				$masterTablesData["cm3_mapa_distrito_madrid"][0]["masterKeys"] = array();
	$masterTablesData["cm3_mapa_distrito_madrid"][0]["masterKeys"][]="idElecCandidatura";
				$masterTablesData["cm3_mapa_distrito_madrid"][0]["detailKeys"] = array();
	$masterTablesData["cm3_mapa_distrito_madrid"][0]["detailKeys"][]="idElecCandidatura";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm3_mapa_distrito_madrid()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "can.`idElecCandidatura`,  can.`Convocatoria_idConvocatoria`,  co.`Descripcion`,  ca.`idCandidatura`,  ca.`Codigo`,  ca.`Titulo`,  ca.`Color`";
$proto3["m_strFrom"] = "FROM eleccandidatura can  join (SELECT  distinct elem.`Convocatoria_idConvocatoria` FROM elecmunicandi elem) ele   on (can.`Convocatoria_idConvocatoria` = ele.`Convocatoria_idConvocatoria` )  join candidatura ca on (ca.`idCandidatura` = can.`Candidatura_idCandidatura`)  join convocatoria co on (can.`Convocatoria_idConvocatoria` = co.`idConvocatoria`)";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idElecCandidatura",
	"m_strTable" => "can",
	"m_srcTableName" => "cm3_mapa_distrito_madrid"
));

$proto9["m_sql"] = "can.`idElecCandidatura`";
$proto9["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "can",
	"m_srcTableName" => "cm3_mapa_distrito_madrid"
));

$proto11["m_sql"] = "can.`Convocatoria_idConvocatoria`";
$proto11["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "Descripcion",
	"m_strTable" => "co",
	"m_srcTableName" => "cm3_mapa_distrito_madrid"
));

$proto13["m_sql"] = "co.`Descripcion`";
$proto13["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "idCandidatura",
	"m_strTable" => "ca",
	"m_srcTableName" => "cm3_mapa_distrito_madrid"
));

$proto15["m_sql"] = "ca.`idCandidatura`";
$proto15["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "Codigo",
	"m_strTable" => "ca",
	"m_srcTableName" => "cm3_mapa_distrito_madrid"
));

$proto17["m_sql"] = "ca.`Codigo`";
$proto17["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "ca",
	"m_srcTableName" => "cm3_mapa_distrito_madrid"
));

$proto19["m_sql"] = "ca.`Titulo`";
$proto19["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto3["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "Color",
	"m_strTable" => "ca",
	"m_srcTableName" => "cm3_mapa_distrito_madrid"
));

$proto21["m_sql"] = "ca.`Color`";
$proto21["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto23=array();
$proto23["m_link"] = "SQLL_MAIN";
			$proto24=array();
$proto24["m_strName"] = "eleccandidatura";
$proto24["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto24["m_columns"] = array();
$proto24["m_columns"][] = "idElecCandidatura";
$proto24["m_columns"][] = "Convocatoria_idConvocatoria";
$proto24["m_columns"][] = "Orden";
$proto24["m_columns"][] = "Candidatura_idCandidatura";
$obj = new SQLTable($proto24);

$proto23["m_table"] = $obj;
$proto23["m_sql"] = "eleccandidatura can";
$proto23["m_alias"] = "can";
$proto23["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto25=array();
$proto25["m_sql"] = "";
$proto25["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto25["m_column"]=$obj;
$proto25["m_contained"] = array();
$proto25["m_strCase"] = "";
$proto25["m_havingmode"] = false;
$proto25["m_inBrackets"] = false;
$proto25["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto25);

$proto23["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto23);

$proto3["m_fromlist"][]=$obj;
												$proto27=array();
$proto27["m_link"] = "SQLL_INNERJOIN";
			$proto28=array();
$proto28["m_strHead"] = "SELECT  distinct";
$proto28["m_strFieldList"] = "elem.`Convocatoria_idConvocatoria`";
$proto28["m_strFrom"] = "FROM elecmunicandi elem";
$proto28["m_strWhere"] = "";
$proto28["m_strOrderBy"] = "";
	
		;
			$proto28["cipherer"] = null;
$proto30=array();
$proto30["m_sql"] = "";
$proto30["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto30["m_column"]=$obj;
$proto30["m_contained"] = array();
$proto30["m_strCase"] = "";
$proto30["m_havingmode"] = false;
$proto30["m_inBrackets"] = false;
$proto30["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto30);

$proto28["m_where"] = $obj;
$proto32=array();
$proto32["m_sql"] = "";
$proto32["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto32["m_column"]=$obj;
$proto32["m_contained"] = array();
$proto32["m_strCase"] = "";
$proto32["m_havingmode"] = false;
$proto32["m_inBrackets"] = false;
$proto32["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto32);

$proto28["m_having"] = $obj;
$proto28["m_fieldlist"] = array();
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "elem",
	"m_srcTableName" => "cm3_mapa_distrito_madrid"
));

$proto34["m_sql"] = "elem.`Convocatoria_idConvocatoria`";
$proto34["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto28["m_fieldlist"][]=$obj;
$proto28["m_fromlist"] = array();
												$proto36=array();
$proto36["m_link"] = "SQLL_MAIN";
			$proto37=array();
$proto37["m_strName"] = "elecmunicandi";
$proto37["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto37["m_columns"] = array();
$proto37["m_columns"][] = "idElecMuniCandi";
$proto37["m_columns"][] = "Convocatoria_idConvocatoria";
$proto37["m_columns"][] = "Candidatura_idCandidatura";
$proto37["m_columns"][] = "Municipio";
$proto37["m_columns"][] = "Votos";
$proto37["m_columns"][] = "PorcVotos";
$obj = new SQLTable($proto37);

$proto36["m_table"] = $obj;
$proto36["m_sql"] = "elecmunicandi elem";
$proto36["m_alias"] = "elem";
$proto36["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto38=array();
$proto38["m_sql"] = "";
$proto38["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto38["m_column"]=$obj;
$proto38["m_contained"] = array();
$proto38["m_strCase"] = "";
$proto38["m_havingmode"] = false;
$proto38["m_inBrackets"] = false;
$proto38["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto38);

$proto36["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto36);

$proto28["m_fromlist"][]=$obj;
$proto28["m_groupby"] = array();
$proto28["m_orderby"] = array();
$proto28["m_srcTableName"]="cm3_mapa_distrito_madrid";		
$obj = new SQLQuery($proto28);

$proto27["m_table"] = $obj;
$proto27["m_sql"] = "join (SELECT  distinct elem.`Convocatoria_idConvocatoria` FROM elecmunicandi elem) ele   on (can.`Convocatoria_idConvocatoria` = ele.`Convocatoria_idConvocatoria` )";
$proto27["m_alias"] = "ele";
$proto27["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto40=array();
$proto40["m_sql"] = "can.`Convocatoria_idConvocatoria` = ele.`Convocatoria_idConvocatoria` ";
$proto40["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "can",
	"m_srcTableName" => "cm3_mapa_distrito_madrid"
));

$proto40["m_column"]=$obj;
$proto40["m_contained"] = array();
$proto40["m_strCase"] = "= ele.`Convocatoria_idConvocatoria`";
$proto40["m_havingmode"] = false;
$proto40["m_inBrackets"] = true;
$proto40["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto40);

$proto27["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto27);

$proto3["m_fromlist"][]=$obj;
												$proto42=array();
$proto42["m_link"] = "SQLL_INNERJOIN";
			$proto43=array();
$proto43["m_strName"] = "candidatura";
$proto43["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto43["m_columns"] = array();
$proto43["m_columns"][] = "idCandidatura";
$proto43["m_columns"][] = "Codigo";
$proto43["m_columns"][] = "Titulo";
$proto43["m_columns"][] = "Descripcion";
$proto43["m_columns"][] = "Color";
$proto43["m_columns"][] = "Logo";
$obj = new SQLTable($proto43);

$proto42["m_table"] = $obj;
$proto42["m_sql"] = "join candidatura ca on (ca.`idCandidatura` = can.`Candidatura_idCandidatura`)";
$proto42["m_alias"] = "ca";
$proto42["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto44=array();
$proto44["m_sql"] = "ca.`idCandidatura` = can.`Candidatura_idCandidatura`";
$proto44["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "idCandidatura",
	"m_strTable" => "ca",
	"m_srcTableName" => "cm3_mapa_distrito_madrid"
));

$proto44["m_column"]=$obj;
$proto44["m_contained"] = array();
$proto44["m_strCase"] = "= can.`Candidatura_idCandidatura`";
$proto44["m_havingmode"] = false;
$proto44["m_inBrackets"] = true;
$proto44["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto44);

$proto42["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto42);

$proto3["m_fromlist"][]=$obj;
												$proto46=array();
$proto46["m_link"] = "SQLL_INNERJOIN";
			$proto47=array();
$proto47["m_strName"] = "convocatoria";
$proto47["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto47["m_columns"] = array();
$proto47["m_columns"][] = "idConvocatoria";
$proto47["m_columns"][] = "Orden";
$proto47["m_columns"][] = "EsAsamblea";
$proto47["m_columns"][] = "Titulo";
$proto47["m_columns"][] = "Descripcion";
$obj = new SQLTable($proto47);

$proto46["m_table"] = $obj;
$proto46["m_sql"] = "join convocatoria co on (can.`Convocatoria_idConvocatoria` = co.`idConvocatoria`)";
$proto46["m_alias"] = "co";
$proto46["m_srcTableName"] = "cm3_mapa_distrito_madrid";
$proto48=array();
$proto48["m_sql"] = "can.`Convocatoria_idConvocatoria` = co.`idConvocatoria`";
$proto48["m_uniontype"] = "SQLL_UNKNOWN";
						$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "can",
	"m_srcTableName" => "cm3_mapa_distrito_madrid"
));

$proto48["m_column"]=$obj;
$proto48["m_contained"] = array();
$proto48["m_strCase"] = "= co.`idConvocatoria`";
$proto48["m_havingmode"] = false;
$proto48["m_inBrackets"] = true;
$proto48["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto48);

$proto46["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto46);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
$proto3["m_srcTableName"]="cm3_mapa_distrito_madrid";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_cm3_mapa_distrito_madrid = createSqlQuery_cm3_mapa_distrito_madrid();


	
		;

							

$tdatacm3_mapa_distrito_madrid[".sqlquery"] = $queryData_cm3_mapa_distrito_madrid;

$tableEvents["cm3_mapa_distrito_madrid"] = new eventsBase;
$tdatacm3_mapa_distrito_madrid[".hasEvents"] = false;

?>