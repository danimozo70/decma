<?php
require_once(getabspath("classes/cipherer.php"));



$tdatacm4_tematico = array();
$tdatacm4_tematico[".ShortName"] = "cm4_tematico";

//	field labels
$fieldLabelscm4_tematico = array();
$pageTitlescm4_tematico = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm4_tematico["Spanish"] = array();
	$fieldLabelscm4_tematico["Spanish"]["cm4_convocatoria_idConvocatoria"] = "Id Interno";
	$fieldLabelscm4_tematico["Spanish"]["cm4_convocatoria_Orden"] = "Orden";
	$fieldLabelscm4_tematico["Spanish"]["cm4_convocatoria_EsAsamblea"] = "Es Asamblea?";
	$fieldLabelscm4_tematico["Spanish"]["cm4_convocatoria_Titulo"] = "Título";
	$fieldLabelscm4_tematico["Spanish"]["cm4_convocatoria_Descripcion"] = "Descripción";
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm4_tematico[""] = array();
}

//	search fields
$tdatacm4_tematico[".searchFields"] = array();

// all search fields
$tdatacm4_tematico[".allSearchFields"] = array();

// good like search fields
$tdatacm4_tematico[".googleLikeFields"] = array();

$tdatacm4_tematico[".dashElements"] = array();

	$dbelement = array( "elementName" => "cm4_convocatoria_list", "table" => "cm4_convocatoria", "type" => 0);
	$dbelement["cellName"] = "cell_0_0";

					$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;


	$tdatacm4_tematico[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm4_convocatoria_details", "table" => "cm4_convocatoria", "type" => 5);
	$dbelement["cellName"] = "cell_0_1";

				$dbelement["notUsedDetailTables"] = array();
	$dbelement["initialTabDetailTable"] = "cm4_mapa_municipio";
	$dbelement["details"] = array();
	$dbelement["details"]["cm4_mapa_municipio"] = array(
		"add" => 0,
		"edit" => 0,
		"view" => 0,
		"delete" => 0
	);
	$dbelement["details"]["cm4_mapa_distrito"] = array(
		"add" => 0,
		"edit" => 0,
		"view" => 0,
		"delete" => 0
	);
	$dbelement["details"]["cm4_leyenda_municipio"] = array(
		"add" => 0,
		"edit" => 0,
		"view" => 0,
		"delete" => 0
	);
	$dbelement["notUsedDetailTables"][] = "cm4_leyenda_municipio";


	$tdatacm4_tematico[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm4_leyenda_municipio_list", "table" => "cm4_leyenda_municipio", "type" => 0);
	$dbelement["cellName"] = "cell_1_0";

					$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "cm4_convocatoria";

	$tdatacm4_tematico[".dashElements"][] = $dbelement;

$tdatacm4_tematico[".shortTableName"] = "cm4_tematico";
$tdatacm4_tematico[".entityType"] = 4;



$tableEvents["cm4_tematico"] = new eventsBase;
$tdatacm4_tematico[".hasEvents"] = false;


$tdatacm4_tematico[".tableType"] = "dashboard";



$tdatacm4_tematico[".addPageEvents"] = false;

$tables_data["cm4_tematico"]=&$tdatacm4_tematico;
$field_labels["cm4_tematico"] = &$fieldLabelscm4_tematico;
$page_titles["cm4_tematico"] = &$pageTitlescm4_tematico;

?>