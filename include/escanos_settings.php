<?php
require_once(getabspath("classes/cipherer.php"));




$tdataescanos = array();
	$tdataescanos[".truncateText"] = true;
	$tdataescanos[".NumberOfChars"] = 80;
	$tdataescanos[".ShortName"] = "escanos";
	$tdataescanos[".OwnerID"] = "";
	$tdataescanos[".OriginalTable"] = "escanos";

//	field labels
$fieldLabelsescanos = array();
$fieldToolTipsescanos = array();
$pageTitlesescanos = array();
$placeHoldersescanos = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelsescanos["Spanish"] = array();
	$fieldToolTipsescanos["Spanish"] = array();
	$placeHoldersescanos["Spanish"] = array();
	$pageTitlesescanos["Spanish"] = array();
	$fieldLabelsescanos["Spanish"]["idEscanos"] = "Id Interno";
	$fieldToolTipsescanos["Spanish"]["idEscanos"] = "";
	$placeHoldersescanos["Spanish"]["idEscanos"] = "";
	$fieldLabelsescanos["Spanish"]["Votos"] = "Votos";
	$fieldToolTipsescanos["Spanish"]["Votos"] = "";
	$placeHoldersescanos["Spanish"]["Votos"] = "";
	$fieldLabelsescanos["Spanish"]["PorcVotos"] = "%Votos";
	$fieldToolTipsescanos["Spanish"]["PorcVotos"] = "";
	$placeHoldersescanos["Spanish"]["PorcVotos"] = "";
	$fieldLabelsescanos["Spanish"]["Escanos"] = "Escaños";
	$fieldToolTipsescanos["Spanish"]["Escanos"] = "";
	$placeHoldersescanos["Spanish"]["Escanos"] = "";
	$fieldLabelsescanos["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldToolTipsescanos["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHoldersescanos["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelsescanos["Spanish"]["Candidatura_idCandidatura"] = "Candidatura";
	$fieldToolTipsescanos["Spanish"]["Candidatura_idCandidatura"] = "";
	$placeHoldersescanos["Spanish"]["Candidatura_idCandidatura"] = "";
	$fieldLabelsescanos["Spanish"]["Color"] = "Color";
	$fieldToolTipsescanos["Spanish"]["Color"] = "";
	$placeHoldersescanos["Spanish"]["Color"] = "";
	$pageTitlesescanos["Spanish"]["list"] = "<strong>{%master.Titulo}</strong>, Escaños y filtro por Candidatura";
	if (count($fieldToolTipsescanos["Spanish"]))
		$tdataescanos[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsescanos[""] = array();
	$fieldToolTipsescanos[""] = array();
	$placeHoldersescanos[""] = array();
	$pageTitlesescanos[""] = array();
	if (count($fieldToolTipsescanos[""]))
		$tdataescanos[".isUseToolTips"] = true;
}


	$tdataescanos[".NCSearch"] = true;



$tdataescanos[".shortTableName"] = "escanos";
$tdataescanos[".nSecOptions"] = 0;
$tdataescanos[".recsPerRowList"] = 1;
$tdataescanos[".recsPerRowPrint"] = 1;
$tdataescanos[".mainTableOwnerID"] = "";
$tdataescanos[".moveNext"] = 1;
$tdataescanos[".entityType"] = 0;

$tdataescanos[".strOriginalTableName"] = "escanos";

	



$tdataescanos[".showAddInPopup"] = true;

$tdataescanos[".showEditInPopup"] = true;

$tdataescanos[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["add"] = "view_bootstrap";
			;
$popupPagesLayoutNames["edit"] = "view_bootstrap";
			;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdataescanos[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataescanos[".fieldsForRegister"] = array();

$tdataescanos[".listAjax"] = false;

	$tdataescanos[".audit"] = false;

	$tdataescanos[".locking"] = false;



$tdataescanos[".list"] = true;





$tdataescanos[".exportFormatting"] = 2;
$tdataescanos[".exportDelimiter"] = ",";
		


$tdataescanos[".exportTo"] = true;



$tdataescanos[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdataescanos[".allowShowHideFields"] = true;
//

// Allow Fields Reordering in GRID
$tdataescanos[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdataescanos[".searchSaving"] = false;
//

$tdataescanos[".showSearchPanel"] = true;
		$tdataescanos[".flexibleSearch"] = true;

$tdataescanos[".isUseAjaxSuggest"] = true;

$tdataescanos[".rowHighlite"] = true;





$tdataescanos[".ajaxCodeSnippetAdded"] = false;

$tdataescanos[".buttonsAdded"] = false;

$tdataescanos[".addPageEvents"] = false;

// use timepicker for search panel
$tdataescanos[".isUseTimeForSearch"] = false;



$tdataescanos[".badgeColor"] = "e8926f";


$tdataescanos[".allSearchFields"] = array();
$tdataescanos[".filterFields"] = array();
$tdataescanos[".requiredSearchFields"] = array();

$tdataescanos[".allSearchFields"][] = "idEscanos";
	$tdataescanos[".allSearchFields"][] = "Convocatoria_idConvocatoria";
	$tdataescanos[".allSearchFields"][] = "Candidatura_idCandidatura";
	$tdataescanos[".allSearchFields"][] = "Votos";
	$tdataescanos[".allSearchFields"][] = "PorcVotos";
	$tdataescanos[".allSearchFields"][] = "Escanos";
	
$tdataescanos[".filterFields"][] = "Convocatoria_idConvocatoria";

$tdataescanos[".googleLikeFields"] = array();
$tdataescanos[".googleLikeFields"][] = "idEscanos";
$tdataescanos[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdataescanos[".googleLikeFields"][] = "Candidatura_idCandidatura";
$tdataescanos[".googleLikeFields"][] = "Votos";
$tdataescanos[".googleLikeFields"][] = "PorcVotos";
$tdataescanos[".googleLikeFields"][] = "Escanos";


$tdataescanos[".advSearchFields"] = array();
$tdataescanos[".advSearchFields"][] = "idEscanos";
$tdataescanos[".advSearchFields"][] = "Convocatoria_idConvocatoria";
$tdataescanos[".advSearchFields"][] = "Candidatura_idCandidatura";
$tdataescanos[".advSearchFields"][] = "Votos";
$tdataescanos[".advSearchFields"][] = "PorcVotos";
$tdataescanos[".advSearchFields"][] = "Escanos";

$tdataescanos[".tableType"] = "list";

$tdataescanos[".printerPageOrientation"] = 0;
$tdataescanos[".nPrinterPageScale"] = 100;

$tdataescanos[".nPrinterSplitRecords"] = 40;

$tdataescanos[".nPrinterPDFSplitRecords"] = 40;



$tdataescanos[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdataescanos[".pageSize"] = 8;

$tdataescanos[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY Escanos DESC, Votos DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataescanos[".strOrderBy"] = $tstrOrderBy;

$tdataescanos[".orderindexes"] = array();
	$tdataescanos[".orderindexes"][] = array(7, (0 ? "ASC" : "DESC"), "Escanos");

	$tdataescanos[".orderindexes"][] = array(5, (0 ? "ASC" : "DESC"), "Votos");


$tdataescanos[".sqlHead"] = "SELECT idEscanos,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Candidatura_idCandidatura AS Color,  Votos,  PorcVotos,  Escanos";
$tdataescanos[".sqlFrom"] = "FROM escanos";
$tdataescanos[".sqlWhereExpr"] = "";
$tdataescanos[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataescanos[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataescanos[".arrGroupsPerPage"] = $arrGPP;

$tdataescanos[".highlightSearchResults"] = true;

$tableKeysescanos = array();
$tableKeysescanos[] = "idEscanos";
$tdataescanos[".Keys"] = $tableKeysescanos;

$tdataescanos[".listFields"] = array();
$tdataescanos[".listFields"][] = "Candidatura_idCandidatura";
$tdataescanos[".listFields"][] = "Color";
$tdataescanos[".listFields"][] = "Escanos";
$tdataescanos[".listFields"][] = "Votos";
$tdataescanos[".listFields"][] = "PorcVotos";

$tdataescanos[".hideMobileList"] = array();


$tdataescanos[".viewFields"] = array();

$tdataescanos[".addFields"] = array();

$tdataescanos[".masterListFields"] = array();
$tdataescanos[".masterListFields"][] = "idEscanos";
$tdataescanos[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdataescanos[".masterListFields"][] = "Candidatura_idCandidatura";
$tdataescanos[".masterListFields"][] = "Color";
$tdataescanos[".masterListFields"][] = "Votos";
$tdataescanos[".masterListFields"][] = "PorcVotos";
$tdataescanos[".masterListFields"][] = "Escanos";

$tdataescanos[".inlineAddFields"] = array();

$tdataescanos[".editFields"] = array();

$tdataescanos[".inlineEditFields"] = array();

$tdataescanos[".updateSelectedFields"] = array();


$tdataescanos[".exportFields"] = array();
$tdataescanos[".exportFields"][] = "idEscanos";
$tdataescanos[".exportFields"][] = "Convocatoria_idConvocatoria";
$tdataescanos[".exportFields"][] = "Candidatura_idCandidatura";
$tdataescanos[".exportFields"][] = "Votos";
$tdataescanos[".exportFields"][] = "PorcVotos";
$tdataescanos[".exportFields"][] = "Escanos";

$tdataescanos[".importFields"] = array();

$tdataescanos[".printFields"] = array();


//	idEscanos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idEscanos";
	$fdata["GoodName"] = "idEscanos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("escanos","idEscanos");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idEscanos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idEscanos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataescanos["idEscanos"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("escanos","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 2;
		$fdata["filterTotalFields"] = "idEscanos";
		$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = true;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 0;

			
	
	
//end of Filters settings


	$tdataescanos["Convocatoria_idConvocatoria"] = $fdata;
//	Candidatura_idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Candidatura_idCandidatura";
	$fdata["GoodName"] = "Candidatura_idCandidatura";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("escanos","Candidatura_idCandidatura");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Candidatura_idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Candidatura_idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "candidatura";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idCandidatura";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "concat(Codigo,' -  ',Titulo)";
	
	

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataescanos["Candidatura_idCandidatura"] = $fdata;
//	Color
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Color";
	$fdata["GoodName"] = "Color";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("escanos","Color");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Candidatura_idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Candidatura_idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "candidatura";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idCandidatura";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Color";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataescanos["Color"] = $fdata;
//	Votos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Votos";
	$fdata["GoodName"] = "Votos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("escanos","Votos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Votos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Votos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataescanos["Votos"] = $fdata;
//	PorcVotos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "PorcVotos";
	$fdata["GoodName"] = "PorcVotos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("escanos","PorcVotos");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "PorcVotos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "PorcVotos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataescanos["PorcVotos"] = $fdata;
//	Escanos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Escanos";
	$fdata["GoodName"] = "Escanos";
	$fdata["ownerTable"] = "escanos";
	$fdata["Label"] = GetFieldLabel("escanos","Escanos");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Escanos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Escanos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataescanos["Escanos"] = $fdata;


$tables_data["escanos"]=&$tdataescanos;
$field_labels["escanos"] = &$fieldLabelsescanos;
$fieldToolTips["escanos"] = &$fieldToolTipsescanos;
$placeHolders["escanos"] = &$placeHoldersescanos;
$page_titles["escanos"] = &$pageTitlesescanos;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["escanos"] = array();
//	electos
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="electos";
		$detailsParam["dOriginalTable"] = "electos";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "electos";
	$detailsParam["dCaptionTable"] = GetTableCaption("electos");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "1";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["escanos"][$dIndex] = $detailsParam;

	
		$detailsTablesData["escanos"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["escanos"][$dIndex]["masterKeys"][]="Convocatoria_idConvocatoria";

	$detailsTablesData["escanos"][$dIndex]["masterKeys"][]="Candidatura_idCandidatura";

				$detailsTablesData["escanos"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["escanos"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";

		
	$detailsTablesData["escanos"][$dIndex]["detailKeys"][]="Candidatura_idCandidatura";

// tables which are master tables for current table (detail)
$masterTablesData["escanos"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
					
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["escanos"][0] = $masterParams;
				$masterTablesData["escanos"][0]["masterKeys"] = array();
	$masterTablesData["escanos"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["escanos"][0]["detailKeys"] = array();
	$masterTablesData["escanos"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_escanos()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "idEscanos,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Candidatura_idCandidatura AS Color,  Votos,  PorcVotos,  Escanos";
$proto3["m_strFrom"] = "FROM escanos";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "ORDER BY Escanos DESC, Votos DESC";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idEscanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "escanos"
));

$proto9["m_sql"] = "idEscanos";
$proto9["m_srcTableName"] = "escanos";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "escanos",
	"m_srcTableName" => "escanos"
));

$proto11["m_sql"] = "Convocatoria_idConvocatoria";
$proto11["m_srcTableName"] = "escanos";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "escanos",
	"m_srcTableName" => "escanos"
));

$proto13["m_sql"] = "Candidatura_idCandidatura";
$proto13["m_srcTableName"] = "escanos";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "escanos",
	"m_srcTableName" => "escanos"
));

$proto15["m_sql"] = "Candidatura_idCandidatura";
$proto15["m_srcTableName"] = "escanos";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "Color";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "escanos"
));

$proto17["m_sql"] = "Votos";
$proto17["m_srcTableName"] = "escanos";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "PorcVotos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "escanos"
));

$proto19["m_sql"] = "PorcVotos";
$proto19["m_srcTableName"] = "escanos";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto3["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "escanos"
));

$proto21["m_sql"] = "Escanos";
$proto21["m_srcTableName"] = "escanos";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto23=array();
$proto23["m_link"] = "SQLL_MAIN";
			$proto24=array();
$proto24["m_strName"] = "escanos";
$proto24["m_srcTableName"] = "escanos";
$proto24["m_columns"] = array();
$proto24["m_columns"][] = "idEscanos";
$proto24["m_columns"][] = "Convocatoria_idConvocatoria";
$proto24["m_columns"][] = "Candidatura_idCandidatura";
$proto24["m_columns"][] = "Votos";
$proto24["m_columns"][] = "PorcVotos";
$proto24["m_columns"][] = "Escanos";
$obj = new SQLTable($proto24);

$proto23["m_table"] = $obj;
$proto23["m_sql"] = "escanos";
$proto23["m_alias"] = "";
$proto23["m_srcTableName"] = "escanos";
$proto25=array();
$proto25["m_sql"] = "";
$proto25["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto25["m_column"]=$obj;
$proto25["m_contained"] = array();
$proto25["m_strCase"] = "";
$proto25["m_havingmode"] = false;
$proto25["m_inBrackets"] = false;
$proto25["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto25);

$proto23["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto23);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto27=array();
						$obj = new SQLField(array(
	"m_strName" => "Escanos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "escanos"
));

$proto27["m_column"]=$obj;
$proto27["m_bAsc"] = 0;
$proto27["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto27);

$proto3["m_orderby"][]=$obj;					
												$proto29=array();
						$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "escanos",
	"m_srcTableName" => "escanos"
));

$proto29["m_column"]=$obj;
$proto29["m_bAsc"] = 0;
$proto29["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto29);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="escanos";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_escanos = createSqlQuery_escanos();


	
		;

							

$tdataescanos[".sqlquery"] = $queryData_escanos;

$tableEvents["escanos"] = new eventsBase;
$tdataescanos[".hasEvents"] = false;

?>