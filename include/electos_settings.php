<?php
require_once(getabspath("classes/cipherer.php"));




$tdataelectos = array();
	$tdataelectos[".truncateText"] = true;
	$tdataelectos[".NumberOfChars"] = 80;
	$tdataelectos[".ShortName"] = "electos";
	$tdataelectos[".OwnerID"] = "";
	$tdataelectos[".OriginalTable"] = "electos";

//	field labels
$fieldLabelselectos = array();
$fieldToolTipselectos = array();
$pageTitleselectos = array();
$placeHolderselectos = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelselectos["Spanish"] = array();
	$fieldToolTipselectos["Spanish"] = array();
	$placeHolderselectos["Spanish"] = array();
	$pageTitleselectos["Spanish"] = array();
	$fieldLabelselectos["Spanish"]["idElectos"] = "Id Interno";
	$fieldToolTipselectos["Spanish"]["idElectos"] = "";
	$placeHolderselectos["Spanish"]["idElectos"] = "";
	$fieldLabelselectos["Spanish"]["Orden"] = "Orden";
	$fieldToolTipselectos["Spanish"]["Orden"] = "";
	$placeHolderselectos["Spanish"]["Orden"] = "";
	$fieldLabelselectos["Spanish"]["NombreyApellidos"] = "Nombre y Apellidos";
	$fieldToolTipselectos["Spanish"]["NombreyApellidos"] = "";
	$placeHolderselectos["Spanish"]["NombreyApellidos"] = "";
	$fieldLabelselectos["Spanish"]["Coeficiente"] = "Coeficiente aplicado";
	$fieldToolTipselectos["Spanish"]["Coeficiente"] = "";
	$placeHolderselectos["Spanish"]["Coeficiente"] = "";
	$fieldLabelselectos["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldToolTipselectos["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderselectos["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelselectos["Spanish"]["Candidatura_idCandidatura"] = "Candidatura";
	$fieldToolTipselectos["Spanish"]["Candidatura_idCandidatura"] = "";
	$placeHolderselectos["Spanish"]["Candidatura_idCandidatura"] = "";
	$pageTitleselectos["Spanish"]["list"] = "<strong>{%master.Convocatoria_idConvocatoria}</strong>, Diputados";
	if (count($fieldToolTipselectos["Spanish"]))
		$tdataelectos[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelselectos[""] = array();
	$fieldToolTipselectos[""] = array();
	$placeHolderselectos[""] = array();
	$pageTitleselectos[""] = array();
	if (count($fieldToolTipselectos[""]))
		$tdataelectos[".isUseToolTips"] = true;
}


	$tdataelectos[".NCSearch"] = true;



$tdataelectos[".shortTableName"] = "electos";
$tdataelectos[".nSecOptions"] = 0;
$tdataelectos[".recsPerRowList"] = 1;
$tdataelectos[".recsPerRowPrint"] = 1;
$tdataelectos[".mainTableOwnerID"] = "";
$tdataelectos[".moveNext"] = 1;
$tdataelectos[".entityType"] = 0;

$tdataelectos[".strOriginalTableName"] = "electos";

	



$tdataelectos[".showAddInPopup"] = true;

$tdataelectos[".showEditInPopup"] = true;

$tdataelectos[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["add"] = "view_bootstrap";
			;
$popupPagesLayoutNames["edit"] = "view_bootstrap";
						
	;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdataelectos[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataelectos[".fieldsForRegister"] = array();

$tdataelectos[".listAjax"] = false;

	$tdataelectos[".audit"] = false;

	$tdataelectos[".locking"] = false;



$tdataelectos[".list"] = true;





$tdataelectos[".exportFormatting"] = 2;
$tdataelectos[".exportDelimiter"] = ",";
		
$tdataelectos[".view"] = true;


$tdataelectos[".exportTo"] = true;

$tdataelectos[".printFriendly"] = true;


$tdataelectos[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdataelectos[".allowShowHideFields"] = true;
//

// Allow Fields Reordering in GRID
$tdataelectos[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdataelectos[".searchSaving"] = false;
//

$tdataelectos[".showSearchPanel"] = true;
		$tdataelectos[".flexibleSearch"] = true;

$tdataelectos[".isUseAjaxSuggest"] = true;

$tdataelectos[".rowHighlite"] = true;





$tdataelectos[".ajaxCodeSnippetAdded"] = false;

$tdataelectos[".buttonsAdded"] = false;

$tdataelectos[".addPageEvents"] = false;

// use timepicker for search panel
$tdataelectos[".isUseTimeForSearch"] = false;



$tdataelectos[".badgeColor"] = "63e966";


$tdataelectos[".allSearchFields"] = array();
$tdataelectos[".filterFields"] = array();
$tdataelectos[".requiredSearchFields"] = array();

$tdataelectos[".allSearchFields"][] = "idElectos";
	$tdataelectos[".allSearchFields"][] = "Convocatoria_idConvocatoria";
	$tdataelectos[".allSearchFields"][] = "Candidatura_idCandidatura";
	$tdataelectos[".allSearchFields"][] = "Orden";
	$tdataelectos[".allSearchFields"][] = "NombreyApellidos";
	$tdataelectos[".allSearchFields"][] = "Coeficiente";
	
$tdataelectos[".filterFields"][] = "Convocatoria_idConvocatoria";
$tdataelectos[".filterFields"][] = "Candidatura_idCandidatura";

$tdataelectos[".googleLikeFields"] = array();
$tdataelectos[".googleLikeFields"][] = "idElectos";
$tdataelectos[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdataelectos[".googleLikeFields"][] = "Candidatura_idCandidatura";
$tdataelectos[".googleLikeFields"][] = "Orden";
$tdataelectos[".googleLikeFields"][] = "NombreyApellidos";
$tdataelectos[".googleLikeFields"][] = "Coeficiente";


$tdataelectos[".advSearchFields"] = array();
$tdataelectos[".advSearchFields"][] = "idElectos";
$tdataelectos[".advSearchFields"][] = "Convocatoria_idConvocatoria";
$tdataelectos[".advSearchFields"][] = "Candidatura_idCandidatura";
$tdataelectos[".advSearchFields"][] = "Orden";
$tdataelectos[".advSearchFields"][] = "NombreyApellidos";
$tdataelectos[".advSearchFields"][] = "Coeficiente";

$tdataelectos[".tableType"] = "list";

$tdataelectos[".printerPageOrientation"] = 0;
$tdataelectos[".nPrinterPageScale"] = 100;

$tdataelectos[".nPrinterSplitRecords"] = 40;

$tdataelectos[".nPrinterPDFSplitRecords"] = 40;



$tdataelectos[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdataelectos[".pageSize"] = 6;

$tdataelectos[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY Candidatura_idCandidatura, Orden";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataelectos[".strOrderBy"] = $tstrOrderBy;

$tdataelectos[".orderindexes"] = array();
	$tdataelectos[".orderindexes"][] = array(3, (1 ? "ASC" : "DESC"), "Candidatura_idCandidatura");

	$tdataelectos[".orderindexes"][] = array(4, (1 ? "ASC" : "DESC"), "Orden");


$tdataelectos[".sqlHead"] = "SELECT idElectos,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Orden,  NombreyApellidos,  Coeficiente";
$tdataelectos[".sqlFrom"] = "FROM electos";
$tdataelectos[".sqlWhereExpr"] = "";
$tdataelectos[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataelectos[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataelectos[".arrGroupsPerPage"] = $arrGPP;

$tdataelectos[".highlightSearchResults"] = true;

$tableKeyselectos = array();
$tableKeyselectos[] = "idElectos";
$tdataelectos[".Keys"] = $tableKeyselectos;

$tdataelectos[".listFields"] = array();
$tdataelectos[".listFields"][] = "Candidatura_idCandidatura";
$tdataelectos[".listFields"][] = "Orden";
$tdataelectos[".listFields"][] = "NombreyApellidos";
$tdataelectos[".listFields"][] = "Coeficiente";

$tdataelectos[".hideMobileList"] = array();


$tdataelectos[".viewFields"] = array();
$tdataelectos[".viewFields"][] = "idElectos";
$tdataelectos[".viewFields"][] = "Convocatoria_idConvocatoria";
$tdataelectos[".viewFields"][] = "Candidatura_idCandidatura";
$tdataelectos[".viewFields"][] = "Orden";
$tdataelectos[".viewFields"][] = "NombreyApellidos";
$tdataelectos[".viewFields"][] = "Coeficiente";

$tdataelectos[".addFields"] = array();
$tdataelectos[".addFields"][] = "Convocatoria_idConvocatoria";
$tdataelectos[".addFields"][] = "Candidatura_idCandidatura";
$tdataelectos[".addFields"][] = "Orden";
$tdataelectos[".addFields"][] = "NombreyApellidos";
$tdataelectos[".addFields"][] = "Coeficiente";

$tdataelectos[".masterListFields"] = array();
$tdataelectos[".masterListFields"][] = "idElectos";
$tdataelectos[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdataelectos[".masterListFields"][] = "Candidatura_idCandidatura";
$tdataelectos[".masterListFields"][] = "Orden";
$tdataelectos[".masterListFields"][] = "NombreyApellidos";
$tdataelectos[".masterListFields"][] = "Coeficiente";

$tdataelectos[".inlineAddFields"] = array();

$tdataelectos[".editFields"] = array();
$tdataelectos[".editFields"][] = "Convocatoria_idConvocatoria";
$tdataelectos[".editFields"][] = "Candidatura_idCandidatura";
$tdataelectos[".editFields"][] = "Orden";
$tdataelectos[".editFields"][] = "NombreyApellidos";
$tdataelectos[".editFields"][] = "Coeficiente";

$tdataelectos[".inlineEditFields"] = array();

$tdataelectos[".updateSelectedFields"] = array();
$tdataelectos[".updateSelectedFields"][] = "Convocatoria_idConvocatoria";
$tdataelectos[".updateSelectedFields"][] = "Candidatura_idCandidatura";
$tdataelectos[".updateSelectedFields"][] = "Orden";
$tdataelectos[".updateSelectedFields"][] = "NombreyApellidos";
$tdataelectos[".updateSelectedFields"][] = "Coeficiente";


$tdataelectos[".exportFields"] = array();
$tdataelectos[".exportFields"][] = "idElectos";
$tdataelectos[".exportFields"][] = "Convocatoria_idConvocatoria";
$tdataelectos[".exportFields"][] = "Candidatura_idCandidatura";
$tdataelectos[".exportFields"][] = "Orden";
$tdataelectos[".exportFields"][] = "NombreyApellidos";
$tdataelectos[".exportFields"][] = "Coeficiente";

$tdataelectos[".importFields"] = array();
$tdataelectos[".importFields"][] = "idElectos";
$tdataelectos[".importFields"][] = "Convocatoria_idConvocatoria";
$tdataelectos[".importFields"][] = "Candidatura_idCandidatura";
$tdataelectos[".importFields"][] = "Orden";
$tdataelectos[".importFields"][] = "NombreyApellidos";
$tdataelectos[".importFields"][] = "Coeficiente";

$tdataelectos[".printFields"] = array();
$tdataelectos[".printFields"][] = "idElectos";
$tdataelectos[".printFields"][] = "Convocatoria_idConvocatoria";
$tdataelectos[".printFields"][] = "Candidatura_idCandidatura";
$tdataelectos[".printFields"][] = "Orden";
$tdataelectos[".printFields"][] = "NombreyApellidos";
$tdataelectos[".printFields"][] = "Coeficiente";


//	idElectos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idElectos";
	$fdata["GoodName"] = "idElectos";
	$fdata["ownerTable"] = "electos";
	$fdata["Label"] = GetFieldLabel("electos","idElectos");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idElectos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idElectos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataelectos["idElectos"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "electos";
	$fdata["Label"] = GetFieldLabel("electos","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "convocatoria";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idConvocatoria";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Titulo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 2;
		$fdata["filterTotalFields"] = "idElectos";
		$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = true;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 0;

			
	
	
//end of Filters settings


	$tdataelectos["Convocatoria_idConvocatoria"] = $fdata;
//	Candidatura_idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Candidatura_idCandidatura";
	$fdata["GoodName"] = "Candidatura_idCandidatura";
	$fdata["ownerTable"] = "electos";
	$fdata["Label"] = GetFieldLabel("electos","Candidatura_idCandidatura");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Candidatura_idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Candidatura_idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 1;
	$edata["LookupTable"] = "candidatura";
	$edata["LookupConnId"] = "decma_at_localhost";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "idCandidatura";
	$edata["LinkFieldType"] = 3;
	$edata["DisplayField"] = "Codigo";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
		$edata["SimpleAdd"] = true;


	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings


//Filters settings
	$fdata["filterTotals"] = 0;
		$fdata["filterMultiSelect"] = 2;
		$fdata["filterTotalFields"] = "idElectos";
		$fdata["filterFormat"] = "Values list";
		$fdata["showCollapsed"] = true;

		$fdata["sortValueType"] = 0;
		$fdata["numberOfVisibleItems"] = 0;

			
	
	
//end of Filters settings


	$tdataelectos["Candidatura_idCandidatura"] = $fdata;
//	Orden
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Orden";
	$fdata["GoodName"] = "Orden";
	$fdata["ownerTable"] = "electos";
	$fdata["Label"] = GetFieldLabel("electos","Orden");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Orden";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Orden";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataelectos["Orden"] = $fdata;
//	NombreyApellidos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "NombreyApellidos";
	$fdata["GoodName"] = "NombreyApellidos";
	$fdata["ownerTable"] = "electos";
	$fdata["Label"] = GetFieldLabel("electos","NombreyApellidos");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "NombreyApellidos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "NombreyApellidos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=100";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataelectos["NombreyApellidos"] = $fdata;
//	Coeficiente
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Coeficiente";
	$fdata["GoodName"] = "Coeficiente";
	$fdata["ownerTable"] = "electos";
	$fdata["Label"] = GetFieldLabel("electos","Coeficiente");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Coeficiente";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Coeficiente";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdataelectos["Coeficiente"] = $fdata;


$tables_data["electos"]=&$tdataelectos;
$field_labels["electos"] = &$fieldLabelselectos;
$fieldToolTips["electos"] = &$fieldToolTipselectos;
$placeHolders["electos"] = &$placeHolderselectos;
$page_titles["electos"] = &$pageTitleselectos;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["electos"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["electos"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 0;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["electos"][0] = $masterParams;
				$masterTablesData["electos"][0]["masterKeys"] = array();
	$masterTablesData["electos"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["electos"][0]["detailKeys"] = array();
	$masterTablesData["electos"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
		
	
				$strOriginalDetailsTable="escanos";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="escanos";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "escanos";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["electos"][1] = $masterParams;
				$masterTablesData["electos"][1]["masterKeys"] = array();
	$masterTablesData["electos"][1]["masterKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["electos"][1]["masterKeys"][]="Candidatura_idCandidatura";
				$masterTablesData["electos"][1]["detailKeys"] = array();
	$masterTablesData["electos"][1]["detailKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["electos"][1]["detailKeys"][]="Candidatura_idCandidatura";
		
	
				$strOriginalDetailsTable="candidatura";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm2_candidatura";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm2_candidatura";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["electos"][2] = $masterParams;
				$masterTablesData["electos"][2]["masterKeys"] = array();
	$masterTablesData["electos"][2]["masterKeys"][]="idCandidatura";
				$masterTablesData["electos"][2]["detailKeys"] = array();
	$masterTablesData["electos"][2]["detailKeys"][]="Candidatura_idCandidatura";
		
	
				$strOriginalDetailsTable="escanos";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm1_2_Candidaturas";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm1_2_Candidaturas";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["electos"][3] = $masterParams;
				$masterTablesData["electos"][3]["masterKeys"] = array();
	$masterTablesData["electos"][3]["masterKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["electos"][3]["masterKeys"][]="Candidatura_idCandidatura";
				$masterTablesData["electos"][3]["detailKeys"] = array();
	$masterTablesData["electos"][3]["detailKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["electos"][3]["detailKeys"][]="Candidatura_idCandidatura";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_electos()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "idElectos,  Convocatoria_idConvocatoria,  Candidatura_idCandidatura,  Orden,  NombreyApellidos,  Coeficiente";
$proto3["m_strFrom"] = "FROM electos";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "ORDER BY Candidatura_idCandidatura, Orden";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idElectos",
	"m_strTable" => "electos",
	"m_srcTableName" => "electos"
));

$proto9["m_sql"] = "idElectos";
$proto9["m_srcTableName"] = "electos";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "electos",
	"m_srcTableName" => "electos"
));

$proto11["m_sql"] = "Convocatoria_idConvocatoria";
$proto11["m_srcTableName"] = "electos";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "electos",
	"m_srcTableName" => "electos"
));

$proto13["m_sql"] = "Candidatura_idCandidatura";
$proto13["m_srcTableName"] = "electos";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "electos",
	"m_srcTableName" => "electos"
));

$proto15["m_sql"] = "Orden";
$proto15["m_srcTableName"] = "electos";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "NombreyApellidos",
	"m_strTable" => "electos",
	"m_srcTableName" => "electos"
));

$proto17["m_sql"] = "NombreyApellidos";
$proto17["m_srcTableName"] = "electos";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "Coeficiente",
	"m_strTable" => "electos",
	"m_srcTableName" => "electos"
));

$proto19["m_sql"] = "Coeficiente";
$proto19["m_srcTableName"] = "electos";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto21=array();
$proto21["m_link"] = "SQLL_MAIN";
			$proto22=array();
$proto22["m_strName"] = "electos";
$proto22["m_srcTableName"] = "electos";
$proto22["m_columns"] = array();
$proto22["m_columns"][] = "idElectos";
$proto22["m_columns"][] = "Convocatoria_idConvocatoria";
$proto22["m_columns"][] = "Candidatura_idCandidatura";
$proto22["m_columns"][] = "Orden";
$proto22["m_columns"][] = "NombreyApellidos";
$proto22["m_columns"][] = "Coeficiente";
$obj = new SQLTable($proto22);

$proto21["m_table"] = $obj;
$proto21["m_sql"] = "electos";
$proto21["m_alias"] = "";
$proto21["m_srcTableName"] = "electos";
$proto23=array();
$proto23["m_sql"] = "";
$proto23["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto23["m_column"]=$obj;
$proto23["m_contained"] = array();
$proto23["m_strCase"] = "";
$proto23["m_havingmode"] = false;
$proto23["m_inBrackets"] = false;
$proto23["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto23);

$proto21["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto21);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto25=array();
						$obj = new SQLField(array(
	"m_strName" => "Candidatura_idCandidatura",
	"m_strTable" => "electos",
	"m_srcTableName" => "electos"
));

$proto25["m_column"]=$obj;
$proto25["m_bAsc"] = 1;
$proto25["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto25);

$proto3["m_orderby"][]=$obj;					
												$proto27=array();
						$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "electos",
	"m_srcTableName" => "electos"
));

$proto27["m_column"]=$obj;
$proto27["m_bAsc"] = 1;
$proto27["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto27);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="electos";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_electos = createSqlQuery_electos();


	
		;

						

$tdataelectos[".sqlquery"] = $queryData_electos;

$tableEvents["electos"] = new eventsBase;
$tdataelectos[".hasEvents"] = false;

?>