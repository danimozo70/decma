<?php
require_once(getabspath("classes/cipherer.php"));



$tdatacm1_escanos2 = array();
$tdatacm1_escanos2[".ShortName"] = "cm1_escanos2";

//	field labels
$fieldLabelscm1_escanos2 = array();
$pageTitlescm1_escanos2 = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm1_escanos2["Spanish"] = array();
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Candidaturas_idEscanos"] = "Id Interno";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Candidaturas_Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Candidaturas_Candidatura_idCandidatura"] = "Candidatura";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Candidaturas_Escanos"] = "Escaños";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Electos_idElectos"] = "Id Interno";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Electos_Convocatoria_idConvocatoria"] = "Convocatoria";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Electos_Candidatura_idCandidatura"] = "Candidatura";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Electos_Orden"] = "Orden";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Electos_NombreyApellidos"] = "Nombre y Apellidos";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Electos_Coeficiente"] = "Coeficiente aplicado";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Convocatoria_idConvocatoria"] = "Id Interno";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Convocatoria_Orden"] = "Orden";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Convocatoria_EsAsamblea"] = "Es Asamblea?";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Convocatoria_Titulo"] = "Título";
	$fieldLabelscm1_escanos2["Spanish"]["cm1_2_Convocatoria_Descripcion"] = "Descripción";
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm1_escanos2[""] = array();
}

//	search fields
$tdatacm1_escanos2[".searchFields"] = array();
$dashField = array();
$dashField[] = array( "table"=>"cm1_2_Candidaturas", "field"=>"idEscanos" );
$tdatacm1_escanos2[".searchFields"]["cm1_2_Candidaturas_idEscanos"] = $dashField;
$dashField = array();
$dashField[] = array( "table"=>"cm1_2_Candidaturas", "field"=>"Convocatoria_idConvocatoria" );
$tdatacm1_escanos2[".searchFields"]["cm1_2_Candidaturas_Convocatoria_idConvocatoria"] = $dashField;
$dashField = array();
$dashField[] = array( "table"=>"cm1_2_Candidaturas", "field"=>"Candidatura_idCandidatura" );
$tdatacm1_escanos2[".searchFields"]["cm1_2_Candidaturas_Candidatura_idCandidatura"] = $dashField;
$dashField = array();
$dashField[] = array( "table"=>"cm1_2_Candidaturas", "field"=>"Escanos" );
$tdatacm1_escanos2[".searchFields"]["cm1_2_Candidaturas_Escanos"] = $dashField;

// all search fields
$tdatacm1_escanos2[".allSearchFields"] = array();
$tdatacm1_escanos2[".allSearchFields"][] = "cm1_2_Candidaturas_idEscanos";
$tdatacm1_escanos2[".allSearchFields"][] = "cm1_2_Candidaturas_Convocatoria_idConvocatoria";
$tdatacm1_escanos2[".allSearchFields"][] = "cm1_2_Candidaturas_Candidatura_idCandidatura";
$tdatacm1_escanos2[".allSearchFields"][] = "cm1_2_Candidaturas_Escanos";

// good like search fields
$tdatacm1_escanos2[".googleLikeFields"] = array();
$tdatacm1_escanos2[".googleLikeFields"][] = "cm1_2_Candidaturas_idEscanos";
$tdatacm1_escanos2[".googleLikeFields"][] = "cm1_2_Candidaturas_Convocatoria_idConvocatoria";
$tdatacm1_escanos2[".googleLikeFields"][] = "cm1_2_Candidaturas_Candidatura_idCandidatura";
$tdatacm1_escanos2[".googleLikeFields"][] = "cm1_2_Candidaturas_Escanos";

$tdatacm1_escanos2[".dashElements"] = array();

	$dbelement = array( "elementName" => "cm1_2_Candidaturas_list", "table" => "cm1_2_Candidaturas", "type" => 0);
	$dbelement["cellName"] = "cell_1_0";

			$dbelement["width"] = 500;
			$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "cm1_2_Convocatoria";

	$tdatacm1_escanos2[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm1_2_Electos_list", "table" => "cm1_2_Electos", "type" => 0);
	$dbelement["cellName"] = "cell_0_1";

					$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;

$dbelement["masterTable"] = "cm1_2_Candidaturas";

	$tdatacm1_escanos2[".dashElements"][] = $dbelement;
	$dbelement = array( "elementName" => "cm1_2_Convocatoria_list", "table" => "cm1_2_Convocatoria", "type" => 0);
	$dbelement["cellName"] = "cell_0_0";

			$dbelement["width"] = 500;
			$dbelement["inlineAdd"] = 0 > 0;
	$dbelement["inlineEdit"] = 0 > 0;
	$dbelement["deleteRecord"] = 0 > 0;

	$dbelement["popupAdd"] = 0 > 0;
	$dbelement["popupEdit"] = 0 > 0;
	$dbelement["popupView"] = 0 > 0;
	
	$dbelement["updateSelected"] = 0 > 0;


	$tdatacm1_escanos2[".dashElements"][] = $dbelement;

$tdatacm1_escanos2[".shortTableName"] = "cm1_escanos2";
$tdatacm1_escanos2[".entityType"] = 4;



include_once(getabspath("include/cm1_escanos2_events.php"));
$tableEvents["cm1_escanos2"] = new eventclass_cm1_escanos2;
$tdatacm1_escanos2[".hasEvents"] = true;


$tdatacm1_escanos2[".tableType"] = "dashboard";



$tdatacm1_escanos2[".addPageEvents"] = false;

$tables_data["cm1_escanos2"]=&$tdatacm1_escanos2;
$field_labels["cm1_escanos2"] = &$fieldLabelscm1_escanos2;
$page_titles["cm1_escanos2"] = &$pageTitlescm1_escanos2;

?>