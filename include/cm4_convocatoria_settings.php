<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm4_convocatoria = array();
	$tdatacm4_convocatoria[".truncateText"] = true;
	$tdatacm4_convocatoria[".NumberOfChars"] = 80;
	$tdatacm4_convocatoria[".ShortName"] = "cm4_convocatoria";
	$tdatacm4_convocatoria[".OwnerID"] = "";
	$tdatacm4_convocatoria[".OriginalTable"] = "convocatoria";

//	field labels
$fieldLabelscm4_convocatoria = array();
$fieldToolTipscm4_convocatoria = array();
$pageTitlescm4_convocatoria = array();
$placeHolderscm4_convocatoria = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm4_convocatoria["Spanish"] = array();
	$fieldToolTipscm4_convocatoria["Spanish"] = array();
	$placeHolderscm4_convocatoria["Spanish"] = array();
	$pageTitlescm4_convocatoria["Spanish"] = array();
	$fieldLabelscm4_convocatoria["Spanish"]["idConvocatoria"] = "Id Interno";
	$fieldToolTipscm4_convocatoria["Spanish"]["idConvocatoria"] = "";
	$placeHolderscm4_convocatoria["Spanish"]["idConvocatoria"] = "";
	$fieldLabelscm4_convocatoria["Spanish"]["Orden"] = "Orden";
	$fieldToolTipscm4_convocatoria["Spanish"]["Orden"] = "";
	$placeHolderscm4_convocatoria["Spanish"]["Orden"] = "";
	$fieldLabelscm4_convocatoria["Spanish"]["EsAsamblea"] = "Es Asamblea?";
	$fieldToolTipscm4_convocatoria["Spanish"]["EsAsamblea"] = "";
	$placeHolderscm4_convocatoria["Spanish"]["EsAsamblea"] = "";
	$fieldLabelscm4_convocatoria["Spanish"]["Titulo"] = "Título";
	$fieldToolTipscm4_convocatoria["Spanish"]["Titulo"] = "";
	$placeHolderscm4_convocatoria["Spanish"]["Titulo"] = "";
	$fieldLabelscm4_convocatoria["Spanish"]["Descripcion"] = "Descripción";
	$fieldToolTipscm4_convocatoria["Spanish"]["Descripcion"] = "";
	$placeHolderscm4_convocatoria["Spanish"]["Descripcion"] = "";
	if (count($fieldToolTipscm4_convocatoria["Spanish"]))
		$tdatacm4_convocatoria[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm4_convocatoria[""] = array();
	$fieldToolTipscm4_convocatoria[""] = array();
	$placeHolderscm4_convocatoria[""] = array();
	$pageTitlescm4_convocatoria[""] = array();
	if (count($fieldToolTipscm4_convocatoria[""]))
		$tdatacm4_convocatoria[".isUseToolTips"] = true;
}


	$tdatacm4_convocatoria[".NCSearch"] = true;



$tdatacm4_convocatoria[".shortTableName"] = "cm4_convocatoria";
$tdatacm4_convocatoria[".nSecOptions"] = 0;
$tdatacm4_convocatoria[".recsPerRowList"] = 1;
$tdatacm4_convocatoria[".recsPerRowPrint"] = 1;
$tdatacm4_convocatoria[".mainTableOwnerID"] = "";
$tdatacm4_convocatoria[".moveNext"] = 1;
$tdatacm4_convocatoria[".entityType"] = 1;

$tdatacm4_convocatoria[".strOriginalTableName"] = "convocatoria";

	



$tdatacm4_convocatoria[".showAddInPopup"] = true;

$tdatacm4_convocatoria[".showEditInPopup"] = true;

$tdatacm4_convocatoria[".showViewInPopup"] = true;

//page's base css files names
$popupPagesLayoutNames = array();
			;
$popupPagesLayoutNames["add"] = "view_bootstrap";
			;
$popupPagesLayoutNames["edit"] = "view_bootstrap";
			;
$popupPagesLayoutNames["view"] = "view_bootstrap";
$tdatacm4_convocatoria[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm4_convocatoria[".fieldsForRegister"] = array();

$tdatacm4_convocatoria[".listAjax"] = false;

	$tdatacm4_convocatoria[".audit"] = false;

	$tdatacm4_convocatoria[".locking"] = false;



$tdatacm4_convocatoria[".list"] = true;











$tdatacm4_convocatoria[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm4_convocatoria[".allowShowHideFields"] = true;
//

// Allow Fields Reordering in GRID
$tdatacm4_convocatoria[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm4_convocatoria[".searchSaving"] = false;
//

	$tdatacm4_convocatoria[".showSearchPanel"] = false;

$tdatacm4_convocatoria[".isUseAjaxSuggest"] = true;

$tdatacm4_convocatoria[".rowHighlite"] = true;





$tdatacm4_convocatoria[".ajaxCodeSnippetAdded"] = false;

$tdatacm4_convocatoria[".buttonsAdded"] = false;

$tdatacm4_convocatoria[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm4_convocatoria[".isUseTimeForSearch"] = false;



$tdatacm4_convocatoria[".badgeColor"] = "CD853F";


$tdatacm4_convocatoria[".allSearchFields"] = array();
$tdatacm4_convocatoria[".filterFields"] = array();
$tdatacm4_convocatoria[".requiredSearchFields"] = array();

$tdatacm4_convocatoria[".allSearchFields"][] = "idConvocatoria";
	$tdatacm4_convocatoria[".allSearchFields"][] = "Orden";
	$tdatacm4_convocatoria[".allSearchFields"][] = "EsAsamblea";
	$tdatacm4_convocatoria[".allSearchFields"][] = "Titulo";
	$tdatacm4_convocatoria[".allSearchFields"][] = "Descripcion";
	

$tdatacm4_convocatoria[".googleLikeFields"] = array();
$tdatacm4_convocatoria[".googleLikeFields"][] = "idConvocatoria";
$tdatacm4_convocatoria[".googleLikeFields"][] = "Orden";
$tdatacm4_convocatoria[".googleLikeFields"][] = "EsAsamblea";
$tdatacm4_convocatoria[".googleLikeFields"][] = "Titulo";
$tdatacm4_convocatoria[".googleLikeFields"][] = "Descripcion";


$tdatacm4_convocatoria[".advSearchFields"] = array();
$tdatacm4_convocatoria[".advSearchFields"][] = "idConvocatoria";
$tdatacm4_convocatoria[".advSearchFields"][] = "Orden";
$tdatacm4_convocatoria[".advSearchFields"][] = "EsAsamblea";
$tdatacm4_convocatoria[".advSearchFields"][] = "Titulo";
$tdatacm4_convocatoria[".advSearchFields"][] = "Descripcion";

$tdatacm4_convocatoria[".tableType"] = "list";

$tdatacm4_convocatoria[".printerPageOrientation"] = 0;
$tdatacm4_convocatoria[".nPrinterPageScale"] = 100;

$tdatacm4_convocatoria[".nPrinterSplitRecords"] = 40;

$tdatacm4_convocatoria[".nPrinterPDFSplitRecords"] = 40;



$tdatacm4_convocatoria[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatacm4_convocatoria[".pageSize"] = 6;

$tdatacm4_convocatoria[".warnLeavingPages"] = true;



$tstrOrderBy = "order by Orden";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm4_convocatoria[".strOrderBy"] = $tstrOrderBy;

$tdatacm4_convocatoria[".orderindexes"] = array();
	$tdatacm4_convocatoria[".orderindexes"][] = array(2, (1 ? "ASC" : "DESC"), "Orden");


$tdatacm4_convocatoria[".sqlHead"] = "SELECT idConvocatoria,  	Orden,  	EsAsamblea,  	Titulo,  	Descripcion";
$tdatacm4_convocatoria[".sqlFrom"] = "FROM convocatoria";
$tdatacm4_convocatoria[".sqlWhereExpr"] = "";
$tdatacm4_convocatoria[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 5;
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm4_convocatoria[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm4_convocatoria[".arrGroupsPerPage"] = $arrGPP;

$tdatacm4_convocatoria[".highlightSearchResults"] = true;

$tableKeyscm4_convocatoria = array();
$tableKeyscm4_convocatoria[] = "idConvocatoria";
$tdatacm4_convocatoria[".Keys"] = $tableKeyscm4_convocatoria;

$tdatacm4_convocatoria[".listFields"] = array();
$tdatacm4_convocatoria[".listFields"][] = "Titulo";
$tdatacm4_convocatoria[".listFields"][] = "Descripcion";

$tdatacm4_convocatoria[".hideMobileList"] = array();


$tdatacm4_convocatoria[".viewFields"] = array();
$tdatacm4_convocatoria[".viewFields"][] = "idConvocatoria";
$tdatacm4_convocatoria[".viewFields"][] = "Orden";
$tdatacm4_convocatoria[".viewFields"][] = "EsAsamblea";
$tdatacm4_convocatoria[".viewFields"][] = "Titulo";
$tdatacm4_convocatoria[".viewFields"][] = "Descripcion";

$tdatacm4_convocatoria[".addFields"] = array();

$tdatacm4_convocatoria[".masterListFields"] = array();
$tdatacm4_convocatoria[".masterListFields"][] = "idConvocatoria";
$tdatacm4_convocatoria[".masterListFields"][] = "Orden";
$tdatacm4_convocatoria[".masterListFields"][] = "EsAsamblea";
$tdatacm4_convocatoria[".masterListFields"][] = "Titulo";
$tdatacm4_convocatoria[".masterListFields"][] = "Descripcion";

$tdatacm4_convocatoria[".inlineAddFields"] = array();

$tdatacm4_convocatoria[".editFields"] = array();

$tdatacm4_convocatoria[".inlineEditFields"] = array();

$tdatacm4_convocatoria[".updateSelectedFields"] = array();


$tdatacm4_convocatoria[".exportFields"] = array();
$tdatacm4_convocatoria[".exportFields"][] = "idConvocatoria";
$tdatacm4_convocatoria[".exportFields"][] = "Orden";
$tdatacm4_convocatoria[".exportFields"][] = "EsAsamblea";
$tdatacm4_convocatoria[".exportFields"][] = "Titulo";
$tdatacm4_convocatoria[".exportFields"][] = "Descripcion";

$tdatacm4_convocatoria[".importFields"] = array();

$tdatacm4_convocatoria[".printFields"] = array();
$tdatacm4_convocatoria[".printFields"][] = "idConvocatoria";
$tdatacm4_convocatoria[".printFields"][] = "Orden";
$tdatacm4_convocatoria[".printFields"][] = "EsAsamblea";
$tdatacm4_convocatoria[".printFields"][] = "Titulo";
$tdatacm4_convocatoria[".printFields"][] = "Descripcion";


//	idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idConvocatoria";
	$fdata["GoodName"] = "idConvocatoria";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm4_convocatoria","idConvocatoria");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm4_convocatoria["idConvocatoria"] = $fdata;
//	Orden
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Orden";
	$fdata["GoodName"] = "Orden";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm4_convocatoria","Orden");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Orden";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Orden";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm4_convocatoria["Orden"] = $fdata;
//	EsAsamblea
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "EsAsamblea";
	$fdata["GoodName"] = "EsAsamblea";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm4_convocatoria","EsAsamblea");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "EsAsamblea";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "EsAsamblea";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm4_convocatoria["EsAsamblea"] = $fdata;
//	Titulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Titulo";
	$fdata["GoodName"] = "Titulo";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm4_convocatoria","Titulo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Titulo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=30";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm4_convocatoria["Titulo"] = $fdata;
//	Descripcion
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Descripcion";
	$fdata["GoodName"] = "Descripcion";
	$fdata["ownerTable"] = "convocatoria";
	$fdata["Label"] = GetFieldLabel("cm4_convocatoria","Descripcion");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Descripcion";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Descripcion";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=500";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm4_convocatoria["Descripcion"] = $fdata;


$tables_data["cm4_convocatoria"]=&$tdatacm4_convocatoria;
$field_labels["cm4_convocatoria"] = &$fieldLabelscm4_convocatoria;
$fieldToolTips["cm4_convocatoria"] = &$fieldToolTipscm4_convocatoria;
$placeHolders["cm4_convocatoria"] = &$placeHolderscm4_convocatoria;
$page_titles["cm4_convocatoria"] = &$pageTitlescm4_convocatoria;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm4_convocatoria"] = array();
//	cm4_mapa_municipio
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm4_mapa_municipio";
		$detailsParam["dOriginalTable"] = "convocatoria";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm4_mapa_municipio";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm4_mapa_municipio");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "1";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm4_convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm4_convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm4_convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["cm4_convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm4_convocatoria"][$dIndex]["detailKeys"][]="idConvocatoria";
//	cm4_mapa_distrito
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm4_mapa_distrito";
		$detailsParam["dOriginalTable"] = "convocatoria";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm4_mapa_distrito";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm4_mapa_distrito");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm4_convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm4_convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm4_convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["cm4_convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm4_convocatoria"][$dIndex]["detailKeys"][]="idConvocatoria";
//	cm4_leyenda_municipio
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm4_leyenda_municipio";
		$detailsParam["dOriginalTable"] = "v_municipio_voto";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm4_leyenda_municipio";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm4_leyenda_municipio");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["cm4_convocatoria"][$dIndex] = $detailsParam;

	
		$detailsTablesData["cm4_convocatoria"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["cm4_convocatoria"][$dIndex]["masterKeys"][]="idConvocatoria";

				$detailsTablesData["cm4_convocatoria"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["cm4_convocatoria"][$dIndex]["detailKeys"][]="Convocatoria_idConvocatoria";

// tables which are master tables for current table (detail)
$masterTablesData["cm4_convocatoria"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm4_convocatoria()
{
$proto3=array();
$proto3["m_strHead"] = "SELECT";
$proto3["m_strFieldList"] = "idConvocatoria,  	Orden,  	EsAsamblea,  	Titulo,  	Descripcion";
$proto3["m_strFrom"] = "FROM convocatoria";
$proto3["m_strWhere"] = "";
$proto3["m_strOrderBy"] = "order by Orden";
	
		;
			$proto3["cipherer"] = null;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto3["m_where"] = $obj;
$proto7=array();
$proto7["m_sql"] = "";
$proto7["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto7["m_column"]=$obj;
$proto7["m_contained"] = array();
$proto7["m_strCase"] = "";
$proto7["m_havingmode"] = false;
$proto7["m_inBrackets"] = false;
$proto7["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto7);

$proto3["m_having"] = $obj;
$proto3["m_fieldlist"] = array();
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "idConvocatoria",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm4_convocatoria"
));

$proto9["m_sql"] = "idConvocatoria";
$proto9["m_srcTableName"] = "cm4_convocatoria";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto3["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm4_convocatoria"
));

$proto11["m_sql"] = "Orden";
$proto11["m_srcTableName"] = "cm4_convocatoria";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto3["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "EsAsamblea",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm4_convocatoria"
));

$proto13["m_sql"] = "EsAsamblea";
$proto13["m_srcTableName"] = "cm4_convocatoria";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto3["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm4_convocatoria"
));

$proto15["m_sql"] = "Titulo";
$proto15["m_srcTableName"] = "cm4_convocatoria";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto3["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "Descripcion",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm4_convocatoria"
));

$proto17["m_sql"] = "Descripcion";
$proto17["m_srcTableName"] = "cm4_convocatoria";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto3["m_fieldlist"][]=$obj;
$proto3["m_fromlist"] = array();
												$proto19=array();
$proto19["m_link"] = "SQLL_MAIN";
			$proto20=array();
$proto20["m_strName"] = "convocatoria";
$proto20["m_srcTableName"] = "cm4_convocatoria";
$proto20["m_columns"] = array();
$proto20["m_columns"][] = "idConvocatoria";
$proto20["m_columns"][] = "Orden";
$proto20["m_columns"][] = "EsAsamblea";
$proto20["m_columns"][] = "Titulo";
$proto20["m_columns"][] = "Descripcion";
$obj = new SQLTable($proto20);

$proto19["m_table"] = $obj;
$proto19["m_sql"] = "convocatoria";
$proto19["m_alias"] = "";
$proto19["m_srcTableName"] = "cm4_convocatoria";
$proto21=array();
$proto21["m_sql"] = "";
$proto21["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto21["m_column"]=$obj;
$proto21["m_contained"] = array();
$proto21["m_strCase"] = "";
$proto21["m_havingmode"] = false;
$proto21["m_inBrackets"] = false;
$proto21["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto21);

$proto19["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto19);

$proto3["m_fromlist"][]=$obj;
$proto3["m_groupby"] = array();
$proto3["m_orderby"] = array();
												$proto23=array();
						$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "convocatoria",
	"m_srcTableName" => "cm4_convocatoria"
));

$proto23["m_column"]=$obj;
$proto23["m_bAsc"] = 1;
$proto23["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto23);

$proto3["m_orderby"][]=$obj;					
$proto3["m_srcTableName"]="cm4_convocatoria";		
$obj = new SQLQuery($proto3);

	return $obj;
}
$queryData_cm4_convocatoria = createSqlQuery_cm4_convocatoria();


	
		;

					

$tdatacm4_convocatoria[".sqlquery"] = $queryData_cm4_convocatoria;

$tableEvents["cm4_convocatoria"] = new eventsBase;
$tdatacm4_convocatoria[".hasEvents"] = false;

?>