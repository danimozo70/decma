<?php
require_once(getabspath("classes/cipherer.php"));




$tdatav_candidatura_mapa = array();
	$tdatav_candidatura_mapa[".truncateText"] = true;
	$tdatav_candidatura_mapa[".NumberOfChars"] = 80;
	$tdatav_candidatura_mapa[".ShortName"] = "v_candidatura_mapa";
	$tdatav_candidatura_mapa[".OwnerID"] = "";
	$tdatav_candidatura_mapa[".OriginalTable"] = "v_candidatura_mapa";

//	field labels
$fieldLabelsv_candidatura_mapa = array();
$fieldToolTipsv_candidatura_mapa = array();
$pageTitlesv_candidatura_mapa = array();
$placeHoldersv_candidatura_mapa = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelsv_candidatura_mapa["Spanish"] = array();
	$fieldToolTipsv_candidatura_mapa["Spanish"] = array();
	$placeHoldersv_candidatura_mapa["Spanish"] = array();
	$pageTitlesv_candidatura_mapa["Spanish"] = array();
	$fieldLabelsv_candidatura_mapa["Spanish"]["idElecCandidatura"] = "Id Elec Candidatura";
	$fieldToolTipsv_candidatura_mapa["Spanish"]["idElecCandidatura"] = "";
	$placeHoldersv_candidatura_mapa["Spanish"]["idElecCandidatura"] = "";
	$fieldLabelsv_candidatura_mapa["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria IdConvocatoria";
	$fieldToolTipsv_candidatura_mapa["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHoldersv_candidatura_mapa["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelsv_candidatura_mapa["Spanish"]["Orden"] = "Orden";
	$fieldToolTipsv_candidatura_mapa["Spanish"]["Orden"] = "";
	$placeHoldersv_candidatura_mapa["Spanish"]["Orden"] = "";
	$fieldLabelsv_candidatura_mapa["Spanish"]["idCandidatura"] = "Id Candidatura";
	$fieldToolTipsv_candidatura_mapa["Spanish"]["idCandidatura"] = "";
	$placeHoldersv_candidatura_mapa["Spanish"]["idCandidatura"] = "";
	$fieldLabelsv_candidatura_mapa["Spanish"]["Codigo"] = "Siglas";
	$fieldToolTipsv_candidatura_mapa["Spanish"]["Codigo"] = "";
	$placeHoldersv_candidatura_mapa["Spanish"]["Codigo"] = "";
	$fieldLabelsv_candidatura_mapa["Spanish"]["Titulo"] = "Titulo";
	$fieldToolTipsv_candidatura_mapa["Spanish"]["Titulo"] = "";
	$placeHoldersv_candidatura_mapa["Spanish"]["Titulo"] = "";
	$fieldLabelsv_candidatura_mapa["Spanish"]["Color"] = "Color";
	$fieldToolTipsv_candidatura_mapa["Spanish"]["Color"] = "";
	$placeHoldersv_candidatura_mapa["Spanish"]["Color"] = "";
	$fieldLabelsv_candidatura_mapa["Spanish"]["Votos"] = "Votos";
	$fieldToolTipsv_candidatura_mapa["Spanish"]["Votos"] = "";
	$placeHoldersv_candidatura_mapa["Spanish"]["Votos"] = "";
	if (count($fieldToolTipsv_candidatura_mapa["Spanish"]))
		$tdatav_candidatura_mapa[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsv_candidatura_mapa[""] = array();
	$fieldToolTipsv_candidatura_mapa[""] = array();
	$placeHoldersv_candidatura_mapa[""] = array();
	$pageTitlesv_candidatura_mapa[""] = array();
	if (count($fieldToolTipsv_candidatura_mapa[""]))
		$tdatav_candidatura_mapa[".isUseToolTips"] = true;
}


	$tdatav_candidatura_mapa[".NCSearch"] = true;



$tdatav_candidatura_mapa[".shortTableName"] = "v_candidatura_mapa";
$tdatav_candidatura_mapa[".nSecOptions"] = 0;
$tdatav_candidatura_mapa[".recsPerRowList"] = 1;
$tdatav_candidatura_mapa[".recsPerRowPrint"] = 1;
$tdatav_candidatura_mapa[".mainTableOwnerID"] = "";
$tdatav_candidatura_mapa[".moveNext"] = 0;
$tdatav_candidatura_mapa[".entityType"] = 0;

$tdatav_candidatura_mapa[".strOriginalTableName"] = "v_candidatura_mapa";

	



$tdatav_candidatura_mapa[".showAddInPopup"] = false;

$tdatav_candidatura_mapa[".showEditInPopup"] = false;

$tdatav_candidatura_mapa[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatav_candidatura_mapa[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatav_candidatura_mapa[".fieldsForRegister"] = array();

$tdatav_candidatura_mapa[".listAjax"] = false;

	$tdatav_candidatura_mapa[".audit"] = false;

	$tdatav_candidatura_mapa[".locking"] = false;



$tdatav_candidatura_mapa[".list"] = true;











$tdatav_candidatura_mapa[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatav_candidatura_mapa[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatav_candidatura_mapa[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatav_candidatura_mapa[".searchSaving"] = false;
//

	$tdatav_candidatura_mapa[".showSearchPanel"] = false;

$tdatav_candidatura_mapa[".isUseAjaxSuggest"] = true;

$tdatav_candidatura_mapa[".rowHighlite"] = true;





$tdatav_candidatura_mapa[".ajaxCodeSnippetAdded"] = false;

$tdatav_candidatura_mapa[".buttonsAdded"] = false;

$tdatav_candidatura_mapa[".addPageEvents"] = false;

// use timepicker for search panel
$tdatav_candidatura_mapa[".isUseTimeForSearch"] = false;



$tdatav_candidatura_mapa[".badgeColor"] = "2f4f4f";


$tdatav_candidatura_mapa[".allSearchFields"] = array();
$tdatav_candidatura_mapa[".filterFields"] = array();
$tdatav_candidatura_mapa[".requiredSearchFields"] = array();



$tdatav_candidatura_mapa[".googleLikeFields"] = array();
$tdatav_candidatura_mapa[".googleLikeFields"][] = "idElecCandidatura";
$tdatav_candidatura_mapa[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdatav_candidatura_mapa[".googleLikeFields"][] = "Orden";
$tdatav_candidatura_mapa[".googleLikeFields"][] = "idCandidatura";
$tdatav_candidatura_mapa[".googleLikeFields"][] = "Codigo";
$tdatav_candidatura_mapa[".googleLikeFields"][] = "Titulo";
$tdatav_candidatura_mapa[".googleLikeFields"][] = "Color";
$tdatav_candidatura_mapa[".googleLikeFields"][] = "Votos";



$tdatav_candidatura_mapa[".tableType"] = "list";

$tdatav_candidatura_mapa[".printerPageOrientation"] = 0;
$tdatav_candidatura_mapa[".nPrinterPageScale"] = 100;

$tdatav_candidatura_mapa[".nPrinterSplitRecords"] = 40;

$tdatav_candidatura_mapa[".nPrinterPDFSplitRecords"] = 40;



$tdatav_candidatura_mapa[".geocodingEnabled"] = false;










// view page pdf

// print page pdf


$tdatav_candidatura_mapa[".pageSize"] = 4;

$tdatav_candidatura_mapa[".warnLeavingPages"] = true;



$tstrOrderBy = "order by Votos desc";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatav_candidatura_mapa[".strOrderBy"] = $tstrOrderBy;

$tdatav_candidatura_mapa[".orderindexes"] = array();
	$tdatav_candidatura_mapa[".orderindexes"][] = array(8, (0 ? "ASC" : "DESC"), "Votos");


$tdatav_candidatura_mapa[".sqlHead"] = "SELECT idElecCandidatura,  	Convocatoria_idConvocatoria,  	Orden,  	idCandidatura,  	Codigo,  	Titulo,  	Color,  	Votos";
$tdatav_candidatura_mapa[".sqlFrom"] = "FROM v_candidatura_mapa";
$tdatav_candidatura_mapa[".sqlWhereExpr"] = "";
$tdatav_candidatura_mapa[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatav_candidatura_mapa[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatav_candidatura_mapa[".arrGroupsPerPage"] = $arrGPP;

$tdatav_candidatura_mapa[".highlightSearchResults"] = true;

$tableKeysv_candidatura_mapa = array();
$tableKeysv_candidatura_mapa[] = "idElecCandidatura";
$tdatav_candidatura_mapa[".Keys"] = $tableKeysv_candidatura_mapa;

$tdatav_candidatura_mapa[".listFields"] = array();
$tdatav_candidatura_mapa[".listFields"][] = "Codigo";
$tdatav_candidatura_mapa[".listFields"][] = "Color";

$tdatav_candidatura_mapa[".hideMobileList"] = array();


$tdatav_candidatura_mapa[".viewFields"] = array();

$tdatav_candidatura_mapa[".addFields"] = array();

$tdatav_candidatura_mapa[".masterListFields"] = array();
$tdatav_candidatura_mapa[".masterListFields"][] = "idElecCandidatura";
$tdatav_candidatura_mapa[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdatav_candidatura_mapa[".masterListFields"][] = "Orden";
$tdatav_candidatura_mapa[".masterListFields"][] = "idCandidatura";
$tdatav_candidatura_mapa[".masterListFields"][] = "Codigo";
$tdatav_candidatura_mapa[".masterListFields"][] = "Titulo";
$tdatav_candidatura_mapa[".masterListFields"][] = "Color";
$tdatav_candidatura_mapa[".masterListFields"][] = "Votos";

$tdatav_candidatura_mapa[".inlineAddFields"] = array();

$tdatav_candidatura_mapa[".editFields"] = array();

$tdatav_candidatura_mapa[".inlineEditFields"] = array();

$tdatav_candidatura_mapa[".updateSelectedFields"] = array();


$tdatav_candidatura_mapa[".exportFields"] = array();

$tdatav_candidatura_mapa[".importFields"] = array();

$tdatav_candidatura_mapa[".printFields"] = array();


//	idElecCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "idElecCandidatura";
	$fdata["GoodName"] = "idElecCandidatura";
	$fdata["ownerTable"] = "v_candidatura_mapa";
	$fdata["Label"] = GetFieldLabel("v_candidatura_mapa","idElecCandidatura");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idElecCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idElecCandidatura";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_candidatura_mapa["idElecCandidatura"] = $fdata;
//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "v_candidatura_mapa";
	$fdata["Label"] = GetFieldLabel("v_candidatura_mapa","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_candidatura_mapa["Convocatoria_idConvocatoria"] = $fdata;
//	Orden
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Orden";
	$fdata["GoodName"] = "Orden";
	$fdata["ownerTable"] = "v_candidatura_mapa";
	$fdata["Label"] = GetFieldLabel("v_candidatura_mapa","Orden");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Orden";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Orden";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_candidatura_mapa["Orden"] = $fdata;
//	idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "idCandidatura";
	$fdata["GoodName"] = "idCandidatura";
	$fdata["ownerTable"] = "v_candidatura_mapa";
	$fdata["Label"] = GetFieldLabel("v_candidatura_mapa","idCandidatura");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idCandidatura";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_candidatura_mapa["idCandidatura"] = $fdata;
//	Codigo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Codigo";
	$fdata["GoodName"] = "Codigo";
	$fdata["ownerTable"] = "v_candidatura_mapa";
	$fdata["Label"] = GetFieldLabel("v_candidatura_mapa","Codigo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Codigo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Codigo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_candidatura_mapa["Codigo"] = $fdata;
//	Titulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Titulo";
	$fdata["GoodName"] = "Titulo";
	$fdata["ownerTable"] = "v_candidatura_mapa";
	$fdata["Label"] = GetFieldLabel("v_candidatura_mapa","Titulo");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Titulo";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_candidatura_mapa["Titulo"] = $fdata;
//	Color
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Color";
	$fdata["GoodName"] = "Color";
	$fdata["ownerTable"] = "v_candidatura_mapa";
	$fdata["Label"] = GetFieldLabel("v_candidatura_mapa","Color");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Color";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Color";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_candidatura_mapa["Color"] = $fdata;
//	Votos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "Votos";
	$fdata["GoodName"] = "Votos";
	$fdata["ownerTable"] = "v_candidatura_mapa";
	$fdata["Label"] = GetFieldLabel("v_candidatura_mapa","Votos");
	$fdata["FieldType"] = 14;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Votos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Votos";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 0;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatav_candidatura_mapa["Votos"] = $fdata;


$tables_data["v_candidatura_mapa"]=&$tdatav_candidatura_mapa;
$field_labels["v_candidatura_mapa"] = &$fieldLabelsv_candidatura_mapa;
$fieldToolTips["v_candidatura_mapa"] = &$fieldToolTipsv_candidatura_mapa;
$placeHolders["v_candidatura_mapa"] = &$placeHoldersv_candidatura_mapa;
$page_titles["v_candidatura_mapa"] = &$pageTitlesv_candidatura_mapa;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["v_candidatura_mapa"] = array();
//	cm3_mapa_municipio
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm3_mapa_municipio";
		$detailsParam["dOriginalTable"] = "eleccandidatura";
		$detailsParam["proceedLink"] = false;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm3_mapa_municipio";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm3_mapa_municipio");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["v_candidatura_mapa"][$dIndex] = $detailsParam;

	
		$detailsTablesData["v_candidatura_mapa"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["v_candidatura_mapa"][$dIndex]["masterKeys"][]="idElecCandidatura";

				$detailsTablesData["v_candidatura_mapa"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["v_candidatura_mapa"][$dIndex]["detailKeys"][]="idElecCandidatura";
//	cm3_mapa_distrito_madrid
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="cm3_mapa_distrito_madrid";
		$detailsParam["dOriginalTable"] = "candidatura";
		$detailsParam["proceedLink"] = false;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "cm3_mapa_distrito_madrid";
	$detailsParam["dCaptionTable"] = GetTableCaption("cm3_mapa_distrito_madrid");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

	$detailsParam["dispChildCount"] = "0";

		$detailsParam["hideChild"] = false;
						$detailsParam["previewOnList"] = "1";
		$detailsParam["previewOnAdd"] = 0;
		$detailsParam["previewOnEdit"] = 0;
		$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["v_candidatura_mapa"][$dIndex] = $detailsParam;

	
		$detailsTablesData["v_candidatura_mapa"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["v_candidatura_mapa"][$dIndex]["masterKeys"][]="idElecCandidatura";

				$detailsTablesData["v_candidatura_mapa"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["v_candidatura_mapa"][$dIndex]["detailKeys"][]="idElecCandidatura";

// tables which are master tables for current table (detail)
$masterTablesData["v_candidatura_mapa"] = array();


	
				$strOriginalDetailsTable="convocatoria";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm3_convocatoria";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm3_convocatoria";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
					
	$masterParams["previewOnList"]= 2;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["v_candidatura_mapa"][0] = $masterParams;
				$masterTablesData["v_candidatura_mapa"][0]["masterKeys"] = array();
	$masterTablesData["v_candidatura_mapa"][0]["masterKeys"][]="idConvocatoria";
				$masterTablesData["v_candidatura_mapa"][0]["detailKeys"] = array();
	$masterTablesData["v_candidatura_mapa"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_v_candidatura_mapa()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "idElecCandidatura,  	Convocatoria_idConvocatoria,  	Orden,  	idCandidatura,  	Codigo,  	Titulo,  	Color,  	Votos";
$proto0["m_strFrom"] = "FROM v_candidatura_mapa";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "order by Votos desc";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "idElecCandidatura",
	"m_strTable" => "v_candidatura_mapa",
	"m_srcTableName" => "v_candidatura_mapa"
));

$proto6["m_sql"] = "idElecCandidatura";
$proto6["m_srcTableName"] = "v_candidatura_mapa";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "v_candidatura_mapa",
	"m_srcTableName" => "v_candidatura_mapa"
));

$proto8["m_sql"] = "Convocatoria_idConvocatoria";
$proto8["m_srcTableName"] = "v_candidatura_mapa";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Orden",
	"m_strTable" => "v_candidatura_mapa",
	"m_srcTableName" => "v_candidatura_mapa"
));

$proto10["m_sql"] = "Orden";
$proto10["m_srcTableName"] = "v_candidatura_mapa";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "idCandidatura",
	"m_strTable" => "v_candidatura_mapa",
	"m_srcTableName" => "v_candidatura_mapa"
));

$proto12["m_sql"] = "idCandidatura";
$proto12["m_srcTableName"] = "v_candidatura_mapa";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "Codigo",
	"m_strTable" => "v_candidatura_mapa",
	"m_srcTableName" => "v_candidatura_mapa"
));

$proto14["m_sql"] = "Codigo";
$proto14["m_srcTableName"] = "v_candidatura_mapa";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "v_candidatura_mapa",
	"m_srcTableName" => "v_candidatura_mapa"
));

$proto16["m_sql"] = "Titulo";
$proto16["m_srcTableName"] = "v_candidatura_mapa";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "Color",
	"m_strTable" => "v_candidatura_mapa",
	"m_srcTableName" => "v_candidatura_mapa"
));

$proto18["m_sql"] = "Color";
$proto18["m_srcTableName"] = "v_candidatura_mapa";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "v_candidatura_mapa",
	"m_srcTableName" => "v_candidatura_mapa"
));

$proto20["m_sql"] = "Votos";
$proto20["m_srcTableName"] = "v_candidatura_mapa";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto22=array();
$proto22["m_link"] = "SQLL_MAIN";
			$proto23=array();
$proto23["m_strName"] = "v_candidatura_mapa";
$proto23["m_srcTableName"] = "v_candidatura_mapa";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "idElecCandidatura";
$proto23["m_columns"][] = "Convocatoria_idConvocatoria";
$proto23["m_columns"][] = "Orden";
$proto23["m_columns"][] = "idCandidatura";
$proto23["m_columns"][] = "Codigo";
$proto23["m_columns"][] = "Titulo";
$proto23["m_columns"][] = "Color";
$proto23["m_columns"][] = "Votos";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "v_candidatura_mapa";
$proto22["m_alias"] = "";
$proto22["m_srcTableName"] = "v_candidatura_mapa";
$proto24=array();
$proto24["m_sql"] = "";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto26=array();
						$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "v_candidatura_mapa",
	"m_srcTableName" => "v_candidatura_mapa"
));

$proto26["m_column"]=$obj;
$proto26["m_bAsc"] = 0;
$proto26["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto26);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="v_candidatura_mapa";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_v_candidatura_mapa = createSqlQuery_v_candidatura_mapa();


	
		;

								

$tdatav_candidatura_mapa[".sqlquery"] = $queryData_v_candidatura_mapa;

$tableEvents["v_candidatura_mapa"] = new eventsBase;
$tdatav_candidatura_mapa[".hasEvents"] = false;

?>