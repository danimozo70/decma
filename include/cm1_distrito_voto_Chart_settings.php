<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacm1_distrito_voto_Chart = array();
	$tdatacm1_distrito_voto_Chart[".ShortName"] = "cm1_distrito_voto_Chart";
	$tdatacm1_distrito_voto_Chart[".OwnerID"] = "";
	$tdatacm1_distrito_voto_Chart[".OriginalTable"] = "v_distrito_voto";

//	field labels
$fieldLabelscm1_distrito_voto_Chart = array();
$fieldToolTipscm1_distrito_voto_Chart = array();
$pageTitlescm1_distrito_voto_Chart = array();
$placeHolderscm1_distrito_voto_Chart = array();

if(mlang_getcurrentlang()=="Spanish")
{
	$fieldLabelscm1_distrito_voto_Chart["Spanish"] = array();
	$fieldToolTipscm1_distrito_voto_Chart["Spanish"] = array();
	$placeHolderscm1_distrito_voto_Chart["Spanish"] = array();
	$pageTitlescm1_distrito_voto_Chart["Spanish"] = array();
	$fieldLabelscm1_distrito_voto_Chart["Spanish"]["Convocatoria_idConvocatoria"] = "Convocatoria IdConvocatoria";
	$fieldToolTipscm1_distrito_voto_Chart["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$placeHolderscm1_distrito_voto_Chart["Spanish"]["Convocatoria_idConvocatoria"] = "";
	$fieldLabelscm1_distrito_voto_Chart["Spanish"]["Municipio"] = "Municipio";
	$fieldToolTipscm1_distrito_voto_Chart["Spanish"]["Municipio"] = "";
	$placeHolderscm1_distrito_voto_Chart["Spanish"]["Municipio"] = "";
	$fieldLabelscm1_distrito_voto_Chart["Spanish"]["Distrito"] = "Distrito";
	$fieldToolTipscm1_distrito_voto_Chart["Spanish"]["Distrito"] = "";
	$placeHolderscm1_distrito_voto_Chart["Spanish"]["Distrito"] = "";
	$fieldLabelscm1_distrito_voto_Chart["Spanish"]["idCandidatura"] = "Candidatura";
	$fieldToolTipscm1_distrito_voto_Chart["Spanish"]["idCandidatura"] = "";
	$placeHolderscm1_distrito_voto_Chart["Spanish"]["idCandidatura"] = "";
	$fieldLabelscm1_distrito_voto_Chart["Spanish"]["Codigo"] = "Codigo";
	$fieldToolTipscm1_distrito_voto_Chart["Spanish"]["Codigo"] = "";
	$placeHolderscm1_distrito_voto_Chart["Spanish"]["Codigo"] = "";
	$fieldLabelscm1_distrito_voto_Chart["Spanish"]["Titulo"] = "Titulo";
	$fieldToolTipscm1_distrito_voto_Chart["Spanish"]["Titulo"] = "";
	$placeHolderscm1_distrito_voto_Chart["Spanish"]["Titulo"] = "";
	$fieldLabelscm1_distrito_voto_Chart["Spanish"]["Color"] = "Color";
	$fieldToolTipscm1_distrito_voto_Chart["Spanish"]["Color"] = "";
	$placeHolderscm1_distrito_voto_Chart["Spanish"]["Color"] = "";
	$fieldLabelscm1_distrito_voto_Chart["Spanish"]["Logo"] = "Logo";
	$fieldToolTipscm1_distrito_voto_Chart["Spanish"]["Logo"] = "";
	$placeHolderscm1_distrito_voto_Chart["Spanish"]["Logo"] = "";
	$fieldLabelscm1_distrito_voto_Chart["Spanish"]["Votos"] = "Votos";
	$fieldToolTipscm1_distrito_voto_Chart["Spanish"]["Votos"] = "";
	$placeHolderscm1_distrito_voto_Chart["Spanish"]["Votos"] = "";
	$fieldLabelscm1_distrito_voto_Chart["Spanish"]["PorcVotos"] = "% Votos";
	$fieldToolTipscm1_distrito_voto_Chart["Spanish"]["PorcVotos"] = "";
	$placeHolderscm1_distrito_voto_Chart["Spanish"]["PorcVotos"] = "";
	if (count($fieldToolTipscm1_distrito_voto_Chart["Spanish"]))
		$tdatacm1_distrito_voto_Chart[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscm1_distrito_voto_Chart[""] = array();
	$fieldToolTipscm1_distrito_voto_Chart[""] = array();
	$placeHolderscm1_distrito_voto_Chart[""] = array();
	$pageTitlescm1_distrito_voto_Chart[""] = array();
	if (count($fieldToolTipscm1_distrito_voto_Chart[""]))
		$tdatacm1_distrito_voto_Chart[".isUseToolTips"] = true;
}


	$tdatacm1_distrito_voto_Chart[".NCSearch"] = true;

	$tdatacm1_distrito_voto_Chart[".ChartRefreshTime"] = 0;


$tdatacm1_distrito_voto_Chart[".shortTableName"] = "cm1_distrito_voto_Chart";
$tdatacm1_distrito_voto_Chart[".nSecOptions"] = 0;
$tdatacm1_distrito_voto_Chart[".recsPerRowPrint"] = 1;
$tdatacm1_distrito_voto_Chart[".mainTableOwnerID"] = "";
$tdatacm1_distrito_voto_Chart[".moveNext"] = 1;
$tdatacm1_distrito_voto_Chart[".entityType"] = 3;

$tdatacm1_distrito_voto_Chart[".strOriginalTableName"] = "v_distrito_voto";

	



$tdatacm1_distrito_voto_Chart[".showAddInPopup"] = false;

$tdatacm1_distrito_voto_Chart[".showEditInPopup"] = false;

$tdatacm1_distrito_voto_Chart[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacm1_distrito_voto_Chart[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacm1_distrito_voto_Chart[".fieldsForRegister"] = array();

$tdatacm1_distrito_voto_Chart[".listAjax"] = false;

	$tdatacm1_distrito_voto_Chart[".audit"] = false;

	$tdatacm1_distrito_voto_Chart[".locking"] = false;



$tdatacm1_distrito_voto_Chart[".list"] = true;



$tdatacm1_distrito_voto_Chart[".reorderRecordsByHeader"] = true;








$tdatacm1_distrito_voto_Chart[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatacm1_distrito_voto_Chart[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatacm1_distrito_voto_Chart[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatacm1_distrito_voto_Chart[".searchSaving"] = false;
//

$tdatacm1_distrito_voto_Chart[".showSearchPanel"] = true;
		$tdatacm1_distrito_voto_Chart[".flexibleSearch"] = true;

$tdatacm1_distrito_voto_Chart[".isUseAjaxSuggest"] = true;






$tdatacm1_distrito_voto_Chart[".ajaxCodeSnippetAdded"] = false;

$tdatacm1_distrito_voto_Chart[".buttonsAdded"] = false;

$tdatacm1_distrito_voto_Chart[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacm1_distrito_voto_Chart[".isUseTimeForSearch"] = false;



$tdatacm1_distrito_voto_Chart[".badgeColor"] = "e07878";


$tdatacm1_distrito_voto_Chart[".allSearchFields"] = array();
$tdatacm1_distrito_voto_Chart[".filterFields"] = array();
$tdatacm1_distrito_voto_Chart[".requiredSearchFields"] = array();

$tdatacm1_distrito_voto_Chart[".allSearchFields"][] = "Convocatoria_idConvocatoria";
	$tdatacm1_distrito_voto_Chart[".allSearchFields"][] = "Municipio";
	$tdatacm1_distrito_voto_Chart[".allSearchFields"][] = "Distrito";
	$tdatacm1_distrito_voto_Chart[".allSearchFields"][] = "idCandidatura";
	$tdatacm1_distrito_voto_Chart[".allSearchFields"][] = "Codigo";
	$tdatacm1_distrito_voto_Chart[".allSearchFields"][] = "Titulo";
	$tdatacm1_distrito_voto_Chart[".allSearchFields"][] = "Color";
	$tdatacm1_distrito_voto_Chart[".allSearchFields"][] = "Logo";
	$tdatacm1_distrito_voto_Chart[".allSearchFields"][] = "Votos";
	$tdatacm1_distrito_voto_Chart[".allSearchFields"][] = "PorcVotos";
	

$tdatacm1_distrito_voto_Chart[".googleLikeFields"] = array();
$tdatacm1_distrito_voto_Chart[".googleLikeFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".googleLikeFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".googleLikeFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".googleLikeFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".googleLikeFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".googleLikeFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".googleLikeFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".googleLikeFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".googleLikeFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".googleLikeFields"][] = "PorcVotos";


$tdatacm1_distrito_voto_Chart[".advSearchFields"] = array();
$tdatacm1_distrito_voto_Chart[".advSearchFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".advSearchFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".advSearchFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".advSearchFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".advSearchFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".advSearchFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".advSearchFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".advSearchFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".advSearchFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".advSearchFields"][] = "PorcVotos";

$tdatacm1_distrito_voto_Chart[".tableType"] = "chart";

$tdatacm1_distrito_voto_Chart[".printerPageOrientation"] = 0;
$tdatacm1_distrito_voto_Chart[".nPrinterPageScale"] = 100;

$tdatacm1_distrito_voto_Chart[".nPrinterSplitRecords"] = 40;

$tdatacm1_distrito_voto_Chart[".nPrinterPDFSplitRecords"] = 40;



$tdatacm1_distrito_voto_Chart[".geocodingEnabled"] = false;



// chart settings
$tdatacm1_distrito_voto_Chart[".chartType"] = "2DBar";
// end of chart settings


$tdatacm1_distrito_voto_Chart[".listGridLayout"] = 3;





// view page pdf

// print page pdf



$tstrOrderBy = "order by sum(PorcVotos) desc";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacm1_distrito_voto_Chart[".strOrderBy"] = $tstrOrderBy;

$tdatacm1_distrito_voto_Chart[".orderindexes"] = array();

$tdatacm1_distrito_voto_Chart[".sqlHead"] = "SELECT Convocatoria_idConvocatoria,  	Municipio,  	Distrito,  	idCandidatura,  	Codigo,  	Titulo,  	Color,  	Logo,  	sum(Votos) Votos,  	sum(PorcVotos) PorcVotos";
$tdatacm1_distrito_voto_Chart[".sqlFrom"] = "FROM v_distrito_voto";
$tdatacm1_distrito_voto_Chart[".sqlWhereExpr"] = "";
$tdatacm1_distrito_voto_Chart[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacm1_distrito_voto_Chart[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacm1_distrito_voto_Chart[".arrGroupsPerPage"] = $arrGPP;

$tdatacm1_distrito_voto_Chart[".highlightSearchResults"] = true;

$tableKeyscm1_distrito_voto_Chart = array();
$tdatacm1_distrito_voto_Chart[".Keys"] = $tableKeyscm1_distrito_voto_Chart;

$tdatacm1_distrito_voto_Chart[".listFields"] = array();
$tdatacm1_distrito_voto_Chart[".listFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".listFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".listFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".listFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".listFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".listFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".listFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".listFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".listFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".listFields"][] = "PorcVotos";

$tdatacm1_distrito_voto_Chart[".hideMobileList"] = array();


$tdatacm1_distrito_voto_Chart[".viewFields"] = array();
$tdatacm1_distrito_voto_Chart[".viewFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".viewFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".viewFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".viewFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".viewFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".viewFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".viewFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".viewFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".viewFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".viewFields"][] = "PorcVotos";

$tdatacm1_distrito_voto_Chart[".addFields"] = array();
$tdatacm1_distrito_voto_Chart[".addFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".addFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".addFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".addFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".addFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".addFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".addFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".addFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".addFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".addFields"][] = "PorcVotos";

$tdatacm1_distrito_voto_Chart[".masterListFields"] = array();
$tdatacm1_distrito_voto_Chart[".masterListFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".masterListFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".masterListFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".masterListFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".masterListFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".masterListFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".masterListFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".masterListFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".masterListFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".masterListFields"][] = "PorcVotos";

$tdatacm1_distrito_voto_Chart[".inlineAddFields"] = array();
$tdatacm1_distrito_voto_Chart[".inlineAddFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".inlineAddFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".inlineAddFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".inlineAddFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".inlineAddFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".inlineAddFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".inlineAddFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".inlineAddFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".inlineAddFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".inlineAddFields"][] = "PorcVotos";

$tdatacm1_distrito_voto_Chart[".editFields"] = array();
$tdatacm1_distrito_voto_Chart[".editFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".editFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".editFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".editFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".editFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".editFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".editFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".editFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".editFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".editFields"][] = "PorcVotos";

$tdatacm1_distrito_voto_Chart[".inlineEditFields"] = array();
$tdatacm1_distrito_voto_Chart[".inlineEditFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".inlineEditFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".inlineEditFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".inlineEditFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".inlineEditFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".inlineEditFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".inlineEditFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".inlineEditFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".inlineEditFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".inlineEditFields"][] = "PorcVotos";

$tdatacm1_distrito_voto_Chart[".updateSelectedFields"] = array();
$tdatacm1_distrito_voto_Chart[".updateSelectedFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".updateSelectedFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".updateSelectedFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".updateSelectedFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".updateSelectedFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".updateSelectedFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".updateSelectedFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".updateSelectedFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".updateSelectedFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".updateSelectedFields"][] = "PorcVotos";


$tdatacm1_distrito_voto_Chart[".exportFields"] = array();
$tdatacm1_distrito_voto_Chart[".exportFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".exportFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".exportFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".exportFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".exportFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".exportFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".exportFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".exportFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".exportFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".exportFields"][] = "PorcVotos";

$tdatacm1_distrito_voto_Chart[".importFields"] = array();
$tdatacm1_distrito_voto_Chart[".importFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".importFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".importFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".importFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".importFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".importFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".importFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".importFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".importFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".importFields"][] = "PorcVotos";

$tdatacm1_distrito_voto_Chart[".printFields"] = array();
$tdatacm1_distrito_voto_Chart[".printFields"][] = "Convocatoria_idConvocatoria";
$tdatacm1_distrito_voto_Chart[".printFields"][] = "Municipio";
$tdatacm1_distrito_voto_Chart[".printFields"][] = "Distrito";
$tdatacm1_distrito_voto_Chart[".printFields"][] = "idCandidatura";
$tdatacm1_distrito_voto_Chart[".printFields"][] = "Codigo";
$tdatacm1_distrito_voto_Chart[".printFields"][] = "Titulo";
$tdatacm1_distrito_voto_Chart[".printFields"][] = "Color";
$tdatacm1_distrito_voto_Chart[".printFields"][] = "Logo";
$tdatacm1_distrito_voto_Chart[".printFields"][] = "Votos";
$tdatacm1_distrito_voto_Chart[".printFields"][] = "PorcVotos";


//	Convocatoria_idConvocatoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "Convocatoria_idConvocatoria";
	$fdata["GoodName"] = "Convocatoria_idConvocatoria";
	$fdata["ownerTable"] = "v_distrito_voto";
	$fdata["Label"] = GetFieldLabel("cm1_distrito_voto_Chart","Convocatoria_idConvocatoria");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Convocatoria_idConvocatoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Convocatoria_idConvocatoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_distrito_voto_Chart["Convocatoria_idConvocatoria"] = $fdata;
//	Municipio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Municipio";
	$fdata["GoodName"] = "Municipio";
	$fdata["ownerTable"] = "v_distrito_voto";
	$fdata["Label"] = GetFieldLabel("cm1_distrito_voto_Chart","Municipio");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Municipio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Municipio";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_distrito_voto_Chart["Municipio"] = $fdata;
//	Distrito
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Distrito";
	$fdata["GoodName"] = "Distrito";
	$fdata["ownerTable"] = "v_distrito_voto";
	$fdata["Label"] = GetFieldLabel("cm1_distrito_voto_Chart","Distrito");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Distrito";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Distrito";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_distrito_voto_Chart["Distrito"] = $fdata;
//	idCandidatura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "idCandidatura";
	$fdata["GoodName"] = "idCandidatura";
	$fdata["ownerTable"] = "v_distrito_voto";
	$fdata["Label"] = GetFieldLabel("cm1_distrito_voto_Chart","idCandidatura");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idCandidatura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idCandidatura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_distrito_voto_Chart["idCandidatura"] = $fdata;
//	Codigo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Codigo";
	$fdata["GoodName"] = "Codigo";
	$fdata["ownerTable"] = "v_distrito_voto";
	$fdata["Label"] = GetFieldLabel("cm1_distrito_voto_Chart","Codigo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Codigo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Codigo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_distrito_voto_Chart["Codigo"] = $fdata;
//	Titulo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Titulo";
	$fdata["GoodName"] = "Titulo";
	$fdata["ownerTable"] = "v_distrito_voto";
	$fdata["Label"] = GetFieldLabel("cm1_distrito_voto_Chart","Titulo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Titulo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Titulo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_distrito_voto_Chart["Titulo"] = $fdata;
//	Color
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Color";
	$fdata["GoodName"] = "Color";
	$fdata["ownerTable"] = "v_distrito_voto";
	$fdata["Label"] = GetFieldLabel("cm1_distrito_voto_Chart","Color");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Color";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Color";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_distrito_voto_Chart["Color"] = $fdata;
//	Logo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "Logo";
	$fdata["GoodName"] = "Logo";
	$fdata["ownerTable"] = "v_distrito_voto";
	$fdata["Label"] = GetFieldLabel("cm1_distrito_voto_Chart","Logo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Logo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "Logo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=500";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_distrito_voto_Chart["Logo"] = $fdata;
//	Votos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "Votos";
	$fdata["GoodName"] = "Votos";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("cm1_distrito_voto_Chart","Votos");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "Votos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sum(Votos)";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_distrito_voto_Chart["Votos"] = $fdata;
//	PorcVotos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "PorcVotos";
	$fdata["GoodName"] = "PorcVotos";
	$fdata["ownerTable"] = "";
	$fdata["Label"] = GetFieldLabel("cm1_distrito_voto_Chart","PorcVotos");
	$fdata["FieldType"] = 14;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "PorcVotos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "sum(PorcVotos)";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["chart"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
	
	
	
	
	$fdata["EditFormats"]["search"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatacm1_distrito_voto_Chart["PorcVotos"] = $fdata;

	$tdatacm1_distrito_voto_Chart[".chartXml"] = '<chart>
		<attr value="tables">
			<attr value="0">cm1_distrito_voto Chart</attr>
		</attr>
		<attr value="chart_type">
			<attr value="type">2d_bar</attr>
		</attr>

		<attr value="parameters">';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="0">
			<attr value="name">PorcVotos</attr>';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '</attr>';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="1">
		<attr value="name">Codigo</attr>
	</attr>';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '</attr>
			<attr value="appearance">';


	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="head">'.xmlencode("Gráfico por % de Voto").'</attr>
<attr value="foot">'.xmlencode("Candidaturas").'</attr>
<attr value="y_axis_label">'.xmlencode("Convocatoria_idConvocatoria").'</attr>


<attr value="slegend">false</attr>
<attr value="sgrid">true</attr>
<attr value="sname">true</attr>
<attr value="sval">true</attr>
<attr value="sanim">true</attr>
<attr value="sstacked">false</attr>
<attr value="slog">false</attr>
<attr value="aqua">0</attr>
<attr value="cview">0</attr>
<attr value="is3d">0</attr>
<attr value="isstacked">0</attr>
<attr value="linestyle">0</attr>
<attr value="autoupdate">0</attr>
<attr value="autoupmin">60</attr>';
$tdatacm1_distrito_voto_Chart[".chartXml"] .= '</attr>

<attr value="fields">';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="0">
		<attr value="name">Convocatoria_idConvocatoria</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_distrito_voto_Chart","Convocatoria_idConvocatoria")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="1">
		<attr value="name">Municipio</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_distrito_voto_Chart","Municipio")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="2">
		<attr value="name">Distrito</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_distrito_voto_Chart","Distrito")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="3">
		<attr value="name">idCandidatura</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_distrito_voto_Chart","idCandidatura")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="4">
		<attr value="name">Codigo</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_distrito_voto_Chart","Codigo")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="5">
		<attr value="name">Titulo</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_distrito_voto_Chart","Titulo")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="6">
		<attr value="name">Color</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_distrito_voto_Chart","Color")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="7">
		<attr value="name">Logo</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_distrito_voto_Chart","Logo")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="8">
		<attr value="name">Votos</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_distrito_voto_Chart","Votos")).'</attr>
		<attr value="search"></attr>
	</attr>';
	$tdatacm1_distrito_voto_Chart[".chartXml"] .= '<attr value="9">
		<attr value="name">PorcVotos</attr>
		<attr value="label">'.xmlencode(GetFieldLabel("cm1_distrito_voto_Chart","PorcVotos")).'</attr>
		<attr value="search"></attr>
	</attr>';
$tdatacm1_distrito_voto_Chart[".chartXml"] .= '</attr>


<attr value="settings">
<attr value="name">cm1_distrito_voto Chart</attr>
<attr value="short_table_name">cm1_distrito_voto_Chart</attr>
</attr>

</chart>';

$tables_data["cm1_distrito_voto Chart"]=&$tdatacm1_distrito_voto_Chart;
$field_labels["cm1_distrito_voto_Chart"] = &$fieldLabelscm1_distrito_voto_Chart;
$fieldToolTips["cm1_distrito_voto_Chart"] = &$fieldToolTipscm1_distrito_voto_Chart;
$placeHolders["cm1_distrito_voto_Chart"] = &$placeHolderscm1_distrito_voto_Chart;
$page_titles["cm1_distrito_voto_Chart"] = &$pageTitlescm1_distrito_voto_Chart;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["cm1_distrito_voto Chart"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["cm1_distrito_voto Chart"] = array();


	
				$strOriginalDetailsTable="v_elec_distri";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="v_elec_distri";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "v_elec_distri";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= 0;
	$masterParams["hideChild"]= 0;
	$masterParams["dispMasterInfo"] = array();
					$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 2;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 1;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm1_distrito_voto Chart"][0] = $masterParams;
				$masterTablesData["cm1_distrito_voto Chart"][0]["masterKeys"] = array();
	$masterTablesData["cm1_distrito_voto Chart"][0]["masterKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["cm1_distrito_voto Chart"][0]["masterKeys"][]="NumeroMunicipio";
				$masterTablesData["cm1_distrito_voto Chart"][0]["masterKeys"][]="NumeroDistrito";
				$masterTablesData["cm1_distrito_voto Chart"][0]["detailKeys"] = array();
	$masterTablesData["cm1_distrito_voto Chart"][0]["detailKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["cm1_distrito_voto Chart"][0]["detailKeys"][]="Municipio";
				$masterTablesData["cm1_distrito_voto Chart"][0]["detailKeys"][]="Distrito";
		
	
				$strOriginalDetailsTable="v_elec_distri";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="cm1_1_v_elec_distri";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "cm1_1_v_elec_distri";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	
		$masterParams["dispChildCount"]= "0";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
					$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
		
	$masterParams["previewOnList"]= 2;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 1;
	$masterParams["proceedLink"]= 0;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["cm1_distrito_voto Chart"][1] = $masterParams;
				$masterTablesData["cm1_distrito_voto Chart"][1]["masterKeys"] = array();
	$masterTablesData["cm1_distrito_voto Chart"][1]["masterKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["cm1_distrito_voto Chart"][1]["masterKeys"][]="NumeroMunicipio";
				$masterTablesData["cm1_distrito_voto Chart"][1]["masterKeys"][]="NumeroDistrito";
				$masterTablesData["cm1_distrito_voto Chart"][1]["detailKeys"] = array();
	$masterTablesData["cm1_distrito_voto Chart"][1]["detailKeys"][]="Convocatoria_idConvocatoria";
				$masterTablesData["cm1_distrito_voto Chart"][1]["detailKeys"][]="Municipio";
				$masterTablesData["cm1_distrito_voto Chart"][1]["detailKeys"][]="Distrito";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_cm1_distrito_voto_Chart()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "Convocatoria_idConvocatoria,  	Municipio,  	Distrito,  	idCandidatura,  	Codigo,  	Titulo,  	Color,  	Logo,  	sum(Votos) Votos,  	sum(PorcVotos) PorcVotos";
$proto0["m_strFrom"] = "FROM v_distrito_voto";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "order by sum(PorcVotos) desc";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "Convocatoria_idConvocatoria",
	"m_strTable" => "v_distrito_voto",
	"m_srcTableName" => "cm1_distrito_voto Chart"
));

$proto6["m_sql"] = "Convocatoria_idConvocatoria";
$proto6["m_srcTableName"] = "cm1_distrito_voto Chart";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Municipio",
	"m_strTable" => "v_distrito_voto",
	"m_srcTableName" => "cm1_distrito_voto Chart"
));

$proto8["m_sql"] = "Municipio";
$proto8["m_srcTableName"] = "cm1_distrito_voto Chart";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Distrito",
	"m_strTable" => "v_distrito_voto",
	"m_srcTableName" => "cm1_distrito_voto Chart"
));

$proto10["m_sql"] = "Distrito";
$proto10["m_srcTableName"] = "cm1_distrito_voto Chart";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "idCandidatura",
	"m_strTable" => "v_distrito_voto",
	"m_srcTableName" => "cm1_distrito_voto Chart"
));

$proto12["m_sql"] = "idCandidatura";
$proto12["m_srcTableName"] = "cm1_distrito_voto Chart";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "Codigo",
	"m_strTable" => "v_distrito_voto",
	"m_srcTableName" => "cm1_distrito_voto Chart"
));

$proto14["m_sql"] = "Codigo";
$proto14["m_srcTableName"] = "cm1_distrito_voto Chart";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Titulo",
	"m_strTable" => "v_distrito_voto",
	"m_srcTableName" => "cm1_distrito_voto Chart"
));

$proto16["m_sql"] = "Titulo";
$proto16["m_srcTableName"] = "cm1_distrito_voto Chart";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "Color",
	"m_strTable" => "v_distrito_voto",
	"m_srcTableName" => "cm1_distrito_voto Chart"
));

$proto18["m_sql"] = "Color";
$proto18["m_srcTableName"] = "cm1_distrito_voto Chart";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "Logo",
	"m_strTable" => "v_distrito_voto",
	"m_srcTableName" => "cm1_distrito_voto Chart"
));

$proto20["m_sql"] = "Logo";
$proto20["m_srcTableName"] = "cm1_distrito_voto Chart";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$proto23=array();
$proto23["m_functiontype"] = "SQLF_SUM";
$proto23["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "Votos",
	"m_strTable" => "v_distrito_voto",
	"m_srcTableName" => "cm1_distrito_voto Chart"
));

$proto23["m_arguments"][]=$obj;
$proto23["m_strFunctionName"] = "sum";
$obj = new SQLFunctionCall($proto23);

$proto22["m_sql"] = "sum(Votos)";
$proto22["m_srcTableName"] = "cm1_distrito_voto Chart";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "Votos";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto25=array();
			$proto26=array();
$proto26["m_functiontype"] = "SQLF_SUM";
$proto26["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "PorcVotos",
	"m_strTable" => "v_distrito_voto",
	"m_srcTableName" => "cm1_distrito_voto Chart"
));

$proto26["m_arguments"][]=$obj;
$proto26["m_strFunctionName"] = "sum";
$obj = new SQLFunctionCall($proto26);

$proto25["m_sql"] = "sum(PorcVotos)";
$proto25["m_srcTableName"] = "cm1_distrito_voto Chart";
$proto25["m_expr"]=$obj;
$proto25["m_alias"] = "PorcVotos";
$obj = new SQLFieldListItem($proto25);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto28=array();
$proto28["m_link"] = "SQLL_MAIN";
			$proto29=array();
$proto29["m_strName"] = "v_distrito_voto";
$proto29["m_srcTableName"] = "cm1_distrito_voto Chart";
$proto29["m_columns"] = array();
$proto29["m_columns"][] = "Convocatoria_idConvocatoria";
$proto29["m_columns"][] = "Municipio";
$proto29["m_columns"][] = "Distrito";
$proto29["m_columns"][] = "idCandidatura";
$proto29["m_columns"][] = "Codigo";
$proto29["m_columns"][] = "Titulo";
$proto29["m_columns"][] = "Color";
$proto29["m_columns"][] = "Logo";
$proto29["m_columns"][] = "Votos";
$proto29["m_columns"][] = "PorcVotos";
$obj = new SQLTable($proto29);

$proto28["m_table"] = $obj;
$proto28["m_sql"] = "v_distrito_voto";
$proto28["m_alias"] = "";
$proto28["m_srcTableName"] = "cm1_distrito_voto Chart";
$proto30=array();
$proto30["m_sql"] = "";
$proto30["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto30["m_column"]=$obj;
$proto30["m_contained"] = array();
$proto30["m_strCase"] = "";
$proto30["m_havingmode"] = false;
$proto30["m_inBrackets"] = false;
$proto30["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto30);

$proto28["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto28);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
												$proto32=array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "1"
));

$proto32["m_column"]=$obj;
$obj = new SQLGroupByItem($proto32);

$proto0["m_groupby"][]=$obj;
												$proto34=array();
						$obj = new SQLNonParsed(array(
	"m_sql" => "4"
));

$proto34["m_column"]=$obj;
$obj = new SQLGroupByItem($proto34);

$proto0["m_groupby"][]=$obj;
$proto0["m_orderby"] = array();
												$proto36=array();
						$proto37=array();
$proto37["m_functiontype"] = "SQLF_SUM";
$proto37["m_arguments"] = array();
						$obj = new SQLField(array(
	"m_strName" => "PorcVotos",
	"m_strTable" => "v_distrito_voto",
	"m_srcTableName" => "cm1_distrito_voto Chart"
));

$proto37["m_arguments"][]=$obj;
$proto37["m_strFunctionName"] = "sum";
$obj = new SQLFunctionCall($proto37);

$proto36["m_column"]=$obj;
$proto36["m_bAsc"] = 0;
$proto36["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto36);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="cm1_distrito_voto Chart";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_cm1_distrito_voto_Chart = createSqlQuery_cm1_distrito_voto_Chart();


	
		;

										

$tdatacm1_distrito_voto_Chart[".sqlquery"] = $queryData_cm1_distrito_voto_Chart;

include_once(getabspath("include/cm1_distrito_voto_Chart_events.php"));
$tableEvents["cm1_distrito_voto Chart"] = new eventclass_cm1_distrito_voto_Chart;
$tdatacm1_distrito_voto_Chart[".hasEvents"] = true;

?>