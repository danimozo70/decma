<?php
$Convocatoria=$data["idConvocatoria"];
$TituloConvocatoria=$data["Titulo"];

$grafico01 = "
	    <!-- Styles -->
<style>
#chartdiv {
  width:  400px;
  height: 150px;
  font-size: 10px;
}

.amcharts-pie-slice {
  transform: scale(1);
  transform-origin: 50% 50%;
  transition-duration: 0.3s;
  transition: all .3s ease-out;
  -webkit-transition: all .3s ease-out;
  -moz-transition: all .3s ease-out;
  -o-transition: all .3s ease-out;
  cursor: pointer;
  box-shadow: 0 0 30px 0 #000;
}

.amcharts-pie-slice:hover {
  transform: scale(1.1);
  filter: url(#shadow);
}							
</style>
<!-- Styles -->


<!-- Resources -->
<script src=\"amcharts/amcharts.js\"></script>
<script src=\"amcharts/pie.js\"></script>
<script src=\"amcharts/light.js\"></script>


<!-- Chart code -->
<script>
/**
 * Define source data
 */
 var chartData = [{";

$grafico02='';

$strSQLExists1 = "SELECT Codigo, Titulo, Color, Votos, PorcVotos, Escanos
FROM escanos join candidatura on (Candidatura_idCandidatura = idCandidatura)
where Escanos <> 0 and Convocatoria_idConvocatoria = $Convocatoria 
order by Escanos desc";

$rsExists1 = db_query($strSQLExists1,$conn);

	while ($PETICION = db_fetch_array($rsExists1)) {
		$Codigo=$PETICION["Codigo"];
		$Color=$PETICION["Color"];
		$Votos=$PETICION["Votos"];
		$PorcVotos=$PETICION["PorcVotos"];
		$Escanos=$PETICION["Escanos"];
		$grafico02.="    \"partido\": \"$Codigo\",
    \"escanos\": $Escanos,
	\"color\": \"$Color\",
	\"porcvotos\": \"$PorcVotos\"
  }, {";

	}
$grafico02 = substr($grafico02, 0, -5) . "} ];"; 

$strSQLExists1 = "SELECT Convocatoria_idConvocatoria, sum(Escanos) TotalEscanos FROM escanos
where Convocatoria_idConvocatoria = $Convocatoria
group by 1";

$rsExists1 = db_query($strSQLExists1,$conn);
$PETICION = db_fetch_array($rsExists1);
$TotalEscanos= $PETICION["TotalEscanos"];

$grafico03= "  
/**
 * Add a 50% slice
 */

var sum = 0;
for ( var x in chartData ) {
  sum += chartData[x].escanos;
}
chartData.push({
  \"escanos\": sum,
  \"alpha\": 0
});


var chart = AmCharts.makeChart(\"chartdiv\", {
  \"type\": \"pie\",
  
  \"marginTop\": 20,
  \"marginBottom\": 20,
  \"marginLeft\": 10,
  \"marginRight\": 10,
  \"pullOutOnlyOne\": true,
  \"outlineThickness\": 0, // Border de la tarta

  
  \"startAngle\": 0,
  \"radius\": \"90%\",
  \"pieY\": \"100%\",
  \"dataProvider\": chartData,
//  \"labelText\": \"[[title]]: [[value]]\",
	\"labelText\": \"[[title]]\",
//	\"balloonText\": \"[[title]]: [[percents]]% ([[value]]) [[description]]\",
	\"balloonText\": \"[[title]]: [[value]] Escaños, [[description]]% de votos válidos\",

  \"labelRadius\": 5,
//  \"labelTickAlpha\": 0,
//  \"labelTickColor\": \"#FFFFFF\",
	\"decimalSeparator\": \",\",
	\"fontSize\": 10,
  
   \"balloon\": {
	\"fillAlpha\": 1,
    \"fillColor\": \"#FFFFFF\"
  },

/* 
  \"titles\": [
		{
			\"text\": \"Chart Title\",
			\"size\": 8,
			\"color\": \"#0060AF\"
		}
	],
*/
  \"allLabels\": [{
    \"y\": \"85%\",
    \"align\": \"center\",
    \"size\": 12,
    \"bold\": true,
    \"text\": \"$TotalEscanos\",
    \"color\": \"#0060AF\"
  }, {
    \"y\": \"75%\",
    \"align\": \"center\",
    \"size\": 10,
    \"text\": \"Escaños\",
    \"color\": \"#0060AF\"
  }],
 
  \"startDuration\": 0.5,
  \"startEffect\": \"easeOutSine\",
   \"theme\": \"light\",
  \"addClassNames\": true,
 
  \"innerRadius\": \"50%\",
  \"defs\": {
    \"filter\": [{
      \"id\": \"shadow\",
      \"width\": \"200%\",
      \"height\": \"200%\",
      \"feOffset\": {
        \"result\": \"offOut\",
        \"in\": \"SourceAlpha\",
        \"dx\": 0,
        \"dy\": 0
      },
      \"feGaussianBlur\": {
        \"result\": \"blurOut\",
        \"in\": \"offOut\",
        \"stdDeviation\": 5
      },
      \"feBlend\": {
        \"in\": \"SourceGraphic\",
        \"in2\": \"blurOut\",
        \"mode\": \"normal\"
      }
    }]
  },
  \"valueField\": \"escanos\",
  \"titleField\": \"partido\",
  \"colorField\": \"color\",
  \"alphaField\": \"alpha\",
  \"descriptionField\": \"porcvotos\",
  \"export\": {
    \"enabled\": true
  }
});

chart.addListener(\"init\", handleInit);

chart.addListener(\"rollOverSlice\", function(e) {
  handleRollOver(e);
});

function handleInit(){
  chart.legend.addListener(\"rollOverItem\", handleRollOver);
}

function handleRollOver(e){
  var wedge = e.dataItem.wedge.node;
  wedge.parentNode.appendChild(wedge);
}
</script>

<!-- HTML -->
<div id=\"chartdiv\"></div>	";
$value=$grafico01.$grafico02.$grafico03;
?>