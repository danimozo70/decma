<?php
$Convocatoria=$data["idConvocatoria"];
$DescConvocatoria=$data['Descripcion'];
$Color='red';
$grafico01 = "
	    <style>
      html, body, #container2 {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
    <div id='container2'></div>
		<script src='anychart/anychart-core.min.js' type='text/javascript'></script>
		<script src='anychart/anychart-map.min.js' type='text/javascript'></script>
		<script src='anychart/DistritosMadrid.js' type='text/javascript'></script>
    <script>
    	anychart.onDocumentReady(function () {
        var dataSet = [";
$grafico02='';
$strSQLExists1 = "SELECT
v.Convocatoria_idConvocatoria,
lpad(v.Distrito,2,'0') Distrito,
m.NombreDistrito,
v.idCandidatura,
v.Codigo,
v.Titulo,
v.Color,
v.Votos,
v.PorcVotos,
e.censo,
e.Validos,
e.Blanco,
e.Nulo
FROM v_distrito_voto v
join
(select Convocatoria_idConvocatoria,
Distrito,
max(Votos) votos
from v_distrito_voto m
group by Convocatoria_idConvocatoria,Distrito ) as ma
on (
v.Convocatoria_idConvocatoria = ma.Convocatoria_idConvocatoria and
v.Distrito =ma.Distrito and
v.Votos = ma.Votos)
left join distrito m on m.NumeroDistrito = v.Distrito
left join elecdistritotal e on v.Distrito= e.Distrito and v.Convocatoria_idConvocatoria = e.Convocatoria_idConvocatoria
where v.Convocatoria_idConvocatoria=$Convocatoria";
$aux=array();
$rsExists1 = db_query($strSQLExists1,$conn);
$paleta=[];
	while ($PETICION = db_fetch_array($rsExists1)) {
		$paleta[$PETICION["idCandidatura"]]=$PETICION['Color'];
		$aux[]="{'id':'".$PETICION["Distrito"]."', 'value':".$PETICION["idCandidatura"].", 'Titulo':'".$PETICION['Titulo']."', 'name':'".$PETICION["NombreDistrito"].
		"', 'voto':".$PETICION["Votos"].", 'PorcVotos':".$PETICION['PorcVotos'].", 'censo':".$PETICION["censo"].", 'valido':".$PETICION["Validos"].", 'blanco':".$PETICION["Blanco"].", 'nulo':".$PETICION["Nulo"]."}";
	}
$grafico02=implode(",",$aux);
$grafico03= "        ];
		// set map Geo data
		var map = anychart.map();
		var geojson = JSON.parse(DistritosMadrid); 
		map.geoData(geojson);		
	  // set map title settings using html
	  map.title()
		.enabled(true)
		.useHtml(true)
		.padding(10, 0)
		.hAlign('center')
		.fontFamily(\"'Verdana', Helvetica, Arial, sans-serif\")
		.text(
		  '<span style=\"color:#7c868e; font-size: 18px\">$DescConvocatoria</span>'
		);
	  // sets credits settings
	  map.credits()
		.enabled(true)
		.url('https://en.wikipedia.org/wiki/United_States_presidential_election,_2008')
		.text('Data source: https://en.wikipedia.org/wiki/United_States_presidential_election,_2008');		
		
		// Barra de colores
		var colorRange = map.colorRange();
		colorRange.enabled(true)
			.padding([20, 0, 0, 0])
			.colorLineSize(10)
			.stroke('#B9B9B9')
			.labels({
				'padding': 3
			})
			.labels({
				'size': 7
			});
		colorRange.ticks()
			.enabled(true)
			.stroke('#B9B9B9')
			.position('outside')
			.length(10);
		colorRange.minorTicks()
			.enabled(true)
			.stroke('#B9B9B9')
			.position('outside')
			.length(5);

        // set the series
			
		var colorScale = anychart.scales.linearColor([\"#FFFFFF\", \"$Color\"]);	
		var series = map.choropleth(dataSet);
		series.colorScale(colorScale);

		// map.colorRange(true);
  
		// líneas de division
		series.stroke('0.5 #FFF');
		series.hovered()
			.fill('#EAFE5F')
			.stroke(anychart.color.darken('#EAFE5F'));
		series.selected()
			.fill('#EAFE5F')
			.stroke(anychart.color.darken('#EAFE5F'));
		series.labels()
			.enabled(true)
			.fontSize(10)
			.fontColor('#212121')
			.format('{%value}');

		series.tooltip()
			.useHtml(true)
			.format(function() {
				return '<span style=\"color: #d9d9d9\">Partido mas votado</span>: ' +
					(this.getData('Titulo')) + ' <br/>' +
					'<span style=\"color: #d9d9d9\">Votos Recibidos: </span> ' +
					parseInt(this.getData('voto')).toLocaleString() + '<br/>' +
					'<span style=\"color: #d9d9d9\">% Votos:</span> ' +
					this.getData('PorcVotos') + '<br/>' +
					'<span style=\"color: #d9d9d9\">Censo:</span> ' +
					parseInt(this.getData('censo')).toLocaleString() + '<br/>' +
					'<span style=\"color: #d9d9d9\">Votos Válidos:</span> ' +
					parseInt(this.getData('valido')).toLocaleString() + '<br/>' +
					'<span style=\"color: #d9d9d9\">Votos en Blanco:</span> ' +
					parseInt(this.getData('blanco')).toLocaleString() + '<br/>' +
					'<span style=\"color: #d9d9d9\">Votos Nulos:</span> ' +
					parseInt(this.getData('nulo')).toLocaleString() + '' +
				'';
			});			
		
		// disable labels
		series.labels(false);";
$aux=[];
foreach ($paleta as $key=>$value){
	$aux[]="{from:".$key.", to:".$key.",color:'".$value."'}";
}
$grafico03.="series.colorScale(anychart.scales.ordinalColor([".implode(",\n",$aux)."]));map.colorRange(false);";

$grafico03 .="
				map.container('container2');
        map.draw();
      });
    </script>
  </body>
</html>";
$value=$grafico01.$grafico02.$grafico03;
// $value="Datos de convocatoria $Convocatoria";
?>
