<?php
$Convocatoria=$data["Descripcion"];
$Codigo=$data["Codigo"];
$Candidatura=$data["Titulo"];
$Color=$data["Color"];
$IdConvocatoria=$data["Convocatoria_idConvocatoria"]; 
$IdCandidatura=$data["idCandidatura"];

$grafico01 = "
	    <style>
      html, body, #container {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
    <div id=\"container\"></div>
		<script src=\"anychart/anychart-core.min.js\" type=\"text/javascript\"></script>
    <script src=\"anychart/anychart-map.min.js\" type=\"text/javascript\"></script>
    <script src=\"anychart/MunicipiosMadrid.js\" type=\"text/javascript\"></script>

    <script>
    	anychart.onDocumentReady(function () {
        var dataSet = [";

$primero=true; // centinela de 1º item del bucle
$grafico02='';

$strSQLExists1 = "SELECT
mu.`Convocatoria_idConvocatoria`,
mu.`Candidatura_idCandidatura`,

m.`NumeroMunicipio`,
m.`NombreMunicipio`,
lpad(m.NumeroMunicipio,3,'0')CodigoMunicipio,
mu.`Votos`,
mu.`PorcVotos`,
tot.`Censo`,
tot.`Validos`,
tot.`Blanco`,
tot.`Nulo`

FROM
elecmunicandi mu
join elecmunitotal tot on (mu.`Convocatoria_idConvocatoria` = tot.`Convocatoria_idConvocatoria` and mu.`Municipio` = tot.`Municipio`)
join municipio m on (mu.`Municipio` = m.`NumeroMunicipio`)
where
mu.`Convocatoria_idConvocatoria` = $IdConvocatoria and mu.`Candidatura_idCandidatura` = $IdCandidatura";

$rsExists1 = db_query($strSQLExists1,$conn);

	while ($PETICION = db_fetch_array($rsExists1)) {
		if ($primero) {
				$primero = false;
				} else {
				$grafico02.=",";
				}
		$grafico02.="{'id':'".$PETICION["CodigoMunicipio"]."', 'value':".$PETICION["PorcVotos"].", 'name':'".$PETICION["NombreMunicipio"].
		"', 'voto':".$PETICION["Votos"].", 'censo':".$PETICION["Censo"].", 'valido':".$PETICION["Validos"].", 'blanco':".$PETICION["Blanco"].", 'nulo':".$PETICION["Nulo"]."}";

	}

$grafico03= "        ];

		// set map Geo data
		var map = anychart.map();
		var geojson = JSON.parse(MunicipiosMadrid); 
		map.geoData(geojson);		
	
	  // set map title settings using html
	  map.title()
		.enabled(true)
		.useHtml(true)
		.padding(10, 0)
		.hAlign('center')
		.fontFamily(\"'Verdana', Helvetica, Arial, sans-serif\")
		.text(
		  '<span style=\"color:#7c868e; font-size: 18px\">$Convocatoria </span>' +
		  ' <br>' +
		  '<span style=\"color:#545f69; font-size: 14px\">$Candidatura </span>'

		);

	  // sets credits settings
	  map.credits()
		.enabled(true)
		.url('https://en.wikipedia.org/wiki/United_States_presidential_election,_2008')
		.text('Data source: https://en.wikipedia.org/wiki/United_States_presidential_election,_2008');		
		

		// Barra de colores
		  var colorRange = map.colorRange();
		  colorRange.enabled(true)
			.padding([20, 0, 0, 0])
			.colorLineSize(10)
			.stroke('#B9B9B9')
			.labels({
			  'padding': 3
			})
			.labels({
			  'size': 7
			});
		  colorRange.ticks()
			.enabled(true)
			.stroke('#B9B9B9')
			.position('outside')
			.length(10);
		  colorRange.minorTicks()
			.enabled(true)
			.stroke('#B9B9B9')
			.position('outside')
			.length(5);
		
		
		
        // set the series
			
		  var colorScale = anychart.scales.linearColor([\"#FFFFFF\", \"$Color\"]);	
		  var series = map.choropleth(dataSet);
		  series.colorScale(colorScale);

		  map.colorRange(true);
		  
		  series.hovered()
				.fill('#EAFE5F')
				.stroke(anychart.color.darken('#EAFE5F'));
		  series.selected()
				.fill('#EAFE5F')
				.stroke(anychart.color.darken('#EAFE5F'));
		  series.labels()
				.enabled(true)
				.fontSize(10)
				.fontColor('#212121')
				.format('{%value}');
/*		  series.tooltip()
			.useHtml(true)
			.format(function() {
			  return '<span style=\"font-size: 13px\">' + this.value + ' valor </span>';
			}); */

		 series.tooltip()
			  .useHtml(true)
			  .format(function() {
				return '<span style=\"color: #d9d9d9\">% Voto a $Codigo</span>: ' +
				  parseFloat(this.value).toLocaleString() + ' sobre voto válido <br/>' +
				  '<span style=\"color: #d9d9d9\">Votos a $Codigo: </span> ' +
				  parseInt(this.getData('voto')).toLocaleString() + '<br/>' +
				  '<span style=\"color: #d9d9d9\">Censo:</span> ' +
				  parseInt(this.getData('censo')).toLocaleString() + '<br/>' +
				  '<span style=\"color: #d9d9d9\">Votos Válidos:</span> ' +
				  parseInt(this.getData('valido')).toLocaleString() + '<br/>' +
				  '<span style=\"color: #d9d9d9\">Votos en Blanco:</span> ' +
				  parseInt(this.getData('blanco')).toLocaleString() + '<br/>' +
				  '<span style=\"color: #d9d9d9\">Votos Nulos:</span> ' +
				  parseInt(this.getData('nulo')).toLocaleString() + '' +
				'';
			  });			
			
    	  // disable labels
        series.labels(false);

        // set the container
        map.container('container');
        map.draw();
      });
    </script>
  </body>
</html>";
$value=$grafico01.$grafico02.$grafico03;
?>