<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");


require_once("include/dbcommon.php");



if (($_SESSION["MyURL"] == "") || (!isLoggedAsGuest())) {
	Security::saveRedirectURL();
}




$layout = new TLayout("menu_bootstrap1", "OfficeOffice", "MobileOffice");
$layout->version = 3;
	$layout->bootstrapTheme = "cerulean";
		$layout->customCssPageName = "_menu";
$layout->blocks["top"] = array();
$layout->containers["menu"] = array();
$layout->container_properties["menu"] = array(  );
$layout->containers["menu"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"hdr" );
$layout->containers["hdr"] = array();
$layout->container_properties["hdr"] = array(  );
$layout->containers["hdr"][] = array("name"=>"logo",
	"block"=>"logo_block", "substyle"=>1  );

$layout->containers["hdr"][] = array("name"=>"bsnavbarcollapse",
	"block"=>"collapse_block", "substyle"=>1  );

$layout->skins["hdr"] = "";


$layout->containers["menu"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"menu_1" );
$layout->containers["menu_1"] = array();
$layout->container_properties["menu_1"] = array(  );
$layout->containers["menu_1"][] = array("name"=>"hmenu",
	"block"=>"menu_block", "substyle"=>1  );

$layout->containers["menu_1"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"login" );
$layout->containers["login"] = array();
$layout->container_properties["login"] = array(  );
$layout->containers["login"][] = array("name"=>"morebutton",
	"block"=>"more_list", "substyle"=>1  );

$layout->skins["login"] = "";


$layout->skins["menu_1"] = "";


$layout->skins["menu"] = "";

$layout->blocks["top"][] = "menu";
$layout->containers["center"] = array();
$layout->container_properties["center"] = array(  );
$layout->containers["center"][] = array("name"=>"welcome",
	"block"=>"", "substyle"=>1  );

$layout->skins["center"] = "";

$layout->blocks["top"][] = "center";
$page_layouts["menu"] = $layout;




require_once('include/xtempl.php');
require_once(getabspath("classes/cipherer.php"));

include_once(getabspath("include/cm1_municipio_voto_Chart_events.php"));
$tableEvents["cm1_municipio_voto Chart"] = new eventclass_cm1_municipio_voto_Chart;
include_once(getabspath("include/cm1_distrito_voto_Chart_events.php"));
$tableEvents["cm1_distrito_voto Chart"] = new eventclass_cm1_distrito_voto_Chart;
include_once(getabspath("include/cm1_escanos_events.php"));
$tableEvents["cm1_escanos"] = new eventclass_cm1_escanos;
include_once(getabspath("include/cm1_escanos_Chart_events.php"));
$tableEvents["cm1_escanos Chart"] = new eventclass_cm1_escanos_Chart;
include_once(getabspath("include/cm1_votos_Chart_events.php"));
$tableEvents["cm1_votos Chart"] = new eventclass_cm1_votos_Chart;
include_once(getabspath("include/cm2_escanos_chart_events.php"));
$tableEvents["cm2_escanos_chart"] = new eventclass_cm2_escanos_chart;
include_once(getabspath("include/cm2_evolucion_events.php"));
$tableEvents["cm2_evolucion"] = new eventclass_cm2_evolucion;
include_once(getabspath("include/cm2_elecmunicandi_Chart_events.php"));
$tableEvents["cm2_elecmunicandi Chart"] = new eventclass_cm2_elecmunicandi_Chart;
include_once(getabspath("include/cm2_elecmunicandi2_Chart_events.php"));
$tableEvents["cm2_elecmunicandi2 Chart"] = new eventclass_cm2_elecmunicandi2_Chart;
include_once(getabspath("include/cm2_escanos2_Chart_events.php"));
$tableEvents["cm2_escanos2 Chart"] = new eventclass_cm2_escanos2_Chart;
include_once(getabspath("include/cm2_elecdistricandi_Chart_events.php"));
$tableEvents["cm2_elecdistricandi Chart"] = new eventclass_cm2_elecdistricandi_Chart;
include_once(getabspath("include/cm2_elecdistricandi2_Chart_events.php"));
$tableEvents["cm2_elecdistricandi2 Chart"] = new eventclass_cm2_elecdistricandi2_Chart;
include_once(getabspath("include/cm3_mapas_events.php"));
$tableEvents["cm3_mapas"] = new eventclass_cm3_mapas;
include_once(getabspath("include/cm1_escanos1_events.php"));
$tableEvents["cm1_escanos1"] = new eventclass_cm1_escanos1;
include_once(getabspath("include/cm1_escanos2_events.php"));
$tableEvents["cm1_escanos2"] = new eventclass_cm1_escanos2;
include_once(getabspath("include/cm3_mapas1_events.php"));
$tableEvents["cm3_mapas1"] = new eventclass_cm3_mapas1;
include_once(getabspath("include/cm2_evolucion1_events.php"));
$tableEvents["cm2_evolucion1"] = new eventclass_cm2_evolucion1;

$xt = new Xtempl();

$id = postvalue("id")!=="" ? postvalue("id") : 1;

//array of params for classes
$params = array();
$params["id"] = $id; 
$params["xt"] = &$xt;
$params["tName"] = NOT_TABLE_BASED_TNAME;
$params["pageType"] = PAGE_MENU;
$params["templatefile"] = "menu.htm";
$params["isGroupSecurity"] = $isGroupSecurity;
$params["needSearchClauseObj"] = false;
$pageObject = new RunnerPage($params);
$pageObject->init();
$pageObject->commonAssign();
// button handlers file names

//	Before Process event
if($globalEvents->exists("BeforeProcessMenu"))
	$globalEvents->BeforeProcessMenu( $pageObject );

$pageObject->body["begin"] .= GetBaseScriptsForPage(false);

$pageObject->addCommonJs();

//fill jsSettings and ControlsHTMLMap
$pageObject->fillSetCntrlMaps();
$pageObject->setLangParams();
$xt->assign("id", $id);

// get redirect location for menu page
$redirect = $pageObject->getRedirectForMenuPage();
if($redirect)
{
	header("Location: ".$redirect); 
	exit();
}

$xt->assign("menu_block",true);
if($globalEvents->exists("BeforeShowMenu"))
	$globalEvents->BeforeShowMenu($xt, $pageObject->templatefile, $pageObject);

$pageObject->body['end'] .= '<script>';
$pageObject->body['end'] .= "window.controlsMap = ".my_json_encode($pageObject->controlsHTMLMap).";";
$pageObject->body['end'] .= "window.viewControlsMap = ".my_json_encode($pageObject->viewControlsHTMLMap).";";
$pageObject->body['end'] .= "Runner.applyPagesData( ".my_json_encode( $pagesData )." );";
$pageObject->body['end'] .= "window.settings = ".my_json_encode($pageObject->jsSettings).";</script>";
$pageObject->body["end"] .= "<script type=\"text/javascript\" src=\"".GetRootPathForResources("include/runnerJS/RunnerAll.js")."\"></script>";
$pageObject->body["end"] .= '<script>'.$pageObject->PrepareJS()."</script>";
$xt->assignbyref("body",$pageObject->body);

$pageObject->display($pageObject->templatefile);
?>